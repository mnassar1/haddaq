<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->foreign('company_type_id', 'fk_users_companies_categories1')->references('id')->on('company_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_types_id', 'fk_users_user_types')->references('id')->on('user_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('city_id', 'users_ibfk_1')->references('id')->on('cities')->onUpdate('SET NULL')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_companies_categories1');
			$table->dropForeign('fk_users_user_types');
			$table->dropForeign('users_ibfk_1');
		});
	}

}
