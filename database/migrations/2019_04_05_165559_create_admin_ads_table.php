<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_ads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->string('title_ar');
			$table->text('details', 65535);
			$table->text('details_ar', 65535);
			$table->text('images', 65535)->nullable();
			$table->string('images_ar');
			$table->timestamps();
			$table->integer('views_count')->nullable()->default(0);
			$table->boolean('active')->nullable()->default(0);
			$table->integer('ordering')->default(0);
			$table->boolean('is_banar')->nullable()->default(0);
			$table->boolean('fixing_shops_or_home')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_ads');
	}

}
