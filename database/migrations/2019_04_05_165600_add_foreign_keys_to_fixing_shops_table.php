<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFixingShopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fixing_shops', function(Blueprint $table)
		{
			$table->foreign('city_id', 'fixing_shops_ibfk_1')->references('id')->on('cities')->onUpdate('SET NULL')->onDelete('SET NULL');
			$table->foreign('fixing_shops_types_id', 'fk_fixing_shops_company_types_copy11')->references('id')->on('fixing_shops_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fixing_shops', function(Blueprint $table)
		{
			$table->dropForeign('fixing_shops_ibfk_1');
			$table->dropForeign('fk_fixing_shops_company_types_copy11');
		});
	}

}
