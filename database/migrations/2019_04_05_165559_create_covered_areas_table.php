<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoveredAreasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('covered_areas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('area', 65535)->nullable();
			$table->integer('country_id')->index('country_id');
			$table->string('area_name')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('covered_areas');
	}

}
