<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixingShopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixing_shops', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('shop_name');
			$table->string('location');
			$table->text('images', 65535)->nullable();
			$table->text('sliders', 65535)->nullable();
			$table->string('mobile', 45)->nullable();
			$table->text('about', 65535);
			$table->string('email', 45)->nullable();
			$table->integer('fixing_shops_types_id')->index('fk_fixing_shops_company_types_copy11_idx');
			$table->string('address')->nullable();
			$table->string('address_ar')->nullable();
			$table->integer('city_id')->unsigned()->nullable()->index('city_id');
			$table->string('shop_name_ar');
			$table->text('about_ar', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fixing_shops');
	}

}
