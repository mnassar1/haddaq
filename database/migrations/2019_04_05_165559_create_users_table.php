<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email')->nullable()->unique('email_UNIQUE');
			$table->string('country_code', 5)->nullable();
			$table->string('mobile')->unique('mobile_UNIQUE');
			$table->string('phone_with_code')->default('0');
			$table->string('full_name');
			$table->string('password');
			$table->boolean('is_admin')->nullable()->default(0);
			$table->string('profile_image_url')->nullable();
			$table->text('sliders', 65535)->nullable();
			$table->boolean('is_verified')->default(0);
			$table->boolean('is_blocked')->default(0);
			$table->string('verification_code', 60)->nullable();
			$table->string('remember_token', 60)->nullable();
			$table->string('reset_token')->nullable();
			$table->string('login_token', 45)->nullable();
			$table->timestamps();
			$table->integer('user_types_id')->nullable()->index('fk_users_user_types_idx');
			$table->integer('company_type_id')->nullable()->default(3)->index('fk_users_companies_categories1_idx');
			$table->text('about', 65535)->nullable();
			$table->boolean('savior_with_price')->nullable()->default(0);
			$table->string('address')->nullable();
			$table->string('location')->nullable();
			$table->integer('city_id')->unsigned()->nullable()->index('city_id');
			$table->string('push_token')->nullable();
			$table->enum('language', array('EN','AR'))->nullable()->default('EN');
			$table->string('mobile_verification_code')->nullable();
			$table->integer('is_mobile_verified')->nullable()->default(0);
			$table->text('roles')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
