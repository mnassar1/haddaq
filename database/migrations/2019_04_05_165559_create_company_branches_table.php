<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyBranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_branches', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('company_id')->index('fk_company_branches_users1_idx');
			$table->string('address')->nullable();
			$table->string('location', 45)->nullable();
			$table->string('title')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_branches');
	}

}
