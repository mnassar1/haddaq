<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSosRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sos_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable()->index('fk_soso_requests_users1_idx');
			$table->string('mobile')->nullable();
			$table->string('location');
			$table->string('device_udid');
			$table->string('push_token');
			$table->integer('sos_problem_type_id')->nullable()->index('fk_soso_requests_sos_problem_types1_idx');
			$table->text('description', 65535)->nullable();
			$table->enum('sos_request_status', array('Opening','Closed'))->nullable()->default('Opening');
			$table->timestamps();
			$table->enum('closed_by', array('Owner','Admin'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sos_requests');
	}

}
