<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sub_domain', 100)->nullable();
			$table->string('name_en', 100)->nullable();
			$table->string('name_ar', 100)->nullable();
			$table->string('iso_code', 100)->nullable();
			$table->string('flag', 191)->nullable();
			$table->string('phone_code', 191)->nullable();
			$table->integer('validation_length');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}
