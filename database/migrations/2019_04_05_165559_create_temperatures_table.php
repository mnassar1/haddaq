<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemperaturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temperatures', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('highest_temp')->nullable();
			$table->string('lowest_temp')->nullable();
			$table->string('wind_speed')->nullable();
			$table->string('sea_waves_by_day')->nullable();
			$table->string('sea_waves_by_night')->nullable();
			$table->string('first_high_tide')->nullable();
			$table->string('second_high_tide')->nullable();
			$table->string('first_low_tide')->nullable();
			$table->string('second_low_tide')->nullable();
			$table->timestamps();
			$table->integer('country_id')->nullable()->index('country_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temperatures');
	}

}
