<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_groups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('group_name')->nullable()->index('group_name');
			$table->string('group_name_ar')->nullable()->index('group_name_ar');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_groups');
	}

}
