<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdsItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ads_items', function(Blueprint $table)
		{
			$table->foreign('category_id', 'ads_items_ibfk_1')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('company_id', 'fk_ads_items_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ads_items', function(Blueprint $table)
		{
			$table->dropForeign('ads_items_ibfk_1');
			$table->dropForeign('fk_ads_items_users1');
		});
	}

}
