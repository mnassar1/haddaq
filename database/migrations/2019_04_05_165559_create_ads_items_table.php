<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdsItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->integer('company_id')->index('fk_ads_items_users1_idx');
			$table->integer('category_id')->index('fk_ads_items_categories1_idx');
			$table->text('details', 65535);
			$table->text('images', 65535)->nullable();
			$table->float('price', 10, 0);
			$table->timestamps();
			$table->boolean('approved_status')->nullable();
			$table->boolean('used_status')->default(0);
			$table->integer('views_count')->default(0);
			$table->string('block_reason')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads_items');
	}

}
