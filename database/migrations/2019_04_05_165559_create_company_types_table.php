<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_types', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('type_name')->unique('category_name_UNIQUE');
			$table->string('type_name_ar');
			$table->integer('number_of_notifications')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_types');
	}

}
