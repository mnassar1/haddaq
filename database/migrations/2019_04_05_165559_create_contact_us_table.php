<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactUsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_us', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable()->default('Guest');
			$table->string('email');
			$table->string('phone', 50)->nullable();
			$table->enum('type', array('suggestion','complain','enquiry'));
			$table->enum('status', array('new','processing','archived'))->default('new');
			$table->string('subject')->nullable();
			$table->text('description', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contact_us');
	}

}
