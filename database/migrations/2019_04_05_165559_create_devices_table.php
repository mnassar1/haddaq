<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devices', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('device_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->enum('os', array('ios','android','web',''));
			$table->enum('device_type', array('web','phone','tablet',''));
			$table->string('os_version');
			$table->string('app_version');
			$table->string('push_token');
			$table->text('info', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devices');
	}

}
