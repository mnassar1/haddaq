<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSosRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sos_requests', function(Blueprint $table)
		{
			$table->foreign('sos_problem_type_id', 'fk_soso_requests_sos_problem_types1')->references('id')->on('sos_problem_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_soso_requests_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sos_requests', function(Blueprint $table)
		{
			$table->dropForeign('fk_soso_requests_sos_problem_types1');
			$table->dropForeign('fk_soso_requests_users1');
		});
	}

}
