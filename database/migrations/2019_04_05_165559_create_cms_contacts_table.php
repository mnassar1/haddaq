<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_contacts', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->enum('type', array('address','email','phone','mobile','social','other'))->default('email');
			$table->string('title_ar')->nullable();
			$table->string('title_en')->nullable();
			$table->text('content_ar', 65535)->nullable();
			$table->text('content_en', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_contacts');
	}

}
