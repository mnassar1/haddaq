<?php

use App\Helpers\FCM;
use App\Helpers\Notification;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'V1/Register'], function () {
    Route::post('/Index', 'V1\RegisterController@index');
    Route::post('/Email', 'V1\RegisterController@email');
    Route::post('/Phone', 'V1\RegisterController@phone');
    Route::post('/Social', 'V1\RegisterController@social');
    Route::post('/verify_code', 'V1\RegisterController@verify_code')->name('verify_code');
});

Route::group(['prefix' => 'V1/Login'], function () {
    Route::post('/Index', 'V1\LoginController@index');
    Route::post('/Email', 'V1\LoginController@email');
    Route::post('/Phone', 'V1\LoginController@phone');
    Route::post('/Social', 'V1\LoginController@social');
    Route::post('/ForgotPassword', 'V1\LoginController@forgotPassword');
    Route::post('/RefreshToken', 'V1\LoginController@refreshToken');

    Route::post('/send_verification_code', 'V1\LoginController@send_verification_code');
    Route::post('/mobile_verify', 'V1\LoginController@mobile_verify');

});

Route::group(['prefix' => 'V1/'], function () {
    Route::group([], function () {
        Route::get('domains', 'V1\MultiTanentController@getDomains');

        Route::apiResource('reminders', 'V1\ReminderController');
        Route::post('reminders/update/{id}', 'V1\ReminderController@update');




        Route::get('problem_types', 'V1\SOSRequestController@problem_types');
        Route::get('companies_and_categories', 'V1\HomeController@companies_and_categories');


        Route::get('company_details/show/{id}/{user_id?}', 'V1\HomeController@company_details');

        Route::group(['prefix' => 'home'], function () {
            Route::get('/', 'V1\HomeController@index');
            Route::get('/gold_companies/{page_number}/{per_page}', 'V1\HomeController@gold_companies');
            Route::get('/silver_companies/{page_number}/{per_page}', 'V1\HomeController@silver_companies');
            Route::get('/bronze_companies/{page_number}/{per_page}', 'V1\HomeController@bronze_companies');
            Route::post('/search_in_companies/{page_number}/{per_page}', 'V1\HomeController@search_in_companies');

        });


    });

    // Finish Normal ..

    Route::middleware(['MultiTenant'])->group(function () {

        Route::get('Categories', 'V1\CategoryController@index');
        Route::get('users_categories', 'V1\HomeController@users_categories');
        Route::get('company_categories/{company_id}', 'V1\HomeController@company_categories');


        Route::get('emergency_numbers', 'V1\SOSRequestController@emergency_numbers');
        Route::post('save_covered_areas', 'Web\CoveredAreasController@store')->name('save_covered_areas');
        Route::apiResource('favorites', 'V1\FavoritesController');
        Route::post('favorites/update/{id}', 'V1\FavoritesController@update');
        Route::post('favorites/delete_from_favourite/{id}/{type}', 'V1\FavoritesController@delete_from_favourite');



        Route::get('emergency_numbers', 'V1\SOSRequestController@emergency_numbers');

        Route::post('save_covered_areas', 'Web\CoveredAreasController@store')->name('save_covered_areas');

        // Admin Ads
        Route::apiResource('AdsAPIs', 'V1\AdsAPIController');

        // Ads From App
        Route::apiResource('AppAds', 'V1\AppAdsController');
        Route::post('AppAds/update/{id}', 'V1\AppAdsController@update');
        Route::get('AppAds/republish/{id}', 'V1\AppAdsController@republish');

        Route::get('get_notifications/{page_number}/{per_page}/{user_id}', 'V1\NotificationsCenter@get_notificationsCenter');
        Route::get('update_notifications/{notification_id}/{user_id}', 'V1\NotificationsCenter@updateNotificationStates');
        Route::get('get_notifications_count/{user_id}', 'V1\NotificationsCenter@getNotificationCount');

        Route::post('send_sos_request', 'V1\NotificationController@index');
        Route::post('respond_sos_request', 'V1\NotificationController@respond_sos_request')->middleware('auth:api');
        Route::get('close_sos_request/{id}', 'V1\NotificationController@close_sos_request');
        Route::get('sos_request_details/{id}', 'V1\NotificationController@sos_request_details');

        Route::get('main_slider/show/{id}', 'V1\HomeController@main_slider_show');
        Route::get('offer_slider/show/{id}', 'V1\HomeController@main_slider_show');
        Route::get('first_banner/show/{id}', 'V1\HomeController@main_slider_show');
        Route::get('second_banner/show/{id}', 'V1\HomeController@main_slider_show');
        Route::get('third_banner/show/{id}', 'V1\HomeController@main_slider_show');

        Route::get('ads_details/show/{id}/{user_id?}', 'V1\HomeController@ads_details');

        //Home ....

        Route::group(['prefix' => 'home'], function () {
            Route::get('/main_sliders/{page_number}/{per_page}', 'V1\HomeController@main_sliders');
            Route::get('/offers_sliders/{page_number}/{per_page}', 'V1\HomeController@offers_sliders');
            Route::get('/new_items/{page_number}/{per_page}', 'V1\HomeController@new_items');
            Route::get('/first_banners/{page_number}/{per_page}', 'V1\HomeController@first_banners');
            Route::get('/second_banners/{page_number}/{per_page}', 'V1\HomeController@second_banners');
            Route::get('/third_banners/{page_number}/{per_page}', 'V1\HomeController@third_banners');
            Route::post('/search_in_new_items/{page_number}/{per_page}', 'V1\HomeController@search_in_new_items');
        });

        // Main Home APIs
        Route::group(['prefix' => 'mainhome'], function () {
            Route::get('/', 'V1\MainHomeController@index');
            Route::get('/main_sliders/{page_number}/{per_page}', 'V1\MainHomeController@main_sliders');
            Route::get('/gold_companies/{page_number}/{per_page}', 'V1\MainHomeController@gold_companies');
            Route::get('/first_banners/{page_number}/{per_page}', 'V1\MainHomeController@first_banners');
            Route::get('/gold_fixing_shops/{page_number}/{per_page}', 'V1\FixingShopsController@gold_fixing_shops');
            Route::get('/second_banners/{page_number}/{per_page}', 'V1\MainHomeController@second_banners');
            Route::get('/first_category_group/{page_number}/{per_page}', 'V1\UsedController@first_category_group');
        });
        // End of Home APIs

        // Used APIs
        Route::group(['prefix' => 'used'], function () {
            Route::get('/', 'V1\UsedController@index');
            Route::get('/main_sliders/{page_number}/{per_page}', 'V1\UsedController@main_sliders');
            Route::get('/offers_sliders/{page_number}/{per_page}', 'V1\UsedController@offers_sliders');
            Route::get('/new_items/{page_number}/{per_page}', 'V1\UsedController@new_items');
            Route::get('/first_banners/{page_number}/{per_page}', 'V1\UsedController@first_banners');
            Route::get('/first_category_group/{page_number}/{per_page}', 'V1\UsedController@first_category_group');
            Route::get('/second_banners/{page_number}/{per_page}', 'V1\UsedController@second_banners');
            Route::get('/second_category_group/{page_number}/{per_page}', 'V1\UsedController@second_category_group');
            Route::get('/third_banners/{page_number}/{per_page}', 'V1\UsedController@third_banners');
            Route::get('/third_category_group/{page_number}/{per_page}', 'V1\UsedController@third_category_group');
            Route::post('/items_by_category/{page_number}/{per_page}', 'V1\UsedController@items_by_category');

        });

        Route::get('/used_categories', 'V1\UsedController@used_categories');


// Fixing Shops
        Route::group(['prefix' => 'fixing_shops'], function () {
            Route::get('/', 'V1\FixingShopsController@index');
            Route::get('/main_sliders/{page_number}/{per_page}', 'V1\FixingShopsController@main_sliders');
            Route::get('/offers_sliders/{page_number}/{per_page}', 'V1\FixingShopsController@offers_sliders');
            Route::get('/first_banners/{page_number}/{per_page}', 'V1\FixingShopsController@first_banners');
            Route::get('/gold_fixing_shops/{page_number}/{per_page}', 'V1\FixingShopsController@gold_fixing_shops');
            Route::get('/second_banners/{page_number}/{per_page}', 'V1\FixingShopsController@second_banners');
            Route::get('/silver_fixing_shops/{page_number}/{per_page}', 'V1\FixingShopsController@silver_fixing_shops');
            Route::get('/third_banners/{page_number}/{per_page}', 'V1\FixingShopsController@third_banners');
            Route::get('/bronze_fixing_shops/{page_number}/{per_page}', 'V1\FixingShopsController@bronze_fixing_shops');
        });

        Route::get('fixing_shops_details/show/{id}/{user_id?}', 'V1\FixingShopsController@fixing_shops_details');
        Route::post('fixing_shops_search/{page_number}/{per_page}', 'V1\FixingShopsController@fixing_shops_search');

    });
    // Finish Multi


    Route::apiResource('Shipment', 'V1\ShipmentController');
    Route::apiResource('Trip', 'V1\TripController');
    Route::apiResource('Group', 'V1\UserGroupController');
    Route::apiResource('Role', 'V1\RoleController');
    Route::apiResource('Group/{Group}/Roles', 'V1\UserGroupRolesController');
    Route::apiResource('Group/{Group}/Users', 'V1\UserGroupUsersController');
    Route::group(['prefix' => 'Admin'], function () {
        Route::post('Register', 'V1\AdminController@register');
        Route::post('Login', 'V1\AdminController@login');
        Route::get('/', 'V1\AdminController@index')->middleware('admin.perms:admin,LIST_ADMIN');
        Route::get('/{id}', 'V1\AdminController@show')->middleware('admin.perms:admin,SHOW_ADMIN');
    });

    Route::apiResource('CMS', 'V1\CMSController');
    Route::get('CMS/{type}/type', 'V1\CMSController@by_type');


//    Route::get('AdminAds/{type}/type', 'V1\AdsAPIController@by_type');


    // SOS Requests
    // TODO Ask On It
    Route::apiResource('sos_requests', 'V1\SOSRequestController');
    Route::get('saviors/{page_number}/{per_page}/{user_id?}', 'V1\SOSRequestController@saviors');
    Route::post('search_in_saviors/{page_number}/{per_page}/{user_id?}', 'V1\SOSRequestController@search_in_saviors');
    ////////////////// THIS /////////////////////

    // Home APIs


    /////// ///////////////////////////////////////


    Route::get('weather', 'V1\HomeController@weather');
    Route::get('weather/{location}', 'V1\HomeController@weather');


    Route::apiResource('Contacts', 'V1\ContactsController');
    Route::get('Contacts/view/dictionary', 'V1\ContactsController@dictionary');
    Route::apiResource('ContactUs', 'V1\ContactUsController');
    Route::apiResource('User', 'V1\UserController');
    Route::apiResource('Deal', 'V1\DealController');
    Route::put('User/{id}/block', 'V1\UserController@toggleBlock');
    Route::post('User/delete', 'V1\UserController@delete');
    Route::group(['prefix' => 'Profile'], function () {
        Route::get('/', 'V1\ProfileController@index');
        Route::get('/trips', 'V1\ProfileController@trips');
        Route::get('/shipments', 'V1\ProfileController@shipments');
        Route::get('/pending_requests', 'V1\ProfileController@pending_requests');
        Route::get('/my_pending_requests', 'V1\ProfileController@my_pending_requests');
        Route::get('/active_requests', 'V1\ProfileController@active_requests');
        Route::get('/my_active_requests', 'V1\ProfileController@my_active_requests');
        //Route::post('/send_verification_code','V1\ProfileController@send_verification_code');
        //Route::post('/mobile_verify','V1\ProfileController@mobile_verify');
        Route::get('/deals_history', 'V1\ProfileController@deals_history');
        Route::post('/edit_profile', 'V1\ProfileController@edit_profile');
        Route::post('/change_password', 'V1\ProfileController@change_password');
        Route::post('/payment_data', 'V1\ProfileController@edit_payment');
        Route::get('/payment_data', 'V1\ProfileController@payment_data');
        Route::get('/requests', 'V1\ProfileController@requests');
        Route::get('/my_requests', 'V1\ProfileController@my_requests');
        Route::put('/updateNotificationSettings', 'V1\ProfileController@updateNotificationSettings');
    });

    /** handle requests routes */
    Route::apiResource('Request', 'V1\RequestController');
    Route::put('Request/{id}/accept', 'V1\RequestController@accept');
    Route::put('Request/{id}/reject', 'V1\RequestController@reject');
    Route::post('Request/{id}/make_deal', 'V1\RequestController@deal');
    Route::post('Request/{id}/confirm_deal', 'V1\RequestController@confirm_deal');
    Route::post('Request/accepted_trips', 'V1\RequestController@accepted_trips');
    Route::post('Request/accepted_shipments', 'V1\RequestController@accepted_shipments');
    Route::post('Request/{id}/log', 'V1\RequestController@log');
    Route::post('Shipment/{id}/request', 'V1\ShipmentController@request_shipment');
    Route::get('Shipment/{id}/requests', 'V1\ShipmentController@list_requests');
    Route::get('Shipment/{id}/deals', 'V1\ShipmentController@list_deals');
    Route::post('Shipment/{id}/add_details', 'V1\ShipmentController@add_details');
    Route::post('Trip/{id}/request', 'V1\TripController@request_trip');
    Route::get('Trip/{id}/requests', 'V1\TripController@list_requests');
    Route::get('Trip/{id}/deals', 'V1\TripController@list_deals');

    Route::group(['prefix' => 'Timeline'], function () {
        Route::get('/', 'V1\TimelineController@index');
        Route::get('trips', 'V1\TimelineController@trips');
        Route::get('shipments', 'V1\TimelineController@shipments');
    });
    Route::group(['prefix' => 'Deal'], function () {
        Route::get('/{id}/image', 'V1\DealController@list_images');
        Route::post('/{id}/image', 'V1\DealController@add_image');
        Route::post('/{id}/delete_image', 'V1\DealController@delete_image');
        Route::post('/{id}/update_status', 'V1\DealController@update_status');
        Route::post('/{id}/close', 'V1\DealController@close');
        Route::post('/{id}/note', 'V1\DealController@note');
        Route::get('/{id}/note', 'V1\DealController@list_notes');
        Route::get('/{id}/generate_qr', 'V1\DealController@generate_qr');
        Route::put('/{id}/confirm', 'V1\DealController@confirm');
        Route::post('/{id}/pay', 'V1\DealController@do_payment');
        Route::post('/{id}/transfer_money', 'V1\DealController@make_transfer');
    });
    Route::apiResource('Country', 'V1\CountryController');
    Route::post('Country/getCities', 'V1\CountryController@getCities')->name('get_cities');
    Route::get('email/send', 'V1\RegisterController@send');
    Route::post('Logout', 'V1\LoginController@logout');
    Route::post('update_user_flag', 'V1\UserController@update_user_flag')->name('update_user_flag');
    Route::post('update_user_lang', 'V1\UserController@update_user_lang')->name('update_user_lang');
    Route::apiResource('Faq', 'V1\FaqController');
});