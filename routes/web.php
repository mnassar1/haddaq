<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\AdminController@index');
Route::get('/verify/{token}', 'V1\RegisterController@verify')->name('verify');
Auth::routes();

Route::get('/firebase', 'Web\FirebaseController@index');
Route::get('/check_location', 'Web\CoveredAreasController@check_location');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/reset/{token}', 'V1\LoginController@reset')->name('reset');
Route::post('/reset', 'V1\LoginController@doReset')->name('do.reset');

Route::group(['prefix' => 'admin', 'middleware' => 'MultiTenant'], function () {
    Route::group(['prefix' => '/'], function () {
        Route::get('/', 'Web\AdminController@index')->name('dashboard');
        Route::get('update_language/{id}', 'Web\AdminController@update_language')->name('update_language');
        Route::get('update_language_dashboard/{id}', 'Web\AdminController@update_language_dashboard')->name('update_language_dashboard');
        Route::get('/profile', 'Web\AdminController@profile')->name('profile');
        Route::post('update/{id}', 'Web\AdminController@update')->name('admin.update');
        Route::get('/edit/{id}', 'Web\AdminController@edit')->name('admin.edit');
        Route::get('/login', 'Web\AdminController@login')->name('login');
        Route::post('/doLogin', 'Web\AdminController@doLogin')->name('doLogin');
        Route::get('/logout', 'Web\AdminController@logout')->name('admin_logout');
        Route::get('/trips', 'Web\AdminController@trips')->name('trips');
        Route::get('/trips/{id}', 'Web\AdminController@show')->name('trip_show');
        Route::get('/shipments', 'Web\AdminController@shipments')->name('shipments');
        Route::get('/shipments/{id}', 'Web\AdminController@show_shipment')->name('shipment_show');
    });
    Route::resource('/deals', 'Web\DealController');
    Route::post('/deals/{id}/change_status', 'Web\DealController@change_status');
    Route::resource('/contact_us', 'Web\ContactUsController');
    Route::post('/contact_us/{id}/change_status', 'Web\ContactUsController@change_status');
    Route::resource('/cms', 'Web\CMSController');

    Route::resource('/company_ads', 'Web\CompanyAdsController');
    Route::resource('/company_profile', 'Web\CompanyProfileController');
    Route::resource('/company_branches', 'Web\CompanyBranchController');

    Route::resource('/covered_areas', 'Web\CoveredAreasController');
    Route::get('create_covered_area', 'Web\CoveredAreasController@create_covered_area');

    Route::resource('/fixing_shops', 'Web\FixingShopsController');

    Route::resource('/cities', 'Web\CityController');
    Route::resource('/notifications', 'Web\NotificationsController');

    Route::resource('/users_push_setting', 'Web\PushController');

    Route::resource('/category_groups', 'Web\CategoryGroupController');
    Route::resource('/problem_types', 'Web\ProblemTypesController');
    Route::resource('/sos_requests', 'Web\SOSRequestsController');
    Route::get('show_on_map/{id}', 'Web\SOSRequestsController@show_on_map');

    Route::resource('/company_types', 'Web\CompanyTypesController');


    Route::resource('/emergency', 'Web\EmergencyController');

    Route::resource('/categories', 'Web\CategoryController');

    Route::resource('/admin_ads', 'Web\AdsController');

    Route::resource('/app_ads', 'Web\AppAdsController');

    Route::resource('/user', 'Web\UserController');
    Route::get('/user/{id}/trips', 'Web\UserController@trips');
    Route::get('/user/{id}/shipments', 'Web\UserController@shipments');
    Route::post('/user/{id}/add-role', 'Web\UserController@addRole');
    Route::resource('/faq', 'Web\FaqController');
    Route::resource('/contacts', 'Web\ContactsController');
    Route::resource('/settings', 'Web\SettingsController');
});
