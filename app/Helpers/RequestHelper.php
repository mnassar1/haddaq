<?php

namespace App\Helpers;

use App\Model\Requests;

trait RequestHelper
{
    use SettingsHelper;

    /**
     * extract request users by
     * @param Requests $request
     * @return mixed
     */
    public function getRequestUsers(Requests $request)
    {
        $by_user = ($request->by_user) ? $request->by_user : null;
        $to_user = ($request->to_user) ? $request->to_user : null;
        $type = $request->type;
        $data['type'] = $type;
        $data['traveller'] = ($type == 'shipment') ? $by_user : $to_user;
        $data['shipper'] = ($type == 'shipment') ? $to_user : $by_user;
        return $data;
    }

    /**
     *
     * @param $amount
     * @return float
     */
    public function calculate_commission($amount)
    {
        $settings = $this->get_settings();
        $commission = (float)$settings['commission_constant'] + ((float)$settings['commission_percent'] * $amount);
        return round($commission, 2);
    }
}

