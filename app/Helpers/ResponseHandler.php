<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\Response;

class ResponseHandler
{

    public static function jsonCollection($response, $response_key = 'response', $status_code = Response::HTTP_OK)
    {
        $json_response['status_code'] = $status_code;
        $json_response[$response_key] = $response;
        return response($json_response, $status_code);
    }

    public static function json($response, $current_page = null, $last_page = null, $response_key = 'response', $status_code = Response::HTTP_OK)
    {
        $json_response['status_code'] = $status_code;

        if ($current_page !== null) {
            $json_response['current_page'] = $current_page;
        }

        if ($last_page !== null) {
            $json_response['last_page'] = $last_page;
        }

        $json_response[$response_key] = $response;

        return response($json_response, $status_code);
    }
}

