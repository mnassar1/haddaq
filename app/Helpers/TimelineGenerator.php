<?php

namespace App\Helpers;

use App\Model\Timeline;

/**
 * Static class to generate timeline actions
 */
class TimelineGenerator
{
    public static function post($type, $object_id, $object, $user_id, $country_id, $city_id)
    {
        Timeline::create(['verb' => 'post', 'type' => $type, 'object_id' => $object_id, 'object' => $object->toJson(), 'user_id' => $user_id, 'country_id' => $country_id, 'city_id' => $city_id]);
    }

    public static function update($type, $object_id, $object, $user_id, $country_id, $city_id)
    {
        // @todo remove last action of object_id from timeline table by set is_deleted to 1
        $timeline_items = Timeline::where(['type' => $type, 'object_id' => $object_id, 'is_deleted' => 0])->whereIn('verb', ['post', 'update']);
        $timeline_items->update(['is_deleted' => 1]);
        Timeline::create(['verb' => 'update', 'type' => $type, 'object_id' => $object_id, 'object' => $object->toJson(), 'user_id' => $user_id, 'country_id' => $country_id, 'city_id' => $city_id]);
    }

    public static function delete()
    {
    }

    public static function cancel()
    {
    }
}

