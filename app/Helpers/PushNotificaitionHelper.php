<?php

namespace App\Helpers;

use Kreait\Firebase\Exception\MessagingException;

class PushNotificaitionHelper
{

    /**
     * Notify shipper when new trip posted in city from
     * @param $trip_id
     * @param $country_from_id
     * @param $city_from_id
     * @param $country_to_id
     * @param $city_to_id
     * @return
     */
    public static function newTrip($trip_id, $country_from_id, $city_from_id, $country_to_id, $city_to_id)
    {
        $title = "New Trip";

        $locations = json_decode(self::decodeLocations($country_from_id, $city_from_id, $country_to_id, $city_to_id));
        $body = "New Trip Posted From " . $locations->country_from->name_en . "  " . $locations->city_from->name_en . " To " . $locations->country_to->name_en . "  " . $locations->city_to->name_en;
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload('show', 'trip', $trip_id);
        $topic_name = "cityid_" . $city_from_id . "_travellers";
        return FCM::shared()->publish($topic_name, $message, $payload);
    }

    /**
     * decode locations
     * @param $country_from_id
     * @param $city_from_id
     * @param $country_to_id
     * @param $city_to_id
     * @return json with decoded locations
     */
    private static function decodeLocations($country_from_id, $city_from_id, $country_to_id, $city_to_id)
    {
        $country_from = CountryHelper::decodeCountry($country_from_id);
        $city_from = CountryHelper::decodeCity($city_from_id);
        $country_to = CountryHelper::decodeCountry($country_to_id);
        $city_to = CountryHelper::decodeCity($city_to_id);
        $locations = ['country_from' => $country_from, 'city_from' => $city_from, 'country_to' => $country_to, 'city_to' => $city_to];
        return json_encode($locations);
    }

    /**
     * Notify travller when new shipment posted in city to
     * @param $shipment_id
     * @param $country_from_id
     * @param $city_from_id
     * @param $country_to_id
     * @param $city_to_id
     * @return
     */
    public static function newShipment($shipment_id, $country_from_id, $city_from_id, $country_to_id, $city_to_id)
    {
        $title = "New Shipment";

        $locations = json_decode(self::decodeLocations($country_from_id, $city_from_id, $country_to_id, $city_to_id));
        $body = "New Shipment Posted From " . $locations->country_from->name_en . "  " . $locations->city_from->name_en . " To " . $locations->country_to->name_en . "  " . $locations->city_to->name_en;
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload('show', 'shipment', $shipment_id);
        $topic_name = "cityid_" . $city_to_id . "_shipments";
        return FCM::shared()->publish($topic_name, $message, $payload);
    }

    /**
     * Notify Shipper when travller request to take his shipment
     * @param $request_id
     * @param $user
     * @param $push_token
     * @return
     */
    public static function shipmentRequest($request_id, $user, $push_token)
    {
        $title = "New Travller Request";
        $body = "$user->name request to take your shipment";
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload('show', 'request', $request_id);
        try {
            return FCM::shared()->send($push_token, $message, $payload);
        } catch (MessagingException $e) {
            // return false;
        }
    }

    /**
     * Notify Traveller when shipper request to send shipment with him.
     * @param $request_id
     * @param $user
     * @param $push_token
     * @return
     */
    public static function tripRequest($request_id, $user, $push_token)
    {
        $title = "New Shipment Request";
        $body = "$user->name request to send his shipment with you";
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload('show', 'request', $request_id);
        try {
            return FCM::shared()->send($push_token, $message, $payload);
        } catch (MessagingException $e) {

        }
    }

    /**
     * Notify Traveller when shipper add details to his hsipment
     * @param $id
     * @param $name
     * @param $push_token
     * @return
     */
    public static function shipmentDetails($id, $name, $push_token)
    {
        $title = "Shipment Details";
        $body = "Shipment shipped with $name";
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload('show', 'shipment_details', $id);
        try {
            return FCM::shared()->send($push_token, $message, $payload);
        } catch (MessagingException $e) {

        }
    }

    /**
     * Notifiy Shipper his request is accepted
     * @param $id
     * @param $traveller_name
     * @param $push_token
     */
    public static function shipperRequestAcceptance($id, $traveller_name, $push_token)
    {
        $title = "Your shipment request has been accepted";
        $body = "$traveller_name accept your request to take your shipment in his trip";
        self::sendNotification($push_token, $title, $body, 'show', 'request', $id);
    }

    /**
     * private function to handle send notification
     * @param $push_token
     * @param $title
     * @param $body
     * @param $action
     * @param $object
     * @param $id
     * @return
     */
    private static function sendNotification($push_token, $title, $body, $action, $object, $id)
    {
        $message = Notification::createMessage($title, $body);
        $payload = Notification::createPayload($action, $object, $id);
        try {
            return FCM::shared()->send($push_token, $message, $payload);
        } catch (MessagingException $e) {

        }
    }

    /**
     * Notify Traveller his request is accepted
     * @param $id
     * @param $shipper_name
     * @param $push_token
     */
    public static function travellerRequestAcceptance($id, $shipper_name, $push_token)
    {
        $title = "Your trip request has been accepted";
        $body = "$shipper_name accept your request to take his shipment with you in your trip";
        self::sendNotification($push_token, $title, $body, 'show', 'request', $id);
    }

    /**
     * Notify Shipper when his request is rejected
     * @param $id
     * @param $traveller_name
     * @param $push_token
     */
    public static function shipperRequestRejection($id, $traveller_name, $push_token)
    {
        $title = "Your shipment request has been rejected";
        $body = "$traveller_name reject your request to take you shipment in his trip";
        self::sendNotification($push_token, $title, $body, 'show', 'request', $id);
    }

    /**
     * Notify traveller when his request is rejected
     * @param $id
     * @param $shipper_name
     * @param $push_token
     */
    public static function travellerRequestRejection($id, $shipper_name, $push_token)
    {
        $title = "Your trip request has been rejected";
        $body = "$shipper_name reject you request to take his shipment with you in your trip";
        self::sendNotification($push_token, $title, $body, 'show', 'request', $id);
    }

    /**
     * Notify traveller and shipper deal is closed
     * @param $id
     * @param $push_token
     */
    public static function dealClosed($id, $push_token)
    {
        $title = "Deal Closed";
        $body = "Deal has been closed";
        self::sendNotification($push_token, $title, $body, 'show', 'deal', $id);
    }

    /**
     * Notify user when deal status changed
     * @param $id ,$status,$push_token
     * @param $status
     * @param $push_token
     */
    public static function dealStatusChanged($id, $status, $push_token)
    {
        if (in_array($status, ['accepted', 'negotiation', 'payment', 'confirmed', 'delegated', 'shipped', 'delivered', 'cancelled', 'waiting'])) {
            $messages = self::decodeDealStatusMessages($status);
            self::sendNotification($push_token, $messages['title'], $messages['body'], 'show', 'deal', $id);
        }
    }

    /**
     *
     * @param $status
     * @return mixed
     */
    private static function decodeDealStatusMessages($status)
    {
        // accepted,negotiation,payment,confirmed,delegated,shipped,delivered,cancelled,waiting
        $messages = [];
        $messages['accepted']['title'] = 'Deal accepted';
        $messages['accepted']['body'] = 'Your deal has been accepted.';

        $messages['negotiation']['title'] = 'Deal negotiated';
        $messages['negotiation']['body'] = 'Your deal now in negotiation deal.';

        $messages['payment']['title'] = 'Deal paid';
        $messages['payment']['body'] = 'Your Deal has been paid.';

        $messages['confirmed']['title'] = 'Deal confirmed';
        $messages['confirmed']['body'] = '';

        $messages['shipped']['title'] = 'Deal shipped';
        $messages['shipped']['body'] = 'Your deal has been shipped';

        $messages['delivered']['title'] = 'Deal delivered';
        $messages['delivered']['body'] = 'Your deal has been delivered';

        $messages['cancelled']['title'] = 'Deal cancelled';
        $messages['cancelled']['body'] = 'Your deal has been cancelled';

        return $messages[$status];
    }

    /**
     * Notify Traveller to confirm him that shipper already pay money.
     * @param $id
     * @param $push_token
     * @param $shipper_name
     * @param $amount
     */
    public static function shipperPayMoney($id, $push_token, $shipper_name, $amount)
    {
        $title = "Your fees has been paid.";
        $body = "$shipper_name has been paid $amount$ to your account.";
        self::sendNotification($push_token, $title, $body, 'show', 'deal', '$id');
    }
}

