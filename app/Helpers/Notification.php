<?php

namespace App\Helpers;
class Notification
{
    public static function createMessage($title, $body)
    {
        return ['title' => $title, 'body' => $body];
    }

    public static function createPayload($action_name, $type, $id = null)
    {
        $payload = [];
        $payload['action_name'] = $action_name;
        $payload['type'] = $type;
        if (isset($id))
            $payload['id'] = (string)$id;
        return $payload;
    }
}

