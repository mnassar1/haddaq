<?php

namespace App\Helpers;

use App\Model\Settings;

trait SettingsHelper
{

    /**
     * get settings dictonary
     *
     */
    public function get_settings()
    {
        $dictonary = [];
        $settings = Settings::all();
        if (count($settings)) {
            foreach ($settings as $setting) {
                $dictonary[$setting->setting_key] = $setting->setting_value;
            }
        }
        return $dictonary;
    }
}

