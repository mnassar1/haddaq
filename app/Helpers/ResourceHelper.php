<?php

namespace App\Helpers;

Trait ResourceHelper
{
    public function getCurrentUrl($route_name, $id)
    {
        $action = request()->route()->getAction();
        $url_prefix = $action["prefix"];
        return url("$url_prefix/$route_name/$id");
    }

    /**
     * get Shipment / Trip request url
     * @param $route_name
     * @param $id ,$type
     */
    public function getRequestUrl($route_name, $id)
    {
        // @todo
    }
}

