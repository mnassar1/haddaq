<?php

namespace App\Helpers;

class Helper
{
    public function isMainDomain(){
        $main = env('APP_URL', '');
        $main = ltrim($main, 'http://');
        $main = ltrim($main, 'https://');
        $main = explode("/",$main);

        $route = \Request::fullUrl();
        $route = ltrim($route, 'http://');
        $route = ltrim($route, 'https://');
        $route = explode("/",$route);

        if($route[0] == $main[0]) return true;
        else return false;
    }
}

