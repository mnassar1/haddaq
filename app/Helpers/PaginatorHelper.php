<?php

namespace App\Helpers;

class PaginatorHelper
{

    /**
     * parse paginator data from elquent pafinator object
     * @param $elquent
     * @return array of paginator data
     */
    public static function getPaginatorData($elquent)
    {
        $elquentArray = $elquent->toArray();
        unset($elquentArray['data']);
        $paginator = [];
        $paginator['links']['first_page_url'] = $elquentArray['first_page_url'];
        $paginator['links']['last_page_url'] = $elquentArray['last_page_url'];
        $paginator['links']['next_page_url'] = $elquentArray['next_page_url'];
        $paginator['links']['prev_page_url'] = $elquentArray['prev_page_url'];
        $paginator['meta']['path'] = $elquentArray['path'];
        $paginator['meta']['current_page'] = $elquentArray['current_page'];
        $paginator['meta']['from'] = $elquentArray['from'];
        $paginator['meta']['per_page'] = $elquentArray['per_page'];
        $paginator['meta']['to'] = $elquentArray['to'];
        $paginator['meta']['total'] = $elquentArray['total'];
        return $paginator;
    }
}

