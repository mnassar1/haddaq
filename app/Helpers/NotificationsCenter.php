<?php

namespace App\Helpers;

use App\Model\Notifications;

class NotificationsCenter
{
    public static function createNotification($type, $title, $body, $url, $item_id)
    {

        Notifications::Create(['type' => $type, 'title' => $title,
                'body' => $body, "url" => $url, "item_id" => $item_id]
        );
    }

    public static function getId($id){
        $item = Notifications::where("item_id", $id)->first();
        die(var_dump($item));
        return $item;
    }

    public static function removeFromList($type, $id)
    {
        Notifications::where("type", $type)->where("item_id", $id)->delete();
    }


}

