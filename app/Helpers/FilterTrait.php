<?php

namespace App\Helpers;

use Carbon\Carbon;

trait FilterTrait
{

    /**
     * @param $number_key ,$from,$to
     * @param $from
     * @param $to
     * @return array $filter[$date_key => ['from' => $from,'to' => $to]]
     */
    public function numberRangeFilter($number_key, $from, $to)
    {
        $number_range_filter = [];
        $number_from = isset($from) ? $from : 1;
        $number_to = isset($to) ? $to : 5;

        if ($number_from > $number_to) {
            // in from > to then swap values
            $x = $number_from;
            $y = $number_to;
            $number_from = $y;
            $number_to = $x;
        }
        $number_range_filter[$number_key]['from'] = (double)$number_from;
        $number_range_filter[$number_key]['to'] = (double)$number_to;
        return $number_range_filter;
    }

    /**
     * @param $time_key ,$from,$to
     * @param null $from
     * @param null $to
     * @return array $filter[$time_key => ['from' => $from,'to' => $to]]
     */
    public function timeRangeFilter($time_key, $from = null, $to = null)
    {
        $time_range_filter = [];
        // if time_from not provided set it to Today
        $time_from = isset($from) ? Carbon::createFromFormat('H:i:s', $from) : Carbon::now();
        // temp object to handle time_to + 1
        $time_from_temp = Carbon::createFromFormat('H:i:s', $time_from->format('H:i:s'));
        // if time_to not provided set it to time_from + 30 days
        //@todo fix 
        $time_to = isset($to) ? Carbon::createFromFormat('H:i:s', $to) : $time_from_temp->addHour();
        // remove date_from_temp from memory
        $time_from_temp = null;
        if ($time_from < $time_to) {
            $time_from_temp = Carbon::createFromFormat('Y-m-d', $time_from->toDateString());
            $time_to = $time_from_temp->addHour();
        }
        $time_range_filter[$time_key]['from'] = $time_from->format('H:i:s');
        $time_range_filter[$time_key]['to'] = $time_to->format('H:i:s');
        return $time_range_filter;
    }

    /**
     * @param $date_key ,$from,$to
     * @param null $from
     * @param null $to
     * @return array $filter[$date_key => ['from' => $from,'to' => $to]]
     */
    public function dateRangeFilter($date_key, $from = null, $to = null)
    {
        $date_range_filter = [];
        // if date_from not provided set it to Today
        $date_from = isset($from) ? Carbon::createFromFormat('Y-m-d', $from) : Carbon::today();
        // temp object to handle date_to + 1
        $date_from_temp = Carbon::createFromFormat('Y-m-d', $date_from->toDateString());
        // if date_to not provided set it to date_from + 30 days 
        $date_to = isset($to) ? Carbon::createFromFormat('Y-m-d', $to) : $date_from_temp->addDays(30);
        // remove date_from_temp from memory
        $date_from_temp = null;
        if ($date_from > $date_to) {
            $date_from_temp = Carbon::createFromFormat('Y-m-d', $date_from->toDateString());
            $date_to = $date_from_temp->addDays(1);
        }
        $date_range_filter[$date_key]['from'] = $date_from->toDateString();
        $date_range_filter[$date_key]['to'] = $date_to->toDateString();
        return $date_range_filter;
    }
}

