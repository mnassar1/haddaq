<?php

namespace App\Helpers;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\MessageToRegistrationToken;
use Kreait\Firebase\Messaging\MessageToTopic;
use Kreait\Firebase\ServiceAccount;

class FCM
{

    protected static $instance;

    protected $firebase;
    protected $messaging;

    public function __construct()
    {
        //@TODO initalize
        $serviceAccount = ServiceAccount::fromJsonFile(base_path() . '/testyaz-1524077465515-8da3631441f9.json');
        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // The following line is optional if the project id in your credentials file
            // is identical to the subdomain of your Firebase project. If you need it,
            // make sure to replace the URL with the URL of your project.
            // ->withDatabaseUri('https://my-project.firebaseio.com')
            ->create();
        $this->messaging = $this->firebase->getMessaging();
    }

    public static function shared()
    {
        if (!static::$instance) {
            static::$instance = new self;
        }
        return static::$instance;
    }

    /** Begin Topic methods */

    /**
     * @param topic_name
     * @return string
     */
    public function createTopic($topic_name)
    {
        $MessageToTopic = MessageToTopic::create($topic_name);
        return $MessageToTopic->topic();
    }

    /**
     * subscribe push token to topic
     * @param $topic
     * @param $push_token
     * @return array
     */
    public function subscribeTo($topic, $push_token)
    {
        $subscribe = $this->messaging->subscribeToTopic($topic, $push_token);
        return $subscribe;
    }

    /**
     * unsubscribe push token from topic
     * @param $topic
     * @param $push_token
     * @return array
     */
    public function unsubscribeFrom($topic, $push_token)
    {
        $unsubscribe = $this->messaging->unsubscribeFromTopic($topic, $push_token);
        return $unsubscribe;
    }

    /**
     * public message to topic
     * @param $topic_name
     * @param $message
     * @param $data
     * @return array
     */
    public function publish($topic_name, $message, $data)
    {
        $notification = MessageToTopic::create($topic_name)
            ->withNotification($message)
            ->withData($data);

        $is_published = $this->messaging->send($notification);
        return $is_published;
    }
    /** End Topic methods */

    /**
     * @param $push_token
     * @param $message
     * @param $data
     * @return array
     */
    public function send($push_token, $message, $data)
    {
        $notification = MessageToRegistrationToken::create((string)$push_token)
            ->withNotification($message)
            ->withData($data);

        $is_notified = $this->messaging->send($notification);
        return $is_notified;
    }
}

