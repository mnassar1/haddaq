<?php

namespace App\Helpers;

use App\Model\City;
use App\Model\Country;

class CountryHelper
{
    public static function decodeCountry($id)
    {
        // @TODO get it from cache
        $country = Country::find($id);
        return ['name_ar' => $country->name_ar, 'name_en' => $country->name_en];
    }

    public static function decodeCity($id)
    {
        // @TODO get it from cache
        $city = City::find($id);
        return ['name_ar' => $city->name_ar, 'name_en' => $city->name_en];
    }
}

