<?php

namespace App\Helpers;

use App\Model\NotificationUser as Model;

class NotificationUser
{
    public static function removeFromList($id)
    {
        Model::where("notification_id", $id)->delete();
    }
}

