<?php

namespace App\Helpers;

use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

trait UserHelperTrait
{
    /**
     * generateHashedPassword
     * @param $password
     * @param $salt
     * @return String hashed password
     */

    public function generateHashedPassword($password, $salt)
    {
        return sha1(md5($password) . $salt);
    }

    /**
     * Create user's access token and expiry date
     * @param User $user
     * @return array ,expires_at
     */

    public function createAccessToken(User $user)
    {
        $db_config = config("database.default");
        $user->accessTokens()->delete();
        \Illuminate\Support\Facades\Config::set("database.default", "mysql");
        $passport_token = $user->createToken($user->device_id);
        $access_token = $passport_token->accessToken;
        $token = $passport_token->token;
        $expires_at = $token->expires_at;
        \Illuminate\Support\Facades\Config::set("database.default", $db_config);
        return [
            'token' => $access_token,
            'expires_at' => $expires_at->timestamp
        ];
    }

    /**
     * check if request has valid access token and valid user
     * @param Request $request
     * @return User
     * @throws AuthenticationException
     */
    public function getUserFromRequest(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        return $user;
    }

    /**
     * generate login token
     * @param User $user
     * @return String
     */
    public function generateLoginToken(User $user)
    {
        if (!$user->login_token) {
            $login_token = str_random(32);
            $user->login_token = $login_token;
            $user->save();
            return $login_token;
        }
        return $user->login_token;
    }
}

