<?php

namespace App\Helpers;

use App\Http\Resources\UserGroup\UserGroupCollection;
use App\User;

trait AdminHelperTrait
{
    /**
     * getUsergroups
     * @param \App\User
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection of usergroups
     */

    public function getUsergroups(User $user)
    {
        return UserGroupCollection::collection($user->usergroups);
    }

    /**
     * generateRolesDictionary
     * @param \App\User
     * @return array associative of level => [roles]
     */

    public function generateRolesDictionary(User $user)
    {
        $roles = $this->getRoles($user);
        $dictionary = [];
        foreach ($roles as $role) {
            $dictionary[$role->level][] = $role->action_name;
        }
        return $dictionary;
    }

    /**
     * getRoles
     * @param User $user
     * @return array of roles
     */

    public function getRoles(User $user)
    {
        $roles = [];
        if ($user->usergroups->count() > 0) {
            foreach ($user->usergroups as $usergroup) {
                if ($usergroup->roles->count() > 0) {
                    foreach ($usergroup->roles as $role) {
                        $roles[] = $role;
                    }
                }
            }
        }
        return $roles;
    }

}

