<?php

namespace App\Helpers;

use App\Model\RequestHistory;

class RequestLog
{
    public static function log($request_id, $status)
    {
        RequestHistory::firstOrCreate(['request_id' => $request_id, 'status' => $status]);
    }
}

