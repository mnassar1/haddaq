<?php

namespace App\Exceptions\Trip;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TripActionNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(15, 'TripActionNotCompletedException', "Trip Action Not Completed Exception", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
