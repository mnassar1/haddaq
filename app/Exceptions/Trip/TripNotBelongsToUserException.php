<?php

namespace App\Exceptions\Trip;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TripNotBelongsToUserException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(16, 'TripNotBelongsToUserException', 'Trip Not Belongs To User Exception', Response::HTTP_UNAUTHORIZED);
    }
}
