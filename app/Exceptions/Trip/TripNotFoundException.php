<?php

namespace App\Exceptions\Trip;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TripNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'TripNotFoundException', 'Trip Not Found Exception', Response::HTTP_NOT_FOUND);
    }
}
