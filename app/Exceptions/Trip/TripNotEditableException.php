<?php

namespace App\Exceptions\Trip;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TripNotEditableException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(16, 'TripNotEditableException', 'You can not edit or delete this Trip.', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
