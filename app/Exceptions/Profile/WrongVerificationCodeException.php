<?php

namespace App\Exceptions\Profile;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class WrongVerificationCodeException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'WrongVerificationCodeException', 'Wrong Verification Code Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
