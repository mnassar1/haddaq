<?php

namespace App\Exceptions\Profile;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserHasNotPaymentDataException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'UserHasNotPaymentDataException', 'UserHasNotPaymentDataException', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
