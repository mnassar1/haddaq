<?php

namespace App\Exceptions\Profile;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class VerificationCodeNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'VerificationCodeNotFoundException', 'Verification Code Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
