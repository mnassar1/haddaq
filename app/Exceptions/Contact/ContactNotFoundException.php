<?php

namespace App\Exceptions\Contact;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ContactNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(19, 'ContactNotFoundException', 'Contact Not Found Exception', Response::HTTP_NOT_FOUND);
    }
}
