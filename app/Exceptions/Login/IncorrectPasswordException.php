<?php

namespace App\Exceptions\Login;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class IncorrectPasswordException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(1, 'IncorrectPasswordException', "Incorrect Password", Response::HTTP_UNAUTHORIZED);
    }
}
