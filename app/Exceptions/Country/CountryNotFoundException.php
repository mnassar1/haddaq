<?php

namespace App\Exceptions\Country;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class CountryNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(30, 'CountryNotFoundException', 'Country Not Found Exception', Response::HTTP_NOT_FOUND);
    }
}
