<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class InvalidQRCodeException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(30, 'InvalidQRCodeException', 'Invalid QR Code Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
