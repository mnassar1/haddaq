<?php

namespace App\Exceptions\Favorites;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class AddedToFavoritesException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(323, 'AddedToFavoritesException', 'You Already have this in your Favorites List', Response::HTTP_NOT_FOUND);
    }
}
