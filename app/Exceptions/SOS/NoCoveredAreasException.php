<?php

namespace App\Exceptions\SOS;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class NoCoveredAreasException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3012, 'NoCoveredAreasException', 'Your country not Covered', Response::HTTP_NOT_FOUND);
    }

}