<?php

namespace App\Exceptions\SOS;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class AreaNotCoveredException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3010, 'AreaNotCoveredException', 'Area Not Covered', Response::HTTP_NOT_FOUND);
    }
}