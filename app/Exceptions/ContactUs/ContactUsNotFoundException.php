<?php

namespace App\Exceptions\ContactUs;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ContactUsNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(30, 'ContactUsNotFoundException', 'Contact Us Message Not Found Exception', Response::HTTP_NOT_FOUND);
    }
}
