<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ImageNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'ImageNotFoundException', 'Image Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
