<?php

namespace App\Exceptions\UserGroup;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserGroupNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(15, 'UserGroupNotFoundException', 'User Group Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
