<?php

namespace App\Exceptions\UserGroup;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserGroupActionNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(15, 'UserGroupActionNotCompletedException', 'UserGroup Action Not Completed Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
