<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    public function apiException($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return $this->renderException(1, 'ModelNotFoundException', 'Model Not Found', Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof NotFoundHttpException) {
            return $this->renderException(2, 'NotFoundHttpException', 'Route Not Found', Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof AuthenticationException) {
            return $this->renderException(2, 'AuthenticationException', 'Unauthorized Action', Response::HTTP_UNAUTHORIZED);
        }
        if ($exception instanceof ValidationException) {

            /** Start convert validation errors array to string. */
            $errors = [];
            if (count($exception->errors())) {
                foreach ($exception->errors() as $field => $err) {
                    if (count($err)) {
                        foreach ($err as $key => $value) {
                            $errors[] = $value;
                        }
                    }
                }
            }
            $errorsString = implode('', $errors);
            /** End*/

            return $this->renderException(2, ' ValidationException', $errorsString, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return parent::render($request, $exception);
    }

    public function renderException($code, $name, $message, Int $http_response_code)
    {
        $exceptionJson['status_code'] = $http_response_code;
        $exceptionJson['error_code'] = $code;
        $exceptionJson['error'] = ['name' => $name, 'message' => $message];
        return response()->json($exceptionJson, $http_response_code);
    }
}

