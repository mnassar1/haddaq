<?php

namespace App\Exceptions\Shipment;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ShipmentNotEditableException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(16, 'ShipmentNotEditableException', 'You can not edit or delete this Shipment.', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
