<?php

namespace App\Exceptions\Shipment;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ShipmentActionNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(13, 'ShipmentActionNotCompletedException', "Shipment Action Not Completed Exception", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
