<?php

namespace App\Exceptions\Shipment;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ShipmentNotBelongsToUserException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'ShipmentNotBelongsToUserException', "Shipment Not Belongs To User Exception", Response::HTTP_UNAUTHORIZED);
    }
}
