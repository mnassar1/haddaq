<?php

namespace App\Exceptions\Shipment;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ShipmentCantReceiveRequestException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'ShipmentCantReceiveRequestException', 'Shipment Cant Receive Request Exception', Response::HTTP_UNAUTHORIZED);
    }
}
