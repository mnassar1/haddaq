<?php

namespace App\Exceptions\Shipment;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ShipmentNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(13, 'ShipmentNotFoundException', "Shipment Not Found Exception", Response::HTTP_NOT_FOUND);
    }
}
