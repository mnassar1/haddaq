<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class NotSameCityDestinationException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'NotSameCityDestinationException', 'Not Same City Destination Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
