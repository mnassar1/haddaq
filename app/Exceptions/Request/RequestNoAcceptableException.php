<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestNoAcceptableException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'RequestNoAcceptableException', 'You can not accept this request because it has trip or shipment with paid or closed deal', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
