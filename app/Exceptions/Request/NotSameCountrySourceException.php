<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class NotSameCountrySourceException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'NotSameCountrySourceException', 'Not Same Country Source Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
