<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TripNotHasEnoughKilosException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(44, 'TripNotHasEnoughKilosException', 'Trip Not Has Enough Kilos Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
