<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestHasNoDealsException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'RequestHasNoDealsException', 'Request Has No Deals Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
