<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestNotBelongsToUserException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'RequestNotBelongsToUserException', 'RequestNotBelongsToUserException', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
