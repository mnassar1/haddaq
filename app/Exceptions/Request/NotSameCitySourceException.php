<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class NotSameCitySourceException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'NotSameCitySourceException', 'Not Same City Source Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
