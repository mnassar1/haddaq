<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestStatusChangeException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(30, 'RequestStatusChangeException', 'This action shouldn\'t be done by you.', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
