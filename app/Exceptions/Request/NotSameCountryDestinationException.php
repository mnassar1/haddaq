<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class NotSameCountryDestinationException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'NotSameCountryDestinationException', 'Not Same Country Destination Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
