<?php

namespace App\Exceptions\Request;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'RequestNotFoundException', 'Request Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
