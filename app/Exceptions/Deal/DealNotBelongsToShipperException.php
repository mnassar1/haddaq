<?php

namespace App\Exceptions\Deal;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DealNotBelongsToShipperException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'DealNotBelongsToShipperException', 'This action should be done by shipper. ', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
