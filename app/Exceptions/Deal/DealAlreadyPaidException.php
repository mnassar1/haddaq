<?php

namespace App\Exceptions\Deal;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DealAlreadyPaidException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'DealAlreadyPaidException', 'You\'ve paid for this deal you can not pay it again.', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
