<?php

namespace App\Exceptions\Deal;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DealNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'DealNotFoundException', 'Deal Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
