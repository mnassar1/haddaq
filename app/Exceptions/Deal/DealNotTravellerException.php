<?php

namespace App\Exceptions\Deal;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DealNotTravellerException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'DealNotTravellerException', 'This action should be done by traveller. ', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
