<?php

namespace App\Exceptions\Deal;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DealNotBelongsToUserException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(35, 'DealNotBelongsToUserException', 'Deal Not Belongs To User Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
