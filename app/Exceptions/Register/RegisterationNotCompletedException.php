<?php

namespace App\Exceptions\Register;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RegisterationNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(1, 'RegisterationNotCompletedException', "Regsiteration Process goes wrong", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
