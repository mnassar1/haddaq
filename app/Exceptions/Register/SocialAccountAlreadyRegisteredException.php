<?php

namespace App\Exceptions\Register;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class SocialAccountAlreadyRegisteredException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(5, 'SocialAccountAlreadyRegisteredException', "This Social Account Already Registered", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
