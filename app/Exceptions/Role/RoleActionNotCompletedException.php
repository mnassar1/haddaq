<?php

namespace App\Exceptions\Role;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RoleActionNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(13, 'RoleActionNotCompletedException', 'Role Action Not Completed Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
