<?php

namespace App\Exceptions\Role;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RoleNotBelongsToUserGroupException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(17, 'RoleNotBelongsToUserGroupException', 'Role Not Belongs To UserGroup Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
