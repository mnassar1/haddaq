<?php

namespace App\Exceptions\Role;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RoleNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(13, 'RoleNotFoundException', 'Role Not Found Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
