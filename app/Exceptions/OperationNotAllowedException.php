<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class OperationNotAllowedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(39, 'OperationNotAllowedException', 'You have already requested this it.', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
