<?php

namespace App\Exceptions\Admin;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class AdminHasNotPermissionException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(21, 'AdminHasNotPermissionException', 'Admin Has Not Permission Exception', Response::HTTP_UNAUTHORIZED);
    }
}
