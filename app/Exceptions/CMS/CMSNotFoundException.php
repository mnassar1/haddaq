<?php

namespace App\Exceptions\CMS;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class CMSNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(21, 'CMSNotFoundException', 'CMS Not Found Exception', Response::HTTP_NOT_FOUND);
    }
}
