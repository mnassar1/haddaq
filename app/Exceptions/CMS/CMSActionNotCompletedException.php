<?php

namespace App\Exceptions\CMS;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class CMSActionNotCompletedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(13, 'CMSActionNotCompletedException', 'CMS Action Not Completed Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
