<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class DatabaseOperationFailureException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(32, 'DatabaseOperationFailureException', 'Database Operation Failure Exception', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
