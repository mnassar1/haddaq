<?php

namespace App\Exceptions\Faq;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class QuestionNotFoundException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(14, 'QuestionNotFoundException', 'Question Not Found', Response::HTTP_NOT_FOUND);
    }
}
