<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserLoginWithPhoneException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3003, 'UserLoginWithPhoneException', 'User Have To Login With The Email', Response::HTTP_UNAUTHORIZED);
    }
}
