<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserNotAdminException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3001, 'UserNotAdminException', 'User Not Admin Exception', Response::HTTP_UNAUTHORIZED);
    }
}
