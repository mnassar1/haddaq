<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserNotVerifiedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3004, 'UserNotVerifiedException', 'User Not Verified Exception', Response::HTTP_UNAUTHORIZED);
    }
}
