<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserNotBelongsToUserGroupException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3002, 'UserNotBelongsToUserGroupException', 'User Not Belongs To User Group Exception', Response::HTTP_UNAUTHORIZED);
    }
}
