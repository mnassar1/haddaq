<?php

namespace App\Exceptions\User;

use App\Exceptions\ExceptionTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UserIsBlockedException extends Exception
{
    use ExceptionTrait;

    public function render()
    {
        return $this->renderException(3000, 'UserIsBlockedException', 'User Is Blocked Exception', Response::HTTP_UNAUTHORIZED);
    }
}
