<?php
/**
 * Created by PhpStorm.
 * User: nassar
 * Date: 26/01/19
 * Time: 12:52
 */

namespace App\Traits;


use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;


trait Roles
{

    public function authorize()
    {
        $reflect = new \ReflectionClass($this);
        $action = $reflect->getShortName();

        $id = Session::get("admin_id");

        $user = User::find($id);

        return $user->apply_action($action);
    }

    protected function failedAuthorization()
    {

        $validator = $this->getValidatorInstance();
        throw (new ValidationException($validator))
            ->errorBag("Authorization Exception")
            ->withMessages(["Authorization" => "User has not Authorizations "])
            ->redirectTo($this->getRedirectUrl());
    }
}
