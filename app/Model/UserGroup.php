<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed roles
 * @property mixed users
 */
class UserGroup extends Model
{
    protected $fillable = ['name'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'usergroup_role', 'usergroup_id', 'role_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_groups_users', 'user_group_id', 'user_id');
    }
}
