<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "cms_contacts";
    protected $fillable = ['title_ar', 'title_en', 'content_ar', 'content_en', 'type'];
}
