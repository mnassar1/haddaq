<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShipmentsDetails extends Model
{
    protected $fillable = ['shipment_id', 'shipper_type', 'name', 'image', 'id_number'];
    protected $hidden = ['updated_at'];
}
