<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class NotificationSettings extends Model
{
    protected $table = 'notification_settings';
    protected $fillable = ['user_id', 'chat_notification', 'traveller_suggestion', 'shipment_suggestion'];

    /**
     * Notification Settings has one user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne relation
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
