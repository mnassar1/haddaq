<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reminders extends Model
{
    protected $table = "reminders";
    protected $fillable = ['user_id', 'title', 'reminder_date', 'expiration_date'];
}
