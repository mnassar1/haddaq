<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    protected $table = "notification_users";
}
