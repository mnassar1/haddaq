<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SOSResponse extends Model
{
    protected $table = "sos_request_responses";
    protected $fillable = ['user_id', 'location', 'push_token',
        'sos_request_id', 'response_status',];
}
