<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DealPayment extends Model
{
    protected $fillable = ['deal_id', 'from_user', 'to_user', 'amount'];
}
