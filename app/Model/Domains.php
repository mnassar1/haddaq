<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Domains extends Model
{
    protected $connection = "mysql";

    protected $table = "domains";
}
