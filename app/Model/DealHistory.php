<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DealHistory extends Model
{
    protected $table = 'deal_history';
    protected $fillable = ['deal_id', 'type'];
}
