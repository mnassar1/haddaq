<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    protected $table = 'user_payment';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'card_number', 'cvv', 'expire_date'];

    /**
     * payment has one user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }
}
