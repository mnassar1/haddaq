<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminAds extends Model
{
    protected $table = "admin_ads";
    protected $fillable = ['title', 'details', 'images', 'title_ar', 'details_ar', 'images_ar', 'views_count', 'active', 'ordering', 'is_banar', 'fixing_shops_or_home'];
}
