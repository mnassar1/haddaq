<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $connection = "mysql";

    protected $table = "setting";
    protected $fillable = ['number_of_notifications',];
}
