<?php

namespace App\Model;

use App\Exceptions\Request\TripNotHasEnoughKilosException;
use App\Exceptions\Trip\TripNotBelongsToUserException;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int is_finished
 * @property mixed id
 * @property mixed kilos
 * @property mixed requests
 * @property mixed user
 */
class Trip extends Model
{
    protected $fillable = ['user_id', 'country_from', 'city_from', 'country_to', 'city_to', 'departure_date', 'departure_time', 'arrival_date', 'arrival_time', 'kilos', 'notes', 'transportation'];

    /**
     * Model evenet to delete related relations
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($trip) {
            $trip->requests()->delete();
            $trip->deals()->delete();
        });
    }

    /**
     * trip belongs to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * trip belongs to country
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from_country()
    {
        return $this->belongsTo(Country::class, 'country_from', 'id');
    }

    public function to_country()
    {
        return $this->belongsTo(Country::class, 'country_to', 'id');
    }

    /**
     * trip belongs to city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from_city()
    {
        return $this->belongsTo(City::class, 'city_from', 'id');
    }

    public function to_city()
    {
        return $this->belongsTo(City::class, 'city_to', 'id');
    }

    /**
     * Trip User Check
     * @param $user_id
     * @throws TripNotBelongsToUserException
     */
    public function tripUserCheck($user_id)
    {
        if ($this->user == null || $this->user->id != $user_id) {
            throw new TripNotBelongsToUserException;
        }
    }

    /**
     * trip Belongs To Many requests
     * return array of shipments requested to this trip
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests($status = null)
    {
        $filter = [];
        if (null !== $status) {
            $status_enum = ['active' => 'accepted', 'pending' => 'waiting', 'canceled' => 'rejected'];
            $filter['status'] = (in_array($status, ['active', 'pending', 'canceled'])) ? $status_enum[$status] : $status;
        }
        return $this->hasMany(Requests::class, 'trip_id')->where($filter);
    }

    /**
     * check if has enough kilos to new ship
     * @param $shipment_weight
     * @throws TripNotHasEnoughKilosException
     */
    public function checkHasEnoughKilos($shipment_weight)
    {
        $requests = $this->requests;
        $total_kilos = $requests->sum(function ($request) {
            return $request->sum('kilos');
        });
        $remain_kilos = $this->kilos - $total_kilos;
        if ($remain_kilos < $shipment_weight) {
            throw new TripNotHasEnoughKilosException;
        }
    }

    /**
     * trip has many deals
     */
    public function deals()
    {
        return $this->hasMany(Deal::class);
    }

    /**
     * check if trip is editable or not by get deal with paid status
     * you'll this flag on update or delete method
     * @return Boolean
     */
    public function isEditable()
    {
        $paied_deal = Deal::where(['trip_id' => $this->id, 'status' => 'payment'])->first();
        if ($paied_deal) {
            return false;
        }
        return true;
    }

    /**
     * set trip finished
     * @return void
     */
    public function setAsFinished()
    {
        $this->is_finished = 1;
        $this->save();
    }
}
