<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillable = ['category_name', 'category_name_ar', 'icon', 'category_type', 'category_group_id'];
}
