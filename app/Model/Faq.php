<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';
    protected $fillable = ['question_ar', 'question_en', 'answer_ar', 'answer_en', 'question_order'];
}
