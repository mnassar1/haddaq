<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
        'device_id', 'device_type', 'os', 'os_version', 'app_version', 'push_token', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}