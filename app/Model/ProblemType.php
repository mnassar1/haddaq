<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProblemType extends Model
{
    protected $connection = "mysql";

    protected $table = "sos_problem_types";
    protected $fillable = ['problem_type', 'problem_type_ar', 'icon'];
}
