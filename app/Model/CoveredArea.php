<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CoveredArea extends Model
{
    protected $table = "covered_areas";
    protected $fillable = ['country_id', 'area', 'area_name'];
}
