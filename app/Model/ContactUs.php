<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed status
 * @property  status
 */
class ContactUs extends Model
{
    protected $table = 'contact_us';
    protected $fillable = ['name', 'email', 'phone', 'description'];
    protected $hidden = ['updated_at'];
}
