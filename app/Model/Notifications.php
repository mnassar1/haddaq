<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = "notifications";
    protected $fillable = ['type', 'title', 'body', 'url', 'item_id'];
    protected $appends = ['state'];

    public function getStateAttribute()
    {
        $user_id = request()->route()->parameter('user_id');
        if ($user_id) {
            $user_state = NotificationUser::where('user_id', $user_id)->where('notification_id', $this->id)->first();
            if ($user_state) {
                return 1;
            }
        }
        return 0;
    }

    public function getUserNotificationCount()
    {
        $user_id = request()->route()->parameter('user_id');
        if ($user_id) {
            $user_notification_count = NotificationUser::where('user_id', $user_id)->count();
            return $user_notification_count;
        }
        return 0;

    }

}
