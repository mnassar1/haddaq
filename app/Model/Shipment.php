<?php

namespace App\Model;

use App\Exceptions\Shipment\ShipmentCantReceiveRequestException;
use App\Exceptions\Shipment\ShipmentNotBelongsToUserException;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int is_finished
 * @property mixed id
 * @property mixed user
 */
class Shipment extends Model
{

    protected $fillable = ['title', 'country_from', 'city_from', 'country_to', 'city_to', 'weight', 'height', 'width', 'length', 'potential_fees', 'price', 'potential_days_from', 'potential_days_to', 'note', 'photo', 'user_id'];

    /**
     * Model evenet to delete related relations
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($shipment) {
            $shipment->requests()->delete();
            $shipment->deals()->delete();
        });
    }

    /**
     * shipment belongs to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * shipment belongs to country
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from_country()
    {
        return $this->belongsTo(Country::class, 'country_from', 'id');
    }

    public function to_country()
    {
        return $this->belongsTo(Country::class, 'country_to', 'id');
    }

    /**
     * shipment belongs to city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from_city()
    {
        return $this->belongsTo(City::class, 'city_from', 'id');
    }

    public function to_city()
    {
        return $this->belongsTo(City::class, 'city_to', 'id');
    }

    /**
     * Shipment User Check
     * @param $user_id
     * @throws ShipmentNotBelongsToUserException
     */
    public function shipmentUserCheck($user_id)
    {
        if ($this->user == null || $this->user->id !== $user_id) {
            throw new ShipmentNotBelongsToUserException;
        }
    }

    /**
     * shipment Belongs To Many requests
     * return array of trips requested to this shipment
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests($status = null)
    {
        $filter = [];
        if (null !== $status) {
            $status_enum = ['active' => 'accepted', 'pending' => 'waiting', 'canceled' => 'rejected'];
            $filter['status'] = (in_array($status, ['active', 'pending', 'canceled'])) ? $status_enum[$status] : $status;
        }
        return $this->hasMany(Requests::class, 'shipment_id')->where($filter);
    }

    /**
     * check if shipper can request to shippment
     * @return void
     * @throws ShipmentCantReceiveRequestException
     */
    public function canReceiveRequest()
    {
        $hasConfirmedDeal = Deal::where(['shipment_id' => $this->id, 'status' => 'payment'])->first();
        if ($hasConfirmedDeal) {
            throw new ShipmentCantReceiveRequestException;
        }
    }

    /**
     * shipment hasOne details
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details()
    {
        return $this->hasOne(ShipmentsDetails::class);
    }

    /**
     * Sjipment has many deals.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deals()
    {
        return $this->hasMany(Deal::class);
    }

    /**
     * Check if shipment editable or not by get deal with paid status
     * you'll use this flag on update and delete method
     * @return Boolean
     */
    public function isEditable()
    {
        $paid_deal = Deal::where(['shipment_id' => $this->id, 'status' => 'payment'])->first();
        if ($paid_deal) {
            return false;
        }
        return true;
    }

    /**
     * set shipment as finished
     * @return void
     */
    public function setAsFinished()
    {
        $this->is_finished = 1;
        $this->save();
    }
}
