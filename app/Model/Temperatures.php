<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Temperatures extends Model
{
    protected $table = "temperatures";
    protected $fillable = [
        'highest_temp', 'lowest_temp', 'wind_speed',
        'sea_waves_by_day', 'sea_waves_by_night', 'first_high_tide',
        'second_high_tide', 'first_low_tide', 'second_low_tide', 'country_id',
    ];


}
