<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CMS extends Model
{
    protected $connection = "mysql";

    protected $table = "cms";
    protected $fillable = ['title_ar', 'title_en', 'content_ar', 'content_en', 'type'];
}
