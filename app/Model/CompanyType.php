<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    protected $connection = "mysql";

    protected $table = "company_types";
    protected $fillable = ['type_name', 'type_name_ar', 'number_of_notifications'];
}
