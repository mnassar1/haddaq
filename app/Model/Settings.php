<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $connection = "mysql";

    protected $fillable = ['title', 'setting_key', 'setting_value'];
}
