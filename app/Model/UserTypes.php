<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTypes extends Model
{
    protected $table = "user_types";
    protected $fillable = ['type_name'];


}
