<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestHistory extends Model
{
    public $table = 'request_history';
    public $fillable = ['request_id', 'status', 'created_at'];
}
