<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    protected $connection = "mysql";
    protected $table = "favorites";
    protected $fillable = ['user_id', 'item_id', 'type','country_code'];
}
