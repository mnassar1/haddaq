<?php

namespace App\Model;

use App\Exceptions\Deal\DealNotBelongsToUserException;
use App\Exceptions\Deal\DealNotTravellerException;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed price
 * @property  traveller_id
 * @property  shipper_id
 * @property  traveller_id
 * @property  shipper_id
 * @property mixed delivery_time
 * @property mixed delivery_place
 * @property mixed pickup_time
 * @property mixed pickup_place
 * @property  shipment_id
 * @property  trip_id
 * @property  request_id
 * @property mixed traveller_id
 * @property mixed shipper_id
 */
class Deal extends Model
{
    protected $fillable = ['request_id', 'trip_id', 'shipper_id', 'traveller_id', 'shipper_id', 'shipment_id', 'pickup_place', 'pickup_time', 'delivery_place', 'delivery_time', 'price', 'qr_code_string'];

    /**
     * Model event to delete deal relations
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($deal) {
            $deal->images()->delete();
            $deal->notes()->delete();
        });
    }

    /**
     * retrun deal traveller
     */
    public function traveller()
    {
        return $this->hasOne(User::class, 'id', 'traveller_id');
    }

    /**
     * return deal shipper
     */
    public function shipper()
    {
        return $this->hasOne(User::class, 'id', 'shipper_id');
    }

    /**
     * return deal's request
     */
    public function request()
    {
        return $this->hasOne(Requests::class, 'id', 'request_id');
    }

    /**
     * deal has many images
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(DealImages::class);
    }

    /**
     * check if deal belongs to user
     * @param $user_id
     * @throws DealNotBelongsToUserException
     */
    public function dealUserCheck($user_id)
    {
        if ($this->traveller_id !== $user_id && $this->shipper_id !== $user_id) {
            throw new DealNotBelongsToUserException;
        }
    }

    /**
     * deal notes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(DealNotes::class);
    }

    /**
     * deal has 1 trip
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function trip()
    {
        return $this->hasOne(Trip::class, 'id', 'trip_id');
    }

    /**
     * deal has 1 shipment
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shipment()
    {
        return $this->hasOne(Shipment::class, 'id', 'shipment_id');
    }

    /** Deal Log relations */

    /**
     * deal has qr_log relations
     *
     */
    public function qr_log()
    {
        return $this->hasOne(DealHistory::class)->where('type', 'qr');
    }

    /**
     * Deal has information_log
     */
    public function information_log()
    {
        return $this->hasOne(DealHistory::class)->where('type', 'information');
    }

    /**
     * deal has negotiation
     */

    public function negotiation_log()
    {
        return $this->hasOne(DealHistory::class)->where('type', 'negotiation');
    }

    /**
     * User DealTravellerCheck
     * @param $user_id
     * @throws DealNotTravellerException
     */
    public function dealTravellerCheck($user_id)
    {
        if ($this->traveller_id !== $user_id) {
            throw new DealNotTravellerException;
        }
    }
}
