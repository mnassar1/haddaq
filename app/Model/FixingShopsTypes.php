<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FixingShopsTypes extends Model
{
    protected $table = "fixing_shops_types";
    protected $fillable = ['type_name'];
}
