<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryGroup extends Model
{
    protected $table = "category_groups";
    protected $fillable = ['group_name', 'group_name_ar'];
}
