<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppAds extends Model
{
    protected $table = "ads_items";
    protected $fillable = ['created_at', 'title', 'details', 'images', 'views_count', 'company_id', 'category_id', 'price', 'approved_status', 'used_status', 'views_count', 'block_reason'];


}
