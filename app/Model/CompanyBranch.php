<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyBranch extends Model
{
    protected $table = "company_branches";
    protected $fillable = ['company_id', 'address', 'location', 'title'];


}
