<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed|string caption
 * @property mixed image
 * @property  deal_id
 */
class DealImages extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
}
