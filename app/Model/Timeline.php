<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    protected $table = 'timeline';
    protected $fillable = ['verb', 'type', 'object_id', 'object', 'user_id', 'user_object', 'country_id', 'city_id', 'created_at'];
}
