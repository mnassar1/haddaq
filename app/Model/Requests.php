<?php

namespace App\Model;

use App\Exceptions\Request\RequestNotBelongsToUserException;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed shipment_id
 * @property mixed trip_id
 * @property mixed by
 * @property mixed to
 * @property mixed type
 * @property mixed to_user
 */
class Requests extends Model
{
    protected $table = 'requests';
    protected $fillable = ['trip_id', 'shipment_id', 'type', 'by', 'to', 'status', 'notes', 'kilos'];
    protected $hidden = ['updated_at'];

    /**
     * get request trip.
     */
    public function trip()
    {
        return Trip::find($this->trip_id);
    }

    /**
     * get request shipment
     */
    public function shipment()
    {
        return Shipment::find($this->shipment_id);
    }

    /**
     * Request User Check
     * @param $user_id
     * @throws RequestNotBelongsToUserException
     */
    public function requestUserCheck($user_id)
    {
        if ($this->to !== $user_id && $this->by !== $user_id) {
            throw new RequestNotBelongsToUserException;
        }
    }

    /**
     * RequestHistroy Relations
     * Request hasOne RequestHistory with 'closed' status
     */
    public function qr_log()
    {
        return $this->hasOne(RequestHistory::class, 'request_id')->where('status', 'closed');
    }

    /**
     * Request hasOne RequestHistory with 'accepted' status
     */
    public function accepted_log()
    {
        return $this->hasOne(RequestHistory::class, 'request_id')->where('status', 'accepted');
    }

    /**
     * Request hasOne RequestHistory with 'negotiation' status
     */
    public function negotiation_log()
    {
        return $this->hasOne(RequestHistory::class, 'request_id')->where('status', 'negotiation');
    }

    /**
     * request done 'by' User
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function by_user()
    {
        return $this->hasOne(User::class, 'id', 'by');
    }

    /**
     * request sent 'to' user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function to_user()
    {
        return $this->hasOne(User::class, 'id', 'to');
    }

    /**
     * check if request is acceptable
     * @return Boolean
     */
    public function isAcceptable()
    {
        // dd($this->trip_id,$this->shipment_id);
        $deal = Deal::whereIn('status', ['payment', 'closed'])->where(function ($query) {
            $query->where('trip_id', $this->trip_id);
            $query->orWhere('shipment_id', $this->shipment_id);
        })->first();
        if ($deal) {
            return false;
        }
        return true;
    }
}
