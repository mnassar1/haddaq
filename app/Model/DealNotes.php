<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DealNotes extends Model
{
    protected $fillable = ['note', 'type', 'deal_id'];
}
