<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FixingShops extends Model
{
    protected $table = "fixing_shops";
    protected $fillable = ['shop_name', 'shop_name_ar', 'location', 'images', 'sliders', 'mobile', 'about', 'email', 'fixing_shops_types_id', 'address', 'address_ar', 'city_id', 'about_ar'];
}
