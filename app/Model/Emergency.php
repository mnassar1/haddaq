<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Emergency extends Model
{
    protected $table = "emergency";
    protected $fillable = ['name', 'name_ar', 'mobile'];
}
