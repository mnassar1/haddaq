<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SOSRequest extends Model
{
    protected $table = "sos_requests";
    protected $fillable = ['user_id', 'mobile', 'location', 'device_udid', 'push_token',
        'sos_problem_type_id', 'description', 'sos_request_status', 'closed_by'];
}
