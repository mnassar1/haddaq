<?php

namespace App;

use App\Model\City;
use App\Model\Country;
use App\Model\Deal;
use App\Model\Device;
use App\Model\NotificationSettings;
use App\Model\Requests;
use App\Model\Role;
use App\Model\UserGroup;
use App\Model\UserPayment;
use App\Notifications\PaymentEmail;
use App\Notifications\ResetPasswordEmail;
use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


/**
 * @property mixed reset_token
 * @property mixed verification_code
 * @property mixed id
 * @property mixed roles
 * @property mixed device
 * @property  notification_settings
 * @property int is_mobile_verified
 * @property int mobile_verification_code
 * @property mixed profile_image_url
 * @property mixed name
 * @property mixed payment_data
 * @property mixed usergroups
 * @property mixed login_token
 * @property mixed device_id
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = "mysql";

    protected $fillable = [
        'country_code', 'mobile', 'full_name', 'email', 'address', 'location', 'password', 'city_id',
        'is_verified', 'profile_image_url', 'sliders', 'verification_code',
        'password', 'is_admin', 'is_blocked', 'user_types_id',
        'savior_with_price', 'about', 'language', 'mobile_verification_code', 'is_mobile_verified', 'roles', 'country_iso_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    public function device()
    {
        return $this->hasOne(Device::class);
    }

    /**
     * send verification email.
     * @return void
     */
    public function sendVerificationEmail()
    {
        $this->notify(new VerifyEmail($this));
    }

    /**
     * send payment confirmation email.
     * @param $shipper
     * @param $deal
     * @return void
     */
    public function sendPaymentConfirmationEmail($shipper, $deal)
    {
        $this->notify(new PaymentEmail($this, $shipper, $deal));
    }

    /**
     * send reset password
     * @return void
     */
    public function sendResetPassword()
    {
        $this->notify(new ResetPasswordEmail($this));
    }

    /**
     * user access tokens
     * @return \Illuminate\Database\Eloquent\Relations\HasMany relationship
     */

    public function accessTokens()
    {
        return $this->hasMany(OauthAccessToken::class);
    }

    /**
     * user password reset tokens
     */

    public function resetPasswordToken()
    {
        return $this->hasOne(PasswordResets::class, 'email', 'email');
    }


    /**
     * isHerDevice
     * @param $device_id
     * @return boolean
     */
    public function isHisDevice($device_id)
    {
        return $this->device->device_id = $device_id;
    }


    /**
     * user's usergroups
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function usergroups()
    {
        return $this->belongsToMany(UserGroup::class, 'user_groups_users', 'user_id', 'user_group_id');
    }

    /**
     * requests that sent to user with status = 'waiting' that needs me to accept \ reject it
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function pending_requests()
    {
        return $this->hasMany(Requests::class, 'to')
            ->where('status', 'waiting')
            ->get();
    }

    /**
     * requests that sent by user with status = 'waiting' that need other side to accept / reject it
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function my_pending_requests()
    {
        return $this->hasMany(Requests::class, 'by')
            ->where('status', 'waiting')
            ->get();
    }

    /**
     * requests that sent to user with status = 'accepted'
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function active_requests()
    {
        return $this->hasMany(Requests::class, 'to')
            ->where('status', 'accepted')
            ->get();
    }

    /**
     * requests hat sent by user with status = 'accepted'
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function my_active_requests()
    {
        return $this->hasMany(Requests::class, 'by')
            ->where('status', 'accepted')
            ->get();
    }

    /**
     * requests that sent to user with status
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function requests($status = null)
    {
        $filter = [];
        if (null !== $status) {
            $filter['status'] = $status;
        }
        return $this->hasMany(Requests::class, 'to')
            ->where($filter)
            ->get();
    }

    /**
     * requests that sent by user with status
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function my_requests($status = null)
    {
        $filter = [];
        if (null !== $status) {
            $filter['status'] = $status;
        }
        return $this->hasMany(Requests::class, 'by')
            ->where($filter)
            ->get();
    }

    /**
     * deals which are all 'accepted' requests related to user 'by' 'to'
     * @return relationship
     * raw query SELECT  *  FROM `requests` WHERE (`requests`.`by` = ? AND `status` = 'accepted') OR (`to` = ? AND `status` = 'accepted')
     */
    // public function deals(){
    //     return Requests::where(function($query){
    //         $query->where(function($query){
    //             $query->where('by',$this->id);
    //             $query->where('status','accepted');
    //         });

    //         $query->orWhere(function($query){
    //             $query->where('to',$this->id);
    //             $query->where('status','accepted');
    //         });
    //     })
    //     ->get();
    // }

    /**
     * Deals
     * @return relations
     */
    public function deals()
    {
        return Deal::where('status', '!=', 'closed')->where(function ($query) {
            $query->where('traveller_id', $this->id);
            $query->orWhere('shipper_id', $this->id);
        })->get();
    }

    /**
     * user relationship with country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * user relationship with city
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * User has one Notification Settings
     * @return \Illuminate\Database\Eloquent\Relations\HasOne relation
     */
    public function notificationSettings()
    {
        return $this->hasOne(NotificationSettings::class);
    }

    /**
     * Payment Data
     */
    public function payment_data()
    {
        return $this->hasOne(UserPayment::class);
    }

    public function apply_action($role)
    {
        $roles = $this->roles;
        $user_roles = json_decode($roles);
        $if_role_here = Role::where("name", $role)->count();
        if ($if_role_here == 0) {
            return true;
        }
        if ($user_roles != null && in_array($role, $user_roles)) {
            return true;
        }
        return false;
    }
}
