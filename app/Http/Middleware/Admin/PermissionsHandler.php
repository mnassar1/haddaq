<?php

namespace App\Http\Middleware\Admin;

use App;
use App\Exceptions\Admin\AdminHasNotPermissionException;
use App\Exceptions\User\UserNotAdminException;
use App\Exceptions\User\UserNotFoundException;
use App\Helpers\AdminHelperTrait;
use App\User;
use Closure;

class PermissionsHandler
{
    use AdminHelperTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $level
     * @param null $permission
     * @return mixed
     * @throws AdminHasNotPermissionException
     * @throws UserNotAdminException
     * @throws UserNotFoundException
     */
    public function handle($request, Closure $next, $level = null, $permission = null)
    {
        $admin_id = $request->header('admin-id');
        if (!$admin_id) {
            throw new UserNotAdminException;
        } else {
            $admin = User::find($admin_id);
            if (!$admin) {
                throw new UserNotFoundException;
            } else {
                if ($admin->is_admin == 0) {
                    throw new UserNotAdminException;
                } else {
                    $permissions = $this->generateRolesDictionary($admin);
                    if (!isset($permissions[$level])) {
                        throw new AdminHasNotPermissionException;
                    } else {
                        if (!in_array($permission, $permissions[$level])) {
                            throw new AdminHasNotPermissionException;
                        }
                    }
                }
            }

            return $next($request);
        }
    }
}
