<?php

namespace App\Http\Middleware\Web;

use Closure;
use Illuminate\Support\Facades\Session;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('is_logged_in') || Session::get('is_logged_in') != true || !Session::has('access_token')) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
