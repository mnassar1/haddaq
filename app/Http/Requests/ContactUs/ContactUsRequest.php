<?php

namespace App\Http\Requests\ContactUs;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required_if:logged,===,1|string',
            'email' => 'sometimes|required_without:user_type_id|required_if:user_type_id,===,3|email',
            'phone' => 'sometimes|required_if:logged,===,1|required_if:user_type_id,!=,3|string',
//            'subject' => 'required|string',
//            'type' => 'string|in:suggestion,complain,enquiry',
            'description' => 'required|string',
//            'status' => 'string|in:new,processing,archived'
        ];
    }
}
