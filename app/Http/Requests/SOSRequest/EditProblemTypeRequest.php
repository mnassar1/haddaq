<?php

namespace App\Http\Requests\ProblemTypes;

use Illuminate\Foundation\Http\FormRequest;

class EditProblemTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "problem_type" => "required|string",
            "problem_type_ar" => "required|string",
            "icon" => "image|mimes:jpeg,png,jpg,gif,svg",

        ];
    }
}
