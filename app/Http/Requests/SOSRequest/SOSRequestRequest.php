<?php

namespace App\Http\Requests\SOSRequest;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed user_id
 * @property mixed location
 * @property mixed sos_problem_type_id
 */
class SOSRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "string",
            "mobile" => "required|string",
            "location" => "required|string",
            "device_udid" => "required|string",
            "push_token" => "required|string",
            "sos_problem_type_id" => "required|string",
            //"description" => "string",
        ];
    }
}
