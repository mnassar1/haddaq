<?php

namespace App\Http\Requests\CategoryGroup;

use Illuminate\Foundation\Http\FormRequest;

class EditCategoryGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "group_name" => "required|string",
            "group_name_ar" => "required|string",


        ];
    }
}
