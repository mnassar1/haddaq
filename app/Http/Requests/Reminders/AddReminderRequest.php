<?php

namespace App\Http\Requests\Reminders;

use Illuminate\Foundation\Http\FormRequest;

class AddReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_name" => "required|string",
            "category_name_ar" => "required|string",
            "icon" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "category_type" => "required|in:1,2,3",

        ];
    }
}
