<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed password
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "device_id" => "string",
            "country_code" => "required|string",
            "mobile" => "sometimes|required_unless:user_types_id,3|string|unique:users|nullable",
            "user_types_id" => "required|in:1,2,3,4",
            "savior_with_price" => "required|numeric",
            "full_name" => "required|string",
            "email" => "sometimes|required_if:user_types_id,==,3|email|unique:users|nullable",
            "password" => "required|min:8",
            "city_id" => "integer",
            "device_type" => "required|in:phone,tablet",
            "os" => "required|in:android,ios",
            "os_version" => "required|string",
            "app_version" => "required|string",
            "push_token" => "required|string"
        ];
    }
}