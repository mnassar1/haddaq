<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

class RegisterSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "device_id" => "required|string",
            "phone" => "required|string",
            "name" => "required|string",
            "email" => "required|email|unique:users",
            "register_type" => "required|in:fb,g+,tw,li",
            "social_id" => "required|string",
            "country_id" => "required|integer",
            "city_id" => "required|integer",
            "device_type" => "required|in:phone,tablet",
            "os" => "required|in:android,ios",
            "os_version" => "required|string",
            "app_version" => "required|string",
            "push_token" => "required|string"
        ];
    }
}
