<?php

namespace App\Http\Requests\FixingShop;

use Illuminate\Foundation\Http\FormRequest;

class AddFixingShopsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "shop_name" => "required|string",
            "shop_name_ar" => "required|string",
            "images" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "sliders.*" => "image|mimes:jpeg,png,jpg,gif,svg",
            "fixing_shops_types_id" => "required|in:1,2,3",
            "about" => "required|string",
            "about_ar" => "required|string",
            "location" => "required|string",
            "address" => "required|string",
            "address_ar" => "required|string",
            "city_id" => "required",
            "mobile" => "required|string",
            "email" => "required|string",

        ];
    }
}
