<?php

namespace App\Http\Requests\AppAds;

use Illuminate\Foundation\Http\FormRequest;

class EditAppAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "details" => "required|string",
            //"images" => "required",
            "images.*" => "image|mimes:jpeg,png,jpg,gif,svg",
            "company_id" => "required|exists:users",
            "category_id" => "required|exists:categories",
            "price" => "required|string",
            "used_status" => "required|in:0,1",
        ];
    }
}
