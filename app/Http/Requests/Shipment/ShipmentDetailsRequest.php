<?php

namespace App\Http\Requests\Shipment;

use Illuminate\Foundation\Http\FormRequest;

class ShipmentDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['traveler_id'] = 'required|integer';
        if ($this->has('shipper_type')) {
            if ($this->get('shipper_type') == 'delegated_person') {
                $rules['name'] = 'required|string';
                // $rules['id_number'] = 'required|string';
                $rules['image'] = 'required|string';
            } else if ($this->get('shipper_type') == 'freight_company') {
                $rules['name'] = 'required|string';
            } else if ($this->get('shipper_type') == 'me') {
                $rules['id_number'] = 'string';
            }
        } else {
            $rules['shipper_type'] = 'required|in:me,delegated_person,freight_company';
        }
        return $rules;
    }
}
