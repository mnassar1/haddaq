<?php

namespace App\Http\Requests\Shipment;

use Illuminate\Foundation\Http\FormRequest;

class ShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "device_id" => "required|string",
            "user_id" => "required|integer",
            "title" => "required|string",
            "country_from" => "required|integer",
            "city_from" => "required|integer",
            "country_to" => "required|integer",
            "city_to" => "required|integer",
            "weight" => "required|between:0,999999.99",
            "height" => "between:0,999999.99",
            "width" => "between:0,999999.99",
            "length" => "between:0,999999.99",
            "potential_fees" => "required|between:0,999999.99",
            "price" => "between:0,999999.99",
            "potential_days_from" => "required|date|before:potential_days_to",
            "potential_days_to" => "required|date",
            "note" => "sometimes",
            "photo" => "string"
        ];
    }
}
