<?php

namespace App\Http\Requests\Login;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed password
 * @property mixed email
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_id' => 'sometimes|required_if:login_type,==,phone|string',
            'country_code' => 'string',
            'phone' => 'required|string',
            'password' => 'required',
            'device_type' => 'required|in:phone,tablet',
            'os' => 'required|in:android,ios',
            'os_version' => 'required|string',
            'app_version' => 'required|string',
            'push_token' => 'required|string'
        ];
    }
}
