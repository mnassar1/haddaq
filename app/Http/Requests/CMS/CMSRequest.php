<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CMSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title_en" => "required|string",
            "title_ar" => "required|string",
            "content_en" => "required|string",
            "content_ar" => "required|string",
            "type" => "required|string|unique:cms",
        ];
    }
}
