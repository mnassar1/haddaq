<?php

namespace App\Http\Requests\CompanyAds;

use Illuminate\Foundation\Http\FormRequest;

class CompanyAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "details" => "required|string",
            "images" => "required",
            "images.*" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "category_id" => "required",
            "price" => "required|string",
            "used_status" => "required|in:0,1",
        ];
    }
}
