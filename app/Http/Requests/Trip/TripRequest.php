<?php

namespace App\Http\Requests\Trip;

use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "device_id" => "required|string",
            "user_id" => "required|integer",
            "country_from" => "required",
            "city_from" => "required",
            "country_to" => "required",
            "city_to" => "required",
            "departure_date" => "required",
            "departure_time" => "required",
            "arrival_date" => "required",
            "arrival_time" => "required",
            "transportation" => "in:plane,autobus,train",
            "kilos" => "required",
            "notes" => "sometimes"
        ];
    }
}
