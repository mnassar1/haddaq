<?php

namespace App\Http\Requests\Trip;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed price
 * @property mixed delivery_time
 * @property mixed delivery_place
 * @property mixed pickup_time
 * @property mixed pickup_place
 */
class DealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "pickup_place" => "required|string",
            "pickup_time" => "required|string",
            "delivery_place" => "required|string",
            "delivery_time" => "required|string",
            "price" => "required|between:0,999999.99"
        ];
    }
}
