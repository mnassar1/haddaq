<?php

namespace App\Http\Requests\AdminAds;

use Illuminate\Foundation\Http\FormRequest;

class AdminAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "details" => "required|string",
            "ordering" => "required|string",
            "images" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"

        ];
    }
}
