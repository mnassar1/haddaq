<?php

namespace App\Http\Requests\AdminAds;

use Illuminate\Foundation\Http\FormRequest;

class AddAdminAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "details" => "required|string",
            "title_ar" => "required|string",
            "details_ar" => "required|string",
            "ordering" => "required|string",
            "images" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "images_ar" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"

        ];
    }
}
