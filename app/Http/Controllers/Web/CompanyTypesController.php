<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddCategoryRequest;
use App\Http\Requests\CompanyType\EditCompanyTypeRequest;
use App\Http\Resources\CompanyType\CompanyTypeCollection;
use App\Http\Resources\CompanyType\CompanyTypeResource;
use App\Model\Category;
use App\Model\CompanyType;
use Illuminate\Http\Request;

class CompanyTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = CompanyType::where($simple_filter);

        $category_name = (isset($filter['simple']['type_name'])) ? $filter['simple']['type_name'] : NULL;

        if (!is_null($category_name)) {
            $categories = CompanyType::where('type_name', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['type_name_ar'])) ? $filter['simple']['type_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = CompanyType::where('type_name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CompanyTypeCollection::collection($categories));
        $data['categories'] = json_decode($collection);

        return view('admin.company_types.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];


        if ($request->has('name') && null !== $request->get('name')) {
            $filter['simple']['type_name'] = $request->get('name');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['type_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryRequest $request)
    {
        $category = $request->all();


        $categories = CompanyType::create($category);
        return redirect(route('company_types.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = CompanyType::find($id);
        if (!$categories) {
            return redirect(route('company_types.index'));
        }
        $resource = json_encode(new CompanyTypeResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.company_types.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $categories = CompanyType::find($id);
        if (!$categories) {
            return redirect(route('company_types.index'));
        }
        $data['categories'] = $categories;

        return view('admin.company_types.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCompanyTypeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyTypeRequest $request, $id)
    {
        $categories = CompanyType::find($id);
        if (!$categories) {
            return redirect(route('company_types.index'));
        }

        $category = $request->all();

        $categories->update($category);
        return redirect(route('company_types.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }
        $categories->delete();
        return redirect(route('categories.index'));
    }


}
