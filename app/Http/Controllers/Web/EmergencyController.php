<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Emergency\AddEmergencyRequest;
use App\Http\Requests\Emergency\EditEmergencyRequest;
use App\Http\Resources\Emergency\EmergencyCollection;
use App\Http\Resources\Emergency\EmergencyResource;
use App\Model\Emergency;
use Illuminate\Http\Request;

class EmergencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = Emergency::where($simple_filter);

        $category_name = (isset($filter['simple']['category_name'])) ? $filter['simple']['category_name'] : NULL;

        if (!is_null($category_name)) {
            $categories = Emergency::where('name', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['category_name_ar'])) ? $filter['simple']['category_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = Emergency::where('name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(EmergencyCollection::collection($categories));
        $data['categories'] = json_decode($collection);
        return view('admin.emergency.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];


        if ($request->has('name') && null !== $request->get('name')) {
            $filter['simple']['category_name'] = $request->get('name');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.emergency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddEmergencyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddEmergencyRequest $request)
    {
        $category = $request->all();

        $categories = Emergency::create($category);
        return redirect(route('emergency.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = Emergency::find($id);
        if (!$categories) {
            return redirect(route('emergency.index'));
        }
        $resource = json_encode(new EmergencyResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.emergency.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = Emergency::find($id);
        if (!$catrgories) {
            return redirect(route('emergency.index'));
        }
        $data['categories'] = $catrgories;
        return view('admin.emergency.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditEmergencyRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditEmergencyRequest $request, $id)
    {
        $categories = Emergency::find($id);
        if (!$categories) {
            return redirect(route('emergency.index'));
        }

        $category = $request->all();

        $categories->update($category);
        return redirect(route('emergency.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Emergency::find($id);
        if (!$categories) {
            return redirect(route('emergency.index'));
        }
        $categories->delete();
        return redirect(route('emergency.index'));
    }


}
