<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\City\AddCityRequest;
use App\Http\Requests\City\EditCityRequest;
use App\Http\Resources\City\CityCollection;
use App\Http\Resources\City\CityResource;
use App\Model\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = City::where($simple_filter);

        $category_name = (isset($filter['simple']['category_name'])) ? $filter['simple']['category_name'] : NULL;

        if (!is_null($category_name)) {
            $categories = City::where('name_en', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['category_name_ar'])) ? $filter['simple']['category_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = City::where('name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CityCollection::collection($categories));
        $data['categories'] = json_decode($collection);

        return view('admin.city.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('category_type') && $request->get('category_type')) {
            $filter['simple']['category_type'] = $request->get('category_type');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['category_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data['countries']= City::where('id','<>',0)->get();
        return view('admin.city.create');//->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCityRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCityRequest $request)
    {
        $category = $request->all();


        $categories = City::create($category);
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = City::find($id);
        if (!$categories) {
            return redirect(route('cities.index'));
        }
        $resource = json_encode(new CityResource($categories));
        $data['categories'] = json_decode($resource);
        //$data['groups']= CategoryGroup::find(['id'=>$categories->category_group_id])->first();
        return view('admin.city.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = City::find($id);
        if (!$catrgories) {
            return redirect(route('cities.index'));
        }
        $data['categories'] = $catrgories;
        // $data['groups']= CategoryGroup::where('id','<>',0)->get();
        return view('admin.city.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCityRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCityRequest $request, $id)
    {
        $categories = City::find($id);
        if (!$categories) {
            return redirect(route('cities.index'));
        }

        $category = $request->all();

        $categories->update($category);
        return redirect(route('cities.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = City::find($id);
        if (!$categories) {
            return redirect(route('cities.index'));
        }
        $categories->delete();
        return redirect(route('cities.index'));
    }


}
