<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyProfile\EditCompanyProfileRequest;
use App\Http\Resources\User\UserResource;
use App\User;
use Illuminate\Http\Request;
use session;

class CompanyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        // $filter = $this->getFilter($request);
        // $data = [];


        // $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        // $users = User::where($simple_filter);
        // $order_filter = (isset($filter['order'])) ? $filter['order'] : [];
        // if(!empty($order_filter)){
        //     $users = $users->orderByRaw($order_filter);
        // }
        // $users= $users->where(['id'=>session('user_id')]);
        // $users = $users->paginate(10);
        // $data['paginate'] = $users;
        // $collection = json_encode(UserCollection::collection($users));
        // $data['users'] = json_decode($collection);
        // $data['user_types'] = UserTypes::orderby('id')->get();
        // return view('admin.company_profile.index')->with($data);
        $id = session('user_id');
        $user = User::find($id);
        if (!$user) {
            return redirect(route('company_profile.index'));
        }
        if ($id != session('user_id')) {
            return redirect(route('company_profile.index'));
        }
        $data['userObject'] = $user;
        $register_enum = ['email' => 'Email', 'li' => 'LinkedIn', 'g+' => 'GMail', 'tw' => 'Twitter', 'fb' => 'Facebook'];
        $data['register_via'] = $register_enum;
        $resource = json_encode(new UserResource($user));
        $data['user'] = json_decode($resource);
        $data['city'] = \App\Model\City::find($user->city_id);
        $data['branches'] = \App\Model\CompanyBranch::where(['company_id' => $id])->get();
        $data['company_types'] = \App\Model\CompanyType::all();
        return view('admin.company_profile.show')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $user = User::find($id);
        if (!$user) {
            return redirect(route('company_profile.index'));
        }
        if ($id != session('user_id')) {
            return redirect(route('company_profile.index'));
        }
        $data['userObject'] = $user;
        $register_enum = ['email' => 'Email', 'li' => 'LinkedIn', 'g+' => 'GMail', 'tw' => 'Twitter', 'fb' => 'Facebook'];
        $data['register_via'] = $register_enum;
        $resource = json_encode(new UserResource($user));
        $data['user'] = json_decode($resource);
        $data['city'] = \App\Model\City::find($user->city_id);
        $data['branches'] = \App\Model\CompanyBranch::where(['company_id' => $id])->get();
        $data['company_types'] = \App\Model\CompanyType::all();
        return view('admin.company_profile.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect(route('company_profile.index'));
        }
        if ($id != session('user_id')) {
            return redirect(route('company_profile.index'));
        }
        $data['user'] = $user;
        $data['cities'] = \App\Model\City::all();
        $data['company_types'] = \App\Model\CompanyType::all();
        return view('admin.company_profile.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCompanyProfileRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyProfileRequest $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect(route('company_profile.index'));
        }
        if ($id != session('user_id')) {
            return redirect(route('company_profile.index'));
        }

        $user_data = $request->all();

        if ($request->hasFile('profile_image_url')) {
            $file = $request->file('profile_image_url');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/profile_images/'), $image_name)) {
                $user_data['profile_image_url'] = "profile_images/" . $image_name;
            }

        } else {
            unset($user_data['profile_image_url']);
        }

        if ($request->hasFile('sliders')) {
            foreach ($request->file('sliders') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                if ($file->move(storage_path('app/public/profile_images/'), $image_name)) {
                    $images[] = "profile_images/" . $image_name;
                }

            }
            $user_data['sliders'] = json_encode($images);
        } else {
            unset($user_data['sliders']);
        }


        $user->update($user_data);
        return redirect(route('company_profile.show', $user->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }


    /**
     * get filter
     * @param Request $request
     * @return array
     */
    public function getFilter(Request $request)
    {
        $filter = [];

        // city_id
        if ($request->has('city_id') && !empty($request->get('city_id'))) {
            $filter['simple']['city_id'] = $request->get('city_id');
        }

        // register_type
        if ($request->has('user_types_id') && $request->get('user_types_id')) {
            $filter['simple']['user_types_id'] = $request->get('user_types_id');
        }

        // is_verified
        if ($request->has('is_verified') && null !== $request->get('is_verified')) {
            $filter['simple']['is_verified'] = $request->get('is_verified');
        }

        // is_admin
        if ($request->has('is_admin') && null !== $request->get('is_admin')) {
            $filter['simple']['is_admin'] = $request->get('is_admin');
        }

        // is_blocked
        if ($request->has('is_blocked') && null !== $request->get('is_blocked')) {
            $filter['simple']['is_blocked'] = $request->get('is_blocked');
        }

        // name
        if ($request->has('name') && null !== $request->get('name')) {
            $filter['simple']['full_name'] = $request->get('name');
        }

        // email
        if ($request->has('email') && null !== $request->get('email')) {
            $filter['simple']['email'] = $request->get('email');
        }

        // phone
        if ($request->has('mobile') && null !== $request->get('mobile')) {
            $filter['simple']['mobile'] = $request->get('mobile');
        }


        return $filter;
    }
}
