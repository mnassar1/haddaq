<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryGroup\AddCategoryGroupRequest;
use App\Http\Requests\CategoryGroup\EditCategoryGroupRequest;
use App\Http\Resources\CategoryGroup\CategoryGroupCollection;
use App\Http\Resources\CategoryGroup\CategoryGroupResource;
use App\Model\CategoryGroup;
use Illuminate\Http\Request;

class CategoryGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = CategoryGroup::where($simple_filter)->where('id', '<>', 0);

        $category_name = (isset($filter['simple']['group_name_en'])) ? $filter['simple']['group_name_en'] : NULL;

        if (!is_null($category_name)) {
            $categories = CategoryGroup::where('group_name_en', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['group_name_ar'])) ? $filter['simple']['group_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = CategoryGroup::where('group_name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CategoryGroupCollection::collection($categories));
        $data['categories'] = json_decode($collection);

        return view('admin.category_groups.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['group_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['group_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category_groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCategoryGroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryGroupRequest $request)
    {
        $category = $request->all();


        $categories = CategoryGroup::create($category);
        return redirect(route('category_groups.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = CategoryGroup::find($id);
        if (!$categories) {
            return redirect(route('category_groups.index'));
        }
        $resource = json_encode(new CategoryGroupResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.category_groups.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $categories = CategoryGroup::find($id);
        if (!$categories) {
            return redirect(route('category_groups.index'));
        }
        $data['categories'] = $categories;

        return view('admin.category_groups.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCategoryGroupRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryGroupRequest $request, $id)
    {
        $categories = CategoryGroup::find($id);
        if (!$categories) {
            return redirect(route('category_groups.index'));
        }

        $category = $request->all();

        $categories->update($category);
        return redirect(route('category_groups.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = CategoryGroup::find($id);
        if (!$categories) {
            return redirect(route('category_groups.index'));
        }
        $categories->delete();
        return redirect(route('category_groups.index'));
    }


}
