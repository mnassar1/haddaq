<?php

namespace App\Http\Controllers\Web;

use App\Helpers\NotificationsCenter;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAds\AdminAdsRequest;
use App\Http\Resources\AppAds\AppAdsCollection;
use App\Http\Resources\AppAds\AppAdsResource;
use App\Model\AppAds;
use App\Model\Category;
use App\Model\CompanyType;
use App\Model\Setting;
use App\Model\UserTypes;
use App\User;
use Illuminate\Http\Request;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

// firebase handler
// single or multi devices
// topic
// end of firebase handler

class AppAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {

        $filter = $this->getFilter($request);

        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $from = (isset($filter['simple']['from'])) ? $filter['simple']['from'] : NULL;
        $to = (isset($filter['simple']['to'])) ? $filter['simple']['to'] : NULL;
        $title = (isset($filter['simple']['title'])) ? $filter['simple']['title'] : NULL;

        unset($simple_filter['company_ids']);
        unset($simple_filter['from']);
        unset($simple_filter['to']);
        unset($simple_filter['user_types_id']);
        unset($simple_filter['title']);
        $approved_status = (isset($filter['simple']['approved_status'])) ? $filter['simple']['approved_status'] : NULL;//$simple_filter['approved_status'];

        if ($approved_status == 2) {
            unset($simple_filter['approved_status']);
        }

        $app_ads = AppAds::where($simple_filter);

        if ($approved_status == 2) {
            $app_ads = $app_ads->whereNull('approved_status');
        }

        if ($title != NULL) {
            $app_ads = $app_ads->where('title', 'LIKE', '%' . $title . '%');
        }

        if (!is_null($from) && !is_null($to)) {
            $from = date($from);
            $from = $from . " " . "00:00:00";
            $to = date($to);
            $to = $to . " " . "23:59:59";
            $app_ads = $app_ads->whereBetween('created_at', [$from, $to]);
        }

        $user_types_id = (isset($filter['simple']['user_types_id'])) ? $filter['simple']['user_types_id'] : [];
        if (!empty($user_types_id)) {
            $app_ads = $app_ads->whereIn('company_id', $user_types_id);

        }

        $company_ids = (isset($filter['simple']['company_ids'])) ? $filter['simple']['company_ids'] : [];
        if (!empty($company_ids)) {
            $app_ads = $app_ads->whereIn('company_id', $company_ids);

        }
        //return $app_ads;
        $app_ads = $app_ads->orderby('updated_at', 'desc');
        $app_ads = $app_ads->paginate(20);


        $data['paginate'] = $app_ads;
        //$collection = json_encode(AppAdsCollection::collection($app_ads));
        $data['app_ads'] = AppAdsCollection::collection($app_ads);//json_decode($collection);
        $data['user_types'] = UserTypes::orderby('id')->get();

        return view('admin.app_ads.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('title') && $request->get('title')) {
            $filter['simple']['title'] = $request->get('title');
        }


        if ($request->has('approved_status') && null !== $request->get('approved_status')) {
            $filter['simple']['approved_status'] = $request->get('approved_status');
        }

        if ($request->has('owner') && null !== $request->get('owner')) {
            $company_ids = User::where('full_name', 'LIKE', '%' . $request->get('owner') . '%')->pluck('id')->toArray();

            $filter['simple']['company_ids'] = $company_ids;
        }

        if ($request->has('user_types_id') && null !== $request->get('user_types_id')) {
            $company_ids = User::where('user_types_id', $request->get('user_types_id'))->pluck('id')->toArray();

            $filter['simple']['user_types_id'] = $company_ids;
        }


        if ($request->has('from') && $request->get('from') && $request->has('to') && $request->get('to')) {
            $filter['simple']['from'] = $request->get('from');
            $filter['simple']['to'] = $request->get('to');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        // return view('admin.app_ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminAdsRequest $request
     * @return void
     */
    public function store(AdminAdsRequest $request)
    {
        // $image = NULL;
        // if($request->hasfile('images'))
        //  {

        //     foreach($request->file('images') as $image)
        //     {
        //         $name=$image->getClientOriginalName();
        //         $image->move(public_path().'/images/', $name);
        //         $image = $name;
        //     }
        //  }
        //  $request = $request->except('images');
        //  $request['images'] = $image;
        //$admin_ads = AdminAds::create($request->all());
        //return redirect(route('app_ads.show',$admin_ads->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $app_ads = AppAds::find($id);
        if (!$app_ads) {
            return redirect(route('app_ads.index'));
        }
        $resource = json_encode(new AppAdsResource($app_ads));
        $data['app_ads'] = json_decode($resource);
        $data['user'] = User::where(['id' => $app_ads->company_id])->first();
        return view('admin.app_ads.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $app_ads = AppAds::find($id);
        if (!$app_ads) {
            return redirect(route('app_ads.index'));
        }
        $data['app_ads'] = $app_ads;
        $data['categories'] = Category::all();
        $data['user'] = User::where(['id' => $app_ads->company_id])->first();
        return view('admin.app_ads.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $app_ads = AppAds::find($id);
        if (!$app_ads) {
            return redirect(route('app_ads.index'));
        }
        $app_ads->update($request->all());


        // Send Push Notification
        $app_ads = AppAds::find($id);
        if ($app_ads->approved_status == 1) {
            $user_id = $app_ads->company_id;
            $user_details = User::find($user_id);
            $user_type = $user_details->user_types_id;
            $company_type = ($user_type == 3) ? $user_details->company_type_id : '0';
            $send_push = 0;

            if ($company_type == 0) {
                $setting = Setting::find(1);
                $no_of_notifications = $setting->number_of_notifications;
                $this_month = date("m");

                $user_ads_this_month = AppAds::where(['company_id' => $user_id,
                    'approved_status' => 1])
                    ->whereMonth('created_at', $this_month)
                    ->count();
                $send_push = ($user_ads_this_month < $no_of_notifications) ? 1 : 0;
            } else {
                $setting = CompanyType::find($company_type);
                $no_of_notifications = $setting->number_of_notifications;
                $this_month = date("m");

                $user_ads_this_month = AppAds::where(['company_id' => $user_id,
                    'approved_status' => 1])
                    ->whereMonth('created_at', $this_month)
                    ->count();
                $send_push = ($user_ads_this_month < $no_of_notifications) ? 1 : 0;
            }
            if ($send_push == 1) {
                $app_ads = AppAds::find($id);
                //$item_details = new AppAdsResource($app_ads);

                $send_data = array(
                    'type' => 'new_ads',
                    'id' => $id,
                    'title' => $app_ads->title,
                );

                $notification_center = new NotificationsCenter();

                $images_array = json_decode($app_ads->images);
                $image_url = $images_array[0];

                $notification_center->createNotification($send_data['type'], $send_data['title'], ""
                    , $image_url, $send_data['id']);


                $server_key = env('FCM_SERVER_KEY', '');

                $client = new Client();
                $client->setApiKey($server_key);
                $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

                $message = new Message();
                $message->setPriority('high');

                //$message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
                $message->addRecipient(new Topic('AppUsers'));

                $message
                    ->setNotification(new Notification('Check New Ads', $send_data['title']))
                    ->setData($send_data);

                $response = $client->send($message);
            }
        } else {
            // Remove From notification list
            $notification_center = new NotificationsCenter();
            $notification_center->removeFromList("new_ads", $id);
        }
        // End Push Notification

        return redirect(route('app_ads.show', $app_ads->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $admin_ads = AppAds::find($id);
        if (!$admin_ads) {
            return redirect(route('app_ads.index'));
        }

        if ($request->approval == 'delete') {
            $admin_ads->delete();
            NotificationsCenter::removeFromList("new_ads", $id);
            return redirect(route('app_ads.index'));
        } elseif ($request->approval == 'approval') {
            $admin_ads->update(['approved_status' => 1]);

            // Send Push Notification
            $app_ads = AppAds::find($id);
            if ($app_ads->approved_status == 1) {
                $user_id = $app_ads->company_id;
                $user_details = User::find($user_id);
                $user_type = $user_details->user_types_id;
                $company_type = ($user_type == 3) ? $user_details->company_type_id : '0';
                $send_push = 0;

                if ($company_type == 0) {
                    $setting = Setting::find(1);
                    $no_of_notifications = $setting->number_of_notifications;
                    $this_month = date("m");

                    $user_ads_this_month = AppAds::where(['company_id' => $user_id,
                        'approved_status' => 1])
                        ->whereMonth('created_at', $this_month)
                        ->count();
                    $send_push = ($user_ads_this_month < $no_of_notifications) ? 1 : 0;
                } else {
                    $setting = CompanyType::find($company_type);
                    $no_of_notifications = $setting->number_of_notifications;
                    $this_month = date("m");

                    $user_ads_this_month = AppAds::where(['company_id' => $user_id,
                        'approved_status' => 1])
                        ->whereMonth('created_at', $this_month)
                        ->count();
                    $send_push = ($user_ads_this_month < $no_of_notifications) ? 1 : 0;
                }
                if ($send_push == 1) {
                    $app_ads = AppAds::find($id);
                    //$item_details = new AppAdsResource($app_ads);

                    $send_data = array(
                        'type' => 'new_ads',
                        'id' => $id,
                        'title' => $app_ads->title,
                    );

                    $notification_center = new NotificationsCenter();
                    $images_array = json_decode($app_ads->images);
                    $image_url = $images_array[0];
                    $notification_center->createNotification($send_data['type'], $send_data['title'], ""
                        , $image_url, $send_data['id']);

                    $server_key = env('FCM_SERVER_KEY', '');

                    $client = new Client();
                    $client->setApiKey($server_key);
                    $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

                    $message = new Message();
                    $message->setPriority('high');

                    //$message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
                    $message->addRecipient(new Topic('AppUsers'));

                    $message
                        ->setNotification(new Notification('Check New Ads', $send_data['title']))
                        ->setData($send_data);

                    $response = $client->send($message);
                }
            }
            // End Push Notification

            return redirect(route('app_ads.index'));
        } else {
            return redirect(route('app_ads.index'));
        }
    }


}
