<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\Contact\ContactCollection;
use App\Http\Resources\Contact\ContactResource;
use App\Model\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContactsController extends Controller
{

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    /**
     * list all Contacts
     * @return view to display Contacts list
     */
    public function index()
    {
        $data = [];
        $contacts = Contact::paginate(20);
        $data['paginate'] = $contacts;
        $collection = json_encode(ContactCollection::collection($contacts));
        $data['contacts'] = json_decode($collection);
        return view('admin.contacts.index')->with($data);
    }

    /**
     * display contact data
     * @param $id
     * @return view to display contact
     */
    public function show($id)
    {
        $data = [];
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect(route('contacts.index'));
        }
        $resource = json_encode(new ContactResource($contact));
        $data['contact'] = json_decode($resource);
        return view('admin.contacts.show')->with($data);
    }

    /**
     * display create form
     * @return create contact form
     */
    public function create()
    {
        $data = [];
        $data['types'] = ['address', 'mobile', 'phone', 'email', 'social'];
        return view('admin.contacts.create')->with($data);
    }

    /**
     * store contact
     * @param Request $request
     * @return show contact
     */
    public function store(Request $request)
    {
        $contact = Contact::create($request->all());
        return redirect(route('contacts.index'));
    }

    /**
     * display edit form
     * @param $id
     * @return edit form
     */
    public function edit($id)
    {
        $data = [];
        $data['types'] = ['address', 'mobile', 'phone', 'email', 'social'];
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect(route('contacts.index'));
        }
        $data['contact'] = $contact;
        return view('admin.contacts.edit')->with($data);
    }

    /**
     * update contact data
     * @param Request $request
     * @param $id
     * @return redirect to contact show view
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect(route('contacts.index'));
        }
        $contact->update($request->all());
        return redirect(route('contacts.show', $contact->id));
    }

    /**
     * delete contact
     * @param $id
     * @return redirect to contact index
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect(route('contacts.index'));
        }
        $contact->delete();
        return redirect(route('contacts.index'));
    }
}
