<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddCategoryRequest;
use App\Http\Requests\Category\EditCategoryRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Model\AppAds;
use App\Model\Category;
use App\Model\CategoryGroup;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = Category::where($simple_filter);

        $category_name = (isset($filter['simple']['category_name_en'])) ? $filter['simple']['category_name_en'] : NULL;

        if (!is_null($category_name)) {
            $categories = Category::where('category_name_en', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['category_name_ar'])) ? $filter['simple']['category_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = Category::where('category_name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CategoryCollection::collection($categories));
        $data['categories'] = json_decode($collection);
        $data['groups'] = CategoryGroup::where('id', '<>', 0)->get();
        return view('admin.category.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('category_type') && $request->get('category_type')) {
            $filter['simple']['category_type'] = $request->get('category_type');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['category_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['groups'] = CategoryGroup::where('id', '<>', 0)->get();
        return view('admin.category.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryRequest $request)
    {
        $category = $request->all();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $category['icon'] = "categories/" . $image_name;
            }

        }

        $categories = Category::create($category);
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }
        $resource = json_encode(new CategoryResource($categories));
        $data['categories'] = json_decode($resource);
        $data['groups'] = CategoryGroup::find(['id' => $categories->category_group_id])->first();
        return view('admin.category.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = Category::find($id);
        if (!$catrgories) {
            return redirect(route('categories.index'));
        }
        $data['categories'] = $catrgories;
        $data['groups'] = CategoryGroup::where('id', '<>', 0)->get();
        $data["count_of_ads"] = AppAds::where("category_id", $id)->count();
        return view('admin.category.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCategoryRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }

        $category = $request->all();

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $category['icon'] = "categories/" . $image_name;
            }

        } else {
            unset($category['icon']);
        }
        $categories->update($category);
        return redirect(route('categories.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }
        $categories->delete();
        return redirect(route('categories.index'));
    }


}
