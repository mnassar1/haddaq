<?php

namespace App\Http\Controllers\Web;

use App\Helpers\FilterTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Deal\DealCollection;
use App\Http\Resources\Deal\DealImagesResource;
use App\Http\Resources\Deal\DealNotesResource;
use App\Http\Resources\Request\DealResource;
use App\Http\Resources\Shipment\ShipmentResource;
use App\Http\Resources\Trip\TripResource;
use App\Model\City;
use App\Model\Country;
use App\Model\Deal;
use App\Model\DealImages;
use App\Model\DealNotes;
use App\Model\Requests;
use Illuminate\Http\Request;

class DealController extends Controller
{
    use FilterTrait;

    public function __construct()
    {
        $this->middleware('is.admin')->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $countries = Country::all();
        $data['countries'] = $countries;

        // filter start here
        $filter = $this->getFilter($request);
        $simple_filter = isset($filter['simple']) ? $filter['simple'] : [];
        $deals = Deal::where($simple_filter);

        // like filter
        $like_filter = isset($filter['like']) ? $filter['like'] : [];
        if (isset($like_filter['created_at'])) {
            $deals = $deals->where('created_at', 'like', $like_filter['created_at'] . '%');
        }

        //country_from && country_to
        if ($request->has('country_from') || $request->has('country_to')) {
            // country_from
            if ($request->get('country_from') && $request->get('country_to')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('country_from', $request->get('country_from'))->where('country_to', $request->get('country_to'));
                });
            } elseif ($request->get('country_from')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('country_from', $request->get('country_from'));
                });
            } elseif ($request->get('country_to')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('country_to', $request->get('country_to'));
                });
            }
        }

        //city_from && city_to
        if ($request->has('city_from') || $request->has('city_to')) {
            if ($request->get('city_from') && $request->get('city_to')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('city_from', $request->get('city_from'))->where('city_to', $request->get('city_to'));
                });
            } elseif ($request->get('city_from')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('city_from', $request->get('city_from'));
                });
            } elseif ($request->get('city_to')) {
                $deals = $deals->whereHas('trip', function ($query) use ($request) {
                    $query->where('city_to', $request->get('city_to'));
                });
            }
        }

        // pickup time range
        if ($request->has('pickup_time_from') || $request->has('pickup_time_to')) {
            if ($request->get('pickup_time_from') && $request->get('pickup_time_to')) {
                $pickup_time_filter = $this->dateRangeFilter('pickup_time', $request->get('pickup_time_from'), $request->get('pickup_time_to'));
                $from = $pickup_time_filter['pickup_time']['from'] . ' 00:00:00';
                $to = $pickup_time_filter['pickup_time']['to'] . ' 23:59:59';
                $deals = $deals->whereBetween('pickup_time', [$from, $to]);
            } elseif ($request->get('pickup_time_from')) {
                $deals = $deals->where('pickup_time', '>=', $request->get('pickup_time_from') . ' 00:00:00');
            } elseif ($request->get('pickup_time_to')) {
                $deals = $deals->where('pickup_time', '<=', $request->get('pickup_time_to') . ' 23:59:59');
            }
        }

        // delivery_time
        if ($request->has('delivery_time_from') || $request->has('delivery_time_to')) {
            if ($request->get('delivery_time_from') && $request->get('delivery_time_to')) {
                $delivery_time_filter = $this->dateRangeFilter('delivery_time', $request->get('delivery_time_from'), $request->get('delivery_time_to'));
                $from = $delivery_time_filter['delivery_time']['from'] . ' 00:00:00';
                $to = $delivery_time_filter['delivery_time']['to'] . ' 23:59:59';
                $deals = $deals->whereBetween('delivery_time', [$from, $to]);
            } elseif ($request->get('delivery_time_from')) {
                $deals = $deals->where('delivery_time', '>=', $request->get('delivery_time_from'));
            } elseif ($request->get('delivery_time_to')) {
                $deals = $deals->where('delivery_time', '<=', $request->get('delivery_time_to'));
            }
        }

        // shipment's weight range
        if ($request->has('weight_from') || $request->has('weight_to')) {
            if ($request->get('weight_from') && $request->get('weight_to')) {
                $deals = $deals->whereHas('shipment', function ($query) use ($request) {
                    $query->whereBetween('weight', [$request->get('weight_from'), $request->get('weight_to')]);
                });
            } elseif ($request->get('weight_from')) {
                $deals = $deals->whereHas('shipment', function ($query) use ($request) {
                    $query->where('weight', '>=', $request->get('weight_from'));
                });
            } elseif ($request->get('weight_to')) {
                $deals = $deals->whereHas('shipment', function ($query) use ($request) {
                    $query->where('weight', '<=', $request->get('weight_to'));
                });
            }
        }

        $deals = $deals->latest()->paginate(10);
        $data['paginate'] = $deals;
        $collection = json_encode(DealCollection::collection($deals));
        $data['deals'] = json_decode($collection);
        if ($request->has('country_from') && $request->get('country_from')) {
            $cities_from = City::where('country_id', $request->get('country_from'))->get();
            $data['cities_from'] = $cities_from;
        }
        if ($request->has('country_to') && $request->get('country_to')) {
            $cities_to = City::where('country_id', $request->get('country_to'))->get();
            $data['cities_to'] = $cities_to;
        }
        return view('admin.deal.index')->with($data);
    }

    /**
     * get filter
     * @param Request $request
     * @return array
     */
    public function getFilter(Request $request)
    {
        $filter = [];

        // country_from
        if ($request->has('status') && $request->get('status')) {
            $filter['simple']['status'] = $request->get('status');
        }

        // created_at
        if ($request->has('created_at') && $request->get('created_at')) {
            $filter['like']['created_at'] = $request->get('created_at');
        }
        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $deal = Deal::find($id);
        if (!$deal) {
            return redirect()->route('deals.index');
        }
        $resource = json_encode(new DealResource($deal));
        $data['deal'] = json_decode($resource);
        $request = Requests::find($deal->request_id);
        if ($request) {
            $shipment = $request->shipment();
            if ($shipment) {
                $shipment_resource = json_encode(new ShipmentResource($shipment));
                $data['shipment'] = json_decode($shipment_resource);
            }
            $trip = $request->trip();
            if ($trip) {
                $trip_resource = json_encode(new TripResource($trip));
                $data['trip'] = json_decode($trip_resource);
            }
        }
        $notes = DealNotes::where('deal_id', $id)->get();
        if ($notes) {
            $notes_collection = json_encode(DealNotesResource::collection($notes));
            $data['notes'] = json_decode($notes_collection);
        }
        $images = DealImages::where('deal_id', $id)->get();
        if ($images) {
            $images_collection = json_encode(DealImagesResource::collection($images));
            $data['images'] = json_decode($images_collection);
        }
        return view('admin.deal.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * change deal status
     * @param Request $request
     * @param status
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_status(Request $request, $id)
    {
        $deal = Deal::find($id);
        if (!$deal) {
            return redirect(route('deals.index'));
        }

        if ($request->has('status') && $request->get('status') != "0") {
            $deal->status = $request->get('status');
            $deal->save();
        }
        return redirect(route('deals.show', $id));
    }
}
