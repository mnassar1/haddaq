<?php

namespace App\Http\Controllers\Web;

use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Shipment\ShipmentCollection;
use App\Http\Resources\Trip\TripCollection;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Model\Role;
use App\Model\Shipment;
use App\Model\Trip;
use App\Model\UserTypes;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use UserHelperTrait;

    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $data = [];


        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $users = User::where($simple_filter);
        $order_filter = (isset($filter['order'])) ? $filter['order'] : [];
        if (!empty($order_filter)) {
            $users = $users->orderByRaw($order_filter);
        }
        $users = $users->orderby('id', 'desc');
        $users = $users->paginate(20);
        $data['paginate'] = $users;
        $collection = json_encode(UserCollection::collection($users));
        $data['users'] = json_decode($collection);
        $data['user_types'] = UserTypes::orderby('id')->get();
        return view('admin.user.index')->with($data);
    }

    /**
     * get filter
     * @param Request $request
     * @return array
     */
    public function getFilter(Request $request)
    {
        $filter = [];

        // city_id
        if ($request->has('city_id') && !empty($request->get('city_id'))) {
            $filter['simple']['city_id'] = $request->get('city_id');
        }

        // register_type
        if ($request->has('user_types_id') && $request->get('user_types_id')) {
            $filter['simple']['user_types_id'] = $request->get('user_types_id');
        }

        // is_verified
        if ($request->has('is_verified') && null !== $request->get('is_verified')) {
            $filter['simple']['is_verified'] = $request->get('is_verified');
        }

        // is_admin
        if ($request->has('is_admin') && null !== $request->get('is_admin')) {
            $filter['simple']['is_admin'] = $request->get('is_admin');
        }

        // is_blocked
        if ($request->has('is_blocked') && null !== $request->get('is_blocked')) {
            $filter['simple']['is_blocked'] = $request->get('is_blocked');
        }

        // name
        if ($request->has('name') && null !== $request->get('name')) {
            $filter['simple']['full_name'] = $request->get('name');
        }

        // email
        if ($request->has('email') && null !== $request->get('email')) {
            $filter['simple']['email'] = $request->get('email');
        }

        // phone
        if ($request->has('mobile') && null !== $request->get('mobile')) {
            $filter['simple']['mobile'] = $request->get('mobile');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $inputs["is_admin"] = 1;
        $inputs["is_verified"] = 1;
        $inputs["is_admin"] = 1;
        $salt = "";
        $inputs["password"] = $this->generateHashedPassword($inputs["password"], $salt);
        try {
            $user = User::create($inputs);

            return redirect(url("admin/user/" . $user->id));
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['Admin Already Found']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Role::all();
        $data = [];
        $user = User::find($id);
        if (!$user) {
            return redirect(route('user.index'));
        }
        $data['userObject'] = $user;
        $register_enum = ['email' => 'Email', 'li' => 'LinkedIn', 'g+' => 'GMail', 'tw' => 'Twitter', 'fb' => 'Facebook'];
        $data['register_via'] = $register_enum;
        $resource = json_encode(new UserResource($user));
        $data['user'] = json_decode($resource);
        $data['city'] = \App\Model\City::find($user->city_id);
        $data['branches'] = \App\Model\CompanyBranch::where(['company_id' => $id])->get();
        $data['reminders'] = \App\Model\Reminders::where(['user_id' => $id])->get();
        $data['company_types'] = \App\Model\CompanyType::all();
        $data['roles'] = $roles;
        $data["user_role"] = json_decode($user->roles);
        return view('admin.user.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function addRole(Request $request, $id)
    {
        $roles = $request->input('roles');
        $user = User::find($id);
        $user->roles = json_encode($roles);
        $user->save();
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * List user's trips
     * @param id
     * @return array of trips
     */
    public function trips($id)
    {
        $data = [];
        $data['show_filter'] = false;
        $trips = Trip::where(['user_id' => $id])->paginate(10);
        $data['paginate'] = $trips;
        $collection = json_encode(TripCollection::collection($trips));
        $data['trips'] = json_decode($collection);
        return view('admin.trip.index')->with($data);
    }

    /**
     * List user's shipments
     * @param id
     * @return array of shipments
     */
    public function shipments($id)
    {
        $data = [];
        $data['show_filter'] = false;
        $shipments = Shipment::where(['user_id' => $id])->paginate(10);
        $data['paginate'] = $shipments;
        $collection = json_encode(ShipmentCollection::collection($shipments));
        $data['shipments'] = json_decode($collection);
        return view('admin.shipment.index')->with($data);
    }
}
