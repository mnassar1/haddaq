<?php

namespace App\Http\Controllers\Web;

use App\Helpers\PointLocation;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\EditCategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\CoveredArea\CoveredAreaCollection;
use App\Model\Category;
use App\Model\CategoryGroup;
use App\Model\CoveredArea;
use Illuminate\Http\Request;


class CoveredAreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin')->except('store');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = CoveredArea::where($simple_filter);

        // $category_name = (isset($filter['simple']['category_name'])) ? $filter['simple']['category_name'] : NULL;

        // if(!is_null($category_name)){
        // $categories = Category::where('category_name', 'LIKE', '%'.$category_name.'%');
        // }

        // $category_name_ar = (isset($filter['simple']['category_name_ar'])) ? $filter['simple']['category_name_ar'] : NULL;

        // if(!is_null($category_name_ar)){
        // $categories = Category::where('category_name_ar', 'LIKE', '%'.$category_name_ar.'%');
        // }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CoveredAreaCollection::collection($categories));
        $data['categories'] = json_decode($collection);

        return view('admin.covered_areas.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('category_type') && $request->get('category_type')) {
            $filter['simple']['category_type'] = $request->get('category_type');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['category_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countries'] = [];
        return view('admin.covered_areas.create')->with($data);
    }

    public function create_covered_area()
    {
        return view('admin.covered_areas.map');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country_id = $request->country_id;
        $area_name = $request->area_name;
        $area = json_encode($request->Coords);
        //return $area;
        $covered_area = array(
            'country_id' => $country_id,
            'area_name' => $area_name,
            'area' => $area
        );
        $covered_area = CoveredArea::create($covered_area);
        return ResponseHandler::json($covered_area);
    }

//     public function check_location(Request $request){
//         $country_id = $request->country_id;
//         $area_name = $request->area_name;


//             $location = "30.1530526,28.0396503";
//             $pieces = $pieces = explode(",", $location);
//              $sos_lat = $pieces[0];
//              $sos_long = $pieces[1];
//             $points = array("$sos_lat $sos_long");
//             $all_polygon = CoveredArea::all();
//             $vertices_x = [];
//             $vertices_y = [];
//             if(!empty($all_polygon)){
//                 $i = 0;
//                 foreach($all_polygon as $polygon){
//                     $polygon = json_decode($polygon->area);

//                     foreach($polygon as $poly){

//                         $vertices_x[$i][] =  (float)$poly->lng;
//                          $vertices_y[$i][] =  (float)$poly->lat;
//                     }
//                      $i++;
//                 }

//             }
//             //return $vertices_x;
//          // $vertices_x = array(37.628134, 37.629867, 37.62324, 37.622424);    // x-coordinates of the vertices of the polygon
//         //$vertices_y = array(-77.458334,-77.449021,-77.445416,-77.457819); // y-coordinates of the vertices of the polygon
//         $points_polygon = count($vertices_x[0]) - 1;  // number vertices - zero-based array
//         //return $points_polygon;
//         $vertices_x = $vertices_x[0];
//         //return $vertices_x;
//         //return vertices_y;
//         $vertices_y = $vertices_y[0];
//         //return $vertices_y;
//         $longitude_x = $sos_long;  // x-coordinate of the point to test
//         $latitude_y = $sos_lat;    // y-coordinate of the point to test
//         //return $this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y);
//         if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
//         echo "Is in polygon!";
//         }
//         else echo "Is not in polygon";


//     }


//     public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
// {
//    // return $latitude_y;
//    $i = $j = $c = 0;
//   for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
//     if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
//      ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
//        $c = !$c;
//        break;
//   }
//   return $c;
// }

    public function check_location(Request $request)
    {
        $country_id = $request->country_id;
        $area_name = $request->area_name;

        $pointLocation = new PointLocation();
        $location = "29.189477,49.630511";
        $pieces = $pieces = explode(",", $location);
        $sos_lat = $pieces[0];
        $sos_long = $pieces[1];
        $points = array("$sos_lat $sos_long");
        $all_polygon = CoveredArea::all();
        $polygon_array = [];
        if (!empty($all_polygon)) {
            $i = 0;

            foreach ($all_polygon as $polygon) {
                $polygon = json_decode($polygon->area);


                $count = count($polygon) - 1;
                $first = true;
                $x = 0;
                foreach ($polygon as $poly) {
                    if ($first == true) {
                        $polygon_last = "$poly->lat $poly->lng";
                    }
                    $first = false;
                    $polygon_array[$i][] = "$poly->lat $poly->lng";

                    if ($x == $count) {
                        $polygon_array[$i][] = $polygon_last;
                    }
                    $x++;
                }
                $i++;
            }

        }
        if (!empty($polygon_array)) {
            //return $points;
            //return $polygon_array[0];
            // exit;
            //$points = array("50 70");;
            $polygon = $polygon_array[0];//array("-50 30","50 70","100 50","80 10","110 -10","110 -30","-20 -50","-30 -40","10 -10","-10 10","-30 -20","-50 30");
            // The last point's coordinates must be the same as the first one's, to "close the loop"
            foreach ($points as $key => $point) {
                echo "point " . ($key + 1) . " ($point): " . $pointLocation->pointInPolygon($point, $polygon) . "<br>";
            }
        } else {
            return 1;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }
        $resource = json_encode(new CategoryResource($categories));
        $data['categories'] = json_decode($resource);
        $data['groups'] = CategoryGroup::find(['id' => $categories->category_group_id])->first();
        return view('admin.category.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = Category::find($id);
        if (!$catrgories) {
            return redirect(route('categories.index'));
        }
        $data['categories'] = $catrgories;
        $data['groups'] = CategoryGroup::where('id', '<>', 0)->get();
        return view('admin.category.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCategoryRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }

        $category = $request->all();

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $category['icon'] = "categories/" . $image_name;
            }

        } else {
            unset($category['icon']);
        }

        $categories->update($category);
        return redirect(route('categories.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = CoveredArea::find($id);
        if (!$categories) {
            return redirect(route('covered_areas.index'));
        }
        $categories->delete();
        return redirect(route('covered_areas.index'));
    }


}
