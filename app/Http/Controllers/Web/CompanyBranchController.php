<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyBranch\AddCompanyBranchRequest;
use App\Http\Requests\CompanyBranch\EditCompanyBranchRequest;
use App\Http\Resources\CompanyBranch\CompanyBranchCollection;
use App\Http\Resources\CompanyBranch\CompanyBranchResource;
use App\Model\Category;
use App\Model\CompanyBranch;
use Illuminate\Http\Request;
use session;

class CompanyBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = CompanyBranch::where($simple_filter);
        $categories = $categories->where(['company_id' => session('user_id')]);

        $category_name = (isset($filter['simple']['category_name_en'])) ? $filter['simple']['category_name_en'] : NULL;

        if (!is_null($category_name)) {
            $categories = CompanyBranch::where('title', 'LIKE', '%' . $category_name . '%')->where(['company_id' => session('user_id')]);
        }

        $category_name_ar = (isset($filter['simple']['category_name_ar'])) ? $filter['simple']['category_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = CompanyBranch::where('address', 'LIKE', '%' . $category_name_ar . '%')->where(['company_id' => session('user_id')]);
        }

        if (!is_null($category_name) && !is_null($category_name_ar)) {
            $categories = CompanyBranch::where('address', 'LIKE', '%' . $category_name_ar . '%')->where('title', 'LIKE', '%' . $category_name . '%')->where(['company_id' => session('user_id')]);
        }
        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(CompanyBranchCollection::collection($categories));
        $data['categories'] = json_decode($collection);
        return view('admin.company_branches.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('category_type') && $request->get('category_type')) {
            $filter['simple']['category_type'] = $request->get('category_type');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['category_name'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company_branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCompanyBranchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCompanyBranchRequest $request)
    {
        $category = $request->all();
        $category['company_id'] = session('user_id');
        $categories = CompanyBranch::create($category);
        return redirect(route('company_branches.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = CompanyBranch::find($id);
        if (!$categories) {
            return redirect(route('company_branches.index'));
        }
        if ($categories->company_id != session('user_id')) {
            return redirect(route('company_branches.index'));
        }


        $resource = json_encode(new CompanyBranchResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.company_branches.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $categories = CompanyBranch::find($id);
        if (!$categories) {
            return redirect(route('company_branches.index'));
        }

        if ($categories->company_id != session('user_id')) {
            return redirect(route('company_branches.index'));
        }

        $data['categories'] = $categories;
        return view('admin.company_branches.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCompanyBranchRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyBranchRequest $request, $id)
    {
        $categories = CompanyBranch::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }

        if ($categories->company_id != session('user_id')) {
            return redirect(route('company_branches.index'));
        }

        $category = $request->all();

        $categories->update($category);
        return redirect(route('company_branches.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return redirect(route('categories.index'));
        }
        $categories->delete();
        return redirect(route('categories.index'));
    }


}
