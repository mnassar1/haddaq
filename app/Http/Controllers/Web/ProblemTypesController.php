<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProblemTypes\AddProblemTypeRequest;
use App\Http\Requests\ProblemTypes\EditProblemTypeRequest;
use App\Http\Resources\ProblemTypes\ProblemTypeCollection;
use App\Http\Resources\ProblemTypes\ProblemTypeResource;
use App\Model\ProblemType;
use Illuminate\Http\Request;

class ProblemTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $categories = ProblemType::where($simple_filter);

        $category_name = (isset($filter['simple']['problem_type_en'])) ? $filter['simple']['problem_type_en'] : NULL;

        if (!is_null($category_name)) {
            $categories = ProblemType::where('problem_type_en', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['problem_type_ar'])) ? $filter['simple']['problem_type_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $categories = ProblemType::where('problem_type_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(ProblemTypeCollection::collection($categories));
        $data['categories'] = json_decode($collection);

        return view('admin.problem_types.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['problem_type_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['problem_type_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.problem_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddProblemTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProblemTypeRequest $request)
    {
        $category = $request->all();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/problem_types/'), $image_name)) {
                $category['icon'] = "problem_types/" . $image_name;
            }

        }

        $categories = ProblemType::create($category);
        return redirect(route('problem_types.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = ProblemType::find($id);
        if (!$categories) {
            return redirect(route('problem_types.index'));
        }
        $resource = json_encode(new ProblemTypeResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.problem_types.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = ProblemType::find($id);
        if (!$catrgories) {
            return redirect(route('problem_types.index'));
        }
        $data['categories'] = $catrgories;
        return view('admin.problem_types.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditProblemTypeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProblemTypeRequest $request, $id)
    {

        $categories = ProblemType::find($id);
        if (!$categories) {
            return redirect(route('problem_types.index'));
        }

        $category = $request->all();

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/problem_types/'), $image_name)) {
                $category['icon'] = "problem_types/" . $image_name;
            }

        } else {
            unset($category['icon']);
        }

        $categories->update($category);
        return redirect(route('problem_types.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = ProblemType::find($id);
        if (!$categories) {
            return redirect(route('problem_types.index'));
        }
        $delete_related_sos = \App\Model\SOSRequest::where('sos_problem_type_id', $id)->delete();
        $categories->delete();
        return redirect(route('problem_types.index'));
    }


}
