<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyAds\CompanyAdsRequest;
use App\Http\Requests\CompanyAds\EditCompanyAdsRequest;
use App\Http\Resources\CompanyAds\CompanyAdsCollection;
use App\Http\Resources\CompanyAds\CompanyAdsResource;
use App\Model\AppAds;
use App\Model\Category;
use Illuminate\Http\Request;
use session;

class CompanyAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        //$categories= $categories->orderby('id','desc');
        $app_ads = AppAds::where(['company_id' => session('user_id')])->orderby('updated_at', 'desc');

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $from = (isset($filter['simple']['from'])) ? $filter['simple']['from'] : NULL;
        $to = (isset($filter['simple']['to'])) ? $filter['simple']['to'] : NULL;
        $title = (isset($filter['simple']['title'])) ? $filter['simple']['title'] : NULL;


        unset($simple_filter['from']);
        unset($simple_filter['to']);
        unset($simple_filter['title']);

        $app_ads = $app_ads->where($simple_filter);

        if ($title != NULL) {
            $app_ads = $app_ads->where('title', 'LIKE', '%' . $title . '%');
        }

        if (!is_null($from) && !is_null($to)) {
            $from = date($from);
            $from = $from . " " . "00:00:00";
            $to = date($to);
            $to = $to . " " . "23:59:59";
            $app_ads = $app_ads->whereBetween('created_at', [$from, $to]);
        }

        $app_ads = $app_ads->paginate(20);

        $data['paginate'] = $app_ads;
        $collection = json_encode(CompanyAdsCollection::collection($app_ads));
        $data['app_ads'] = json_decode($collection);
        return view('admin.company_ads.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('title') && $request->get('title')) {
            $filter['simple']['title'] = $request->get('title');
        }


        if ($request->has('approved_status') && null !== $request->get('approved_status')) {
            $filter['simple']['approved_status'] = $request->get('approved_status');
        }


        if ($request->has('from') && $request->get('from') && $request->has('to') && $request->get('to')) {
            $filter['simple']['from'] = $request->get('from');
            $filter['simple']['to'] = $request->get('to');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::all();
        return view('admin.company_ads.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyAdsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyAdsRequest $request)
    {


        $ads = $request->all();
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                    $images[] = "ads/" . $image_name;
                }

            }
        }

        $ads['images'] = json_encode($images);

        $ads['company_id'] = session('user_id');
        $ads['views_count'] = 0;
        $admin_ads = AppAds::create($ads);
        //return($ads);
        return redirect(route('company_ads.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $app_ads = AppAds::where(['id' => $id, 'company_id' => session('user_id')])->first();
        if (!$app_ads) {
            return redirect(route('company_ads.index'));
        }
        //return new CompanyAdsResource($app_ads);
        $resource = json_encode(new CompanyAdsResource($app_ads));
        $data['company_ads'] = json_decode($resource);
        return view('admin.company_ads.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $company_ads = AppAds::where(['id' => $id, 'company_id' => session('user_id')])->first();
        if (!$company_ads) {
            return redirect(route('company_ads.index'));
        }

        $data['company_ads'] = $company_ads;
        $data['categories'] = Category::all();
        return view('admin.company_ads.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditCompanyAdsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyAdsRequest $request, $id)
    {
        $company_ads = AppAds::find($id);

        if (!$company_ads) {
            return redirect(route('company_ads.index'));
        }

        $ads = $request->all();
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                    $images[] = "ads/" . $image_name;
                }

            }
            $ads['images'] = json_encode($images);
        } else {
            unset($ads['images']);
        }
        $ads['approved_status'] = NULL;
        $company_ads->update($ads);
        return redirect(route('company_ads.show', $company_ads->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_ads = AppAds::find($id);
        if (!$admin_ads) {
            return redirect(route('company_ads.index'));
        }
        $admin_ads->delete();
        return redirect(route('company_ads.index'));
    }


}
