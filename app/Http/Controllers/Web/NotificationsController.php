<?php

namespace App\Http\Controllers\Web;

use App\Helpers\NotificationsCenter;
use App\Http\Controllers\Controller;
use App\Http\Requests\SOSRequest\PushRequest;
use App\Http\Resources\SOSRequest\SOSRequestResource;
use App\Model\SOSRequest;
use Illuminate\Http\Request;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

// use App\Http\Requests\SOSRequest\AddProblemTypeRequest;
// use App\Http\Requests\SOSRequest\EditProblemTypeRequest;

// firebase handler
// single or multi devices
// topic
// end of firebase handler

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        //     $filter = $this->getFilter($request);

        //     $data = [];
        //     $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        //     $sender_name = (isset($filter['simple']['sender_name'])) ? $filter['simple']['sender_name'] : NULL;
        //      unset($simple_filter['sender_name']);
        //      $categories = SOSRequest::where($simple_filter);


        //      if($sender_name != null && $sender_name == 'GUEST' ){
        //         $categories = $categories->WhereNull('user_id');
        //      }
        //      if($sender_name != null && $sender_name != 'GUEST' ){
        //         $sender_ids = \App\User::where('full_name', 'LIKE', '%'.$sender_name.'%')->pluck('id')->toArray();
        //         $categories = $categories->whereIn('user_id', $sender_ids);

        //      }

        //     $categories= $categories->orderby('id','desc');
        //     $categories = $categories->paginate(20);
        //     $data['paginate'] = $categories;
        //     $collection = json_encode(SOSRequestCollection::collection($categories));
        //    // return $collection;
        //     $data['categories'] = json_decode($collection);
        //     $data['problem_types'] = ProblemType::all();

        //     return view('admin.sos_requests.index')->with($data);
        return view('admin.notifications.create');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PushRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PushRequest $request)
    {
        $push = $request->all();

        $send_data = array(
            'type' => 'admin_push',
            'title' => ($push['title']) ? $push['title'] : "",
            'description' => (strip_tags($push['details'])) ? strip_tags($push['details']) : "",

        );
        $notification_center = new NotificationsCenter();

        $notification_center->createNotification($send_data['type'], $send_data['title'],
            $send_data['description'], "", "");


        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message_topic = config('database.default') . "-RegularUsersAndFreeSaviors";
        $message->addRecipient(new Topic($message_topic));

        $notification = new Notification($send_data['title'], $send_data['description']);
        $notification->setTitle($send_data['title'])
            ->setSound('haddaq.wav')
            ->setBody($send_data['description']);

        $message
            ->setNotification($notification)
            ->setData($send_data);

        $response = $client->send($message);

        return redirect(route('notifications.create'));
    }


    public function destroy($id)
    {
        $sos_request = SOSRequest::find($id);
        if (!$sos_request) {
            return redirect(route('sos_requests.index'));
        }
        if ($sos_request->sos_request_status == 'Closed') {
            return redirect(route('sos_requests.index')); //return ResponseHandler::json('Request Already Closed , Thanks');
        }
        $update_status = array('sos_request_status' => 'Closed', 'closed_by' => 'Admin');
        $sos_request = $sos_request->update($update_status);

        $sos_request = SOSRequest::find($id);
        $data = new SOSRequestResource($sos_request);

        $send_data = array(
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'location' => $data->location,
            'device_udid' => $data->device_udid,
            'push_token' => $data->push_token,
            'sos_problem_type_id' => $data->sos_problem_type_id,
            'sos_problem_type_details' => $data->sos_problem_type_details,
            'description' => ($data->description) ? $data->description : "",
            'sos_request_status' => $data->sos_request_status,
        );
        //var_dump ($send_data);
        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
        $message
            ->setNotification(new Notification('HaddaQ', 'Close SOS Request'))
            ->setData($send_data);

        $response = $client->send($message);

        // var_dump ($response);
        return redirect(route('sos_requests.index'));
    }


}
