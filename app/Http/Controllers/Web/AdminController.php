<?php

namespace App\Http\Controllers\Web;

use App\Helpers\FilterTrait;
use App\Helpers\Helper;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditProfileRequest;
use App\Http\Resources\Shipment\ShipmentCollection;
use App\Http\Resources\Shipment\ShipmentResource;
use App\Http\Resources\Trip\TripCollection;
use App\Http\Resources\Trip\TripResource;
use App\Http\Resources\User\UserResource;
use App\Model\AppAds;
use App\Model\ContactUs;
use App\Model\Country;
use App\Model\FixingShops;
use App\Model\Shipment;
use App\Model\SOSRequest;
use App\Model\Trip;
use App\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class AdminController extends Controller
{
    use UserHelperTrait;
    use FilterTrait;

    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin')->only(['index', 'show']);
    }

    /**
     * load logged in admin dashboard view
     * @return view
     */
    public function index()
    {
        $data = NULL;
        $users = User::where('user_types_id', 1)->count();
        $admins = User::where("is_admin", 1)->count();
        $Saviors = User::where('user_types_id', 2)->count();
        $companies = User::where('user_types_id', 3)->count();
        $ContactUs = ContactUs::count();
        $AppAds = AppAds::whereNull('approved_status')->count();
        $isMainDomian = (new Helper())->isMainDomain();
        $data['fixing_shops'] = FixingShops::count();
        $user_type_id = (session('user_type_id')) ? session('user_type_id') : NULL;
        //echo $user_type_id;
        $user_id = session('user_id');

        if ($user_type_id == 3) {
            $data['branches'] = \App\Model\CompanyBranch::where(['company_id' => $user_id])->count();
            $data['approved_ads'] = AppAds::where(['company_id' => $user_id, 'approved_status' => '1'])->count();
            $data['blocked_ads'] = AppAds::where(['company_id' => $user_id, 'approved_status' => '0'])
                ->whereNotNull('block_reason')->count();
            //$data['waiting_ads'] = AppAds::where(['company_id'=>$user_id,'approved_status'=>'0'])
            //->whereNull('block_reason')->count();
        }

        $SOSRequest = SOSRequest::where('sos_request_status', '<>', 'Closed')->count();
        return view('admin.dashboard')->with($data)->with(['admins' => $admins, 'users' => $users, 'companies' => $companies,
            'ContactUs' => $ContactUs, 'SOSRequest' => $SOSRequest, 'AppAds' => $AppAds, 'Saviors' => $Saviors, 'isMainDomian'=>$isMainDomian]);
    }

    public function profile()
    {
        $data = [];
        $id = session('user_id');
        $user = User::find($id);
        if (!$user) {
            return redirect(route('company_profile.index'));
        }


        $resource = json_encode(new UserResource($user));
        $data['user'] = json_decode($resource);
        $data['city'] = \App\Model\City::find($user->city_id);
        $data['branches'] = \App\Model\CompanyBranch::where(['company_id' => $id])->get();
        $data['company_types'] = \App\Model\CompanyType::all();
        return view('admin.show')->with($data);
    }

    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect(route('profile'));
        }
        if ($id != session('user_id')) {
            return redirect(route('profile'));
        }
        $data['user'] = $user;
        $data['cities'] = \App\Model\City::all();
        $data['company_types'] = \App\Model\CompanyType::all();
        return view('admin.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditProfileRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProfileRequest $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect(route('profile'));
        }
        if ($id != session('user_id')) {
            return redirect(route('profile'));
        }

        $user_data = $request->all();

        if ($request->hasFile('profile_image_url')) {
            $file = $request->file('profile_image_url');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/profile_images/'), $image_name)) {
                $user_data['profile_image_url'] = "profile_images/" . $image_name;
            }

        } else {
            unset($user_data['profile_image_url']);
        }


        if (isset($request->password) && $request->password != "") {
            $user_data['password'] = sha1(md5($request->password));
        } else {
            unset($user_data['password']);
        }

        $user->update($user_data);
        return redirect(route('profile'));
    }

    public function update_language(Request $request, $id)
    {
        $user_id = session('user_id');
        $user = User::find($user_id);
        if (!$user) {
            return redirect(route('profile'));
        }

        Session::put('language', $id);
        $user_data['language'] = $id;
        $user->update($user_data);
        return redirect(route('dashboard'));
    }

    public function update_language_dashboard(Request $request, $id)
    {
        Session::put('language', $id);
        app()->setLocale($id);
        return redirect()->back();
    }


    /**
     * load admin's login view
     * @return
     */
    public function login()
    {
        return view('admin.login')->withToken(str_random(60));
    }

    /**
     * handle login
     * @param Request $request
     * @return redirect
     */
    public function doLogin(Request $request)
    {
        $admin = User::where(['email' => $request->email])->first();
        if (!$admin) {
            $errors['email'] = 'Invalid Email!';
            return redirect()->back()->withErrors($errors);
        }

        $password = $this->generateHashedPassword($request->password, "");
        if ($password != $admin->password) {
            $errors['password'] = 'Invalid Password!';
            return redirect()->back()->withErrors($errors);
        }

        if ($admin->is_verified == 0) {
            $errors['email'] = 'Verify Your Email!';
            return redirect()->back()->withErrors($errors);
        }

        if ($admin->user_types_id != 3) {
            if ($admin->is_admin != 1) {
                $errors['email'] = 'Not Admin User, You have not permission to login to this area!';
                return redirect()->back()->withErrors($errors);
            }
        }
        $token = $this->createAccessToken($admin);
        Session::put("admin_id", $admin->id);
        Session::put('name', $admin->full_name);
        Session::put('is_logged_in', true);
        Session::put('access_token', $token['token']);
        Session::put('user_id', $admin->id);
        Session::put('is_admin', $admin->is_admin);
        Session::put('user_type_id', $admin->user_types_id);
        Session::put('expire_at', $token['expires_at']);
        Session::put('language', "en");
        app()->setLocale("en");
        return redirect()->route('dashboard');
    }

    /**
     * logout by flush session values
     * @return login view
     */
    public function logout()
    {
        Session::flush();
        return redirect()->route('login');
    }

    /**
     * admin trips search
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trips(Request $request)
    {
        $filter['is_deleted'] = 0;
        $filter = $this->getFilter($request);
        $simple_filter = isset($filter['simple']) ? $filter['simple'] : [];
        $trips = Trip::where($simple_filter);
        // arrival_date range
        if (($request->has('arrival_date_from') || $request->has('arrival_date_to')) && ($request->get('arrival_date_to') && $request->get('arrival_date_from'))) {
            $arrival_filter = $this->dateRangeFilter('arrival_date', $request->get('arrival_date_from'), $request->get('arrival_date_to'));
            $from = $arrival_filter['arrival_date']['from'];
            $to = $arrival_filter['arrival_date']['to'];
            $trips = $trips->whereBetween('arrival_date', [$from, $to]);
        }

        //departure_date range
        if (($request->has('departure_date_from') || $request->has('departure_date_to')) && ($request->get('departure_date_to') && $request->get('departure_date_from'))) {
            $departure_filter = $this->dateRangeFilter('departure_date', $request->get('departure_date_from'), $request->get('departure_date_to'));
            $from = $departure_filter['departure_date']['from'];
            $to = $departure_filter['departure_date']['to'];
            $trips = $trips->whereBetween('departure_date', [$from, $to]);
        }

        // arrival_time range
        if (($request->has('arrival_time_from') || $request->has('arrival_time_to')) && ($request->get('arrival_time_to') && $request->get('arrival_time_from'))) {
            $arrival_filter = $this->timeRangeFilter('arrival_time', $request->get('arrival_time_from') . ":00", $request->get('arrival_time_to') . ":00");
            $from = $arrival_filter['arrival_time']['from'];
            $to = $arrival_filter['arrival_time']['to'];
            $trips = $trips->whereBetween('arrival_time', [$from, $to]);
        }

        //kilos range
        /*if($request->has('kilos_from') || $request->has('kilos_to')){
            $kilos_filter = $this->numberRangeFilter('kilos',$request->get('kilos_from'),$request->get('kilos_to'));
            $from = $kilos_filter['kilos']['from'];
            $to = $kilos_filter['kilos']['to'];
            $trips = $trips->whereBetween('kilos',[$from,$to]);
        }*/
        // kilos greater than or equal entered value
        if ($request->has('kilos') && $request->get('kilos')) {
            $trips = $trips->where('kilos', '>=', $request->get('kilos'));
        }

        $countries = Country::all();
        $trips = $trips->paginate(10);
        $collection = json_encode(TripCollection::collection($trips));
        return view('admin.trip.index')->with(['trips' => json_decode($collection), 'paginate' => $trips, 'countries' => $countries, 'show_filter' => true]);
    }

    /**
     * Filter Trips by
     * country_from , country_to
     * city_from, city_to
     * Weight range => weight_from , weight_to
     * Date range => date_from , date_to
     * Time range => time_from , time_to
     * Type transportation (plane-train-autobus)
     * @param Request $request
     * @return array of key => values
     */
    private function getFilter(Request $request)
    {
        $filter = [];
        // country_from
        if ($request->has('country_from') && !empty($request->get('country_from'))) {
            $filter['simple']['country_from'] = $request->get('country_from');
        }
        // country_to
        if ($request->has('country_to') && !empty($request->get('country_to'))) {
            $filter['simple']['country_to'] = $request->get('country_to');
        }

        // city_from
        if ($request->has('city_from') && !empty($request->get('city_from'))) {
            $filter['simple']['city_from'] = $request->get('city_from');
        }
        //city_to
        if ($request->has('city_to') && !empty($request->get('city_to'))) {
            $filter['simple']['city_to'] = $request->get('city_to');
        }

        //transportation
        if ($request->has('transportation') && !empty($request->get('transportation')) && in_array($request->get('transportation'), ['autobus', 'plane', 'train'])) {
            $filter['simple']['transportation'] = $request->get('transportation');
        }
        return $filter;
    }

    /**
     * admin shipment search
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shipments(Request $request)
    {
        $countries = Country::all();
        $filter = $this->getFilter($request);
        $simple_filter = isset($filter['simple']) ? $filter['simple'] : [];
        $shipments = Shipment::where($simple_filter);

        // weight range
        if (($request->has('weight_from') || $request->has('weight_to')) && ($request->get('weight_from') && $request->get('weight_to'))) {
            $weight_filter = $this->numberRangeFilter('weight', $request->get('weight_from'), $request->get('weight_to'));
            $from = $weight_filter['weight']['from'];
            $to = $weight_filter['weight']['to'];
            $shipments = $shipments->whereBetween('weight', [$from, $to]);
        }

        // height range
        if (($request->has('height_from') || $request->has('height_to')) && ($request->get('height_from') && $request->get('height_to'))) {
            $height_filter = $this->numberRangeFilter('height', $request->get('height_from'), $request->get('height_to'));
            $from = $height_filter['height']['from'];
            $to = $height_filter['height']['to'];
            $shipments = $shipments->whereBetween('height', [$from, $to]);
        }

        // width range
        if (($request->has('width_from') || $request->has('width_to')) && ($request->get('width_from') && $request->get('width_to'))) {
            $width_filter = $this->numberRangeFilter('width', $request->get('width_from'), $request->get('width_to'));
            $from = $width_filter['width']['from'];
            $to = $width_filter['width']['to'];
            $shipments = $shipments->whereBetween('width', [$from, $to]);
        }

        // length range
        if (($request->has('length_from') || $request->has('length_to')) && ($request->get('length_from') && $request->get('length_to'))) {
            $length_filter = $this->numberRangeFilter('length', $request->get('length_from'), $request->get('length_to'));
            $from = $length_filter['length']['from'];
            $to = $length_filter['length']['to'];
            $shipments = $shipments->whereBetween('length', [$from, $to]);
        }

        // fees range
        if (($request->has('potential_fees_from') || $request->has('potential_fees_to')) && ($request->get('potential_fees_from') && $request->get('potential_fees_to'))) {
            $fees_filter = $this->numberRangeFilter('potential_fees', $request->get('potential_fees_from'), $request->get('potential_fees_to'));
            $from = $fees_filter['potential_fees']['from'];
            $to = $fees_filter['potential_fees']['to'];
            $shipments = $shipments->whereBetween('potential_fees', [$from, $to]);
        }

        // price range
        if (($request->has('price_to') || ($request->has('price_from'))) && ($request->get('price_to') && $request->get('price_from'))) {
            $price_filter = $this->numberRangeFilter('price', $request->get('price_from'), $request->get('price_to'));
            $from = $price_filter['price']['from'];
            $to = $price_filter['price']['to'];
            $shipments = $shipments->whereBetween('price', [$from, $to]);
        }

        //days_from
        if (($request->has('days_from_start') || $request->has('days_from_end')) && ($request->get(
                    'days_from_start') && $request->get('days_from_end'))) {
            $days_from_filter = $this->dateRangeFilter('days_from', $request->get('days_from_start'), $request->get('days_from_end'));
            $from = $days_from_filter['days_from']['from'];
            $to = $days_from_filter['days_from']['to'];
            $shipments = $shipments->whereBetween('potential_days_from', [$from, $to]);
        }

        // days_to
        if (($request->has('days_to_start') || $request->has('days_to_end')) && ($request->get('days_to_start') && $request->get('days_to_end'))) {
            $days_to_filter = $this->dateRangeFilter('days_to', $request->get('days_to_start'), $request->get('days_to_end'));
            $from = $days_to_filter['days_to']['from'];
            $to = $days_to_filter['days_to']['to'];
            $shipments = $shipments->whereBetween('potential_days_to', [$from, $to]);
        }

        $shipments = $shipments->paginate(10);
        $collection = json_encode(ShipmentCollection::collection($shipments));
        return view('admin.shipment.index')->with(['shipments' => json_decode($collection), 'paginate' => $shipments, 'countries' => $countries, 'show_filter' => true]);
    }

    /**
     * show shipment
     * @param id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show_shipment($id)
    {
        $shipment = Shipment::find($id);
        if (!$shipment) {
            return redirect()->route('shipments');
        }
        $resource = json_encode(new ShipmentResource($shipment));
        return view('admin.shipment.show')->with(['shipment' => json_decode($resource)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = Trip::find($id);
        if (!$trip) {
            return redirect()->route('trips');
        }
        $resource = json_encode(new TripResource($trip));
        return view('admin.trip.show')->with(['trip' => json_decode($resource)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
