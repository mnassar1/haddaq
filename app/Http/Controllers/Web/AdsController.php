<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAds\AddAdminAdsRequest;
use App\Http\Requests\AdminAds\EditAdminAdsRequest;
use App\Http\Resources\AdminAds\AdminAdsCollection;
use App\Http\Resources\AdminAds\AdminAdsResource;
use App\Model\AdminAds;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {

        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        if (isset($simple_filter['fixing_shops_or_home']) && $simple_filter['fixing_shops_or_home'] == 7) {
            unset($simple_filter['fixing_shops_or_home']);
        }
        //return ($simple_filter);
        $admin_ads = AdminAds::where($simple_filter);

        $admin_ads = $admin_ads->orderby('id', 'desc');
        $admin_ads = $admin_ads->paginate(20);
        $data['paginate'] = $admin_ads;
        $collection = json_encode(AdminAdsCollection::collection($admin_ads));
        $data['admin_ads'] = json_decode($collection);
        return view('admin.admin_ads.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('is_banar') && $request->get('is_banar')) {
            $filter['simple']['is_banar'] = $request->get('is_banar');
        }


        if ($request->has('fixing_shops_or_home') && null !== $request->get('fixing_shops_or_home')) {
            $filter['simple']['fixing_shops_or_home'] = $request->get('fixing_shops_or_home');
        }

        return $filter;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin_ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddAdminAdsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddAdminAdsRequest $request)
    {
        $admin_ads = $request->all();
        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                $admin_ads['images'] = "ads/" . $image_name;
            }
        }
        if ($request->hasFile('images_ar')) {
            $file = $request->file('images_ar');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                $admin_ads['images_ar'] = "ads/" . $image_name;
            }

        }

        $admin_ads = AdminAds::create($admin_ads);
        return redirect(route('admin_ads.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return redirect(route('admin_ads.index'));
        }
        $resource = json_encode(new AdminAdsResource($admin_ads));
        $data['admin_ads'] = json_decode($resource);
        return view('admin.admin_ads.show')->with($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return redirect(route('admin_ads.index'));
        }
        $data['admin_ads'] = $admin_ads;
        return view('admin.admin_ads.edit')->with($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param EditAdminAdsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditAdminAdsRequest $request, $id)
    {
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return redirect(route('admin_ads.index'));
        }

        $admin_ad = $request->all();

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/users/ads'), $image_name)) {
                $admin_ad['images'] = "ads/" . $image_name;
            }

        } else {
            unset($admin_ad['images']);
        }


        if ($request->hasFile('images_ar')) {
            $file = $request->file('images_ar');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                $admin_ad['images_ar'] = "ads/" . $image_name;
            }

        } else {
            unset($admin_ad['images_ar']);
        }

        $admin_ads->update($admin_ad);
        return redirect(route('admin_ads.show', $admin_ads->id));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return redirect(route('admin_ads.index'));
        }
        $admin_ads->delete();
        return redirect(route('admin_ads.index'));
    }


}
