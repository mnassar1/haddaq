<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddCategoryRequest;
use App\Http\Requests\Setting\EditSettingRequest;
use App\Http\Resources\Setting\SettingCollection;
use App\Http\Resources\Setting\SettingResource;
use App\Model\Category;
use App\Model\Setting;
use Illuminate\Http\Request;

class PushController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $simple_filter = [];
        $categories = Setting::where($simple_filter);


        $categories = $categories->orderby('id', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(SettingCollection::collection($categories));
        $data['categories'] = json_decode($collection);
        return view('admin.setting.index')->with($data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$data['groups']= CategoryGroup::where('id','<>',0)->get();
        //return view('admin.category.create')->with($data);
        return redirect(route('users_push_setting.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryRequest $request)
    {
        $category = $request->all();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move('storage/app/public/categories/', $image_name)) {
                $category['icon'] = "categories/" . $image_name;
            }

        }

        $categories = Category::create($category);
        return redirect(route('categories.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = Setting::find($id);
        if (!$categories) {
            return redirect(route('users_push_setting.index'));
        }
        $resource = json_encode(new SettingResource($categories));
        $data['categories'] = json_decode($resource);
        return view('admin.setting.show')->with($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = Setting::find($id);
        if (!$catrgories) {
            return redirect(route('users_push_setting.index'));
        }
        $data['categories'] = $catrgories;

        return view('admin.setting.edit')->with($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param EditSettingRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditSettingRequest $request, $id)
    {
        $categories = Setting::find($id);
        if (!$categories) {
            return redirect(route('users_push_setting.index'));
        }

        $category = $request->all();
        $categories->update($category);
        return redirect(route('users_push_setting.show', $categories->id));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return array
     */
    // public function destroy($id)
    // {
    //     $categories = Category::find($id);
    //     if(!$categories){
    //         return redirect(route('categories.index'));
    //     }
    //     $categories->delete();
    //     return redirect(route('categories.index'));
    // }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('category_type') && $request->get('category_type')) {
            $filter['simple']['category_type'] = $request->get('category_type');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['category_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['category_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }


}
