<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactUs\ContactUsCollection;
use App\Model\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $data = [];
        $filter = $this->getFilter($request);
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $contact_us = ContactUs::where($simple_filter)->orderby('created_at', 'desc')->paginate(20);
        $data['paginate'] = $contact_us;
        $contact_us_collection = json_encode(ContactUsCollection::collection($contact_us));
        $data['contact_us'] = json_decode($contact_us_collection);
        return view('admin.contact_us.index')->with($data);
    }

    private function getFilter(Request $request)
    {
        $filter = [];
        // status
        if ($request->has('status') && !empty($request->get('status'))) {
            $filter['simple']['status'] = $request->get('status');
        }
        // type
        if ($request->has('type') && !empty($request->get('type'))) {
            $filter['simple']['type'] = $request->get('type');
        }
        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $contact_us = ContactUs::find($id);
        if (!$contact_us) {
            return redirect(route('contact_us.index'));
        }
        // if($contact_us->status == 'new'){
        //     $this->make_message_as($contact_us,'processing');
        // }
        $resource = json_encode($contact_us);
        $data['message'] = json_decode($contact_us);
        return view('admin.contact_us.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * change message status
     * @param Request $request
     * @param id, status
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_status(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|in:new,processing,archived'
        ]);
        $message = ContactUs::find($id);
        if (!$message) {
            return redirect(route('contact_us.index'));
        }
        $message->status = $request->get('status');
        $message->save();
        return redirect(route('contact_us.show', $id));
    }

    /**
     * change message status
     * @param ContactUs $message
     * @param ContactUs,status
     */
    private function make_message_as(ContactUs $message, $status)
    {
        $message->status = $status;
        $message->save();
    }
}
