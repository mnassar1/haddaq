<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\Faq\FaqCollection;
use App\Http\Resources\Faq\FaqResource;
use App\Model\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('is.admin')->only(['index', 'show']);
    }

    /**
     * list all FAQs
     * @return view to display FAQs list
     */
    public function index()
    {
        $data = [];
        $faqs = Faq::orderBy('question_order')->paginate(20);
        $data['paginate'] = $faqs;
        $collection = json_encode(FaqCollection::collection($faqs));
        $data['faqs'] = json_decode($collection);
        return view('admin.faq.index')->with($data);
    }

    /**
     * show individual faq
     * @param $id
     * @return view to display individual faq
     */
    public function show($id)
    {
        $data = [];
        $faq = Faq::find($id);
        if (!$faq) {
            return redirect(route('faq.index'));
        }
        $resource = json_encode(new FaqResource($faq));
        $data['faq'] = json_decode($resource);
        return view('admin.faq.show')->with($data);
    }

    /**
     * load create FAQ form
     * @return view to display create faq
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * store faq to database
     * @param Request $request
     * @return redirect to show FAQ
     */
    public function store(Request $request)
    {
        $faq = Faq::create($request->all());
        return redirect(route('faq.index'));
    }

    /**
     * load edit FAQ form
     * @param $id
     * @return view to edit FAQ
     */
    public function edit($id)
    {
        $data = [];
        $faq = Faq::find($id);
        if (!$faq) {
            return redirect(route('faq.index'));
        }
        $resource = json_encode(new FaqResource($faq));
        $data['faq'] = json_decode($resource);
        return view('admin.faq.edit')->with($data);
    }

    /**
     * update faq data
     * @param Request $request
     * @param $id
     * @return redirect to show FAQ
     */
    public function update(Request $request, $id)
    {
        $faq = Faq::find($id);
        if (!$faq) {
            return redirect(route('faq.index'));
        }
        $faq->update($request->all());
        return redirect(route('faq.show', $faq->id));
    }

    /**
     * delete FAQ
     * @param $id
     * @return redirect to FAQs index
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        if (!$faq) {
            return redirect(route('faq.index'));
        }
        $faq->delete();
        return redirect(route('faq.index'));
    }
}
