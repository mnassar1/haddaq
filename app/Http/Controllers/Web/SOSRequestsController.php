<?php

namespace App\Http\Controllers\Web;

use App\Helpers\NotificationsCenter;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProblemTypes\AddProblemTypeRequest;
use App\Http\Requests\ProblemTypes\EditProblemTypeRequest;
use App\Http\Resources\SOSRequest\SOSRequestCollection;
use App\Http\Resources\SOSRequest\SOSRequestResource;
use App\Model\ProblemType;
use App\Model\SOSRequest;
use App\Model\SOSResponse;
use App\User;
use Illuminate\Http\Request;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

// use App\Http\Requests\SOSRequest\AddProblemTypeRequest;
// use App\Http\Requests\SOSRequest\EditProblemTypeRequest;

// firebase handler
// single or multi devices
// topic
// end of firebase handler


class SOSRequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $sender_name = (isset($filter['simple']['sender_name'])) ? $filter['simple']['sender_name'] : NULL;
        unset($simple_filter['sender_name']);
        $categories = SOSRequest::where($simple_filter);


        if ($sender_name != null && $sender_name == 'GUEST') {
            $categories = $categories->WhereNull('user_id');
        }
        if ($sender_name != null && $sender_name != 'GUEST') {
            $sender_ids = User::where('full_name', 'LIKE', '%' . $sender_name . '%')->pluck('id')->toArray();
            $categories = $categories->whereIn('user_id', $sender_ids);

        }
        $categories = $categories->orderby('sos_request_status', 'asc');
        $categories = $categories->orderby('created_at', 'desc');
        $categories = $categories->paginate(20);
        $data['paginate'] = $categories;
        $collection = json_encode(SOSRequestCollection::collection($categories));
        // return $collection;
        $data['categories'] = json_decode($collection);
        $data['problem_types'] = ProblemType::all();

        return view('admin.sos_requests.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('sender_name') && null !== $request->get('sender_name')) {
            $filter['simple']['sender_name'] = strtoupper($request->get('sender_name'));
        }

        if ($request->has('problem_type') && null !== $request->get('problem_type')) {
            $filter['simple']['sos_problem_type_id'] = $request->get('problem_type');
        }

        if ($request->has('status') && null !== $request->get('status')) {
            $filter['simple']['sos_request_status'] = $request->get('status');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.problem_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddProblemTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProblemTypeRequest $request)
    {
        $category = $request->all();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $category['icon'] = "problem_types/" . $image_name;
            }

        }

        $categories = ProblemType::create($category);
        return redirect(route('problem_types.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $categories = SOSRequest::find($id);
        if (!$categories) {
            return redirect(route('sos_requests.index'));
        }
        $resource = json_encode(new SOSRequestResource($categories));
        $data['categories'] = json_decode($resource);
        $data['problem_type'] = ProblemType::find($categories->sos_problem_type_id);
        $data['user_details'] = User::find($categories->user_id);
        $data['sos_responses'] = SOSResponse::where('sos_request_id', $id)->get();
        return view('admin.sos_requests.show')->with($data);
    }

    public function show_on_map($id)
    {
        $categories = SOSRequest::find($id);
        if (!$categories) {
            return redirect(route('sos_requests.index'));
        }
        $resource = json_encode(new SOSRequestResource($categories));
        $request_details = json_decode($resource);
        // $data['problem_type'] = ProblemType::find($categories->sos_problem_type_id);
        $user_details = User::find($categories->user_id);
        $sos_responses = SOSResponse::where('sos_request_id', $id)
            ->where('response_status', 'Accepted')->distinct('user_id')->get();
        $data['locations'] = array();
        $pieces = explode(",", $request_details->location);
        $data['sos_lat'] = $pieces[0];
        $data['sos_long'] = $pieces[1];
        $data['name'] = ($user_details) ? $user_details->full_name : 'Guest';

        if (!empty($sos_responses)) {
            foreach ($sos_responses as $response) {
                $pieces = explode(",", $response->location);
                $sos_lat = $pieces[0];
                $sos_long = $pieces[1];
                $savior_details = User::find($response->user_id);
                $name = ($savior_details) ? $savior_details->full_name : 'Savior';

                $data['locations'][] = [
                    $name,
                    $sos_lat,
                    $sos_long,
                    2
                ];

            }
        }
        //return ($data['locations']);
        $data['locations'] = json_encode($data['locations']);
        //return $data['locations'];
        return view('admin.sos_requests.map')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $catrgories = ProblemType::find($id);
        if (!$catrgories) {
            return redirect(route('problem_types.index'));
        }
        $data['categories'] = $catrgories;
        return view('admin.problem_types.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditProblemTypeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProblemTypeRequest $request, $id)
    {

        $categories = ProblemType::find($id);
        if (!$categories) {
            return redirect(route('problem_types.index'));
        }

        $category = $request->all();

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $category['icon'] = "problem_types/" . $image_name;
            }

        } else {
            unset($category['icon']);
        }

        $categories->update($category);
        return redirect(route('problem_types.show', $categories->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sos_request = SOSRequest::find($id);
        if (!$sos_request) {
            return redirect(route('sos_requests.index'));
        }
        if ($sos_request->sos_request_status == 'Closed') {
            return redirect(route('sos_requests.index')); //return ResponseHandler::json('Request Already Closed , Thanks');
        }
        $update_status = array('sos_request_status' => 'Closed', 'closed_by' => 'Admin');
        $sos_request = $sos_request->update($update_status);

        $sos_request = SOSRequest::find($id);
        $data = new SOSRequestResource($sos_request);
        NotificationsCenter::removeFromList("new_request", $id);
        $send_data = array(
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'location' => $data->location,
            'device_udid' => $data->device_udid,
            'push_token' => $data->push_token,
            'sos_problem_type_id' => $data->sos_problem_type_id,
            'sos_problem_type_details' => $data->sos_problem_type_details,
            'description' => ($data->description) ? $data->description : "",
            'sos_request_status' => $data->sos_request_status,
        );
        //var_dump ($send_data);
        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
        $message
            ->setNotification(new Notification('HaddaQ', 'Close SOS Request'))
            ->setData($send_data);

        $response = $client->send($message);

        // var_dump ($response);
        return redirect(route('sos_requests.index'));
    }


}
