<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\CMSRequest;
use App\Http\Resources\Cms\CmsCollection;
use App\Http\Resources\Cms\CmsResource;
use App\Model\CMS;
use Illuminate\Http\Request;

class CMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $data = [];
        $cms = CMS::paginate(20);
        $data['paginate'] = $cms;
        $collection = json_encode(CmsCollection::collection($cms));
        $data['cms'] = json_decode($collection);
        return view('admin.cms.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CMSRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CMSRequest $request)
    {
        $cms = CMS::create($request->all());
        return redirect(route('cms.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $cms = CMS::find($id);
        if (!$cms) {
            return redirect(route('cms.index'));
        }
        $resource = json_encode(new CmsResource($cms));
        $data['cms'] = json_decode($resource);
        return view('admin.cms.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $cms = CMS::find($id);
        if (!$cms) {
            return redirect(route('cms.index'));
        }
        $data['cms'] = $cms;
        return view('admin.cms.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cms = CMS::find($id);
        if (!$cms) {
            return redirect(route('cms.index'));
        }
        $cms->update($request->all());
        return redirect(route('cms.show', $cms->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms = CMS::find($id);
        if (!$cms) {
            return redirect(route('cms.index'));
        }
        $cms->delete();
        return redirect(route('cms.index'));
    }
}
