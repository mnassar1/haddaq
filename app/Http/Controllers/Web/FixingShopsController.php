<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\FixingShop\AddFixingShopsRequest;
use App\Http\Requests\FixingShop\EditFixingShopsRequest;
use App\Http\Resources\FixingShop\FixingShopsCollection;
use App\Http\Resources\FixingShop\FixingShopsResource;
use App\Model\FixingShops;
use Illuminate\Http\Request;

class FixingShopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function __construct()
    {
        $this->middleware('is.admin');
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);

        $data = [];
        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $fixing_shops = FixingShops::where($simple_filter);

        $category_name = (isset($filter['simple']['shop_name_en'])) ? $filter['simple']['shop_name_en'] : NULL;

        if (!is_null($category_name)) {
            $fixing_shops = FixingShops::where('shop_name_en', 'LIKE', '%' . $category_name . '%');
        }

        $category_name_ar = (isset($filter['simple']['shop_name_ar'])) ? $filter['simple']['shop_name_ar'] : NULL;

        if (!is_null($category_name_ar)) {
            $fixing_shops = FixingShops::where('shop_name_ar', 'LIKE', '%' . $category_name_ar . '%');
        }

        $fixing_shops = $fixing_shops->orderby('id', 'desc');
        $fixing_shops = $fixing_shops->paginate(20);
        $data['paginate'] = $fixing_shops;
        $collection = json_encode(FixingShopsCollection::collection($fixing_shops));
        $data['fixing_shops'] = json_decode($collection);
        $data['fixing_shops_types'] = \App\Model\FixingShopsTypes::all();
        $data['cities'] = \App\Model\City::all();
        return view('admin.fixing_shops.index')->with($data);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('fixing_shops_types_id') && $request->get('fixing_shops_types_id')) {
            $filter['simple']['fixing_shops_types_id'] = $request->get('fixing_shops_types_id');
        }


        if ($request->has('name_en') && null !== $request->get('name_en')) {
            $filter['simple']['shop_name_en'] = $request->get('name_en');
        }

        if ($request->has('name_ar') && null !== $request->get('name_ar')) {

            $filter['simple']['shop_name_ar'] = $request->get('name_ar');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['fixing_shops_types'] = \App\Model\FixingShopsTypes::all();
        $data['cities'] = \App\Model\City::all();
        return view('admin.fixing_shops.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFixingShopsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddFixingShopsRequest $request)
    {
        //return $request;
        $fixing_shop = $request->all();

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/fixing_shops/'), $image_name)) {
                $fixing_shop['images'] = "fixing_shops/" . $image_name;
            }

        }
        $images = array();
        if ($request->hasFile('sliders')) {
            foreach ($request->file('sliders') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                if ($file->move(storage_path('app/public/fixing_shops/'), $image_name)) {
                    $images[] = "fixing_shops/" . $image_name;
                }

            }
            $fixing_shop['sliders'] = json_encode($images);
        } else {
            unset($fixing_shop['sliders']);
        }


        $fixing_shop = FixingShops::create($fixing_shop);

        return redirect(route('fixing_shops.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $fixing_shops = FixingShops::find($id);
        if (!$fixing_shops) {
            return redirect(route('fixing_shops.index'));
        }
        $resource = json_encode(new FixingShopsResource($fixing_shops));
        $data['fixing_shops'] = json_decode($resource);
        $data['City'] = \App\Model\City::find($fixing_shops->city_id);
        return view('admin.fixing_shops.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $fixing_shops = FixingShops::find($id);
        if (!$fixing_shops) {
            return redirect(route('fixing_shops.index'));
        }
        $data['fixing_shops'] = $fixing_shops;
        $data['fixing_shops_types'] = \App\Model\FixingShopsTypes::all();
        $data['cities'] = \App\Model\City::all();
        return view('admin.fixing_shops.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditFixingShopsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFixingShopsRequest $request, $id)
    {

        $fixing_shops = FixingShops::find($id);
        if (!$fixing_shops) {
            return redirect(route('fixing_shops.index'));
        }

        $fixing_shop = $request->all();

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/categories/'), $image_name)) {
                $fixing_shop['images'] = "categories/" . $image_name;
            }

        } else {
            unset($fixing_shop['images']);
        }
        $images = array();
        if ($request->hasFile('sliders')) {
            foreach ($request->file('sliders') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                echo $image_name;
                if ($file->move(storage_path('app/public/fixing_shops/'), $image_name)) {
                    $images[] = "fixing_shops/" . $image_name;
                }

            }
            $fixing_shop['sliders'] = json_encode($images);
        } else {
            unset($fixing_shop['sliders']);
        }
        $fixing_shops->update($fixing_shop);
        return redirect(route('fixing_shops.show', $fixing_shops->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fixing_shops = FixingShops::find($id);
        if (!$fixing_shops) {
            return redirect(route('fixing_shops.index'));
        }
        $fixing_shops->delete();
        return redirect(route('fixing_shops.index'));
    }


}
