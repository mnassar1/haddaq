<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Login\IncorrectPasswordException;
use App\Exceptions\Profile\WrongVerificationCodeException;
use App\Exceptions\User\UserIsBlockedException;
use App\Exceptions\User\UserLoginWithPhoneException;
use App\Exceptions\User\UserNotFoundException;
use App\Exceptions\User\UserNotVerifiedException;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginRequest;
use App\Http\Requests\Login\ResetPasswordRequest;
use App\Http\Requests\Login\VerificationCodeRequest;
use App\Http\Requests\Login\VerifyCodeRequest;
use App\Http\Resources\User\UserResource;
use App\Model\Device;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Illuminate\Support\Facades\Password;

class LoginController extends Controller
{
    use UserHelperTrait;

    public function index(LoginRequest $request){
        if(!empty($request->login_type) && $request->login_type == 'email'){
            return $this->email($request);
        }
        if(!empty($request->login_type) && $request->login_type == 'phone'){
            return $this->phone($request);
        }
    }

    /**
     * Login using email and password
     * @param $request
     * @return profile object,access_token
     * @throws IncorrectPasswordException
     * @throws UserIsBlockedException
     * @throws UserNotFoundException
     * @throws UserNotVerifiedException
     */

    public function email($request)
    {
        $user = User::where('email', $request->phone)->first();
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            if ($user->password == $this->generateHashedPassword($request->password, $user->salt)) {

                /** handle user not verified exception  */
                if ($user->is_verified == 0) {
                    throw new UserNotVerifiedException;
                }

                /** handle user is blocked exception */
                if ($user->is_blocked == 1) {
                    throw new UserIsBlockedException;
                }

                $this->createDevice($request, $user->id);
                $access_token = $this->createAccessToken($user);
                $login_token = $this->generateLoginToken($user);

                $user['access_token'] = $access_token;
                $user['another_token'] = $login_token;

                return ResponseHandler::json(new UserResource($user));

            }
            throw new IncorrectPasswordException;
        }
    }

    private function createDevice($request, $user_id)
    {
        $request['user_id'] = $user_id;
        $device = Device::where('user_id', $user_id)->orderBy('id', 'Desc')->first();
        // if device already registerd unmatch it and match with new user_id
        if ($device) {
            $update = $device->update($request->toArray());
            if ($update) {
                return true;
            }
        } else {
            $new_device = Device::create($request->toArray());
            if ($new_device) {
                return true;
            }
        }
        return false;
    }

    /**
     * Login using phone and password
     * @param $request
     * @return profile object,access_token
     * @throws IncorrectPasswordException
     * @throws UserIsBlockedException
     * @throws UserNotFoundException
     * @throws UserNotVerifiedException
     */
    public function phone($request)
    {
        $user = User::where('mobile', '=', $request->phone)->orWhere('phone_with_code', '=', $request->phone)->first();
        if (!$user) {
            throw new UserNotFoundException;
        }
        elseif ($user->user_type_id == 3){
            throw new UserLoginWithPhoneException();
        }
        else {
            if ($user->password == $this->generateHashedPassword($request->password, $user->salt)) {

                /** handle user not verified exception  */
                if ($user->is_mobile_verified == 0) {
                    throw new UserNotVerifiedException;
                }

                /** handle user is blocked exception */
                if ($user->is_blocked == 1) {
                    throw new UserIsBlockedException;
                }


                $this->createDevice($request, $user->id);
                $access_token = $this->createAccessToken($user);
                $login_token = $this->generateLoginToken($user);

                $user['access_token'] = $access_token;
                $user['another_token'] = $login_token;

                return ResponseHandler::json(new UserResource($user));

            }
            throw new IncorrectPasswordException;
        }
    }


    //function to send sms

    public function send_verification_code(VerificationCodeRequest $request)
    {
        $user = User::where('mobile', '=', $request->phone)->orWhere('phone_with_code', '=', $request->phone)->first();
        if (!$user) {
            throw new UserNotFoundException;
        }
        else {
            $verification_code = random_int(1000, 9999);
            $user->mobile_verification_code = $verification_code;
            $user->is_mobile_verified = 0;
            $saved = $user->save();
            if (!$saved) {
                return ResponseHandler::json(['verification_code_sent' => false]);
            }
            // @todo send sms to user's phone number
            $phone = $user->mobile;
            $country_code = $user->country_code;
            $mobile = '+' . $country_code . $phone;
            $phone = ltrim($mobile, '+');  //substr($mobile, 1);
            $msg = $verification_code . " is your HaddaQ verification code";
            $this->send_sms($mobile, $msg);

            //
            return ResponseHandler::json(['verification_code_sent' => true]);
        }
    }

    function send_sms($phone, $msg)
    {
        $curl_url = "http://62.215.226.174/fccsms_P.aspx?UID=haddaquser&P=haddaqusr11&S=Haddaq&G=" . urlencode($phone) . "&M=" . urlencode($msg) . "&L=L";
        //return  $curl_url;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: e59cf561-9334-a63a-5020-172c93c6ca82"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

    }

    /**
     * verify user's mobile
     * @param VerifyCodeRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws UserNotFoundException
     * @throws WrongVerificationCodeException
     */
    public function mobile_verify(VerifyCodeRequest $request)
    {
        $user = User::where('mobile', '=', $request->phone)->orWhere('phone_with_code', '=', $request->phone)->first();
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            if (!$user->is_mobile_verified) {
                if ($request->mobile_verification_code !== $user->mobile_verification_code) {
                    throw new WrongVerificationCodeException;
                }
                $user->is_mobile_verified = 1;
                $user->mobile_verification_code = null;
                $saved = $user->save();
                return ResponseHandler::json(['mobile_verified' => $saved]);
            }
            return ResponseHandler::json(['mobile_verified' => true]);
        }
    }

    public function forgotPassword(Request $request){
        if(!empty($request->login_type) && $request->login_type == 'email'){
            return $this->forgotPasswordEmail($request);
        }
        if(!empty($request->login_type) && $request->login_type == 'phone'){
            return $this->forgotPasswordPhone($request);
        }
    }

    /**
     * Forget password create OTP one time password and send it to user mail to use it
     * in resetting password
     * @param Request $request
     * @return boolean
     * @throws UserNotFoundException
     * @throws UserNotVerifiedException
     */

    public function forgotPasswordPhone(Request $request)
    {
        $user = User::where('mobile', '=', $request->mobile)->orWhere('phone_with_code', '=', $request->mobile)->first();
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            if ($user->is_mobile_verified == 0) {
                throw new UserNotVerifiedException;
            }
            $token = Str::random(60);
            $user->reset_token = $token;
            $user->save();
            // send Message....

            $url = url("reset/" . $token);
            $msg = "To reset password you can use this url " . $url;

            $phone = $request->mobile;
            $country_code = $request->country_code;


            $phone = ltrim($mobile, '+');
            $this->send_sms($mobile, $msg);
            return ResponseHandler::json(true);
        }
    }

    public function forgotPasswordEmail(Request $request)
    {
        $user = User::where('email', $request->mobile)->first();
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            if ($user->is_verified == 0) {
                throw new UserNotVerifiedException;
            }
            $token = Str::random(60);
            $user->reset_token = $token;
            $user->save();
            // send Message....

            $this->validateEmail($request);
            $response = $this->broker()->sendResetLink(
                $request->only('mobile')
            );
            return $response == Password::RESET_LINK_SENT
                ? ResponseHandler::json(true)
                : ResponseHandler::json(false);
        }
    }

    /**
     * load reset password view
     * @param token
     * @return view
     */
    public function reset($token)
    {
        return view('login.reset')->with(['token' => $token]);
    }

    /**
     * reset password logic
     * @param ResetPasswordRequest $request
     * @return void
     */
    public function doReset(ResetPasswordRequest $request)
    {
        $user = User::where('reset_token', $request->token)->first();
        if (!$user) {
            $errors['email'] = 'Invalid Reset Password Token!';
            return redirect()->back()->withErrors($errors);
        }

        $salt = "";
        $new_password = $this->generateHashedPassword($request->password, $salt);
        $user->password = $new_password;
        //$user->salt = $salt;
        $user->save();
        return view('login.reset_success');
    }

    /**
     * logout from app by remove all api tokens related to current user.
     * @param Request $request
     * @return json with boolean value
     * @throws UserNotFoundException
     */

    public function logout(Request $request)
    {
        $user = User::find($request->user_id);
        if (!$user) {
            throw new UserNotFoundException;
        }
        $user->tokens()->delete();
        return ResponseHandler::json(true);
    }

    /**
     * refresh token
     * @param Request $request
     * @return profile object and access token
     * @throws AuthenticationException
     */
    public function refreshToken(Request $request)
    {
        $request->validate([
            'login_token' => 'required|string'
        ]);
        $user = User::where(['login_token' => $request->get('login_token')])->first();
        if (!$user) {
            throw new AuthenticationException;
        }

        $access_token = $this->createAccessToken($user);
        $login_token = $this->generateLoginToken($user);

        $user['access_token'] = $access_token;
        $user['another_token'] = $login_token;

        return ResponseHandler::json(new UserResource($user));
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
    protected function validateEmail(Request $request)
    {
        $this->validate($request, ['mobile' => 'required|email']);
    }
}
