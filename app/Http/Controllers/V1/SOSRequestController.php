<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Emergency\EmergencyCollection;
use App\Http\Resources\ProblemTypes\ProblemTypeCollection;
use App\Http\Resources\SOSRequest\UserCollection;
use App\Model\Category;
use App\Model\Emergency;
use App\Model\ProblemType;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class SOSRequestController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('auth:api')->except(['saviors', 'problem_types', 'search_in_saviors', 'emergency_numbers']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        //$categories = ProblemType::all();
        //return ResponseHandler::jsonCollection(ProblemTypeCollection::collection($categories));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function problem_types(Request $request)
    {
        $problem_types = ProblemType::all();
        return ResponseHandler::jsonCollection(ProblemTypeCollection::collection($problem_types));
    }

    public function emergency_numbers(Request $request)
    {
        $Emergency = Emergency::all();
        return ResponseHandler::jsonCollection(EmergencyCollection::collection($Emergency));
    }

    public function saviors($page_number, $perpage, $user_id = NULL)
    {
        $route = \Request::fullUrl();
        $route = ltrim($route, 'http://');
        $route = ltrim($route, 'https://');
        $route = explode("/",$route);
        $subdomain = explode(".",$route[0]);
        $domains = \App\Model\Domains::where(['iso_code' => $subdomain])->first();
        $phone_code = $domains['phone_code'];

        $offset = $page_number * $perpage;

        $total = \App\User::where(['is_verified' => 1, 'is_blocked' => 0,'country_code'=>$phone_code])
            ->whereIn('user_types_id', [2])
            ->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);

        $main_sliders = \App\User::where(['is_verified' => 1, 'is_blocked' => 0,'country_code'=>$phone_code])
            ->whereIn('user_types_id', [2])
            ->orderby('full_name', 'asc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        //$main_sliders['user_id'] = $user_id;
        //return $main_sliders;
        $response = UserCollection::collection($main_sliders);
        //return $response;
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function search_in_saviors(Request $request, $page_number, $perpage, $user_id = NULL)
    {
        $offset = $page_number * $perpage;

        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $full_name = (isset($filter['simple']['full_name'])) ? $filter['simple']['full_name'] : NULL;
        unset($simple_filter['full_name']);

        $total = \App\User::where($simple_filter);

        if ($full_name != NULL) {
            $total = $total->where('full_name', 'LIKE', '%' . $full_name . '%');
        }


        $total = $total->where([
            'is_verified' => 1, 'is_blocked' => 0])
            ->whereIn('user_types_id', [2])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);
        //$main_sliders = \App\User::where($simple_filter);

        $main_sliders = \App\User::where([
            'is_verified' => 1, 'is_blocked' => 0])
            ->whereIn('user_types_id', [2]);

        if ($full_name != NULL) {
            $main_sliders = $main_sliders->where('full_name', 'LIKE', '%' . $full_name . '%');
        }


        $main_sliders = $main_sliders->orderby('full_name', 'asc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        //$main_sliders['user_id'] = $user_id;
        $response = UserCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('name') && $request->get('name')) {
            $filter['simple']['full_name'] = $request->get('name');
        }


        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return ResponseHandler::json([false]);
        } else {
            return response([new CategoryResource($categories)], Response::HTTP_OK);
        }
    }


}
