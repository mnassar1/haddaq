<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ContactUs\ContactUsNotFoundException;
use App\Exceptions\DatabaseOperationFailureException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUs\ContactUsRequest;
use App\Http\Resources\ContactUs\ContactUsCollection;
use App\Http\Resources\ContactUs\ContactUsResource;
use App\Model\ContactUs;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactUsController extends Controller
{
    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:contactus,LIST_CONTACT_US', ['only' => ['index']]);
        $this->middleware('admin.perms:contactus,SHOW_CONTACT_US', ['only' => ['show']]);
        $this->middleware('admin.perms:contactus,UPDATE_CONTACT_US', ['only' => ['update']]);
        $this->middleware('admin.perms:contactus,DELETE_CONTACT_US', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->has('type')) {
            $filter['type'] = $request->get('type');
        }
        if ($request->has('status')) {
            $filter['status'] = $request->get('status');
        }
        $messages = ContactUs::latest()->where($filter)->paginate(5);
        return ContactUsCollection::collection($messages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactUsRequest $request
     * @return \Illuminate\Http\Response
     * @throws DatabaseOperationFailureException
     */
    public function store(ContactUsRequest $request)
    {
        $message = ContactUs::create($request->all());
        if (!$message) {
            throw new DatabaseOperationFailureException;
        }
        return ResponseHandler::json($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactUsNotFoundException
     */
    public function show($id)
    {
        $message = ContactUs::find($id);
        if (!$message) {
            throw new ContactUsNotFoundException;
        } else {
            // if new message change it's status to processing
            $this->changeMessageStatus($message);
            return response(['message' => new ContactUsResource($message)], Response::HTTP_OK);
        }
    }

    /**
     * used for changing contactus message status
     * Used in show method to change new message to processing
     * @param ContactUs $message
     * @param ContactUs
     */
    private function changeMessageStatus(ContactUs $message, $status = null)
    {
        if ($status != null) {
            $message->update(['status' => $status]);
        } else {
            if ($message->status == 'new') {
                $message->update(['status' => 'processing']);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactUsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactUsNotFoundException
     */
    public function update(ContactUsRequest $request, $id)
    {
        $message = ContactUs::find($id);
        if (!$message) {
            throw new ContactUsNotFoundException;
        } else {
            if (!$request->has('status')) {
                $message['status'] = 'new';
            }
            $message->update($request->only(['type', 'status']));
            return response(['message' => $message], Response::HTTP_CREATED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactUsNotFoundException
     */
    public function destroy($id)
    {
        $message = ContactUs::find($id);
        if (!$message) {
            throw new ContactUsNotFoundException;
        } else {
            $message->delete();
            return response(['is_deleted' => true], Response::HTTP_OK);
        }
    }
}
