<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Contact\ContactNotFoundException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\ContactRequest;
use App\Http\Resources\Contact\ContactCollection;
use App\Http\Resources\Contact\ContactResource;
use App\Model\Contact;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactsController extends Controller
{
    /**
     * applying admin.prems middleware
     */

    public function __construct()
    {
        $this->middleware('admin.perms:cms,CREATE_CMS', ['only' => ['store']]);
        $this->middleware('admin.perms:cms,UPDATE_CMS', ['only' => ['update']]);
        $this->middleware('admin.perms:cms,DELETE_CMS', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->has('type') && !empty($request->get('type'))) {
            $filter['type'] = $request->get('type');
        }
        $contacts = Contact::where($filter)->get();
        $collection = ContactCollection::collection($contacts);
        return ResponseHandler::jsonCollection($collection);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $contact = Contact::create($request->all());
        if (!$contact) {
        } else {
            return response(['contact' => new ContactResource($contact)], Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactNotFoundException
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            throw new ContactNotFoundException;
        }
        return ResponseHandler::json(new ContactResource($contact));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactNotFoundException
     */
    public function update(ContactRequest $request, $id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            throw new ContactNotFoundException;
        } else {
            $contact->update($request->all());
            return response(['contact' => new ContactResource($contact)], Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ContactNotFoundException
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            throw new ContactNotFoundException;
        } else {
            $contact->delete();
            return response(['delete' => 1], Response::HTTP_OK);
        }
    }

    /**
     * get Contacts by type
     * @return dictionary
     */
    public function dictionary()
    {
        $dictionary = [];
        $contacts = Contact::all();
        if (count($contacts)) {
            foreach ($contacts as $contact) {
                $dictionary[$contact->type][] = new ContactResource($contact);
            }
        }
        return ResponseHandler::json($dictionary);
    }
}
