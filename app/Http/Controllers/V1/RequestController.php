<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\DatabaseOperationFailureException;
use App\Exceptions\Deal\DealNotBelongsToShipperException;
use App\Exceptions\Request\RequestHasNoDealsException;
use App\Exceptions\Request\RequestNoAcceptableException;
use App\Exceptions\Request\RequestNotBelongsToUserException;
use App\Exceptions\Request\RequestNotFoundException;
use App\Exceptions\Request\RequestStatusChangeException;
use App\Exceptions\Trip\TripNotFoundException;
use App\Exceptions\User\UserNotFoundException;
use App\Helpers\PaginatorHelper;
use App\Helpers\PushNotificaitionHelper;
use App\Helpers\RequestHelper;
use App\Helpers\RequestLog;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trip\DealRequest;
use App\Http\Resources\Request\DealResource;
use App\Http\Resources\Request\RequestCollection;
use App\Http\Resources\Request\RequestResource;
use App\Model\Deal;
use App\Model\RequestHistory;
use App\Model\Requests;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestController extends Controller
{
    use RequestHelper;
    use UserHelperTrait;

    /**
     * apply auth:api middleware
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws UserNotFoundException
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            // filter requests by status
            $status_enum = ['active' => 'accepted', 'pending' => 'waiting', 'canceled' => 'rejected'];
            $filter = [];
            if ($request->has('status') && null !== $request->get('status') && in_array($request->get('status'), ['active', 'pending', 'canceled'])) {
                $filter['status'] = $status_enum[$request->get('status')];
            }

            $requests = Requests::where(['to' => $user->id])->where($filter)->paginate(5);
            $paginator = PaginatorHelper::getPaginatorData($requests);
            return ResponseHandler::jsonCollection(['data' => RequestCollection::collection($requests), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws RequestNotBelongsToUserException
     * @throws RequestNotFoundException
     */
    public function show(Request $request, $id)
    {
        $show_request = Requests::find($id);
        if (!$show_request) {
            throw new RequestNotFoundException;
        } else {
            $user = $request->user();
            if ($show_request->requestUserCheck($user->id)) {
                throw new RequestNotBelongsToUserException;
            } else {
                return ResponseHandler::json(new RequestResource($show_request));
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * User as Shipper OR Traveller can accept request
     * @param Request $request
     * @param request_id
     * @return response with boolean value
     * @throws DatabaseOperationFailureException
     * @throws RequestNoAcceptableException
     * @throws RequestNotBelongsToUserException
     * @throws RequestNotFoundException
     * @throws RequestStatusChangeException
     */
    public function accept(Request $request, $id)
    {
        $accept_request = Requests::find($id);
        if (!$accept_request) {
            throw new RequestNotFoundException;
        } else {
            $user = $request->user();
            if (!$accept_request->isAcceptable()) {
                throw new RequestNoAcceptableException;
            }
            if ($accept_request->to !== $user->id && $accept_request->by !== $user->id) {
                throw new RequestNotBelongsToUserException;
            }
            if ($accept_request->to !== $user->id) {
                throw new RequestStatusChangeException;
            }
            $acceptance = $accept_request->update(['status' => 'accepted']);
            if (!$acceptance) {
                throw new DatabaseOperationFailureException;
            } else {
                $request_users = $this->getRequestUsers($accept_request);
                $type = $request_users['type'];
                $traveller = $request_users['traveller'];
                $shipper = $request_users['shipper'];
                if ($type == 'shipment' && null !== $shipper && null !== $traveller) {
                    // send notification to shipper
                    $device = $shipper->device;
                    $push_token = $device->push_token;
                    PushNotificaitionHelper::shipperRequestAcceptance($id, $traveller->name, $push_token);
                }

                if ($type == 'trip' && null !== $traveller && null !== $shipper) {
                    // send notification to traveller
                    $device = $traveller->device;
                    $push_token = $device->push_token;
                    PushNotificaitionHelper::travellerRequestAcceptance($id, $shipper->name, $push_token);
                }

                // create deal with null data
                $trip = $accept_request->trip();
                $shipment = $accept_request->shipment();
                if (null !== $trip && null !== $shipment && null != $traveller && null !== $shipper) {
                    $deal = Deal::where('request_id', $id)->first();
                    if (!$deal) {
                        Deal::create(['request_id' => $id, 'trip_id' => $trip->id, 'traveller_id' => $traveller->id, 'shipment_id' => $shipment->id, 'shipper_id' => $shipper->id]);
                    } else {
                        $deal->update(['request_id' => $id, 'trip_id' => $trip->id, 'traveller_id' => $traveller->id, 'shipment_id' => $shipment->id, 'shipper_id' => $shipper->id]);
                    }
                }
                RequestLog::log($id, 'accepted');
                return ResponseHandler::json(['success' => true]);
            }
        }
    }

    /**
     * User as Shipper OR Traveller can reject request
     * @param Request $request
     * @param request_id
     * @return response with boolean value
     * @throws AuthenticationException
     * @throws RequestNotBelongsToUserException
     * @throws RequestNotFoundException
     * @throws RequestStatusChangeException
     */
    public function reject(Request $request, $id)
    {
        $reject_request = Requests::find($id);
        if (!$reject_request) {
            throw new RequestNotFoundException;
        }
        $user = $this->getUserFromRequest($request);
        if ($reject_request->to !== $user->id && $reject_request->by !== $user->id) {
            throw new RequestNotBelongsToUserException;
        }
        if ($reject_request->to !== $user->id) {
            throw new RequestStatusChangeException;
        }
        $request_users = $this->getRequestUsers($reject_request);
        $reject_request->delete();
        $type = $request_users['type'];
        $traveller = $request_users['traveller'];
        $shipper = $request_users['shipper'];
        if ($type == 'shipment' && null !== $traveller && null !== $shipper) {
            // send notification to shipper
            $device = $shipper->device;
            $push_token = $device->push_token;
            PushNotificaitionHelper::shipperRequestRejection($id, $traveller->name, $push_token);
        }

        if ($type == 'trip' && null !== $traveller && null !== $shipper) {
            // send notification to traveller
            $device = $traveller->device;
            $push_token = $device->push_token;
            PushNotificaitionHelper::travellerRequestRejection($id, $shipper->name, $push_token);
        }
        RequestLog::log($id, 'rejected');
        return ResponseHandler::json(['success' => true]);
    }

    /**
     * fill deal confirmation data
     * @param DealRequest $request
     * @param id,pickup_place,pickup_time,delivery_place,delivery_time,price
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws RequestNotFoundException
     * @throws TripNotFoundException
     */
    public function deal(DealRequest $request, $id)
    {
        $deal_request = Requests::find($id);
        if (!$deal_request) {
            throw new RequestNotFoundException;
        }
        // get trip from request then check if belongs to user as Travller in this case
        $user = $request->user();
        $trip = $deal_request->trip();
        if (!$trip) {
            throw new TripNotFoundException;
        }
        $trip->tripUserCheck($user->id);

        // check if request has confirmation data
        $is_exists = Deal::where('request_id', $id)->first();
        // update with data
        if ($is_exists) {
            $is_exists->trip_id = $deal_request->trip_id;
            $is_exists->shipment_id = $deal_request->shipment_id;
            $is_exists->pickup_place = $request->pickup_place;
            $is_exists->pickup_time = $request->pickup_time;
            $is_exists->delivery_place = $request->delivery_place;
            $is_exists->delivery_time = $request->delivery_time;
            $is_exists->price = $request->price;
            if ($deal_request->type == 'trip') {
                $is_exists->shipper_id = $deal_request->by;
                $is_exists->traveller_id = $deal_request->to;
            } else {
                $is_exists->shipper_id = $deal_request->to;
                $is_exists->traveller_id = $deal_request->by;
            }
            $saved = $is_exists->save();
            RequestLog::log($id, 'confirmed');
            return ResponseHandler::json(new DealResource($is_exists));
        } else {
            // create new deal
            $deal = new Deal;
            $deal->request_id = $id;
            $deal->trip_id = $deal_request->trip_id;
            $deal->shipment_id = $deal_request->shipment_id;
            $deal->pickup_place = $request->pickup_place;
            $deal->pickup_time = $request->pickup_time;
            $deal->delivery_place = $request->delivery_place;
            $deal->delivery_time = $request->delivery_time;
            $deal->price = $request->price;
            if ($deal_request->type == 'trip') {
                $deal->shipper_id = $deal_request->by;
                $deal->traveller_id = $deal_request->to;
            } else {
                $deal->shipper_id = $deal_request->to;
                $deal->traveller_id = $deal_request->by;
            }
            $saved = $deal->save();
            RequestLog::log($id, 'confirmed');
            return ResponseHandler::json(new DealResource($deal));
        }
    }

    /**
     * confirm deal by shipper
     * @param Request $request
     * @param shipper_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws DealNotBelongsToShipperException
     * @throws RequestHasNoDealsException
     */
    public function confirm_deal(Request $request, $id)
    {
        $user = $request->user();
        $deal = Deal::where('request_id', $id)->first();
        if (!$deal) {
            throw new RequestHasNoDealsException;
        }

        if ($deal->shipper_id != $user->id) {
            throw new DealNotBelongsToShipperException;
        }
        // @Todo add confirm to request and handle it in transaction
        $deal->is_confirmed = 1;
        $deal->save();
        return ResponseHandler::json(['is_confirmed' => true]);
    }

    /**
     * get user accepted trip requests
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws UserNotFoundException
     */
    public function accepted_trips(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        //Accepted request means( I send request to other user and other user accept it or I receive request from other user and I accept it ) 
        // @todo discuss with sally and fronend hoq actually we'll handle deals
        $accepted = Requests::where(['type' => 'trip', 'status' => 'accepted'])->where('by', $user->id)->get();
        return ResponseHandler::jsonCollection(RequestCollection::collection($accepted));
    }

    /**
     * get user accepted shipments requests
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws UserNotFoundException
     */
    public function accepted_shipments(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        //Accepted request means( I send request to other user and other user accept it or I receive request from other user and I accept it ) 
        // @todo discuss with sally and fronend hoq actually we'll handle deals
        $accepted = Requests::where(['type' => 'shipment', 'status' => 'accepted'])->where('by', $user->id)->get();
        return ResponseHandler::jsonCollection(RequestCollection::collection($accepted));
    }

    /**
     * Log request status
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws AuthenticationException
     * @throws RequestNotFoundException
     */
    public function log(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|in:accepted,negotiation,confirmed,closed,cancelled'
        ]);
        $log_request = Requests::find($id);
        if (!$log_request) {
            throw new RequestNotFoundException;
        }
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $log_request->requestUserCheck($user->id);
        RequestHistory::firstOrCreate(['request_id' => $id, 'status' => $request->get('status')]);
        return ResponseHandler::json(['success' => true]);
    }
}
