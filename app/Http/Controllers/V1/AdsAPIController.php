<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAds\AdminAdsRequest;
use App\Http\Resources\AdminAds\AdminAdsCollection;
use App\Http\Resources\AdminAds\AdminAdsResource;
use App\Model\AdminAds;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdsAPIController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('admin.perms:cms,CREATE_AdminAds', ['only' => ['store']]);
        $this->middleware('admin.perms:cms,UPDATE_AdminAds', ['only' => ['update']]);
        $this->middleware('admin.perms:cms,DELETE_AdminAds', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->has('type') && !empty($request->get('type'))) {
            $filter['type'] = $request->get('type');
        }
        return ResponseHandler::jsonCollection(AdminAdsCollection::collection(AdminAds::where($filter)->get()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminAdsRequest $request
     * @return \Illuminate\Http\Response|string
     */
    public function store(AdminAdsRequest $request)
    {
        $admin_ads = AdminAds::create($request->all());
        if (!$admin_ads) {
            return 'false';
        } else {
            return response(['admin_ads' => new AdminAdsResource($admin_ads)], Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return 'false';
        } else {
            return response(['admin_ads' => new AdminAdsResource($admin_ads)], Response::HTTP_OK);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return 'false';
        } else {
            $admin_ads->update($request->all());
            return response(['admin_ads' => new AdminAdsResource($admin_ads)], Response::HTTP_CREATED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_ads = AdminAds::find($id);
        if (!$admin_ads) {
            return 'false';
        } else {
            $deleted = $admin_ads->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }


}
