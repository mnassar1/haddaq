<?php

namespace App\Http\Controllers\V1;

// firebase handler
use App\Exceptions\SOS\AreaNotCoveredException;
use App\Exceptions\SOS\NoCoveredAreasException;
use App\Helpers\NotificationsCenter;
use App\Helpers\NotificationUser;
use App\Helpers\PointLocation;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\SOSRequest\SOSRequestRequest;
use App\Http\Requests\SOSRequest\SOSRespondRequest;
use App\Http\Resources\SOSRequest\SOSRequestResource;
use App\Http\Resources\SOSRequest\SOSRespondResource;
use App\Model\CoveredArea;
use App\Model\Notifications;
use App\Model\ProblemType;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

// single or multi devices
// topic
// end of firebase handler


class NotificationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param SOSRequestRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SOSRequestRequest $request)
    {
        if ($request->user_id == 0) {
            unset($request['user_id']);
        }

        if ($request->sos_problem_type_id == 0) {
            unset($request['sos_problem_type_id']);
        }
        $pointLocation = new PointLocation();
        $location = $request->location;
        $location = str_replace(' ', '', $location);
        $pieces = $pieces = explode(",", $location);
        $sos_lat = $pieces[0];
        $sos_long = $pieces[1];
        $points = array("$sos_lat $sos_long");
        $all_polygon = CoveredArea::all();
        $polygon_array = [];
        if (!empty($all_polygon)) {
            $i = 0;

            foreach ($all_polygon as $polygon) {
                $polygon = json_decode($polygon->area);


                $count = count($polygon) - 1;
                $first = true;
                $x = 0;
                foreach ($polygon as $poly) {
                    if ($first == true) {
                        $polygon_last = "$poly->lat $poly->lng";
                    }
                    $first = false;
                    $polygon_array[$i][] = "$poly->lat $poly->lng";

                    if ($x == $count) {
                        $polygon_array[$i][] = $polygon_last;
                    }
                    $x++;
                }
                $i++;
            }

        }

        if (!empty($polygon_array)) {
            $polygon = $polygon_array[0];//array("-50 30","50 70","100 50","80 10","110 -10","110 -30","-20 -50","-30 -40","10 -10","-10 10","-30 -20","-50 30");
            // The last point's coordinates must be the same as the first one's, to "close the loop"
            foreach ($points as $key => $point) {
                if ($pointLocation->pointInPolygon($point, $polygon) == 'outside') {
                    //return ResponseHandler::json('Your location not covered yet , thanks');
                    throw new AreaNotCoveredException;
                }
            }
        }

        $sos_request = \App\Model\SOSRequest::create($request->toArray());

        $sos_data = new SOSRequestResource($sos_request);
        //return $sos_data;
        // creata collection into RealTime DB with the request id,
        $firebase_url = env('FCM_URL', "https://haddaq-9e719.firebaseio.com/");
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/haddaq-9e719-be0a1a884e99.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri($firebase_url)
            ->create();

        $database = $firebase->getDatabase();
        $collection_name = config('database.default') . '-sos_request-' . $sos_data->id;

        $sos_data = json_decode(json_encode($sos_data), true);
        $sos_data['collection_name'] = $collection_name;
        $sos_problem_type_details = $sos_data['sos_problem_type_details'];

        $database->getReference($collection_name)->push([
            'id' => $sos_data['id'],
            'user_id' => ($sos_data['user_id']) ? $sos_data['user_id'] : "0",
            'location' => $sos_data['location'],
            'name' => ($sos_data['user_id']) ? $sos_data['user_details']['name'] : "Guest",
            'mobile' => $sos_data['mobile'],//$sos_data->mobile,

        ]);
        $send_data = array(
            'type' => 'new_request',
            'collection_name' => $collection_name,
            'id' => $sos_data['id'],
            'user_id' => ($sos_data['user_id']) ? $sos_data['user_id'] : "0",
            'user_details' => $sos_data['user_details'],
            'name' => ($sos_data['user_id']) ? $sos_data['user_details']['name'] : "Guest",
            'mobile' => $sos_data['mobile'],//$sos_data->mobile,
            'location' => $sos_data['location'],// $sos_data->location,
            'device_udid' => $sos_data['device_udid'],// $sos_data->device_udid,
            'push_token' => $sos_data['push_token'],//$sos_data->push_token,
            'sos_problem_type_id' => $sos_data['sos_problem_type_id'],//$sos_data->sos_problem_type_id,
            'sos_problem_type_details' => $sos_problem_type_details,
            "problem_type" => ($sos_data['sos_problem_type_id']) ? $sos_problem_type_details['problem_type_en'] : "Other",
            "problem_type_ar" => ($sos_data['sos_problem_type_id']) ? $sos_problem_type_details['problem_type_ar'] : "Other",
            'description' => ($sos_data['description']) ? $sos_data['description'] : "",
            'sos_request_status' => 'Opening'
        );
        $notification_center = new NotificationsCenter();
        $not_title = $send_data['name'] . ' Requested SOS Request';
        $sos_problem_type = ProblemType::find($sos_data['sos_problem_type_id']);

        $notification_center->createNotification($send_data['type'], $sos_problem_type->problem_type
            , $send_data['location'], $send_data["collection_name"], $send_data["id"]);

        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message_topic = config('database.default') . "-RegularUsersAndFreeSaviors";
        $message->addRecipient(new Topic($message_topic));

        $notification = new Notification('HaddaQ', 'New SOS Request');
        $notification->setTitle('HaddaQ')
            ->setSound('haddaq.wav')
            ->setBody('New SOS Request');

        $message
            ->setNotification($notification)
            ->setData($send_data);

        $response = $client->send($message);

        return ResponseHandler::json($sos_data);

    }

    public function respond_sos_request(SOSRespondRequest $request)
    {
        $id = $request->sos_request_id;
        $sos_request = \App\Model\SOSRequest::find($id);
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }

        if ($sos_request->sos_request_status == 'Closed') {
            return ResponseHandler::json('Request Closed , Thanks');
        }

        $user_id = $request->user_id;
        $sos_response = \App\Model\SOSResponse::where(['id' => $id, 'user_id' => $user_id])->first();

        if (!empty($sos_response)) {
            return ResponseHandler::json('You Respond To This Request Before , Thanks');
        }
        //return $sos_response;
        $sos_request = \App\Model\SOSResponse::create($request->toArray());
        $collection_name = config('database.default') . '-sos_request-' . $sos_request->id;
        $data = new SOSRespondResource($sos_request);
        $data = json_decode(json_encode($data), true);
        $send_data = array(
            'type' => 'respond_request',
            'id' => $data['id'],
            'user_id' => ($data['user_id']) ? $data['user_id'] : "0",
            'savior_details' => $data['savior_details'],//$data->savior_details,
            'location' => $data['location'],//$data->location,
            'push_token' => $data['push_token'],// $data->push_token,
            'sos_request_id' => $data['sos_request_id'],//$data->sos_request_id,
            'sos_request_details' => $data['sos_request_details'],//$data->sos_request,
            'response_status' => $data['response_status'],//$data->response_status,
        );
        try{
            // return $send_data;
            $server_key = env('FCM_SERVER_KEY', '');

            $client = new Client();
            $client->setApiKey($server_key);
            $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

            $message = new Message();
            $message->setPriority('high');

            $message->addRecipient(new Topic($collection_name));

            $notification = new Notification('HaddaQ', 'Respond To Request');
            $notification->setTitle('HaddaQ')
                ->setSound('haddaq.wav')
                ->setBody('Respond To Request');

            $message
                ->setNotification($notification)
                ->setData($send_data);

            $response = $client->send($message);
        }
        catch (\Exception $e){
            return ResponseHandler::json($e->getMessage());
        }

        return ResponseHandler::json($data);
    }

    public function close_sos_request(Request $request, $id)
    {
        $sos_request = \App\Model\SOSRequest::find($id);
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }

        if ($sos_request->sos_request_status == 'Closed') {
            return ResponseHandler::json('Request Already Closed , Thanks');
        }

        $update_status = array('sos_request_status' => 'Closed', 'closed_by' => 'Owner');
        $sos_request = $sos_request->update($update_status);

        $sos_request = \App\Model\SOSRequest::find($id);
        $data = new SOSRequestResource($sos_request);
        $collection_name = config('database.default') . '-sos_request-' . $data->id;
        $sos_data = json_decode(json_encode($data), true);
        $sos_data['collection_name'] = $collection_name;
        $sos_problem_type_details = $sos_data['sos_problem_type_details'];


        $send_data = array(
            'type' => 'close_request',
            'collection_name' => $collection_name,
            'id' => $sos_data['id'],
            'user_id' => ($sos_data['user_id']) ? $sos_data['user_id'] : "0",
            'location' => $sos_data['location'],// $sos_data->location,
            'device_udid' => $sos_data['device_udid'],// $sos_data->device_udid,
            'push_token' => $sos_data['push_token'],//$sos_data->push_token,
            'sos_problem_type_id' => $sos_data['sos_problem_type_id'],//$sos_data->sos_problem_type_id,
            'sos_problem_type_details' => $sos_problem_type_details,
            'description' => ($sos_data['description']) ? $sos_data['description'] : "",
            'sos_request_status' => "Closed",// $sos_data['sos_problem_type_id']
        );
        //return $send_data;
        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic($collection_name));

        $notification = new Notification('HaddaQ', 'Close SOS Request');
        $notification->setTitle('HaddaQ')
            ->setBody('Close SOS Request');

        $message->setNotification($notification)
            ->setData($send_data);

        NotificationUser::removeFromList(Notifications::where("item_id", $id)->first()['id']);
        NotificationsCenter::removeFromList("new_request", $id);
        $response = $client->send($message);

        return ResponseHandler::json($data);
    }

    public function sos_request_details(Request $request, $id)
    {

        $sos_request = \App\Model\SOSRequest::where("id", $id)->where("sos_request_status", "Opening")->first();
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }
        // return new SOSRequestResource($sos_request);
        $data = new SOSRequestResource($sos_request);
        return ResponseHandler::json($data);
    }
}
