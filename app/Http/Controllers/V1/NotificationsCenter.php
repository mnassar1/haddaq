<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationsCenter\NotificationsCenterCollection;
use App\Model\Notifications;
use App\Model\NotificationUser;
use Illuminate\Http\Request;

class NotificationsCenter extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $page_number
     * @param $perpage
     * @return \Illuminate\Http\Response
     */
    public function get_notificationsCenter(Request $request, $page_number, $perpage, $user_id = null)
    {
        $offset = $page_number * $perpage;
        $notification = new Notifications();

        $total = $notification::count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);

        $main_sliders = $notification::orderby('created_at', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();

        $response = NotificationsCenterCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function updateNotificationStates($notification_id, $user_id)
    {
        $notificationUser = new NotificationUser();
        $notificationUser->user_id = $user_id;
        $notificationUser->notification_id = $notification_id;
        $notificationUser->save();
        $notification = new Notifications();
        $output = ["status" => "done","count" => $this->getNotificationCount()['count']];
        return $output;
    }

    public function getNotificationCount()
    {
        $notification = new Notifications();
        $total = $notification::where('type','!=','admin_push')->count();
        $user_notification_count = $notification->getUserNotificationCount();
        $unread_count = $total - $user_notification_count;
        if($total <  $user_notification_count) $unread_count = $total;
        return ["count" => $unread_count];
    }
}
