<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\CMS\CMSActionNotCompletedException;
use App\Exceptions\CMS\CMSNotFoundException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\CMSRequest;
use App\Http\Resources\Cms\CmsCollection;
use App\Http\Resources\Cms\CmsResource;
use App\Model\CMS;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CMSController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('admin.perms:cms,CREATE_CMS', ['only' => ['store']]);
        $this->middleware('admin.perms:cms,UPDATE_CMS', ['only' => ['update']]);
        $this->middleware('admin.perms:cms,DELETE_CMS', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->has('type') && !empty($request->get('type'))) {
            $filter['type'] = $request->get('type');
        }
        return ResponseHandler::jsonCollection(CmsCollection::collection(CMS::where($filter)->get()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CMSRequest $request
     * @return \Illuminate\Http\Response
     * @throws CMSActionNotCompletedException
     */
    public function store(CMSRequest $request)
    {
        $cms = CMS::create($request->all());
        if (!$cms) {
            throw new CMSActionNotCompletedException;
        } else {
            return response(['cms' => new CmsResource($cms)], Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws CMSNotFoundException
     */
    public function show($id)
    {
        $cms = Cms::find($id);
        if (!$cms) {
            throw new CMSNotFoundException;
        } else {
            return response(['cms' => new CmsResource($cms)], Response::HTTP_OK);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws CMSNotFoundException
     */
    public function update(Request $request, $id)
    {
        $cms = CMS::find($id);
        if (!$cms) {
            throw new CMSNotFoundException;
        } else {
            $cms->update($request->all());
            return response(['cms' => new CmsResource($cms)], Response::HTTP_CREATED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws CMSNotFoundException
     */
    public function destroy($id)
    {
        $cms = CMS::find($id);
        if (!$cms) {
            throw new CMSNotFoundException;
        } else {
            $deleted = $cms->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }

    /**
     * get cms by name
     * @param $type
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws CMSNotFoundException
     */
    public function by_type($type)
    {
        $cms = CMS::where('type', $type)->first();
        if (!$cms) {
            throw new CMSNotFoundException;
        }
        return ResponseHandler::json(new CmsResource($cms));
    }
}
