<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Register\RegisterationNotCompletedException;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Register\RegisterPhoneRequest;
use App\Http\Requests\Register\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Model\Device;
use App\PasswordResets;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    use UserHelperTrait;

    public function index(RegisterRequest $request){
        if($request->user_types_id === 3){
            return $this->email($request);
        }
        else if($request->user_types_id !== 3){
            return $this->phone($request);
        }
    }

    /**
     * registering new user using email and passowrd.
     * @param RegisterRequest $request
     * @return json with profile object.
     * @throws RegisterationNotCompletedException
     */
    public function email($request)
    {
        $salt = "";
        $request['password'] = $this->generateHashedPassword($request->password, $salt);
        //$request['salt'] = $salt;
        $verification_code = str_random(32);
        $request['verification_code'] = $verification_code;
        $user = User::create($request->toArray());
        PasswordResets::create(['email' => $user->email, 'token' => Str::random(60)]);
        if ($user) {
            $device = $this->createDevice($request, $user->id);
            //NotificationSettings::create(['user_id' => $user->id]);
            if ($device) {
                // @todo handle send verification email
                $user->sendVerificationEmail();
                return ResponseHandler::json(new UserResource($user));
            }
        }
        throw new RegisterationNotCompletedException;
    }

    /**
     * create device after registeraing user.
     * @param $request
     * @param $user_id
     * @return boolean
     */
    private function createDevice($request, $user_id)
    {
        $request['user_id'] = $user_id;
        $device = Device::where('user_id', $user_id)->orderBy('id', 'Desc')->first();
        // if device already registerd unmatch it and match with new user_id
        if ($device) {
            $update = $device->update($request->toArray());
            if ($update) {
                return true;
            }
        } else {
            $new_device = Device::create($request->toArray());
            if ($new_device) {
                return true;
            }
        }
        return false;
    }

    //function to send sms

    /**
     * registering new user using email and passowrd.
     * @param RegisterPhoneRequest $request
     * @return json with profile object.
     * @throws RegisterationNotCompletedException
     */
    public function phone($request)
    {
        $salt = "";
        $request['password'] = $this->generateHashedPassword($request->password, $salt);
        //$request['salt'] = $salt;
        $verification_code = str_random(32);
        $request['verification_code'] = $verification_code;
        $request['phone_with_code'] = $request->country_code.$request->mobile;
        $user = User::create($request->toArray());
        if ($user) {
            $device = $this->createDevice($request, $user->id);

            if ($device) {
                // @todo handle send verification email
                if ($user->user_types_id == 3) {
                    $user->sendVerificationEmail();
                } else {
                    // send sms to verify mobile
                    $verification_code = random_int(1000, 9999);
                    $user->mobile_verification_code = $verification_code;
                    $user->phone_with_code = $request->country_code.$request->mobile;
                    $user->is_mobile_verified = 0;
                    $saved = $user->save();
                    if (!$saved) {
                        return ResponseHandler::json(['verification_code_sent' => false]);
                    }
                    // @todo send sms to user's phone number

                    $phone = $user->mobile;
                    $country_code = $user->country_code;
                    $mobile = '+' . $country_code . $phone;
                    $phone = ltrim($mobile, '+');  //substr($mobile, 1);
                    $msg = $verification_code . " is your HaddaQ verification code";
                    $this->send_sms($mobile, $msg);

                    //

                }
                return ResponseHandler::json(new UserResource($user));
            }
        }
        throw new RegisterationNotCompletedException;

    }

    function send_sms($phone, $msg)
    {

        $curl_url = "http://62.215.226.174/fccsms_P.aspx?UID=haddaquser&P=haddaqusr11&S=Haddaq&G=" . urlencode($phone) . "&M=" . urlencode($msg) . "&L=L";
        //return  $curl_url;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: e59cf561-9334-a63a-5020-172c93c6ca82"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
        } else {
            //echo $response;
        }

    }

    public function verify($token)
    {
        $user = User::where('verification_code', $token)->firstOrFail();
        $user->update(['verification_code' => null, 'is_verified' => 1]);
        return view('admin.verify');
    }

    public function send()
    {
        Mail::send([], ['name' => 'Ahmed'], function ($message) {
            $message->to('ahmedmarzouk2050@gmail.com', 'HaddaQ test')->subject('Test email');
            $message->from('ahmedmarzouk2050@gmail.com', 'HaddaQ');
        });
    }
}
