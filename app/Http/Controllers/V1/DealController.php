<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Deal\DealAlreadyPaidException;
use App\Exceptions\Deal\DealNotBelongsToShipperException;
use App\Exceptions\Deal\DealNotFoundException;
use App\Exceptions\ImageNotFoundException;
use App\Exceptions\InvalidQRCodeException;
use App\Exceptions\Profile\UserHasNotPaymentDataException;
use App\Exceptions\Request\RequestStatusChangeException;
use App\Helpers\PushNotificaitionHelper;
use App\Helpers\RequestHelper;
use App\Helpers\RequestLog;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Deal\DealCollection;
use App\Http\Resources\Deal\DealNotesResource;
use App\Http\Resources\Request\DealResource;
use App\Model\Deal;
use App\Model\DealImages;
use App\Model\DealNotes;
use App\Model\DealPayment;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class DealController extends Controller
{
    use UserHelperTrait;
    use RequestHelper;

    /**
     * Apply auth:api middleware
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * list all user deals
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $deals = $user->deals();
        return ResponseHandler::jsonCollection(['data' => DealCollection::collection($deals)]);
    }

    /**
     * show deal
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     */
    public function show(Request $request, $id)
    {
        $deal = Deal::find($id);
        $user = $this->getUserFromRequest($request);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $deal->dealUserCheck($user->id);
        return ResponseHandler::json(new DealResource($deal));
    }

    /**
     * add image to deal
     * @param Request $request
     * @param id,image_url
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws DealNotFoundException
     */
    public function add_image(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|string'
        ]);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $image = New DealImages;
        $image->deal_id = $id;
        $image->image = $request->get('image');
        $image->caption = $request->has('caption') ? $request->get('caption') : "";
        $image->save();
        return ResponseHandler::json(new DealResource($deal));
    }

    /**
     * list deal's images
     * @param Request $request
     * @param id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     */
    public function list_images(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $images = $deal->images;
        return ResponseHandler::jsonCollection($images);
    }

    /**
     * delete image to deal
     * @param Request $request
     * @param deal_id,image_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     * @throws ImageNotFoundException
     */
    public function delete_image(Request $request, $id)
    {
        $request->validate([
            'image_id' => 'required|integer'
        ]);
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $deal->dealUserCheck($user->id);
        $image = DealImages::where(['id' => $request->get('image_id'), 'deal_id' => $id])->first();
        if (!$image) {
            throw new ImageNotFoundException;
        }
        $image->delete();
        return ResponseHandler::json(['is_deleted' => true]);
    }

    /**
     * update status
     * @param Request $request
     * @param deal_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     */
    public function update_status(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|string|in:accepted,negotiation,payment,confirmed,delegated,shipped,delivered,cancelled,waiting'
        ]);
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $deal->dealUserCheck($user->id);
        $deal->status = $request->get('status');
        $deal->save();
        RequestLog::log($deal->request->id, $request->get('status'));
        $traveller = $deal->traveller;
        $shipper = $deal->shipper;
        if ($traveller->id != $user->id) {
            $device = $traveller->device;
            if ($device) {
                $push_token = $device->push_token;
                if ($push_token) {
                    PushNotificaitionHelper::dealStatusChanged($id, $request->get('status'), $push_token);
                }
            }
        } else {
            $device = $shipper->device;
            if ($device) {
                $push_token = $device->push_token;
                if ($push_token) {
                    PushNotificaitionHelper::dealStatusChanged($id, $request->get('status'), $push_token);
                }
            }
        }
        return ResponseHandler::json(new DealResource($deal));
    }

    /**
     * close deal
     * @param Request $request
     * @param id,qr_code_string
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     * @throws InvalidQRCodeException
     * @throws RequestStatusChangeException
     */
    public function close(Request $request, $id)
    {
        $request->validate([
            'qr_code_string' => 'required|string'
        ]);
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        if ($deal->traveller_id !== $user->id) {
            throw new RequestStatusChangeException;
        }

        if ($deal->qr_code_string !== $request->get('qr_code_string')) {
            throw new InvalidQRCodeException;
        }
        $deal->status = 'closed';
        $deal->save();
        RequestLog::log($deal->request->id, 'closed');

        // set trip is_finished to 1
        $trip = $deal->trip;
        if ($trip) {
            $trip->setAsFinished();
        }

        // set shipment is_finished to 1
        $shipment = $deal->shipment;
        if ($shipment) {
            $shipment->setAsFinished();
        }

        $deal_request = $deal->request;
        if (null !== $deal_request) {
            $request_users = $this->getRequestUsers($deal_request);
            if (null !== $request_users['shipper']) {
                $shipper = $request_users['shipper'];
                $device = $shipper->device;
                $push_token = $device->push_token;
                PushNotificaitionHelper::dealClosed($id, $push_token);
            }
        }
        return ResponseHandler::json(new DealResource($deal));
    }

    /**
     * add note to deal
     * @param Request $request
     * @param deal_id,note,type
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function note(Request $request, $id)
    {
        $request->validate([
            'note' => 'required|string',
            'type' => 'required|in:freight_number,text'
        ]);
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        $deal->dealUserCheck($user->id);
        $request['deal_id'] = $id;
        $note = DealNotes::create($request->all());
        return ResponseHandler::json(new DealNotesResource($note));
    }

    /**
     * list deal's notes
     * @param Request $request
     * @param deal_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function list_notes(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        $deal->dealUserCheck($user->id);
        $notes = $deal->notes;
        return ResponseHandler::jsonCollection(DealNotesResource::collection($notes));
    }

    /**
     * generate qr_code_string
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function generate_qr(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        $deal->dealUserCheck($user->id);
        $qr_code_string = str_random(6);
        $deal->qr_code_string = $qr_code_string;
        $deal->save();
        $deal_request = $deal->request;
        RequestLog::log($deal_request->id, 'closed');
        return ResponseHandler::json(new DealResource($deal));
    }

    /**
     * confirm deal
     * this action should be done by shipper.
     * @param Request $request
     * @param id,shipper_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotBelongsToShipperException
     * @throws DealNotFoundException
     */
    public function confirm(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        if ($deal->shipper_id !== $user->id) {
            throw new DealNotBelongsToShipperException;
        }
        $deal->status = 'shipper_confirmed';
        $deal->save();
        return ResponseHandler::json(['is_confirmed' => true]);
    }

    /**
     * do payemtn by deduct
     * this action should be done by shipper
     * @param Request $request
     * @param id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealAlreadyPaidException
     * @throws DealNotBelongsToShipperException
     * @throws DealNotFoundException
     */
    public function do_payment(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }

        if ($deal->shipper_id !== $user->id) {
            throw new DealNotBelongsToShipperException;
        }

        if ($deal->status == 'payment' && $deal->status !== 'shipper_confirmed') {
            throw new DealAlreadyPaidException;
        }

        $traveller = $deal->traveller;
        $shipper = $deal->shipper;
        if ($traveller && $shipper) {

            // handle payment amount here
            $amount = $this->calculate_commission($deal->price);
            $deal_payment = DealPayment::where(['deal_id' => $id, 'from_user' => $traveller->id, 'to_user' => $shipper->id])->first();
            if ($deal_payment) {
                $deal_payment->update(['deal_id' => $id, 'from_user' => $traveller->id, 'to_user' => $shipper->id, 'amount' => $amount]);
            } else {
                $deal_payment = DealPayment::create(['deal_id' => $id, 'from_user' => $traveller->id, 'to_user' => $shipper->id, 'amount' => $amount]);
            }

            // update status to payment
            if ($deal_payment) {
                $deal->status = 'payment';
            }

            $device = $traveller->device;
            if ($device) {
                $push_token = $device->push_token;
                PushNotificaitionHelper::shipperPayMoney($id, $push_token, $shipper->name, $amount);
            }

            // send email to traveller
            $traveller->sendPaymentConfirmationEmail($shipper, $deal);
        }

        return ResponseHandler::json(['success' => true]);
    }

    /**
     * make a transaction
     * @param Request $request
     * @param id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws DealNotFoundException
     * @throws UserHasNotPaymentDataException
     */
    public function make_transfer(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $deal = Deal::find($id);
        if (!$deal) {
            throw new DealNotFoundException;
        }
        $deal->dealTravellerCheck($user->id);
        $payment_data = $user->payment_data;
        if (!$payment_data) {
            throw new UserHasNotPaymentDataException;
        }
        $deal_payment = DealPayment::where(['deal_id' => $id, 'to_user' => $user->id])->first();
        //@ TODO handle transaction here

        return ResponseHandler::json(['success' => true]);
    }
}
