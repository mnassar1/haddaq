<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Country\CountryNotFoundException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\Country\CountryResource;
use App\Model\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  country $phonecode
     * @return \Illuminate\Http\Response
     * @throws CountryNotFoundException
     */
    public function show($phonecode)
    {
        $country = Country::where('phonecode', $phonecode)->first();
        if (!$country) {
            throw new CountryNotFoundException;
        } else {
            return ResponseHandler::json(new CountryResource($country));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * get cities by country_id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws CountryNotFoundException
     */
    public function getCities(Request $request)
    {
        // dd($request);
        $country = Country::where('id', $request->country_id)->first();
        if (!$country) {
            throw new CountryNotFoundException;
        } else {
            return ResponseHandler::json(new CountryResource($country));
        }
    }
}
