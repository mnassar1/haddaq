<?php

namespace App\Http\Controllers\V1;

// firebase handler
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\SOSRequest\SOSRequestRequest;
use App\Http\Requests\SOSRequest\SOSRespondRequest;
use App\Http\Resources\SOSRequest\SOSRequestResource;
use App\Http\Resources\SOSRequest\SOSRespondResource;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;

// single or multi devices
// topic
// end of firebase handler


class FirebaseController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth:api')->except(['respond_sos_request']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param SOSRequestRequest $request
     * @return array
     */
    public function index(SOSRequestRequest $request)
    {
        if ($request->user_id == 0) {
            unset($request['user_id']);
        }
        //return $request->toArray();
        $sos_request = \App\Model\SOSRequest::create($request->toArray());

        $data = new SOSRequestResource($sos_request);

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/haddaq-9e719-9317d6b6f017.json');

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://haddaq-9e719.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();
        $collection_name = 'sos_request-' . $data->id;
        $newPost = $database->getReference($collection_name)->push([
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'location' => $data->location,
            //'device_udid' => $data->device_udid,
            //'push_token' => $data->push_token,
            //'sos_problem_type_id' => $data->sos_problem_type_id,
            //'sos_problem_type_details'=> $data->sos_problem_type_details,
            //'description' => ($data->description) ? $data->description : "",
            //'sos_request_status' => 'Opening',

        ]);


        print_r($newPost->getvalue());

        $send_data = array(
            'collection_name' => $collection_name,
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'location' => $data->location,
            'device_udid' => $data->device_udid,
            'push_token' => $data->push_token,
            'sos_problem_type_id' => $data->sos_problem_type_id,
            'sos_problem_type_details' => $data->sos_problem_type_details,
            'description' => ($data->description) ? $data->description : "",
            'sos_request_status' => 'Opening',
        );
        return $send_data;
        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
        $message
            ->setNotification(new Notification('HaddaQ', 'New SOS Request'))
            ->setData($send_data);

        $response = $client->send($message);

        return ResponseHandler::json($data);

    }


    public function respond_sos_request(SOSRespondRequest $request)
    {
        $id = $request->sos_request_id;
        $sos_request = \App\Model\SOSRequest::find($id);
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }

        if ($sos_request->sos_request_status == 'Closed') {
            return ResponseHandler::json('Request Closed , Thanks');
        }

        $user_id = $request->user_id;
        $sos_response = \App\Model\SOSResponse::where('id', $id)->where('user_id', $user_id)->get();
        //return $sos_response;
        if (!empty($sos_response)) {
            return ResponseHandler::json('You Respond To This Request Before , Thanks');
        }

        $sos_request = \App\Model\SOSResponse::create($request->toArray());

        $data = new SOSRespondResource($sos_request);
        $send_data = array(
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'savior_details' => $data->savior_details,
            'location' => $data->location,
            'push_token' => $data->push_token,
            'sos_request_id' => $data->sos_request_id,
            'sos_request_details' => $data->sos_request,
            'response_status' => $data->response_status,
        );

        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
        $message
            ->setNotification(new Notification('HaddaQ', 'Respond To Request'))
            ->setData($send_data);

        $response = $client->send($message);
        return ResponseHandler::json($data);

        // return ResponseHandler::json('Thanks for Your Response'); 

    }


    public function close_sos_request(Request $request, $id)
    {

        $sos_request = \App\Model\SOSRequest::find($id);
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }

        if ($sos_request->sos_request_status == 'Closed') {
            return ResponseHandler::json('Request Already Closed , Thanks');
        }

        $update_status = array('sos_request_status' => 'Closed');
        $sos_request = $sos_request->update($update_status);

        $sos_request = \App\Model\SOSRequest::find($id);
        $data = new SOSRequestResource($sos_request);

        $send_data = array(
            'id' => $data->id,
            'user_id' => ($data->user_id) ? $data->user_id : "",
            'location' => $data->location,
            'device_udid' => $data->device_udid,
            'push_token' => $data->push_token,
            'sos_problem_type_id' => $data->sos_problem_type_id,
            'sos_problem_type_details' => $data->sos_problem_type_details,
            'description' => ($data->description) ? $data->description : "",
            'sos_request_status' => $data->sos_request_status,
        );

        $server_key = env('FCM_SERVER_KEY', '');

        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Topic('RegularUsersAndFreeSaviors'));
        $message
            ->setNotification(new Notification('HaddaQ', 'Close SOS Request'))
            ->setData($send_data);

        $response = $client->send($message);

        return ResponseHandler::json($data);
    }

    public function sos_request_details(Request $request, $id)
    {

        $sos_request = \App\Model\SOSRequest::find($id);
        if (!$sos_request) {
            return ResponseHandler::json('false');
        }
        // return new SOSRequestResource($sos_request);
        $data = new SOSRequestResource($sos_request);
        return ResponseHandler::json($data);
    }


}
