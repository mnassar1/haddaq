<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\AppAds\ApiAppAdsResource;
use App\Http\Resources\AppAds\AppAdsCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Home\CompanyCollection;
use App\Http\Resources\Home\CompanyProfileResource;
use App\Http\Resources\Home\MainSlidersCollection;
use App\Http\Resources\Home\MainSlidersResource;
use App\Model\AppAds;
use App\Model\Category;
use App\Model\Domains;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = [
            'home/main_sliders/',
            'home/offers_sliders/',
            'home/new_items/',
            'home/first_banners/',
            'home/gold_companies/',
            'home/second_banners/',
            'home/silver_companies/',
            'home/third_banners/',
            'home/bronze_companies/',
        ];
        return ResponseHandler::json($response);
    }


    public function weather($location = '29.378586,47.990341')
    {
        $url = "api.worldweatheronline.com/premium/v1/marine.ashx?key=8c1196eadd1f490b96a102246192802&format=json&q=".$location."&tide=yes";

        // Get Tide

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $data = curl_exec($ch);
        curl_close($ch);
        $data_json = json_decode($data, true);

        $data_json = $data_json["data"]["weather"][0];
        $weather = [];
        $weather["highest_temp"] = $data_json["maxtempC"];
        $weather["lowest_temp"] = $data_json["mintempC"];

        $hourly = $data_json["hourly"][0];

        $weather["wind_speed"] = $hourly["windspeedKmph"];
        $weather["sea_waves_by_day"] = $hourly["swellHeight_ft"];
        $weather["sea_waves_by_night"] = $hourly["swellHeight_ft"];


        $tide = $data_json["tides"][0]["tide_data"];


        if (count($tide) == 4) {
            $weather["first_high_tide"] = $this->metersToFeet($tide[0]["tideHeight_mt"]);
            $weather["second_high_tide"] = $this->metersToFeet($tide[2]["tideHeight_mt"]);
            $weather["first_low_tide"] = $this->metersToFeet($tide[1]["tideHeight_mt"]);
            $weather["second_low_tide"] = $this->metersToFeet($tide[3]["tideHeight_mt"]);
        } else {
            $weather["first_high_tide"] = "";
            $weather["second_high_tide"] = "";
            $weather["first_low_tide"] = "";
            $weather["second_low_tide"] = "";
        }
        $weather["date"] = Carbon::today()->toDateString();
        $arr = array();
        $arr["status_code"] = 200;
        $arr["response"] = $weather;
        return $arr;
    }

    function metersToFeet($meters) {
        return (int)($meters * 3.280839895);
    }

    public function users_categories()
    {
        $cats = Category::where("category_type", 1)->get();
        return $cats;
    }

    public function companies_and_categories(Request $request)
    {
        $url = explode('.', parse_url($request->url(), PHP_URL_HOST));
        $sub_domain = $url[0];
        $domain = Domains::where('iso_code', $sub_domain)->first();
        $country_code = null;
        if ($domain) {
            $country_code = $domain->phone_code;
        }

        $companies = User::where(['user_types_id' => 3, 'is_verified' => 1, 'is_blocked' => 0, 'country_code' => $country_code])
            ->get();
        $new_child = array();

        //$final = 0;
        if (!empty($companies)) {
            $i = 0;
            foreach ($companies as $company) {
                $new_child[$i]['company_details'] =
                    array('id' => $company->id,
                        'name' => $company->full_name);

                $items = AppAds::where(['approved_status' => 1])
                    ->whereIn('company_id', $company)->groupBy('category_id')->pluck('category_id');
                if (count($items) > 0) {
                    foreach ($items as $item) {
                        $item_details = Category::find($item);
                        if ($item_details) {
                            $new_child[$i]['company_details']['categories'][] = array('id' => $item_details->id,
                                'category_name' => $item_details->category_name,
                                'category_name_ar' => $item_details->category_name_ar,
                                'icon' => ($item_details->icon != NULL || $item_details->icon != "") ? asset("storage/" . $item_details->icon) : "",
                            );
                        }
                    }
                } else {
                    $new_child[$i]['company_details']['categories'] = array();
                }
                $i++;
            }
        }
        $final = $i++;
        $new_child[$final]['company_details'] =
            array('id' => 0,
                'name' => "all");

        $items = Category::where('category_type', 3)->pluck('id');
        foreach ($items as $item) {
            $item_details = Category::find($item);
            if ($item_details) {
                $new_child[$final]['company_details']['categories'][] = array('id' => $item_details->id,
                    'category_name' => $item_details->category_name,
                    'category_name_ar' => $item_details->category_name_ar,
                    'icon' => ($item_details->icon != NULL || $item_details->icon != "") ? asset("storage/" . $item_details->icon) : "",
                );
            }
        }
        return ResponseHandler::json($new_child);
    }


    public function company_categories($company_id)
    {


        $company_details = User::where(['id' => $company_id, 'user_types_id' => 3, 'is_verified' => 1, 'is_blocked' => 0])
            ->first();
        $new_child = array();

        //$final = 0;
        if (!$company_details) {
            return ResponseHandler::json([false]);
        }
        $i = 0;

        $new_child[$i]['company_details'] =
            array('id' => $company_details->id,
                'name' => $company_details->full_name);

        $items = AppAds::where(['approved_status' => 1])
            ->where('company_id', $company_id)->groupBy('category_id')->pluck('category_id');

        if (count($items) > 0) {
            foreach ($items as $item) {
                $item_details = Category::find($item);
                if ($item_details) {
                    $new_child[$i]['company_details']['categories'][] = array('id' => $item_details->id,
                        'category_name' => $item_details->category_name,
                        'category_name_ar' => $item_details->category_name_ar,
                        'icon' => ($item_details->icon != NULL || $item_details->icon != "") ? asset("storage/" . $item_details->icon) : "",
                    );
                }
            }
        } else {
            $new_child[$i]['company_details']['categories'] = array();
        }


        return ResponseHandler::json($new_child);
    }

    public function main_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 1])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 1])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function main_slider_show($id)
    {
        $slider = \App\Model\AdminAds::find($id);
        if (!$slider) {
            return ResponseHandler::json([false]);
        } else {
            $count = $slider->views_count + 1;
            $slider->update(['views_count' => $count]);
            return ResponseHandler::json(new MainSlidersResource($slider));
        }
    }


    public function offers_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 2])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 2])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function new_items($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $categories = Category::where('category_type', '<>', 1)->pluck('id');
        $companies = User::where(['user_types_id' => 3, 'is_verified' => 1, 'is_blocked' => 0])->pluck('id');

        $total = AppAds::where(['approved_status' => 1])
            ->whereIn('company_id', $companies)
            ->whereIn('category_id', $categories)->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = AppAds::where(['approved_status' => 1])
            ->whereIn('company_id', $companies)
            ->whereIn('category_id', $categories)
            ->orderby('created_at', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = AppAdsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function search_in_new_items(Request $request, $page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $categories = Category::where('category_type', '<>', 2)->pluck('id');
        $companies = User::where(['user_types_id' => 3, 'is_verified' => 1, 'is_blocked' => 0])->pluck('id');

        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $title = (isset($filter['simple']['full_name'])) ? $filter['simple']['full_name'] : NULL;
        $company_id = (isset($filter['simple']['company_id'])) ? $filter['simple']['company_id'] : NULL;
        $category_id = (isset($filter['simple']['category_id'])) ? $filter['simple']['category_id'] : NULL;

        $price_from = (isset($filter['simple']['price_from'])) ? $filter['simple']['price_from'] : NULL;

        $price_to = (isset($filter['simple']['price_to'])) ? $filter['simple']['price_to'] : NULL;

//        $search_type = (isset($filter['simple']['search_type '])) ? $filter['simple']['search_type '] : NULL;
        $search_type = $request->get("search_type");
        unset($simple_filter['full_name']);
        unset($simple_filter['category_id']);
        unset($simple_filter['company_id']);
        unset($simple_filter['price_from']);
        unset($simple_filter['price_to']);
        unset($simple_filter['search_type']);

        $total = AppAds::where($simple_filter);
        $total = $total->where(['approved_status' => 1])
            ->whereIn('category_id', $categories);

        if ($search_type == "0" or $search_type = null) {
            $total = $total->whereIn('company_id', $companies);
        } elseif ($search_type == "1") {
            $total = $total->whereNotIn('company_id', $companies);
        }

        if ($title != NULL) {
            $total = $total->where('title', 'LIKE', '%' . $title . '%');
        }
        if ($company_id != NULL && $company_id != 0) {
            $total = $total->where('company_id', $company_id);
        }

        if ($category_id != NULL && $category_id != 0) {
            $total = $total->where('category_id', $category_id);
        }

        if ($price_from != NULL && $price_to != NULL) {
            $total = $total->whereBetween('price', [$price_from, $price_to]);
        }


        $total = $total->count();
        $search_type = $request->get("search_type");
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = AppAds::where($simple_filter);

        if ($title != NULL) {
            $main_sliders = $main_sliders->where('title', 'LIKE', '%' . $title . '%');
        }
        if ($company_id != NULL && $company_id != 0) {
            $main_sliders = $main_sliders->where('company_id', $company_id);
        }

        if ($category_id != NULL && $category_id != 0) {
            $main_sliders = $main_sliders->where('category_id', $category_id);
        }

        if ($price_from != NULL && $price_to != NULL) {
            $main_sliders = $main_sliders->whereBetween('price', [$price_from, $price_to]);
        }


        $main_sliders = $main_sliders->where(['approved_status' => 1])
            ->whereIn('category_id', $categories)
            ->orderby('created_at', 'desc');
        if ($search_type == "0" or $search_type == null) {
            $main_sliders = $main_sliders->whereIn('company_id', $companies);
        } elseif ($search_type == "1") {
            $main_sliders = $main_sliders->whereNotIn('company_id', $companies);
        }
        $main_sliders = $main_sliders->skip($offset)
            ->take($perpage)
            ->get();
        $response = AppAdsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('name') && $request->get('name')) {
            $filter['simple']['full_name'] = $request->get('name');
        }

        if ($request->has('company_type_id') && null !== $request->get('company_type_id')) {
            $filter['simple']['company_type_id'] = $request->get('company_type_id');
        }

        if ($request->has('company_id') && null !== $request->get('company_id')) {
            $filter['simple']['company_id'] = $request->get('company_id');
        }

        if ($request->has('category_id') && null !== $request->get('category_id')) {
            $filter['simple']['category_id'] = $request->get('category_id');
        }

        if ($request->has('price_from') && null !== $request->get('price_from')) {
            $filter['simple']['price_from'] = $request->get('price_from');
        }


        if ($request->has('price_to') && null !== $request->get('price_to')) {
            $filter['simple']['price_to'] = $request->get('price_to');
        }

        if ($request->has('search_type') && null !== $request->get('search_type')) {
            $filter['simple']['search_type'] = $request->get('search_type');
        }


        return $filter;
    }

    public function ads_details($id, $user_id = NULL)
    {
        $ads_details = AppAds::find($id);

        if (!$ads_details) {
            return ResponseHandler::json([false]);
        } else {

            $count = $ads_details->views_count + 1;
            $ads_details->update(['views_count' => $count]);
            $ads_details['user_id'] = $user_id;
            $response = new ApiAppAdsResource($ads_details);
            //return $response;
            //AppAdsCollection::collection($ads_details);

            return ResponseHandler::json($response);
        }
    }

    public function company_details($id, $user_id = NULL)
    {
        $company_details = User::find($id);

        if (!$company_details) {
            return ResponseHandler::json([false]);
        } else {
            $company_details['user_id'] = $user_id;
            $response = new CompanyProfileResource($company_details);
            return ResponseHandler::json($response);
        }
    }

    public function first_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 3])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 3])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function gold_companies($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $total = User::where(['user_types_id' => 3,
            'company_type_id' => 1, 'is_verified' => 1, 'is_blocked' => 0])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = User::where(['user_types_id' => 3,
            'company_type_id' => 1, 'is_verified' => 1, 'is_blocked' => 0])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = CompanyCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function search_in_companies(Request $request, $page_number, $perpage)
    {

        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $full_name = (isset($filter['simple']['full_name'])) ? $filter['simple']['full_name'] : NULL;
        unset($simple_filter['full_name']);

        $total = User::where($simple_filter);

        if ($full_name != NULL) {
            $total = $total->where('full_name', 'LIKE', '%' . $full_name . '%');
        }

        $total = $total->where(['user_types_id' => 3,
            'is_verified' => 1, 'is_blocked' => 0])
            ->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $offset = $page_number * $perpage;
        $current_page = (int)$page_number;
        $main_sliders = User::where($simple_filter)
            ->where(['user_types_id' => 3,
                'is_verified' => 1, 'is_blocked' => 0]);
        if ($full_name != NULL) {
            $main_sliders = $main_sliders->where('full_name', 'LIKE', '%' . $full_name . '%');
        }
        $main_sliders = $main_sliders->skip($offset)
            ->take($perpage)
            ->get();

        $response = CompanyCollection::collection($main_sliders);
        //return $response;
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function second_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 4])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 4])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function silver_companies($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = User::where(['user_types_id' => 3,
            'company_type_id' => 2, 'is_verified' => 1, 'is_blocked' => 0])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = User::where(['user_types_id' => 3,
            'company_type_id' => 2, 'is_verified' => 1, 'is_blocked' => 0])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = CompanyCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function third_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 5])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 5])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $lastPage, $current_page);
    }


    public function bronze_companies($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = User::where(['user_types_id' => 3,
            'company_type_id' => 3, 'is_verified' => 1, 'is_blocked' => 0])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = User::where(['user_types_id' => 3,
            'company_type_id' => 3, 'is_verified' => 1, 'is_blocked' => 0])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = CompanyCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return ResponseHandler::json([false]);
        } else {
            return response([new CategoryResource($categories)], Response::HTTP_OK);
        }
    }


}
