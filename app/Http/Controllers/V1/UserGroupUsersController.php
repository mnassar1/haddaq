<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\User\UserNotBelongsToUserGroupException;
use App\Exceptions\User\UserNotFoundException;
use App\Exceptions\UserGroup\UserGroupNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Model\UserGroup;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserGroupUsersController extends Controller
{
    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:group,ADD_USER_TO_GROUP', ['only' => ['store']]);
        $this->middleware('admin.perms:group,REMOVE_USER_FROM_GROUP', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserGroup $Group
     * @return array
     */
    public function index(UserGroup $Group)
    {
        return ['group' => new UserGroupResource($Group), 'users' => UserCollection::collection($Group->users)];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $group_id
     * @return array
     * @throws UserGroupNotFoundException
     * @throws UserNotFoundException
     */
    public function store(Request $request, $group_id)
    {
        $user_group = UserGroup::find($group_id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            $user = User::find($request->user_id);
            if (!$user) {
                throw new UserNotFoundException;
            } else {
                $user_belongs_to_usergroup = $user_group->users->contains($request->user_id);
                if (!$user_belongs_to_usergroup) {
                    $user_group->users()->attach($request->user_id);
                }
                return [
                    'group' => new UserGroupResource($user_group),
                    'user' => new UserResource($user)
                ];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $group_id
     * @param $user_id
     * @return \Illuminate\Http\Response
     * @throws UserGroupNotFoundException
     * @throws UserNotBelongsToUserGroupException
     * @throws UserNotFoundException
     */
    public function destroy($group_id, $user_id)
    {
        $user_group = UserGroup::find($group_id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            $user_belongs_to_usergroup = $user_group->users->contains($user_id);
            if (!$user_belongs_to_usergroup) {
                throw new UserNotBelongsToUserGroupException;
            } else {
                $role = User::find($user_id);
                if (!$role) {
                    throw new UserNotFoundException;
                } else {
                    $deleted = $user_group->users()->detach($user_id);
                    return response(['is_delete' => $deleted], Response::HTTP_OK);
                }
            }
        }
    }
}
