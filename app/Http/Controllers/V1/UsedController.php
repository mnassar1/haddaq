<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\AppAds\AppAdsCollection;
use App\Http\Resources\Home\MainSlidersCollection;
use App\Http\Resources\Used\CategoryCollection;
use App\Http\Resources\Used\CategoryGroupResource;
use Illuminate\Http\Request;


class UsedController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = [
            'used/main_sliders/',
            'used/offers_sliders/',
            'used/new_items/',
            'used/first_banners/',
            'used/first_category_group/',
            'used/second_banners/',
            'used/second_category_group/',
            'used/third_banners/',
            'used/third_category_group/',
        ];
        return ResponseHandler::json($response);
    }


    public function main_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 1])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 1])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function offers_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 2])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 2])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function new_items($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
//        $categories = \App\Model\Category::where('category_type', 1)->pluck('id');
        $companies = \App\User::where(['is_verified' => 1, 'is_blocked' => 0])->where('user_types_id', '!=', 3)->pluck('id');

        $total = \App\Model\AppAds::where(['approved_status' => 1])
            ->whereIn('company_id', $companies)
            ->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = \App\Model\AppAds::where(['approved_status' => 1])
            ->whereIn('company_id', $companies)
            ->orderby('created_at', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = AppAdsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function items_by_category(Request $request, $page_number, $perpage)
    {

        $offset = $page_number * $perpage;
        $categories = \App\Model\Category::where('category_type', 1)->pluck('id');
        $companies = \App\User::where(['is_verified' => 1, 'is_blocked' => 0])->where('user_types_id', 3)->pluck('id');


        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];
        $title = (isset($filter['simple']['title'])) ? $filter['simple']['title'] : NULL;
        $category_id = (isset($filter['simple']['category_id'])) ? $filter['simple']['category_id'] : NULL;


        unset($simple_filter['title']);
        unset($simple_filter['category_id']);
        $company_id = $request->get("company_id", null);
        $total = \App\Model\AppAds::where($simple_filter);

        $total = $total->where(['approved_status' => 1]);
        if ($company_id != null) {
            $total = $total->where('company_id', $company_id);
        }
        $total = $total->whereIn('category_id', $categories);

        if ($title != NULL) {
            $total = $total->where('title', 'LIKE', '%' . $title . '%');
        }


        if ($category_id != NULL && $category_id != 0) {
            $total = $total->where('category_id', $category_id);
        }

        $total = $total->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = \App\Model\AppAds::where($simple_filter);
        if ($title != NULL) {
            $main_sliders = $main_sliders->where('title', 'LIKE', '%' . $title . '%');
        }

        if ($category_id != NULL && $category_id != 0) {
            $main_sliders = $main_sliders->where('category_id', $category_id);
        }

        $company_id = $request->get("company_id", null);
        $main_sliders = $main_sliders->where(['approved_status' => 1]);
        if ($company_id != null) {
            $main_sliders = $main_sliders->where('company_id', $company_id);
        }
        $main_sliders = $main_sliders->whereIn('category_id', $categories)
            ->orderby('created_at', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = AppAdsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('title') && $request->get('title')) {
            $filter['simple']['title'] = $request->get('title');
        }


        if ($request->has('category_id') && null !== $request->get('category_id')) {
            $filter['simple']['category_id'] = $request->get('category_id');
        }


        return $filter;
    }

    public function used_categories()
    {

        $categories = \App\Model\Category::where('category_type', 1)->get();

        return ResponseHandler::json(CategoryCollection::collection($categories));
    }


    public function first_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 3])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 3])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function first_category_group($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $group = \App\Model\CategoryGroup::where('id', 1)->first();
        if (!$group)
            return ResponseHandler::json([false]);

        $total = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)$page_number;
        $categories = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = array(
            'group' => new CategoryGroupResource($group),
            'categories' => CategoryCollection::collection($categories),
        );
        return ResponseHandler::json($response, $current_page, $lastPage);

    }


    public function second_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 4])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 4])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function second_category_group($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $group = \App\Model\CategoryGroup::where('id', 2)->first();
        if (!$group)
            return ResponseHandler::json([false]);

        $total = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)$page_number;
        $categories = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)
            ->skip($offset)
            ->take($perpage)
            ->get();
        //return $categories;
        $response = array(
            'group' => new CategoryGroupResource($group),
            'categories' => CategoryCollection::collection($categories),
        );
        return ResponseHandler::json($response, $current_page, $lastPage);

    }


    public function third_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 5])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 2,
            'active' => 1, 'is_banar' => 5])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $lastPage, $current_page);
    }


    public function third_category_group($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $group = \App\Model\CategoryGroup::where('id', 3)->first();
        if (!$group)
            return ResponseHandler::json([false]);

        $total = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)$page_number;
        $categories = \App\Model\Category::where('category_group_id', $group->id)
            ->where('category_type', 1)
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = array(
            'group' => new CategoryGroupResource($group),
            'categories' => CategoryCollection::collection($categories),
        );
        return ResponseHandler::json($response, $current_page, $lastPage);

    }


}
