<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\OperationNotAllowedException;
use App\Exceptions\Request\NotSameCityDestinationException;
use App\Exceptions\Request\NotSameCitySourceException;
use App\Exceptions\Request\NotSameCountryDestinationException;
use App\Exceptions\Request\NotSameCountrySourceException;
use App\Exceptions\Shipment\ShipmentNotFoundException;
use App\Exceptions\Trip\TripActionNotCompletedException;
use App\Exceptions\Trip\TripNotEditableException;
use App\Exceptions\Trip\TripNotFoundException;
use App\Helpers\FilterTrait;
use App\Helpers\PaginatorHelper;
use App\Helpers\PushNotificaitionHelper;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request\UserTripRequest;
use App\Http\Requests\Trip\TripRequest;
use App\Http\Resources\Deal\DealCollection;
use App\Http\Resources\Request\RequestResource;
use App\Http\Resources\Trip\TripCollection;
use App\Http\Resources\Trip\TripResource;
use App\Model\Deal;
use App\Model\Requests;
use App\Model\Shipment;
use App\Model\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    use FilterTrait;
    use UserHelperTrait;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        $filter = $this->getFilter($request);
        $simple_filter = isset($filter['simple']) ? $filter['simple'] : [];
        $simple_filter['is_deleted'] = 0;
        $simple_filter['is_finished'] = 0;
        $trips = Trip::where($simple_filter);

        // like filter 
        $like_filter = isset($filter['like']) ? $filter['like'] : [];
        if (isset($like_filter['creation_date'])) {
            $trips = $trips->where('created_at', 'like', $like_filter['creation_date'] . '%');
        }
        // arrival_date range
        if ($request->has('arrival_date_from') || $request->has('arrival_date_to')) {
            $arrival_filter = $this->dateRangeFilter('arrival_date', $request->get('arrival_date_from'), $request->get('arrival_date_to'));
            $from = $arrival_filter['arrival_date']['from'];
            $to = $arrival_filter['arrival_date']['to'];
            $trips = $trips->whereBetween('arrival_date', [$from, $to]);
        }

        //departure_date range
        if ($request->has('departure_date_from') || $request->has('departure_date_to')) {
            $departure_filter = $this->dateRangeFilter('departure_date', $request->get('departure_date_from'), $request->get('departure_date_to'));
            $from = $departure_filter['departure_date']['from'];
            $to = $departure_filter['departure_date']['to'];
            $trips = $trips->whereBetween('departure_date', [$from, $to]);
        }

        // arrival_time range
        if ($request->has('arrival_time_from') || $request->has('arrival_time_to')) {
            $arrival_filter = $this->timeRangeFilter('arrival_time', $request->get('arrival_time_from'), $request->get('arrival_time_to'));
            $from = $arrival_filter['arrival_time']['from'];
            $to = $arrival_filter['arrival_time']['to'];
            $trips = $trips->whereBetween('arrival_time', [$from, $to]);
        }

        //kilos range
        if ($request->has('kilos_from') || $request->has('kilos_to')) {
            $kilos_filter = $this->numberRangeFilter('kilos', $request->get('kilos_from'), $request->get('kilos_to'));
            $from = $kilos_filter['kilos']['from'];
            $to = $kilos_filter['kilos']['to'];
            $trips = $trips->whereBetween('kilos', [$from, $to]);
        }

        // created_at range
        if ($request->has('creation_date_from') || $request->has('creation_date_to')) {
            $created_at_filter = $this->dateRangeFilter('creation_date', $request->get('creation_date_from'), $request->get('creation_date_to'));
            $from = $created_at_filter['creation_date']['from'];
            $to = $created_at_filter['creation_date']['to'] . ' 23:59:59';
            $trips = $trips->whereBetween('created_at', [$from, $to]);
        }

        // available
        if ($request->has('available') && $request->get('available') == 1) {
            $trips = $trips->doesntHave('requests');
            $trips = $trips->orWhereHas('requests', function ($query) {
                $query->where('status', '!=', 'accepted');
            });
        }

        // if user loggedin not get user's trips 
        if ($user) {
            $trips = $trips->where('user_id', '!=', $user->id);
            $city_order = "FIELD(city_from,$user->city_id) DESC";
            $trips = $trips->orderByRaw($city_order);
            $country_order = "FIELD(country_from,$user->country_id) DESC";
            $trips = $trips->orderByRaw($country_order);
        }

        $trips = $trips->latest()->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($trips);
        return ResponseHandler::jsonCollection(['data' => TripCollection::collection($trips), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * Filter Trips by
     * country_from , country_to
     * city_from, city_to
     * Weight range => weight_from , weight_to
     * Date range => date_from , date_to
     * Time range => time_from , time_to
     * Type transportation (plane-train-autobus)
     * @param Request $request
     * @return array of key => values
     */
    private function getFilter(Request $request)
    {
        $filter = [];
        // country_from
        if ($request->has('country_from') && !empty($request->get('country_from'))) {
            $filter['simple']['country_from'] = $request->get('country_from');
        }
        // country_to
        if ($request->has('country_to') && !empty($request->get('country_to'))) {
            $filter['simple']['country_to'] = $request->get('country_to');
        }

        // city_from
        if ($request->has('city_from') && !empty($request->get('city_from'))) {
            $filter['simple']['city_from'] = $request->get('city_from');
        }
        //city_to
        if ($request->has('city_to') && !empty($request->get('city_to'))) {
            $filter['simple']['city_to'] = $request->get('city_to');
        }

        //transportation
        if ($request->has('transportation') && !empty($request->get('transportation')) && in_array($request->get('transportation'), ['autobus', 'plane', 'train'])) {
            $filter['simple']['transportation'] = $request->get('transportation');
        }

        // weight
        if ($request->has('kilos')) {
            $filter['simple']['kilos'] = $request->get('kilos');
        }

        //created_at
        if ($request->has('creation_date')) {
            $filter['like']['creation_date'] = $request->get('creation_date');
        }
        return $filter;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TripRequest $request
     * @return \Illuminate\Http\Response
     * @throws TripActionNotCompletedException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(TripRequest $request)
    {
        $user = $this->getUserFromRequest($request);
        $request['user_id'] = $user->id;
        $trip = Trip::create($request->toArray());
        if (!$trip) {
            throw new TripActionNotCompletedException;
        } else {
            $trip->user = $user;
            //send notification
            $is_notified = PushNotificaitionHelper::newTrip($trip->id, $trip->country_from, $trip->city_from, $trip->country_to, $trip->city_to);
            return ResponseHandler::json(new TripResource($trip));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws TripNotFoundException
     */
    public function show($id)
    {
        $trip = Trip::find($id);
        if (!$trip) {
            throw new TripNotFoundException;
        }
        return ResponseHandler::json(new TripResource($trip));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TripRequest $request
     * @param Trip $Trip
     * @return \Illuminate\Http\Response
     * @throws TripNotEditableException
     * @throws \App\Exceptions\Trip\TripNotBelongsToUserException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(TripRequest $request, Trip $Trip)
    {
        $user = $this->getUserFromRequest($request);
        if (!$Trip->isEditable()) {
            throw new TripNotEditableException;
        }
        $Trip->tripUserCheck($user->id);
        $Trip->update($request->all());
        return ResponseHandler::json(new TripResource($Trip));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws TripNotEditableException
     * @throws TripNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function destroy(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $trip = Trip::find($id);
        if (!$trip) {
            throw new TripNotFoundException;
        }
        if (!$trip->isEditable()) {
            throw new TripNotEditableException;
        }
        $trip->tripUserCheck($user->id);
        $deleted = $trip->delete();
        return ResponseHandler::json(['is_delete' => $deleted]);
    }

    /**
     * shipper request to send shipment with traveler
     * @param UserTripRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws NotSameCityDestinationException
     * @throws NotSameCitySourceException
     * @throws NotSameCountryDestinationException
     * @throws NotSameCountrySourceException
     * @throws OperationNotAllowedException
     * @throws ShipmentNotFoundException
     * @throws TripNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function request_trip(UserTripRequest $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $trip = Trip::find($id);
        if (!$trip) {
            throw new TripNotFoundException;
        }
        $shipment = Shipment::find($request->shipment_id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        } else {
            $shipment->shipmentUserCheck($user->id);
            // check if trip's country is same as shipment's country 
            if ($trip->country_from != $shipment->country_from) {
                throw new NotSameCountrySourceException;
            } else if ($trip->city_from != $shipment->city_from) {
                throw new NotSameCitySourceException;
            } else if ($trip->country_to != $shipment->country_to) {
                throw new NotSameCountryDestinationException;
            } else if ($trip->city_to != $shipment->city_to) {
                throw new NotSameCityDestinationException;
            } else {
                // get push token
                $push_token = '';
                $traveller = $trip->user;
                if ($traveller) {
                    $device = $traveller->device;
                    if ($device) {
                        $push_token = $device->push_token;
                    }
                }

                // handle request here
                $is_requested = Requests::where(['shipment_id' => $request->shipment_id, 'trip_id' => $id])->first();
                if ($is_requested) {
                    // user can update her request's note only if request in waiting status 
                    if ($is_requested->status == 'waiting') {
                        $is_requested->update(['trip_id' => $id, 'shipment_id' => $request->shipment_id, 'by' => $user->id, 'to' => $trip->user_id, 'type' => 'trip', 'kilos' => $shipment->weight, 'notes' => $request->notes]);
                        // notify traveller
                        $is_notified = PushNotificaitionHelper::tripRequest($is_requested->id, $user, $push_token);
                        return ResponseHandler::json(new RequestResource($is_requested));
                    } else {
                        // throw that request if status not waiting
                        throw new OperationNotAllowedException;
                    }
                } else {
                    $trip->checkHasEnoughKilos($shipment->weight);
                    $created_request = Requests::create(['trip_id' => $id, 'shipment_id' => $request->shipment_id, 'by' => $user->id, 'to' => $trip->user_id, 'type' => 'trip', 'kilos' => $shipment->weight, 'notes' => $request->notes, 'status' => 'waiting']);
                    // notify traveller
                    $is_notified = PushNotificaitionHelper::tripRequest($created_request->id, $user, $push_token);
                    return ResponseHandler::json(new RequestResource($created_request));
                }
            }
        }

    }

    /**
     * list all trip's requests
     * @param Request $request
     * @param id,request
     * @return array of requests
     * @throws TripNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */

    public function list_requests(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $trip = Trip::find($id);
        if (!$trip) {
            throw new TripNotFoundException;
        }
        $trip->tripUserCheck($user->id);

        // filter requests by status
        $filter = ($request->has('status') && null !== $request->get('status')) ? $request->get('status') : null;

        $requests = $trip->requests($filter)->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($requests);
        return ResponseHandler::jsonCollection(['data' => RequestResource::collection($requests), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * list all trip's deals
     * @param Request $request
     * @param id,request
     * @return array of deals
     * @throws TripNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function list_deals(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $trip = Trip::find($id);
        if (!$trip) {
            throw new TripNotFoundException;
        }
        $trip->tripUserCheck($user->id);

        // filter requests by status
        $filter = ($request->has('status') && null !== $request->get('status')) ? $request->get('status') : null;
        $deals = Deal::where('trip_id', $id)->get();
        $collection = DealCollection::collection($deals);
        return ResponseHandler::jsonCollection(['data' => $collection]);
    }
}
