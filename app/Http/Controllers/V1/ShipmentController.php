<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\OperationNotAllowedException;
use App\Exceptions\Request\NotSameCityDestinationException;
use App\Exceptions\Request\NotSameCitySourceException;
use App\Exceptions\Request\NotSameCountryDestinationException;
use App\Exceptions\Request\NotSameCountrySourceException;
use App\Exceptions\Shipment\ShipmentActionNotCompletedException;
use App\Exceptions\Shipment\ShipmentNotEditableException;
use App\Exceptions\Shipment\ShipmentNotFoundException;
use App\Exceptions\Trip\TripNotFoundException;
use App\Helpers\FilterTrait;
use App\Helpers\PaginatorHelper;
use App\Helpers\PushNotificaitionHelper;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request\UserShipmentRequest;
use App\Http\Requests\Shipment\ShipmentDetailsRequest;
use App\Http\Requests\Shipment\ShipmentRequest;
use App\Http\Resources\Deal\DealCollection;
use App\Http\Resources\Request\RequestResource;
use App\Http\Resources\Shipment\ShipmentCollection;
use App\Http\Resources\Shipment\ShipmentResource;
use App\Model\Deal;
use App\Model\Requests;
use App\Model\Shipment;
use App\Model\ShipmentsDetails;
use App\Model\Trip;
use App\User;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    use FilterTrait;
    use UserHelperTrait;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        $shipments = Shipment::where(['is_deleted' => 0, 'is_finished' => 0]);

        $filter = $this->getFilter($request);
        $simple_filter = isset($filter['simple']) ? $filter['simple'] : [];
        $shipments->where($simple_filter);

        // like filter
        $like_filter = isset($filter['like']) ? $filter['like'] : [];
        if (isset($like_filter['creation_date'])) {
            $shipments = $shipments->where('created_at', 'like', $like_filter['creation_date'] . '%');
        }
        // weight range
        if ($request->has('weight_from') || $request->has('weight_to')) {
            $weight_filter = $this->numberRangeFilter('weight', $request->get('weight_from'), $request->get('weight_to'));
            $from = $weight_filter['weight']['from'];
            $to = $weight_filter['weight']['to'];
            $shipments = $shipments->whereBetween('weight', [$from, $to]);
        }

        // creation_date range
        if (($request->has('creation_date_from') || $request->has('creation_date_to')) && ($request->get('creation_date_from') && $request->get('creation_date_to'))) {
            $creation_date_filter = $this->dateRangeFilter('creation_date', $request->get('creation_date_from'), $request->get('creation_date_to'));
            $from = $creation_date_filter['creation_date']['from'];
            $to = $creation_date_filter['creation_date']['to'] . ' 23:59:59';
            $shipments = $shipments->whereBetween('created_at', [$from, $to]);
        }

        //potential_fees range
        if (($request->has('fees_from') || $request->has('fees_to')) && ($request->get('fees_to') && $request->get('fees_from'))) {
            $fees_filter = $this->numberRangeFilter('fees', $request->get('fees_from'), $request->get('fees_to'));
            $from = $fees_filter['fees']['from'];
            $to = $fees_filter['fees']['to'];
            $shipments = $shipments->whereBetween('potential_fees', [$from, $to]);
        }

        // potential_days_from range
        if (($request->has('days_from_start') || $request->has('days_from_end')) && ($request->get('days_from_start') & $request->get('days_from_end'))) {
            $days_from_filter = $this->dateRangeFilter('days_from', $request->get('days_from_start'), $request->get('days_from_end'));
            $from = $days_from_filter['days_from']['from'];
            $to = $days_from_filter['days_from']['to'];
            $shipments = $shipments->whereBetween('potential_days_from', [$from, $to]);
        }

        // potential_days_to range
        if (($request->has('days_to_start') || $request->has('days_to_end')) && ($request->get('days_to_start') && $request->get('days_to_end'))) {
            $days_to_filter = $this->dateRangeFilter('days_to', $request->get('days_to_start'), $request->get('days_to_end'));
            $from = $days_to_filter['days_to']['from'];
            $to = $days_to_filter['days_to']['to'];
            $shipments = $shipments->whereBetween('potential_days_to', [$from, $to]);
        }

        // price range
        if (($request->has('price_from') || $request->get('price_to')) && ($request->get('price_from') && $request->get('price_to'))) {
            $price_filter = $this->numberRangeFilter('price', $request->get('price_from'), $request->get('price_to'));
            $from = $price_filter['price']['from'];
            $to = $price_filter['price']['to'];
            $shipments = $shipments->whereBetween('price', [$from, $to]);
        }

        // best_price
        if ($request->has('best_price') && $request->get('best_price')) {
            if (strtolower($request->get('best_price')) == 'desc' || strtolower($request->get('best_price')) == 'asc') {
                $shipments = $shipments->orderBy('price', $request->get('best_price'));
            }
        }

        // available
        if ($request->has('available') && $request->get('available') == 1) {
            $shipments = $shipments->doesntHave('requests');
            $shipments = $shipments->orWhereHas('requests', function ($query) {
                $query->where('status', '!=', 'accepted');
            });
        }

        // if user loggedin not get user's shipments
        if ($user) {
            $shipments = $shipments->where('user_id', '!=', $user->id);
            $city_order = "Field(city_from,$user->city_id) DESC";
            $shipments = $shipments->orderByRaw($city_order);
            $country_order = "Field(country_from,$user->country_id) DESC";
            $shipments = $shipments->orderByRaw($country_order);
        }

        $shipments = $shipments->latest()->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($shipments);
        return ResponseHandler::jsonCollection(['data' => ShipmentCollection::collection($shipments), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * Filter Trips by
     * country_from , country_to
     * city_from, city_to
     * Weight range => weight_from , weight_to
     * Date range => date_from , date_to
     * Time range => time_from , time_to
     * Type transportation (plane-train-autobus)
     * @param Request $request
     * @return array of key => values
     */
    private function getFilter(Request $request)
    {
        $filter = [];
        // country_from
        if ($request->has('country_from') && !empty($request->get('country_from'))) {
            $filter['simple']['country_from'] = $request->get('country_from');
        }
        // country_to
        if ($request->has('country_to') && !empty($request->get('country_to'))) {
            $filter['simple']['country_to'] = $request->get('country_to');
        }

        // city_from
        if ($request->has('city_from') && !empty($request->get('city_from'))) {
            $filter['simple']['city_from'] = $request->get('city_from');
        }
        //city_to
        if ($request->has('city_to') && !empty($request->get('city_to'))) {
            $filter['simple']['city_to'] = $request->get('city_to');
        }

        // creation_date
        if ($request->has('creation_date') && $request->get('creation_date')) {
            $filter['like']['creation_date'] = $request->get('creation_date');
        }

        // weight
        if ($request->has('weight') && $request->get('weight')) {
            $filter['simple']['weight'] = $request->get('weight');
        }

        // potential_fees
        if ($request->has('fees') && $request->get('fees')) {
            $filter['simple']['potential_fees'] = $request->get('fees');
        }

        //potential_days_from
        if ($request->has('days_from') && $request->get('days_from')) {
            $filter['simple']['potential_days_from'] = $request->get('days_from');
        }

        // potential_days_to
        if ($request->has('days_to') && $request->get('days_to')) {
            $filter['simple']['potential_days_to'] = $request->get('days_to');
        }

        // price
        if ($request->has('price') && $request->get('price')) {
            $filter['simple']['price'] = $request->get('price');
        }

        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ShipmentRequest $request
     * @return \Illuminate\Http\Response
     * @throws ShipmentActionNotCompletedException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(ShipmentRequest $request)
    {
        $user = $this->getUserFromRequest($request);
        $request['user_id'] = $user->id;
        $shipment = Shipment::create($request->toArray());
        if (!$shipment) {
            throw new ShipmentActionNotCompletedException;
        } else {
            $shipment->user = $user;
            $is_notified = PushNotificaitionHelper::newShipment($shipment->id, $shipment->country_from, $shipment->city_from, $shipment->country_to, $shipment->city_to);
            return ResponseHandler::json(new ShipmentResource($shipment));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ShipmentNotFoundException
     */
    public function show($id)
    {
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        }
        return ResponseHandler::json(new ShipmentResource($shipment));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ShipmentRequest $request
     * @param Shipment $Shipment
     * @return \Illuminate\Http\Response
     * @throws ShipmentNotEditableException
     * @throws \App\Exceptions\Shipment\ShipmentNotBelongsToUserException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(ShipmentRequest $request, Shipment $Shipment)
    {
        $user = $this->getUserFromRequest($request);
        if (!$Shipment->isEditable()) {
            throw new ShipmentNotEditableException;
        }
        $Shipment->shipmentUserCheck($user->id);
        $request['user_id'] = $user->id;
        $Shipment->update($request->all());
        return ResponseHandler::json(new ShipmentResource($Shipment));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws ShipmentNotEditableException
     * @throws ShipmentNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function destroy(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        }
        if (!$shipment->isEditable()) {
            throw new ShipmentNotEditableException;
        }
        $shipment->shipmentUserCheck($user->id);
        $deleted = $shipment->delete();
        return ResponseHandler::json(['is_delete' => $deleted]);
    }

    /**
     * traveler request to handle shipment
     * @param UserShipmentRequest $request
     * @param $id
     * @throws NotSameCityDestinationException
     * @throws NotSameCitySourceException
     * @throws NotSameCountryDestinationException
     * @throws NotSameCountrySourceException
     * @throws OperationNotAllowedException
     * @throws ShipmentNotFoundException
     * @throws TripNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function request_shipment(UserShipmentRequest $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        } else {
            $trip = Trip::find($request->trip_id);
            if (!$trip) {
                throw new TripNotFoundException;
            } else {
                $shipment->canReceiveRequest();
                $trip->tripUserCheck($user->id);
                // check if trip's country is same as shipment's country 
                if ($trip->country_from != $shipment->country_from) {
                    throw new NotSameCountrySourceException;
                } else if ($trip->city_from != $shipment->city_from) {
                    throw new NotSameCitySourceException;
                } else if ($trip->country_to != $shipment->country_to) {
                    throw new NotSameCountryDestinationException;
                } else if ($trip->city_to != $shipment->city_to) {
                    throw new NotSameCityDestinationException;
                } else {
                    // handle request here
                    $is_requested = Requests::where(['shipment_id' => $id, 'trip_id' => $request->trip_id])->first();
                    if ($is_requested) {
                        // user can update her request's note only if request in waiting status 
                        if ($is_requested->status == 'waiting') {
                            $is_requested->update(['trip_id' => $request->trip_id, 'shipment_id' => $id, 'by' => $user->id, 'to' => $shipment->user_id, 'type' => 'shipment', 'notes' => $request->notes]);
                            // notify shipper
                            $shipper = $shipment->user;
                            if ($shipper->device) {
                                $push_token = $shipper->device->push_token;
                                $is_notified = PushNotificaitionHelper::shipmentRequest($shipment->id, $user, $push_token);
                            }
                            return ResponseHandler::json(new RequestResource($is_requested));
                        } else {
                            // throw that request if status not waiting
                            throw new OperationNotAllowedException;
                        }
                    } else {
                        $created_request = Requests::create(['trip_id' => $request->trip_id, 'shipment_id' => $id, 'by' => $user->id, 'to' => $shipment->user_id, 'type' => 'shipment', 'notes' => $request->notes, 'status' => 'waiting']);
                        return ResponseHandler::json(new RequestResource($created_request));
                    }
                }
            }
        }
    }

    /**
     * list all shipment's requests
     * @param Request $request
     * @param id,request
     * @return array of requests
     * @throws ShipmentNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */

    public function list_requests(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        }
        $shipment->shipmentUserCheck($user->id);

        // filter requests by status
        $filter = ($request->has('status') && null !== $request->get('status')) ? $request->get('status') : null;

        $requests = $shipment->requests($filter)->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($requests);
        return ResponseHandler::jsonCollection(['data' => RequestResource::collection($requests), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * add shipment details
     * @param ShipmentDetailsRequest $request
     * @param shipper_type[normal,freight_company,delegated_person], name, image optional only for fright company
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws ShipmentNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function add_details(ShipmentDetailsRequest $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        }
        $shipment->shipmentUserCheck($user->id);
        $old_details = ShipmentsDetails::where(['shipment_id' => $id])->first();
        if ($old_details) {
            $old_details->delete();
        }
        $request['shipment_id'] = $id;
        if ($request->get('shipper_type') == 'me') {
            $request['name'] = $user->name;
            $request['image'] = $user->profile_image_url;
        }
        $details = ShipmentsDetails::create($request->all());
        $traveler = User::find($request->get('traveler_id'));
        if ($traveler) {
            $device = $traveler->device;
            if ($device && $device->push_token) {
                $is_notified = PushNotificaitionHelper::shipmentDetails($details->id, $details->name, $device->push_token);
            }
        }
        return ResponseHandler::json($details);
    }

    /**
     * List shipment's deals
     * @param Request $request
     * @param id,request
     * @return array of deals
     * @throws ShipmentNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function list_deals(Request $request, $id)
    {
        $user = $this->getUserFromRequest($request);
        $shipment = Shipment::find($id);
        if (!$shipment) {
            throw new ShipmentNotFoundException;
        }
        $shipment->shipmentUserCheck($user->id);

        $deals = Deal::where('shipment_id', $id)->get();
        $collection = DealCollection::collection($deals);
        return ResponseHandler::jsonCollection(['data' => $collection]);
    }
}
