<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\FixingShops\FixingShopsCollection;
use App\Http\Resources\FixingShops\FixingShopsResource;
use App\Http\Resources\Home\MainSlidersCollection;
use App\Http\Resources\Home\MainSlidersResource;
use Illuminate\Http\Request;


class FixingShopsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = [
            'fixing_shops/main_sliders/',
            'fixing_shops/offers_sliders/',
            'fixing_shops/gold_fixing_shops/',
            'fixing_shops/first_banners/',
            'fixing_shops/silver_fixing_shops/',
            'fixing_shops/second_banners/',
            'fixing_shops/bronze_fixing_shops/',
            'fixing_shops/third_banners/',

        ];
        return ResponseHandler::json($response);
    }


    public function main_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 1])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $current_page = (int)($page_number);

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 1])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function main_slider_show($id)
    {
        $slider = \App\Model\AdminAds::find($id);
        if (!$slider) {
            return ResponseHandler::json([false]);
        } else {
            $count = $slider->views_count + 1;
            $slider->update(['views_count' => $count]);
            return ResponseHandler::json(new MainSlidersResource($slider));
        }
    }

    public function fixing_shops_details($id, $user_id = NULL)
    {
        $fixing_shops = \App\Model\FixingShops::find($id);
        if (!$fixing_shops) {
            return ResponseHandler::json([false]);
        } else {
            $fixing_shops['user_id'] = $user_id;
            return ResponseHandler::json(new FixingShopsResource($fixing_shops));
        }
    }

    public function fixing_shops_search(Request $request, $page_number, $perpage)
    {

        $filter = $this->getFilter($request);
        $data = [];

        $simple_filter = (isset($filter['simple'])) ? $filter['simple'] : [];

        $name = (isset($filter['simple']['name'])) ? $filter['simple']['name'] : NULL;
        $type_id = (isset($filter['simple']['type_id'])) ? $filter['simple']['type_id'] : 0;
        unset($simple_filter['name']);
        unset($simple_filter['type_id']);

        $total = \App\Model\FixingShops::where($simple_filter);

        if ($name != NULL) {
            $total = $total->where('shop_name', 'LIKE', '%' . $name . '%');
            $total = $total->orwhere('shop_name_ar', 'LIKE', '%' . $name . '%');
        }


        if ($type_id != 0) {
            $total = $total->where('fixing_shops_types_id', $type_id);
        }

        $total = $total->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }

        $offset = $page_number * $perpage;
        $current_page = (int)$page_number;

        $main_sliders = \App\Model\FixingShops::where($simple_filter);
        if ($name != NULL) {
            $main_sliders = $main_sliders->where('shop_name', 'LIKE', '%' . $name . '%');
            $main_sliders = $main_sliders->orwhere('shop_name_ar', 'LIKE', '%' . $name . '%');
        }

        if ($type_id != 0) {
            $main_sliders = $main_sliders->where('fixing_shops_types_id', '=', $type_id);
        }
        $main_sliders = $main_sliders->skip($offset)
            ->take($perpage)
            ->get();

        $response = FixingShopsCollection::collection($main_sliders);
        //return $response;
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function getFilter(Request $request)
    {
        $filter = [];

        if ($request->has('name') && $request->get('name')) {
            $filter['simple']['name'] = $request->get('name');
        }

        if ($request->has('type_id') && null !== $request->get('type_id')) {
            $filter['simple']['type_id'] = $request->get('type_id');
        }

        return $filter;
    }


    public function offers_sliders($page_number, $perpage)
    {
        $offset = $page_number * $perpage;
        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 2])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;


        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 2])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();


        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }

    public function first_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 3])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 0,
            'active' => 1, 'is_banar' => 3])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function gold_fixing_shops($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 1])->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }
        $current_page = (int)$page_number;

        $main_sliders = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 1])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = FixingShopsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function second_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 4])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 4])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function silver_fixing_shops($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 2])->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }
        $current_page = (int)$page_number;

        $main_sliders = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 2])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = FixingShopsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


    public function third_banners($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 5])->count();
        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }


        $current_page = (int)$page_number;

        $main_sliders = \App\Model\AdminAds::where(['fixing_shops_or_home' => 1,
            'active' => 1, 'is_banar' => 5])
            ->orderby('ordering', 'desc')
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = MainSlidersCollection::collection($main_sliders);
        return ResponseHandler::json($response, $lastPage, $current_page);
    }


    public function bronze_fixing_shops($page_number, $perpage)
    {
        $offset = $page_number * $perpage;

        $total = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 3])->count();

        if ($total <= $perpage) {
            $lastPage = 0;
        } else {
            $lastPage = ceil($total / $perpage) - 1;
        }
        $current_page = (int)$page_number;

        $main_sliders = \App\Model\FixingShops::where([
            'fixing_shops_types_id' => 3])
            ->skip($offset)
            ->take($perpage)
            ->get();
        $response = FixingShopsCollection::collection($main_sliders);
        return ResponseHandler::json($response, $current_page, $lastPage);
    }


}
