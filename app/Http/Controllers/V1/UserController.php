<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\User\UserNotFoundException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:user,LIST_USER', ['only' => ['index']]);
        $this->middleware('admin.perms:user,SHOW_USER', ['only' => ['show']]);
        $this->middleware('admin.perms:user,CHANGE_USER_STATUS', ['only' => 'toggleBlock']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        return UserCollection::collection(User::where($filter)->latest()->paginate(10));
    }

    /**
     * Filter Users by
     * status => verified,blocked
     * register_type => email,social
     * @param Request $request
     * @return array of key => values
     */
    private function getFilter(Request $request)
    {
        $filter = [];

        /** status filter */
        if ($request->has('status')) {
            $status = $request->get('status');
            if ($status == 'blocked' || $status == 'verified') {
                $filter['is_' . $status] = 1;
            }
        }

        /** register_type filter*/
        if ($request->has('user_types_id')) {
            $type = $request->get('user_types_id');
            if ($type == '1' || $type == '2' || $type == '3') {
                $filter['user_types_id'] = $type;
            }
        }
        return $filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws UserNotFoundException
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            return response(['user' => new UserResource($user)], Response::HTTP_OK);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Block/Unblock user by changing is_blocked flag in user table
     * @param Request $request
     * @param $id
     * @return User
     * @throws UserNotFoundException
     */
    public function toggleBlock(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new UserNotFoundException;
        } else {
            $user->update(['is_blocked' => $request->get('is_blocked')]);
            return response(['user' => new UserResource($user)], Response::HTTP_OK);
        }
    }

    /**
     * delete user for test purpose
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws AuthenticationException
     * @throws UserNotFoundException
     */
    public function delete(Request $request)
    {
        $random_str = '6Iu3ZogAaiUa61qBkEhG';
        if ($random_str != $request->random_str) {
            throw new AuthenticationException;
        }
        $user = User::where(['email' => $request->email, 'phone' => $request->phone])->first();
        if (!$user) {
            throw new UserNotFoundException;
        }
        $user->device()->delete();
        $deleted = $user->delete();
        return response(['is_delete' => $deleted], Response::HTTP_OK);
    }

    /**
     * update user's flags
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update_user_flag(Request $request)
    {
        $user = User::find($request->user_id);
        $field = $request->field;
        $user->$field = $request->value;
        $user->save();
        return ResponseHandler::json(new UserResource($user));
    }

    public function update_user_lang(Request $request)
    {
        $user = User::find($request->user_id);
        $field = $request->field;
        $user->$field = $request->value;
        $user->save();
        return ResponseHandler::json(new UserResource($user));
    }
}
