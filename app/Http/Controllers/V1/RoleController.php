<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Role\RoleActionNotCompletedException;
use App\Exceptions\Role\RoleNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Role\RoleResource;
use App\Model\Role;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{
    /**
     *   apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:role,LIST_ROLE', ['only' => ['index']]);
        $this->middleware('admin.perms:role,SHOW_ROLE', ['only' => ['show']]);
        $this->middleware('admin.perms:role,CREATE_ROLE', ['only' => ['store']]);
        $this->middleware('admin.perms:role,UPDATE_ROLE', ['only' => ['update']]);
        $this->middleware('admin.perms:role,DELETE_ROLE', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $roles = Role::where('is_deleted', 0)->get();
        return RoleCollection::collection($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     * @throws RoleActionNotCompletedException
     */
    public function store(RoleRequest $request)
    {
        $role = Role::create($request->all());
        if (!$role) {
            throw new RoleActionNotCompletedException;
        } else {
            return response(['role' => new RoleResource($role)], Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return array
     * @throws RoleNotFoundException
     */
    public function show($id)
    {
        $role = Role::find($id);
        if (!$role) {
            throw new RoleNotFoundException;
        } else {
            return ['role' => new RoleResource($role)];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Role $Role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $Role)
    {
        $Role->update($request->all());
        return response(['role' => new RoleResource($Role)], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $Role
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Role $Role)
    {
        $deleted = $Role->delete();
        return response(['is_delete' => $deleted], Response::HTTP_OK);
    }
}
