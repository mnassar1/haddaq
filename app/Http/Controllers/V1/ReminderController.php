<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\User\AuthenticationException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reminders\ReminderRequest;
use App\Http\Resources\Reminders\ReminderCollection;
use App\Model\Reminders;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ReminderController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $reminders = Reminders::where('user_id', $user->id)->get();
        return ResponseHandler::jsonCollection(ReminderCollection::collection($reminders));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ReminderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReminderRequest $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $request['user_id'] = $user->id;
        //return ($request->toArray());
        $reminder = Reminders::create($request->toArray());
        if (!$reminder) {
            return ResponseHandler::json(false);
        }
        return ResponseHandler::json($reminder);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $reminder = Reminders::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            return ResponseHandler::json($reminder);
            //return response([ new ReminderResource($reminder)],Response::HTTP_OK);
        }
    }


    public function update(ReminderRequest $request, $id)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $reminder = Reminders::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            $reminder->update($request->all());
            return ResponseHandler::json($reminder);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $reminder = Reminders::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            $deleted = $reminder->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }


}
