<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Login\IncorrectPasswordException;
use App\Exceptions\User\UserNotAdminException;
use App\Exceptions\User\UserNotFoundException;
use App\Exceptions\User\UserNotVerifiedException;
use App\Helpers\AdminHelperTrait;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRegisterRequest;
use App\Http\Requests\Login\LoginRequest;
use App\Http\Resources\Admin\AdminCollection;
use App\Http\Resources\Admin\AdminResource;
use App\User;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    use UserHelperTrait;
    use AdminHelperTrait;

    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:admin,LIST_ADMIN', ['only' => ['index']]);
        $this->middleware('admin.perms:admin,SHOW_ADMIN', ['only' => ['show']]);
    }

    public function index()
    {
        $admins = User::all();
        return response(['admins' => AdminCollection::collection($admins)], Response::HTTP_OK);
    }

    public function register(AdminRegisterRequest $request)
    {
        $admin = User::where(['email' => $request->email, 'phone' => $request->phone])->first();
        if (!$admin) {
            throw new UserNotFoundException;
        } else {
            if ($admin->is_verified == 0) {
                throw new UserNotVerifiedException;
            } else {
                $admin->update(['is_admin' => 1]);
                return ['admin' => new AdminResource($admin)];
            }
        }
    }

    public function login(LoginRequest $request)
    {
        $admin = User::where('email', $request->email)->first();
        if (!$admin) {
            throw new UserNotFoundException;
        } else {
            if ($admin->password == $this->generateHashedPassword($request->password, $admin->salt)) {
                if (!$admin->is_admin) {
                    throw new UserNotAdminException;
                } else {
                    return [
                        'admin' => new AdminResource($admin),
                        'usergroups' => $this->getUsergroups($admin),
                        'roles' => $this->generateRolesDictionary($admin),
                        'access_token' => $this->createAccessToken($admin)
                    ];
                }
            }
            throw new IncorrectPasswordException;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws UserNotAdminException
     * @throws UserNotFoundException
     */

    public function show($id)
    {
        $admin = User::find($id);
        if (!$admin) {
            throw new UserNotFoundException;
        } else {
            if ($admin->is_admin == 0) {
                throw new UserNotAdminException;
            } else {
                $admin_response = [
                    'admin' => new AdminResource($admin),
                    'roles' => $this->generateRolesDictionary($admin),
                    'usergroups' => $this->getUsergroups($admin)
                ];
                return response($admin_response, Response::HTTP_OK);
            }
        }
    }
}
