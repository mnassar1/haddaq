<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Model\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('admin.perms:cms,CREATE_AdminAds', ['only' => ['store']]);
        $this->middleware('admin.perms:cms,UPDATE_AdminAds', ['only' => ['update']]);
        $this->middleware('admin.perms:cms,DELETE_AdminAds', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::where('category_type', '<>', '1')->get();
        return ResponseHandler::jsonCollection(CategoryCollection::collection($categories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::find($id);
        if (!$categories) {
            return ResponseHandler::json([false]);
        } else {
            return response([new CategoryResource($categories)], Response::HTTP_OK);
        }
    }


}
