<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\UserGroup\UserGroupActionNotCompletedException;
use App\Exceptions\UserGroup\UserGroupNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserGroup\UserGroupCollection;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Model\UserGroup;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserGroupController extends Controller
{
    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:group,LIST_GROUP', ['only' => ['index']]);
        $this->middleware('admin.perms:group,SHOW_GROUP', ['only' => ['show']]);
        $this->middleware('admin.perms:group,CREATE_GROUP', ['only' => ['strore']]);
        $this->middleware('admin.perms:group,UPDATE_GROUP', ['only' => ['update']]);
        $this->middleware('admin.perms:group,DELETE_GROUP', ['only' => ['destroy']]);
    }

    public function index()
    {
        $user_groups = UserGroup::where('is_deleted', 0)->get();
        return UserGroupCollection::collection($user_groups);
    }

    public function show($id)
    {
        $user_group = UserGroup::find($id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            return ['group' => new UserGroupResource($user_group)];
        }
    }

    public function store(Request $request)
    {
        $user_group = UserGroup::create($request->all());
        if (!$user_group) {
            throw new UserGroupActionNotCompletedException;
        } else {
            return response(['group' => new UserGroupResource($user_group)], Response::HTTP_CREATED);
        }
    }

    public function update(Request $request, UserGroup $Group)
    {
        $Group->update($request->all());
        return response(['group' => new UserGroupResource($Group)], Response::HTTP_CREATED);
    }

    public function destroy(UserGroup $Group)
    {
        $deleted = $Group->delete();
        return response(['is_delete' => $deleted], Response::HTTP_OK);
    }
}
