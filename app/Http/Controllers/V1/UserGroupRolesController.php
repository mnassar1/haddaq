<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Role\RoleNotBelongsToUserGroupException;
use App\Exceptions\Role\RoleNotFoundException;
use App\Exceptions\UserGroup\UserGroupNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\UserGroup\UserGroupResource;
use App\Model\Role;
use App\Model\UserGroup;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserGroupRolesController extends Controller
{
    /**
     * apply admin.perms middleware
     */
    public function __construct()
    {
        $this->middleware('admin.perms:role,ASSIGN_ROLE', ['only' => ['store']]);
        $this->middleware('admin.perms:role,UNASSIGN_ROLE', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserGroup $Group
     * @return array
     */
    public function index(UserGroup $Group)
    {
        return [
            'group' => new UserGroupResource($Group),
            'roles' => RoleCollection::collection($Group->roles)
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Assign role to usergroup.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $group_id
     * @return array
     * @throws RoleNotFoundException
     * @throws UserGroupNotFoundException
     */
    public function store(Request $request, $group_id)
    {
        $user_group = UserGroup::find($group_id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            $role_belongs_to_usergroup = $user_group->roles->contains($request->role_id);
            if (!$role_belongs_to_usergroup) {
                $role = Role::find($request->role_id);
                if (!$role) {
                    throw new RoleNotFoundException;
                } else {
                    $user_group->roles()->attach($request->role_id);
                    $user_group->roles->add($role);
                }
            }
            return [
                'group' => new UserGroupResource($user_group),
                'roles' => RoleCollection::collection($user_group->roles)
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $group_id
     * @param $role_id
     * @return array
     * @throws RoleNotBelongsToUserGroupException
     * @throws RoleNotFoundException
     * @throws UserGroupNotFoundException
     */
    public function show($group_id, $role_id)
    {
        $user_group = UserGroup::find($group_id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            $role_belongs_to_usergroup = $user_group->roles->contains($role_id);
            if (!$role_belongs_to_usergroup) {
                throw new RoleNotBelongsToUserGroupException;
            } else {
                $role = Role::find($role_id);
                if (!$role) {
                    throw new RoleNotFoundException;
                } else {
                    return ['role' => new RoleResource($role)];
                }
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * unassign role from usergroup.
     *
     * @param $group_id
     * @param $role_id
     * @return \Illuminate\Http\Response
     * @throws RoleNotBelongsToUserGroupException
     * @throws RoleNotFoundException
     * @throws UserGroupNotFoundException
     */
    public function destroy($group_id, $role_id)
    {
        $user_group = UserGroup::find($group_id);
        if (!$user_group) {
            throw new UserGroupNotFoundException;
        } else {
            $role_belongs_to_usergroup = $user_group->roles->contains($role_id);
            if (!$role_belongs_to_usergroup) {
                throw new RoleNotBelongsToUserGroupException;
            } else {
                $role = Role::find($role_id);
                if (!$role) {
                    throw new RoleNotFoundException;
                } else {
                    $deleted = $user_group->roles()->detach($role_id);
                    return response(['is_delete' => $deleted], Response::HTTP_OK);
                }
            }
        }
    }
}
