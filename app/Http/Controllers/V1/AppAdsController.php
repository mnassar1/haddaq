<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppAds\AppAdsRequest;
use App\Http\Resources\AppAds\AppAdsCollection;
use App\Http\Resources\AppAds\AppAdsResource;
use App\Model\AppAds;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AppAdsController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        $user = $request->user();
        $filter['company_id'] = $user->id;
        return ResponseHandler::jsonCollection(AppAdsCollection::collection(AppAds::where($filter)->orderby('created_at', 'desc')->get()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AppAdsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppAdsRequest $request)
    {
        $ads = $request->all();
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $file = $image;
                $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                    $images[] = "ads/" . $image_name;
                }

            }
        }

        $ads['images'] = json_encode($images);

        $ads = AppAds::create($ads);
        if (!$ads) {
            return ResponseHandler::json(false);
        } else {

            return ResponseHandler::json(
                new AppAdsResource($ads)
            );
            //return ResponseHandler::json(
            //        new AppAdsResource($ads),Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ads = AppAds::find($id);
        if (!$ads) {
            return ResponseHandler::json(false);
        } else {
            return ResponseHandler::json(
                new AppAdsResource($ads)
            );
            //return response([new AppAdsResource($admin_ads)],Response::HTTP_OK);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $admin_ads = AppAds::where(['id' => $id, 'company_id' => $user->id])->first();
        //return $admin_ads;
        if (!$admin_ads) {
            return ResponseHandler::json(false);
        } else {
            $ads = $request->all();
            if ($request->hasFile('images') && !empty($request->file('images'))) {
                foreach ($request->file('images') as $image) {
                    $file = $image;
                    $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
                    if ($file->move(storage_path('app/public/ads/'), $image_name)) {
                        $images[] = "ads/" . $image_name;
                    }

                }
                $ads['images'] = json_encode($images);
            } else {
                unset($ads['images']);
            }

            $ads['approved_status'] = NULL;
            $admin_ads->update($ads);
            //return $admin_ads;
            return ResponseHandler::json(
                new AppAdsResource($admin_ads)
            );
        }
    }

    public function republish(Request $request, $id)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $admin_ads = AppAds::where(['id' => $id, 'company_id' => $user->id])->first();

        if (!$admin_ads) {
            return ResponseHandler::json(false);
        }

        $now = Carbon::now();
        $ads['created_at'] = $now;
        $admin_ads->update($ads);

        return ResponseHandler::json(
            new AppAdsResource($admin_ads)
        );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin_ads = AppAds::find($id);
        if (!$admin_ads) {
            return ResponseHandler::json([false]);
        } else {
            $deleted = $admin_ads->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }


}
