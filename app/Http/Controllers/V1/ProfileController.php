<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Profile\UserHasNotPaymentDataException;
use App\Exceptions\User\UserNotFoundException;
use App\Helpers\PaginatorHelper;
use App\Helpers\ResponseHandler;
use App\Helpers\UserHelperTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\MobileVerifyRequest;
use App\Http\Resources\Deal\DealCollection;
use App\Http\Resources\Request\RequestCollection;
use App\Http\Resources\Shipment\ShipmentCollection;
use App\Http\Resources\Trip\TripCollection;
use App\Http\Resources\User\UserPaymentResource;
use App\Http\Resources\User\UserResource;
use App\Model\Deal;
use App\Model\NotificationSettings;
use App\Model\Shipment;
use App\Model\Trip;
use App\Model\UserPayment;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Profiler\Profile;

class ProfileController extends Controller
{
    use UserHelperTrait;

    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws UserNotFoundException
     */
    public function index(Request $request)
    {
        $profile = $request->user();
        if (!$profile) {
            throw new UserNotFoundException;
        }
        return ResponseHandler::json(new UserResource($profile));
    }

    /**
     * @param Request $request
     * @return Array of [Trip]
     * @throws UserNotFoundException
     */
    public function trips(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        // handle request status
        if ($request->has('can_request')
            && null !== $request->get('can_request')
            && $request->get('can_request') == 1) {
            $trips = Trip::where('user_id', $user->id);
            $trips = $trips->leftJoin('requests', function ($join) {
                $join->on('requests.trip_id', 'trips.id');
            });
            $trips = $trips->where(function ($query) {
                $query->whereNull('requests.id');
                $query->orWhere('requests.status', '!=', 'accepted');

            });
            $trips = $trips->paginate(100, ['trips.*']);
        } else {
            $trips = Trip::where('user_id', $user->id);
            $trips = $user->trips()->paginate(5);
        }

        $paginator = PaginatorHelper::getPaginatorData($trips);
        return ResponseHandler::jsonCollection(['data' => TripCollection::collection($trips), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * @param Request $request
     * @return Array of [Shipments]
     * @throws UserNotFoundException
     */
    public function shipments(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }

        if ($request->has('can_request')
            && null !== $request->get('can_request')
            && $request->get('can_request') == 1) {
            $shipments = Shipment::where('user_id', $user->id);
            $shipments = $shipments->leftJoin('requests', function ($join) {
                $join->on('requests.shipment_id', 'shipments.id');
            });
            $shipments = $shipments->where(function ($query) {
                $query->whereNull('requests.id');
                $query->orWhere('requests.status', '!=', 'accepted');
            });
            $shipments = $shipments->paginate(100, ['shipments.*']);
        } else {
            $shipments = Shipment::where('user_id', $user->id);
            $shipments = $user->shipments()->paginate(5);
        }
        $paginator = PaginatorHelper::getPaginatorData($shipments);
        return ResponseHandler::jsonCollection(['data' => ShipmentCollection::collection($shipments), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * get requests that sent to user where its status = 'waiting' (pending)
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws UserNotFoundException
     */
    public function pending_requests(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        $pending = $user->pending_requests();
        return ResponseHandler::jsonCollection(RequestCollection::collection($pending));
    }

    /**
     * get request that send by user where its status = 'waiting' (pending)
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws UserNotFoundException
     */
    public function my_pending_requests(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        $my_pending = $user->my_pending_requests();
        return ResponseHandler::jsonCollection(RequestCollection::collection($my_pending));
    }

    /**
     * get requests that sent to user where it's status = 'accepted' (active)
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function active_requests(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $active = $user->active_requests();
        return ResponseHandler::jsonCollection(RequestCollection::collection($active));
    }

    /**
     * get requests that sent by user where it's status = 'accepted' (active)
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function my_active_requests(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $my_active = $user->my_active_requests();
        return ResponseHandler::jsonCollection(RequestCollection::collection($my_active));
    }

    /**
     * get closed deals history which means get all deals with 'closed' status
     * @param Request $request
     * @return array of deals with 'closed' status
     * @throws AuthenticationException
     */
    public function deals_history(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $history = Deal::whereIn('status', ['cancelled'])->where(function ($query) use ($user) {
            $query->where('traveller_id', $user->id);
            $query->orWhere('shipper_id', $user->id);
        })->get();
        //return ResponseHandler::json(DealCollection::collection($history));

        $collection = DealCollection::collection($history);
        return ResponseHandler::jsonCollection(['data' => $collection]);
    }

    /**
     * get deals history which means get all deals with 'shipper_verification' status
     * @param Request $request
     * @return array of deals with 'shipper_verification' status
     * @throws AuthenticationException
     */
    public function completed_deals_history(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $history = Deal::whereIn('status', ['closed'])->where(function ($query) use ($user) {
            $query->where('traveller_id', $user->id);
            $query->orWhere('shipper_id', $user->id);
        })->get();
        //return ResponseHandler::json(DealCollection::collection($history));

        $collection = DealCollection::collection($history);
        return ResponseHandler::jsonCollection(['data' => $collection]);
    }


    /**
     * send verification_code to mobile phone
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function send_verification_code(Request $request)
    {
        $user = $this->getUserFromRequest($request);

        $verification_code = random_int(1000, 9999);
        $user->mobile_verification_code = $verification_code;
        $user->is_mobile_verified = 0;
        $saved = $user->save();
        if (!$saved) {
            return ResponseHandler::json(['verification_code_sent' => false]);
        }
        // @todo send sms to user's phone number


        //
        return ResponseHandler::json(['verification_code_sent' => true]);
    }

    /**
     * verify user's mobile
     * @param MobileVerifyRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function mobile_verify(MobileVerifyRequest $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        if (!$user->is_mobile_verified) {
            // if(!$user->mobile_verification_code){
            //     throw new VerificationCodeNotFoundException;
            // }

            // if($request->get('verification_code') != $user->mobile_verification_code){
            //     throw new WrongVerificationCodeException;
            // }

            $user->is_mobile_verified = 1;
            $user->mobile_verification_code = null;
            $saved = $user->save();
            return ResponseHandler::json(['mobile_verified' => $saved]);
        }
        return ResponseHandler::json(['mobile_verified' => true]);
    }

    /**
     * edit profile
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function edit_profile(Request $request)
    {

        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $request->validate([
            'full_name' => 'required|string',
            'email' => 'required|email|unique:users,email,' . $user->id,
            "user_types_id" => "required|in:1,2,3",
            "savior_with_price" => "required|numeric",
            'city_id' => 'integer',
            'mobile' => 'required|string|unique:users,mobile,' . $user->id,
            'profile_image_url' => "image|mimes:jpeg,png,jpg,gif,svg",
        ]);
        $edit_profile = User::where('id',$user->id)->first();
        $new_data = $request->all();

        if ($request->hasFile('profile_image_url')) {
            $file = $request->file('profile_image_url');
            $image_name = time() . '_' . str_random(5) . '.' . $file->getClientOriginalExtension();
            if ($file->move(storage_path('app/public/users/'), $image_name)) {
                $new_data['profile_image_url'] = "users/" . $image_name;
            }
        } else {
            unset($new_data['profile_image_url']);
        }
        $edit_profile->update($new_data);
        return ResponseHandler::json(new UserResource($edit_profile));
    }


    public function change_password(Request $request)
    {

        $user = $request->user();

        if (!$user) {
            throw new AuthenticationException;
        }

        $request->validate([
            'old_password' => 'required|string',
            'new_password' => 'required|string',
        ]);

        $old_password = $user->password;
        $password = sha1(md5($request->old_password));
        if ($old_password != $password) {
            return ResponseHandler::json(['password_changed' => false, 'message' => 'Old Password is inCorrect']);
        }

        $new_data = array(
            'password' => sha1(md5($request->new_password)),
        );
        $edit_profile = User::find($user->id);
        $edit_profile->update($new_data);


        $new_user = User::find($user->id);

        // }
        return ResponseHandler::json(['password_changed' => true, 'message' => 'Password Changed Successfully']);
    }


    /**
     * to add or edit user payment data
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function edit_payment(Request $request)
    {
        $request->validate([
            'card_number' => 'required|string|min:16|max:16',
            'cvv' => 'required|integer|digits:3',
            'expire_date' => 'required|date'
        ]);

        $user = $this->getUserFromRequest($request);
        $payment = UserPayment::find($user->id);
        if (!$payment) {
            $payment = new UserPayment;
            $request['user_id'] = $user->id;
            $payment->create($request->all());
        } else {
            $payment->update($request->all());
        }
        return ResponseHandler::json(new UserPaymentResource($payment));
    }

    /**
     * get payment data
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     * @throws UserHasNotPaymentDataException
     */
    public function payment_data(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $payment = UserPayment::find($user->id);
        if (!$payment) {
            throw new UserHasNotPaymentDataException;
        }
        return ResponseHandler::json(new UserPaymentResource($payment));
    }

    /**
     * get requests sent to user
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function requests(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $status_enum = ['active' => 'accepted', 'pending' => 'waiting', 'canceled' => 'rejected'];
        if ($request->has('status') && null !== $request->get('status') && in_array($request->get('status'), ['active', 'pending', 'canceled'])) {
            $requests = $user->requests($status_enum[$request->get('status')]);
        } else {
            $requests = $user->requests();
        }
        return ResponseHandler::json(RequestCollection::collection($requests));
    }

    /**
     * Get my request that sent by me
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws AuthenticationException
     */
    public function my_requests(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $status_enum = ['active' => 'accepted', 'pending' => 'waiting', 'canceled' => 'rejected'];
        if ($request->has('status') && null !== $request->get('status') && in_array($request->get('status'), ['active', 'pending', 'canceled'])) {
            $requests = $user->my_requests($status_enum[$request->get('status')]);
        } else {
            $requests = $user->my_requests();
        }
        return ResponseHandler::json(RequestCollection::collection($requests));
    }

    /**
     * update user's NotificationSettings
     * @param Request $request
     * @return profile object
     * @throws AuthenticationException
     */
    public function updateNotificationSettings(Request $request)
    {
        $user = $this->getUserFromRequest($request);
        $request->validate([
            'chat_notification' => 'integer|in:0,1',
            'traveller_suggestion' => 'integer|in:0,1',
            'shipment_suggestion' => 'integer|in:0,1'
        ]);

        if ($user->notificationSettings) {
            $user->notificationSettings->update($request->all());
        } else {
            $request['user_id'] = $user->id;
            $notification_settings = NotificationSettings::create($request->all());
            $user->notification_settings = $notification_settings;
        }
        return ResponseHandler::json(new UserResource($user));
    }
}
