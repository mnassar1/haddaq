<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\User\UserNotFoundException;
use App\Helpers\PaginatorHelper;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\Shipment\ShipmentCollection;
use App\Http\Resources\Timeline\TimelineCollection;
use App\Http\Resources\Trip\TripCollection;
use App\Model\Shipment;
use App\Model\Timeline;
use App\Model\Trip;
use Illuminate\Http\Request;

class TimelineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $timeline = Timeline::latest()->where(['country_id' => $user->country_id, 'city_id' => $user->city_id])->paginate(5);
        return ResponseHandler::jsonCollection(TimelineCollection::collection($timeline));
    }

    /**
     * return all trips in my country and city excpect my trips
     * @param Request $request
     * @return array of trips
     * @throws UserNotFoundException
     */
    public function trips(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        $trips = Trip::latest()->where(['city_from' => $user->city_id, 'country_from' => $user->country_id])->where('user_id', '!=', $user->id)->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($trips);
        return ResponseHandler::jsonCollection(['data' => TripCollection::collection($trips), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * return all shipments in my country and city expect my shipments
     * @param Request $request
     * @return array of shipments
     * @throws UserNotFoundException
     */
    public function shipments(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new UserNotFoundException;
        }
        $shipments = Shipment::latest()->where(['city_from' => $user->city_id, 'country_from' => $user->country_id])->where('user_id', '!=', $user->id)->paginate(5);
        $paginator = PaginatorHelper::getPaginatorData($shipments);
        return ResponseHandler::jsonCollection(['data' => ShipmentCollection::collection($shipments), 'links' => $paginator['links'], 'meta' => $paginator['meta']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
