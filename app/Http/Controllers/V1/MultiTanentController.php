<?php

namespace App\Http\Controllers\V1;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Model\Domains;

class MultiTanentController extends Controller
{

    public function getDomains()
    {
        $domains = Domains::all();
        return ResponseHandler::json($domains);
    }

}
