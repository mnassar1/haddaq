<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\Favorites\AddedToFavoritesException;
use App\Exceptions\User\AuthenticationException;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\Favorites\FavoriteRequest;
use App\Http\Resources\Favorites\FavoriteCollection;
use App\Http\Resources\Favorites\FavoriteResource;
use App\Model\Favorites;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FavoritesController extends Controller
{
    /**
     * apply admin.perms
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $reminders = Favorites::where('user_id', $user->id)->get();
        return ResponseHandler::jsonCollection(FavoriteCollection::collection($reminders));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param FavoriteRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FavoriteRequest $request)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }

        switch ($request->type) {
            case 'Company':
                $item_details = \App\User::where(['user_types_id' => 3, 'id' => $request->item_id])->first();
                if (!$item_details) {
                    return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                }
                break;
            case 'FixingShop':
                $item_details = \App\Model\FixingShops::find($request->item_id);
                if (!$item_details) {
                    return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                }
                break;
            case 'Product':
                $item_details = \App\Model\AppAds::find($request->item_id);
                if (!$item_details) {
                    return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                }
                break;
            case 'Savior':
                $item_details = \App\User::where(['user_types_id' => 2, 'id' => $request->item_id])->first();
                if (!$item_details) {
                    return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                }
                break;
        }


        $favourite = Favorites::where(['type' => $request->type, 'user_id' => $user->id, 'item_id' => $request->item_id])->first();
        if ($favourite) {
            throw new AddedToFavoritesException;
        }
        $reminder = Favorites::create();
        $reminder->type = $request->type;
        $reminder->user_id = $user->id;
        $reminder->country_code = config('database.default');
        $reminder->item_id = $request->item_id;

        $reminder->save();
        if (!$reminder) {
            return ResponseHandler::json(false);
        }
        return ResponseHandler::json($reminder);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException;
        }

        $reminder = Favorites::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            return ResponseHandler::json(new FavoriteResource($reminder));
            //return response([ new FavoriteResource($reminder)],Response::HTTP_OK);
        }
    }


    public function update(FavoriteRequest $request, $id)
    {
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $reminder = Favorites::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            switch ($request->type) {
                case 'Company':
                    $item_details = \App\User::find($request->item_id);
                    if (!$item_details) {
                        return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                    }
                    break;
                case 'FixingShop':
                    $item_details = \App\Model\FixingShops::find($request->item_id);
                    if (!$item_details) {
                        return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                    }
                    break;
                case 'Product':
                    $item_details = \App\Model\AppAds::find($request->item_id);
                    if (!$item_details) {
                        return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                    }
                    break;
                case 'Savior':
                    $item_details = \App\User::find($request->item_id);
                    if (!$item_details) {
                        return ResponseHandler::json("This Item not belong to  " . $request->type . " " . "List");
                    }
                    break;
            }

            $reminder->update($request->all());
            return ResponseHandler::json($reminder);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $reminder = Favorites::where(['id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            $deleted = $reminder->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }

    public function delete_from_favourite($id, $type)
    {
        $user = Auth::user();
        if (!$user) {
            throw new AuthenticationException;
        }
        $reminder = Favorites::where(['type' => $type, 'item_id' => $id, 'user_id' => $user->id])->first();
        if (!$reminder) {
            return ResponseHandler::json([false]);
        } else {
            $deleted = $reminder->delete();
            return response(['is_delete' => $deleted], Response::HTTP_OK);
        }
    }


}
