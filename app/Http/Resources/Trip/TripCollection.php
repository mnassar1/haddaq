<?php

namespace App\Http\Resources\Trip;

use App\Helpers\ResourceHelper;
use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed is_finished
 * @property mixed created_at
 * @property mixed id
 * @property mixed notes
 * @property mixed kilos
 * @property mixed transportation
 * @property mixed arrival_time
 * @property mixed arrival_date
 * @property mixed departure_time
 * @property mixed departure_date
 * @property mixed to_city
 * @property mixed to_country
 * @property mixed country_to
 * @property mixed from_city
 * @property mixed from_country
 * @property mixed country_from
 */
class TripCollection extends Resource
{
    use ResourceHelper;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'from' => [
                'id' => $this->country_from,
                'name_ar' => ($this->from_country) ? $this->from_country->name_ar : '',
                'name_en' => ($this->from_country) ? $this->from_country->name_en : '',
                'city' => new CityResource($this->from_city)
            ],
            'to' => [
                'id' => $this->country_to,
                'name_ar' => ($this->to_country) ? $this->to_country->name_ar : '',
                'name_en' => ($this->to_country) ? $this->to_country->name_en : '',
                'city' => new CityResource($this->to_city)
            ],
            "departure_date" => $this->departure_date,
            "departure_time" => $this->departure_time,
            "arrival_date" => $this->arrival_date,
            "arrival_time" => $this->arrival_time,
            "transportation" => $this->transportation,
            "kilos" => $this->kilos,
            "notes" => $this->notes,
            "url" => $this->getCurrentUrl("Trip", $this->id),
            'posted_at' => $this->created_at->timestamp,
            "is_finished" => $this->is_finished,
            "traveller" => (isset($this->user)) ? [
                "id" => $this->user->id,
                "name" => $this->user->name,
                "profile_image_url" => $this->user->photo
            ] : null,
            "requests" => $this->requests('pending')->count()
        ];
    }
}
