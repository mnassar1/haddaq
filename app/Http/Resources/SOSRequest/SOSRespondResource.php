<?php

namespace App\Http\Resources\SOSRequest;

use Illuminate\Http\Resources\Json\Resource;


/**
 * @property mixed response_status
 * @property mixed sos_request_id
 * @property mixed push_token
 * @property mixed location
 * @property mixed user_id
 * @property mixed id
 * @property mixed sos_request
 * @property mixed savior_details
 */
class SOSRespondResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // $sos_problem_type_details = \App\Model\ProblemType::find($this->sos_problem_type_id);
        // if($sos_problem_type_details){
        //     $sos_problem_type_details = new ProblemTypeResource($sos_problem_type_details);
        // }
        $sos_request = \App\Model\SOSRequest::find($this->sos_request_id);
        $sos_request = new SOSRequestResource($sos_request);

        $savior_details = \App\User::find($this->user_id);
        $savior_details = new UserResource($savior_details);

        return [
            'id' => $this->id,
            'user_id' => ($this->user_id) ? $this->user_id : "",
            'savior_details' => $savior_details,
            'location' => $this->location,
            // 'device_udid' => $this->device_udid,
            'push_token' => $this->push_token,
            'sos_request_id' => $this->sos_request_id,
            'sos_request_details' => $sos_request,
            'response_status' => $this->response_status,
        ];
    }
}
