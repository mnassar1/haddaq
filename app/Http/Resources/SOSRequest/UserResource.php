<?php

namespace App\Http\Resources\SOSRequest;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed profile_image_url
 * @property mixed mobile
 * @property mixed full_name
 * @property mixed id
 */
class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'phone' => $this->mobile,
            //'address' => $this->address,
            //'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",
        ];
    }
}
