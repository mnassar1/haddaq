<?php

namespace App\Http\Resources\SOSRequest;

use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed city_id
 * @property mixed profile_image_url
 * @property mixed savior_with_price
 * @property mixed user_types_id
 * @property mixed mobile
 * @property mixed email
 * @property mixed full_name
 * @property mixed id
 */
class UserCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $city_details = \App\Model\City::find($this->city_id);
        // return $request->user_id;
        $is_favourite = 0;
        if ($request->user_id != NULL || $request->user_id != 0) {
            $type = 'Savior';
            $favourites_details = \App\Model\Favorites::where(['type' => $type, 'user_id' => $request->user_id, 'item_id' => $this->id])->first();
            if ($favourites_details) {
                $is_favourite = 1;
            }
        }
        return [
            'id' => $this->id,
            'is_favourite' => $is_favourite,
            'name' => $this->full_name,
            'email' => $this->email,
            'phone' => $this->mobile,
            'user_type_id' => $this->user_types_id,
            'savior_with_price' => $this->savior_with_price,
            //'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",
            'city_id' => ($this->city_id) ? ($this->city_id) : "",
            'city_details' => ($this->city_id) ? new CityResource($city_details) : [],
            // 'address' => ($this->address) ? $this->address : "",
            // 'is_verified' => ($this->is_verified && $this->is_verified == 1) ? true : false,
            // 'is_blocked' => ($this->is_blocked && $this->is_blocked == 1) ? true : false ,
            // 'is_admin' => $this->is_admin
        ];
    }
}
