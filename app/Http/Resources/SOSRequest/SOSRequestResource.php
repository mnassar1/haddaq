<?php

namespace App\Http\Resources\SOSRequest;

use App\Http\Resources\ProblemTypes\ProblemTypeResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed description
 * @property mixed sos_problem_type_id
 * @property mixed push_token
 * @property mixed device_udid
 * @property mixed location
 * @property mixed created_at
 * @property mixed mobile
 * @property mixed user_id
 * @property mixed id
 * @property mixed sos_request_status
 */
class SOSRequestResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $sos_problem_type_details = \App\Model\ProblemType::find($this->sos_problem_type_id);
        if ($sos_problem_type_details) {
            $sos_problem_type_details = new ProblemTypeResource($sos_problem_type_details);
        }

        $saviors_ids = \App\Model\SOSResponse::where('sos_request_id', $this->id)->pluck('user_id');
        $saviors_list = \App\User::whereIn('id', $saviors_ids)->get();
        if ($this->user_id) {
            $user_details = \App\User::find($this->user_id);
            $user_details = new UserResource($user_details);
        }
        //return $saviors_list;
        return [
            'sos_request_status' => ($this->sos_request_status) ? $this->sos_request_status : "Opening",
            'id' => $this->id,
            'collection_name' => config('database.default') .'-sos_request-' . $this->id,
            'user_id' => ($this->user_id) ? $this->user_id : "0",
            'user_details' => ($this->user_id) ? $user_details : [],
            'mobile' => $this->mobile,
            'created_at' => $this->created_at->toDateString() . ' ' . $this->created_at->toTimeString(), 'location' => $this->location,
            'device_udid' => $this->device_udid,
            'push_token' => $this->push_token,
            'sos_problem_type_id' => ($this->sos_problem_type_id) ? $this->sos_problem_type_id : "0",
            'sos_problem_type_details' => ($this->sos_problem_type_id) ? $sos_problem_type_details : [],
            'description' => ($this->description) ? $this->description : "",
            'saviors' => (!empty($saviors_list)) ? UserCollection::collection($saviors_list) : [],
        ];
    }
}
