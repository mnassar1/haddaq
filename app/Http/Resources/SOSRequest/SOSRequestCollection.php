<?php

namespace App\Http\Resources\SOSRequest;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed closed_by
 * @property mixed created_at
 * @property mixed sos_request_status
 * @property mixed description
 * @property mixed sos_problem_type_id
 * @property mixed push_token
 * @property mixed device_udid
 * @property mixed location
 * @property mixed user_id
 * @property mixed id
 */
class SOSRequestCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $sos_problem_type_details = \App\Model\ProblemType::find($this->sos_problem_type_id);
        //$sos_problem_type_details = new ProblemTypeResource($sos_problem_type_details);
        $sos_problem_type_name = ($sos_problem_type_details) ? $sos_problem_type_details->problem_type : "Not Defined";
        //return $sos_problem_type_name;
        $user_details = \App\User::find($this->user_id);
        $sender_name = ($user_details) ? $user_details->full_name : "Guest";
        //$user_details = new UserResource($user_details);

        ///return $sender_name;
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'sender_name' => $sender_name,//(!null($user_details)) ? $user_details->full_name : "Guest",
            'location' => $this->location,
            'device_udid' => $this->device_udid,
            'push_token' => $this->push_token,
            'sos_problem_type_id' => $this->sos_problem_type_id,
            'sos_problem_type_name' => $sos_problem_type_name,//($sos_problem_type_details) ? $sos_problem_type_details->problem_type : "Not Defind",
            'description' => $this->description,
            'sos_request_status' => $this->sos_request_status,
            'created_at' => $this->created_at->toDateString() . ' ' . $this->created_at->toTimeString(),
            'closed_by' => $this->closed_by,

        ];
    }
}
