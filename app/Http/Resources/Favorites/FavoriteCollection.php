<?php

namespace App\Http\Resources\Favorites;

use App\Http\Resources\AppAds\AppAdsResource;
use App\Http\Resources\FixingShops\FixingShopsResource;
use App\Http\Resources\User\CompanyResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\Resource;


/**
 * @property mixed created_at
 * @property mixed type
 * @property mixed item_id
 * @property mixed id
 */
class FavoriteCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $connection = 'mysql';
        if ($this->country_code) {
            $connection = $this->country_code;
        }
        switch ($this->type) {

            case 'Company':
                $item_details = \App\User::find($this->item_id);
                $item_details = ($item_details) ? new CompanyResource($item_details) : [];
                break;
            case 'FixingShop':
                $item_details = \App\Model\FixingShops::on($connection)->find($this->item_id);
                $item_details = ($item_details) ? new FixingShopsResource($item_details) : [];
                break;
            case 'Product':
                $item_details = \App\Model\AppAds::on($connection)->find($this->item_id);
                $item_details = ($item_details) ? new AppAdsResource($item_details) : [];
                break;
            case 'Savior':
                $item_details = \App\User::find($this->item_id);
                $item_details = ($item_details) ? new UserResource($item_details) : [];
                break;


            default:
                $item_details = [];
                break;
        }

        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'item_details' => $item_details,
            'type' => $this->type,
            'created_at' => $this->created_at->toDateString(),
            'country' => $this->country_code,
        ];
    }
}
