<?php

namespace App\Http\Resources\Used;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed group_name_ar
 * @property mixed group_name
 * @property mixed id
 */
class CategoryGroupCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->group_name,
            'name_ar' => $this->group_name_ar,

        ];
    }
}
