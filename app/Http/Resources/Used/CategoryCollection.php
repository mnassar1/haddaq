<?php

namespace App\Http\Resources\Used;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed category_type
 * @property mixed icon
 * @property mixed category_name_ar
 * @property mixed category_name
 * @property mixed id
 */
class CategoryCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->category_name,
            'name_ar' => $this->category_name_ar,
            'profile_image_url' => ($this->icon != NULL || $this->icon != "") ? asset("storage/" . $this->icon) : "",
            'category_type' => $this->category_type,

        ];
    }
}
