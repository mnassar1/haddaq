<?php

namespace App\Http\Resources\CoveredArea;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed area_name
 * @property mixed id
 */
class CoveredAreaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'area_name' => $this->area_name,

        ];
    }
}
