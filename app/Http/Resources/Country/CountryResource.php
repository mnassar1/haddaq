<?php

namespace App\Http\Resources\Country;

use App\Http\Resources\City\CityCollection;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed cities
 * @property mixed phonecode
 * @property mixed name_ar
 * @property mixed name_en
 * @property mixed id
 */
class CountryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'phone_code' => $this->phonecode,
            'cities' => CityCollection::collection($this->cities)
        ];
    }
}
