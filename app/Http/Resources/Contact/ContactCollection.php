<?php

namespace App\Http\Resources\Contact;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed id
 * @property mixed content_en
 * @property mixed content_ar
 * @property mixed title_en
 * @property mixed title_ar
 * @property mixed type
 */
class ContactCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'title' => [
                'ar' => $this->title_ar,
                'en' => $this->title_en
            ],
            'content' => [
                'ar' => $this->content_ar,
                'en' => $this->content_en
            ],
            'url' => route('Contacts.show', ['id' => $this->id])
        ];
    }
}
