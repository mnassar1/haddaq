<?php

namespace App\Http\Resources\Emergency;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed mobile
 * @property mixed name_ar
 * @property mixed name
 * @property mixed id
 */
class EmergencyCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name,
            'name_ar' => $this->name_ar,
            'mobile' => $this->mobile,

        ];
    }
}
