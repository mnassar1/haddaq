<?php

namespace App\Http\Resources\AdminAds;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed title
 * @property mixed created_at
 * @property mixed fixing_shops_or_home
 * @property mixed is_banar
 * @property mixed ordering
 * @property mixed active
 * @property mixed views_count
 * @property mixed images_ar
 * @property mixed images
 * @property mixed details_ar
 * @property mixed details
 * @property mixed title_ar
 * @property mixed id
 */
class AdminAdsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title_en' => $this->title,
            'title_ar' => $this->title_ar,
            'details_en' => $this->details,
            'details_ar' => $this->details_ar,
            'images' => ($this->images != NULL || $this->images != "") ? asset("storage/" . $this->images) : "",
            'images_ar' => ($this->images_ar != NULL || $this->images_ar != "") ? asset("storage/" . $this->images_ar) : "",
            'views_count' => $this->views_count,
            'active' => $this->active,
            'ordering' => $this->ordering,
            'is_banar' => $this->is_banar,
            'fixing_shops_or_home' => $this->fixing_shops_or_home,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
