<?php

namespace App\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed content_en
 * @property mixed content_ar
 * @property mixed title_en
 * @property mixed title_ar
 * @property mixed type
 * @property mixed id
 */
class CmsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'title_ar' => $this->title_ar,
            'title_en' => $this->title_en,
            'content_ar' => $this->content_ar,
            'content_en' => $this->content_en,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
