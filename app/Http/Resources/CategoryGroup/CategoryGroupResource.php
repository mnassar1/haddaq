<?php

namespace App\Http\Resources\CategoryGroup;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed group_name_ar
 * @property mixed group_name
 * @property mixed id
 */
class CategoryGroupResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'group_name_en' => $this->group_name,
            'group_name_ar' => $this->group_name_ar,


        ];
    }
}
