<?php

namespace App\Http\Resources\Shipment;

use App\Helpers\ResourceHelper;
use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed is_finished
 * @property mixed created_at
 * @property mixed id
 * @property mixed photo
 * @property mixed note
 * @property mixed potential_days_to
 * @property mixed potential_days_from
 * @property mixed price
 * @property mixed potential_fees
 * @property mixed length
 * @property mixed width
 * @property mixed height
 * @property mixed weight
 * @property mixed to_city
 * @property mixed to_country
 * @property mixed country_to
 * @property mixed from_city
 * @property mixed from_country
 * @property mixed country_from
 * @property mixed title
 */
class ShipmentCollection extends Resource
{
    use ResourceHelper;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'from' => [
                'id' => $this->country_from,
                'name_ar' => $this->from_country->name_ar,
                'name_en' => $this->from_country->name_en,
                'city' => new CityResource($this->from_city)
            ],
            'to' => [
                'id' => $this->country_to,
                'name_ar' => $this->to_country->name_ar,
                'name_en' => $this->to_country->name_en,
                'city' => new CityResource($this->to_city)
            ],
            'weight' => (float)$this->weight,
            'dimensions' => [
                'height' => (float)$this->height,
                'width' => (float)$this->width,
                'length' => (float)$this->length,
            ],
            'potential_fees' => (float)$this->potential_fees,
            'price' => (float)$this->price,
            'potential_days' => [
                'from' => $this->potential_days_from,
                'to' => $this->potential_days_to,
            ],
            'note' => $this->note,
            'photo' => $this->photo,
            'url' => $this->getCurrentUrl('Shipment', $this->id),
            'posted_at' => $this->created_at->timestamp,
            'is_finished' => $this->is_finished,
            'owner' => isset($this->user) ? ['id' => $this->user->id, 'name' => $this->user->name] : null,
            'requests' => $this->requests('pending')->count()
        ];
    }
}
