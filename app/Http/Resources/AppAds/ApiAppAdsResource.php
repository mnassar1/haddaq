<?php

namespace App\Http\Resources\AppAds;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\City\CityResource;
use App\Http\Resources\User\CompanyResource;
use Illuminate\Http\Resources\Json\Resource;


/**
 * @property mixed company_id
 * @property mixed created_at
 * @property mixed views_count
 * @property mixed block_reason
 * @property mixed used_status
 * @property mixed category_id
 * @property mixed price
 * @property mixed details
 * @property mixed title
 * @property mixed id
 * @property mixed approved_status
 * @property mixed user_id
 * @property mixed images
 */
class ApiAppAdsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */


    public function toArray($request)
    {
        $category_details = \App\Model\Category::find($this->category_id);
        $company_details = \App\User::find($this->company_id);

        $city_details = \App\Model\City::find($company_details->city_id);
        //return $city_details;
        //$result = new CategoryResource($category_details);
        //return $result;
        $images_array = json_decode($this->images);
        //return $images_array;
        $images = array();
        foreach ($images_array as $image) {
            $images[] = asset("storage/" . $image);
        }
        $is_favourite = 0;
        if ($this->user_id != NULL || $this->user_id != 0) {
            $type = 'Product';
            $favourites_details = \App\Model\Favorites::where(['type' => $type, 'user_id' => $this->user_id, 'item_id' => $this->id])->first();
            if ($favourites_details) {
                $is_favourite = 1;
            }
        }
//        if ($company_details->user_types_id !=3){
//
//        }

        if ($this->approved_status != null) {
            $approve_state = $this->approved_status;
        } else {
            $approve_state = "";
        }
        $returned_data = [
            'id' => $this->id,
            'is_favourite' => $is_favourite,
            'title' => $this->title,
            'details' => $this->details,
            'images' => (!empty($images)) ? $images : [],
            'price' => (float)$this->price,
            'city_id' => ($company_details->city_id) ? $company_details->city_id : "",
            'city_details' => ($company_details->city_id) ? new CityResource($city_details) : null,
            'category_id' => $this->category_id,
            'category_details' => new CategoryResource($category_details),
            'used_status' => $this->used_status,
            'approved_status' => $approve_state,
            'block_reason' => ($this->block_reason) ? $this->block_reason : "",
            'views_count' => ($this->views_count) ? $this->views_count : "0",
            'updated_at' => $this->created_at->toDateString(),
            'created_at' => $this->created_at->toDateString()
        ];

        $returned_data["company_id"] = $this->company_id;
        $returned_data["company_details"] = new CompanyResource($company_details);

        return $returned_data;
    }
}
