<?php

namespace App\Http\Resources\AppAds;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed views_count
 * @property mixed block_reason
 * @property mixed used_status
 * @property mixed category_id
 * @property mixed company_id
 * @property mixed price
 * @property mixed details
 * @property mixed title
 * @property mixed id
 * @property mixed approved_status
 * @property mixed images
 */
class AppAdsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $images_array = json_decode($this->images);
        //return $images_array;
        $images = array();
        foreach ($images_array as $image) {
            $images[] = asset("storage/" . $image);
        }

        if ($this->approved_status != null) {
            $approve_state = $this->approved_status;
        } else {
            $approve_state = "";
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'details' => $this->details,
            'images' => (!empty($images)) ? $images : [],
            'price' => (float)$this->price,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id,
            'used_status' => ($this->used_status) ? $this->used_status : "0",
            'approved_status' => $approve_state,
            'block_reason' => ($this->block_reason) ? $this->block_reason : "",
            'views_count' => ($this->views_count) ? $this->views_count : "0",
            'updated_at' => $this->created_at->toDateString(),
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
