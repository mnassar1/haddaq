<?php

namespace App\Http\Resources\CompanyType;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed number_of_notifications
 * @property mixed type_name_ar
 * @property mixed type_name
 * @property mixed id
 */
class CompanyTypeCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type_name_en' => $this->type_name,
            'type_name_ar' => $this->type_name_ar,
            'number_of_notifications' => $this->number_of_notifications,
        ];
    }
}
