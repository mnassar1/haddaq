<?php

namespace App\Http\Resources\NotificationsCenter;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed title
 * @property mixed id
 */
class NotificationsCenterResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
