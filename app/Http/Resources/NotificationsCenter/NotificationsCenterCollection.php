<?php

namespace App\Http\Resources\NotificationsCenter;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed item_id
 * @property mixed body
 * @property mixed title
 * @property mixed type
 * @property mixed state
 * @property mixed id
 */
class NotificationsCenterCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (!empty($this->url)) {
            if ($this->type == "new_ads") {
                $url = asset("storage/" . $this->url);
            } else {
                $url = $this->url;
            }
        } else {
            $url = "";
        }
        return [
            'id' => $this->id,
            'type' => $this->type,
            'title' => $this->title,
            'body' => $this->body,
            'url' => $url,
            "item_id" => $this->item_id,
            'state' => $this->state,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
