<?php

namespace App\Http\Resources\FixingShops;

use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed city_id
 * @property mixed address_ar
 * @property mixed address
 * @property mixed email
 * @property mixed mobile
 * @property mixed location
 * @property mixed fixing_shops_types_id
 * @property mixed images
 * @property mixed about_ar
 * @property mixed about
 * @property mixed shop_name_ar
 * @property mixed shop_name
 * @property mixed id
 * @property mixed user_id
 * @property mixed sliders
 */
class FixingShopsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $city_details = \App\Model\City::find($this->city_id);
        $images_array = json_decode($this->sliders);

        $images = array();
        if (!empty($images_array)) {
            foreach ($images_array as $image) {
                $images[] = asset("storage/" . $image);
            }
        }

        $is_favourite = 0;
        if ($request->user_id != NULL || $request->user_id != 0) {
            $type = 'FixingShop';
            $favourites_details = \App\Model\Favorites::where(['type' => $type, 'user_id' => $request->user_id, 'item_id' => $request->id])->first();
            if ($favourites_details) {
                $is_favourite = 1;
            }
        }

        return [
            'id' => $this->id,
            'is_favourite' => $is_favourite,
            'name_en' => $this->shop_name,
            'name_ar' => $this->shop_name_ar,
            'about_en' => "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>HaddaQ</title></head><body>" . $this->about . " </body></html>",
            'about_ar' => "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>HaddaQ</title></head><body>" . $this->about_ar . " </body></html>",
            'profile_image_url' => ($this->images != NULL || $this->images != "") ? asset("storage/" . $this->images) : "",
            'sliders' => (!empty($images)) ? $images : [],
            'fixing_shops_types_id' => $this->fixing_shops_types_id,
            'location' => $this->location,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'address_en' => $this->address,
            'address_ar' => $this->address_ar,
            'city_id' => ($this->city_id) ? ($this->city_id) : "",
            'city_details' => ($this->city_id) ? new CityResource($city_details) : [],

        ];
    }
}
