<?php

namespace App\Http\Resources\Request;

use App\Http\Resources\Shipment\ShipmentResource;
use App\Http\Resources\Trip\TripResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed status
 * @property mixed notes
 * @property mixed type
 * @property mixed id
 */
class RequestResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $trip = $this->trip();
        $shipment = $this->shipment();
        return [
            'id' => $this->id,
            'type' => $this->type,
            'trip' => new TripResource($trip),
            'shipment' => new ShipmentResource($shipment),
            'notes' => $this->notes,
            'status' => $this->status
        ];
    }
}
