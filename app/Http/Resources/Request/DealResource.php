<?php

namespace App\Http\Resources\Request;

use App\Helpers\SettingsHelper;
use App\Http\Resources\Shipment\ShipmentResource;
use App\Http\Resources\Trip\TripResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed price
 * @property mixed status
 * @property mixed qr_code_string
 * @property mixed images
 * @property mixed created_at
 * @property mixed delivery_time
 * @property mixed delivery_place
 * @property mixed pickup_time
 * @property mixed pickup_place
 * @property mixed id
 * @property mixed request
 * @property mixed shipper
 * @property mixed traveller
 */
class DealResource extends Resource
{
    use SettingsHelper;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $traveller = $this->traveller;
        $shipper = $this->shipper;
        $request_data = $this->request;
        $trip = $request_data->trip();
        $shipment = $request_data->shipment();
        $shipment_details = $shipment->details;
        $settings = $this->get_settings();
        return [
            'id' => $request_data->id,
            'type' => $request_data->type,
            'trip' => new TripResource($trip),
            'shipment' => new ShipmentResource($shipment),
            'notes' => $request_data->notes,
            'status' => $request_data->status,
            'deal_id' => $this->id,
            'accepted' => ($request_data->accepted_log) ? $request_data->accepted_log->created_at->format('Y-m-d h:i:s') : null,
            'information' => (null !== $this->pickup_time && null !== $this->pickup_place && null !== $this->delivery_time && null !== $this->delivery_place) ? [
                'pickup' => [
                    'place' => $this->pickup_place,
                    'time' => $this->pickup_time
                ],
                'delivery' => [
                    'place' => $this->delivery_place,
                    'time' => $this->delivery_time
                ],
                'price' => $this->price,
                'commission' => $this->get_company_commission($settings),
                'date' => $this->created_at->format('Y-m-d h:i:s')
            ] : null,
            'images' => $this->images,
            'qr_code' => ($this->qr_code_string) ? [
                'string' => $this->qr_code_string,
                'date' => ($request_data->qr_log) ? $request_data->qr_log->created_at->format('Y-m-d h:i:s') : ''
            ] : null,
            'negotiation' => ($request_data->negotiation_log) ? $request_data->negotiation_log->created_at->format('Y-m-d h:i:s') : null,
            'delegated_person' => ($shipment_details && $shipment_details->shipper_type == 'delegated_person') ? [
                'name' => $shipment_details->name,
                'id_number' => $shipment_details->id_number,
                'image' => $shipment_details->image,
                'date' => $shipment_details->created_at->format('Y-m-d h:i:s')
            ] : null,
            'freight_company' => ($shipment_details && $shipment_details->shipper_type == 'freight_company') ? [
                'name' => $shipment_details->name,
                'image' => $shipment_details->image,
                'date' => $shipment_details->created_at->format('Y-m-d h:i:s')
            ] : null,
            'deal_status' => $this->status
        ];
    }

    /**
     * Calculate company price commission
     * @param $settings
     * @return float
     */
    private function get_company_commission($settings)
    {
        $commission = (float)$settings['commission_constant'] + ((float)$settings['commission_percent'] * $this->price);
        return round($commission, 2);
    }
}
