<?php

namespace App\Http\Resources\City;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed country_id
 * @property mixed name_ar
 * @property mixed name_en
 * @property mixed id
 */
class CityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'country_id' => $this->country_id
        ];
    }
}
