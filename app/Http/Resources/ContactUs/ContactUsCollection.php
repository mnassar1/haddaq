<?php

namespace App\Http\Resources\ContactUs;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed status
 * @property mixed description
 * @property mixed subject
 * @property mixed type
 * @property mixed phone
 * @property mixed email
 * @property mixed name
 * @property mixed id
 */
class ContactUsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'type' => $this->type,
            'subject' => $this->subject,
            'description' => $this->description,
            'status' => $this->status,
            'created_at' => $this->created_at
        ];
    }
}
