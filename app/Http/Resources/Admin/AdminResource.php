<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed is_admin
 * @property mixed is_verified
 * @property mixed profile_image_url
 * @property mixed register_type
 * @property mixed phone
 * @property mixed email
 * @property mixed name
 * @property mixed id
 */
class AdminResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'register_type' => ($this->register_type) ? $this->register_type : 'email',
            'profile_image_url' => $this->profile_image_url,
            'is_verified' => ($this->is_verified && $this->is_verified == 1) ? true : false,
            'is_admin' => ($this->is_admin && $this->is_admin == 1) ? true : false

        ];
    }
}
