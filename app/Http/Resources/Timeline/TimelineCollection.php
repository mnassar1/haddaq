<?php

namespace App\Http\Resources\Timeline;

use App\Http\Resources\Shipment\ShipmentResource;
use App\Http\Resources\Trip\TripResource;
use App\Model\Shipment;
use App\Model\Trip;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed object
 * @property mixed type
 * @property mixed object_id
 * @property mixed verb
 * @property mixed id
 */
class TimelineCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'verb' => $this->verb,
            'type' => $this->type,
            'object_id' => $this->object_id,
            'object' => $this->handleObjectResource($this->type, $this->object)
        ];
    }

    /**
     * Handle Object Resource
     * @param $type
     * @param $model
     * @return ShipmentResource|TripResource|null
     */
    private function handleObjectResource($type, $model)
    {
        if ($model != null) {
            $model_json = json_decode($model, true);
            if ($type == 'shipment') {
                return new ShipmentResource(new Shipment($model_json));
            } else if ($type == 'trip') {
                return new TripResource(new Trip($model_json));
            }
        }
        return null;
    }
}
