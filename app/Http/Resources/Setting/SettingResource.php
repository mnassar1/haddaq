<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed number_of_notifications
 * @property mixed id
 */
class SettingResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number_of_notifications' => $this->number_of_notifications,

        ];
    }
}
