<?php

namespace App\Http\Resources\Home;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed profile_image_url
 * @property mixed address
 * @property mixed city_id
 * @property mixed company_type_id
 * @property mixed user_types_id
 * @property mixed mobile
 * @property mixed email
 * @property mixed full_name
 * @property mixed id
 */
class CompanyCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'email' => $this->email,
            'phone' => $this->mobile,
            'user_type_id' => $this->user_types_id,
            'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'city' => ($this->city_id) ? $this->city_id : "",
            'address' => ($this->address) ? $this->address : "",
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",

        ];
    }
}
