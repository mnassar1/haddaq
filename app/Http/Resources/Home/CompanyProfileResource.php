<?php

namespace App\Http\Resources\Home;

use App\Http\Resources\City\CityResource;
use App\Http\Resources\CompanyBranch\CompanyBranchCollection;
use App\Http\Resources\CompanyType\CompanyTypeResource;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed about
 * @property mixed profile_image_url
 * @property mixed company_type_id
 * @property mixed address
 * @property mixed city_id
 * @property mixed mobile
 * @property mixed email
 * @property mixed full_name
 * @property mixed id
 * @property mixed user_id
 * @property mixed location
 */
class CompanyProfileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $city_details = \App\Model\City::find($this->city_id);
        $company_type_details = \App\Model\CompanyType::find($this->company_type_id);
        $company_branches = \App\Model\CompanyBranch::where('company_id',$this->id)->get();

        $images = array();
        if (!empty($images_array)) {
            foreach ($images_array as $image) {
            $images[] = asset("storage/".$image);
        }}

        $is_favourite = 0;
        if ($this->user_id != NULL || $this->user_id != 0) {
            $type = 'Company';
            $favourites_details = \App\Model\Favorites::where(['type' => $type, 'user_id' => $this->user_id, 'item_id' => $this->id])->first();
            if ($favourites_details) {
                $is_favourite = 1;
            }
        }

        $location = str_replace(' ', '', $this->location);
        $pieces = $pieces = explode(",", $location);
        $lat = isset($pieces[0])? $pieces[0]:'';
        $long = isset($pieces[1])? $pieces[1]:'';

        return [
            'id' => $this->id,
            'is_favourite' => $is_favourite,
            'name' => $this->full_name,
            'email' => $this->email,
            'phone' => $this->mobile,
            'location' => $this->location,
            'lat' => $lat,
            'long' => $long,
            'city_id' => ($this->city_id) ? ($this->city_id) : "",
            'city_details' => ($this->city_id) ? new CityResource($city_details) : [],
            'address' => ($this->address) ? $this->address : "",
            'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'company_type_details' => ($this->company_type_id) ? new CompanyTypeResource($company_type_details) : [],
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",
            'sliders' => (!empty($images)) ? $images : [],
            'about' => "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>HaddaQ</title></head><body>" . $this->about . " </body></html>",
            'company_branches' => (!empty($company_branches)) ? CompanyBranchCollection::collection($company_branches) : [],
        ];
    }
}
