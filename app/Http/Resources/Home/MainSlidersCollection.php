<?php

namespace App\Http\Resources\Home;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed ordering
 * @property mixed active
 * @property mixed views_count
 * @property mixed images_ar
 * @property mixed images
 * @property mixed details_ar
 * @property mixed details
 * @property mixed title_ar
 * @property mixed title
 * @property mixed id
 */
class MainSlidersCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title_en' => $this->title,
            'title_ar' => $this->title_ar,
            'details_en' => "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>HaddaQ</title></head><body>" . $this->details . " </body></html>",
            'details_ar' => "<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>HaddaQ</title></head><body>" . $this->details_ar . " </body></html>",
            'images_en' => ($this->images != NULL || $this->images != "") ? asset("storage/" . $this->images) : "",
            'images_ar' => ($this->images_ar != NULL || $this->images_ar != "") ? asset("storage/" . $this->images_ar) : "",
            'views_count' => $this->views_count,
            'active' => $this->active,
            'ordering' => $this->ordering,
            //'is_banar' => $this->is_banar,
            //'fixing_shops_or_home' => $this->fixing_shops_or_home,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
