<?php

namespace App\Http\Resources\Home;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

date_default_timezone_set('Asia/Kuwait');

/**
 * @property mixed created_at
 * @property mixed second_low_tide
 * @property mixed first_low_tide
 * @property mixed second_high_tide
 * @property mixed first_high_tide
 * @property mixed sea_waves_by_night
 * @property mixed sea_waves_by_day
 * @property mixed wind_speed
 * @property mixed lowest_temp
 * @property mixed highest_temp
 */
class WeatherResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    //echo $mytime->toDateTimeString();

    public function toArray($request)
    {
        $mytime = Carbon::now();

        return [
            'date' => $mytime->toDateString(),
            'highest_temp' => $this->highest_temp,
            'lowest_temp' => $this->lowest_temp,
            'wind_speed' => $this->wind_speed,
            'sea_waves_by_day' => $this->sea_waves_by_day,
            'sea_waves_by_night' => $this->sea_waves_by_night,
            'first_high_tide' => $this->first_high_tide,
            'second_high_tide' => $this->second_high_tide,
            'first_low_tide' => $this->first_low_tide,
            'second_low_tide' => $this->second_low_tide,
            //'country_id'=>$this->country_id,
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
