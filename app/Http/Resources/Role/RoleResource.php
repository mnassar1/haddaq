<?php

namespace App\Http\Resources\Role;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed action_name
 * @property mixed level
 * @property mixed name
 * @property mixed id
 */
class RoleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'level' => $this->level,
            'action_name' => $this->action_name
        ];
    }
}
