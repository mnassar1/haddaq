<?php

namespace App\Http\Resources\Deal;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed deal_id
 * @property mixed note
 * @property mixed type
 * @property mixed id
 */
class DealNotesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'type' => $this->type,
            'note' => $this->note,
            'deal_id' => $this->deal_id
        ];
    }
}
