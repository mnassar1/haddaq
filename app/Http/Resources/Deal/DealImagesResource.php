<?php

namespace App\Http\Resources\Deal;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed caption
 * @property mixed image
 * @property mixed id
 */
class DealImagesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'image' => $this->image,
            'caption' => $this->caption
        ];
    }
}
