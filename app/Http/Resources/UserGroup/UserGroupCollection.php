<?php

namespace App\Http\Resources\UserGroup;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed name
 * @property mixed id
 */
class UserGroupCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
