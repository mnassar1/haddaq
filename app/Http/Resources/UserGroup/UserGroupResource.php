<?php

namespace App\Http\Resources\UserGroup;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed name
 * @property mixed id
 */
class UserGroupResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
