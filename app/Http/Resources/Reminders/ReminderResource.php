<?php

namespace App\Http\Resources\Reminders;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed expiration_date
 * @property mixed reminder_date
 * @property mixed title
 * @property mixed id
 */
class ReminderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'reminder_date' => $this->reminder_date,
            'expiration_date' => $this->expiration_date,
        ];
    }
}
