<?php

namespace App\Http\Resources\Faq;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed id
 * @property mixed question_order
 * @property mixed answer_en
 * @property mixed answer_ar
 * @property mixed question_en
 * @property mixed question_ar
 */
class FaqCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'question_ar' => $this->question_ar,
            'question_en' => $this->question_en,
            'answer_ar' => $this->answer_ar,
            'answer_en' => $this->answer_en,
            'order' => $this->question_order,
            'url' => route('Faq.show', ['id' => $this->id]),
            'created_at' => $this->created_at->toDateTimeString()
        ];
    }
}
