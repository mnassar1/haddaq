<?php

namespace App\Http\Resources\FixingShop;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed city_id
 * @property mixed address_ar
 * @property mixed address
 * @property mixed email
 * @property mixed mobile
 * @property mixed location
 * @property mixed fixing_shops_types_id
 * @property mixed images
 * @property mixed about_ar
 * @property mixed about
 * @property mixed shop_name_ar
 * @property mixed shop_name
 * @property mixed id
 * @property mixed sliders
 */
class FixingShopsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $images_array = json_decode($this->sliders);

        $images = array();
        if (!empty($images_array)) {
            foreach ($images_array as $image) {
                $images[] = asset("storage/" . $image);
            }
        }

        return [
            'id' => $this->id,
            'shop_name_en' => $this->shop_name,
            'shop_name_ar' => $this->shop_name_ar,
            'about_en' => $this->about,
            'about_ar' => $this->about_ar,
            'images' => ($this->images != NULL || $this->images != "") ? asset("storage/" . $this->images) : "",
            'sliders' => (!empty($images)) ? $images : [],
            'fixing_shops_types_id' => $this->fixing_shops_types_id,
            'location' => $this->location,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'address_en' => $this->address,
            'address_ar' => $this->address_ar,
            'city_id' => $this->city_id,

        ];
    }
}
