<?php

namespace App\Http\Resources\CompanyBranch;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed location
 * @property mixed address
 * @property mixed title
 * @property mixed id
 */
class CompanyBranchCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'address' => $this->address,
            'location' => $this->location,

        ];
    }
}
