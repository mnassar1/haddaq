<?php

namespace App\Http\Resources\CompanyAds;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed created_at
 * @property mixed views_count
 * @property mixed block_reason
 * @property mixed approved_status
 * @property mixed used_status
 * @property mixed category_id
 * @property mixed company_id
 * @property mixed price
 * @property mixed details
 * @property mixed title
 * @property mixed id
 */
class CompanyAdsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'details' => $this->details,
            //'images' => ($this->images != NULL || $this->images != "") ? asset("storage/".$this->images) :  "",
            'price' => (float)$this->price,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id,
            'used_status' => ($this->used_status) ? $this->used_status : "0",
            'approved_status' => ($this->approved_status) ? $this->approved_status : "0",
            'block_reason' => ($this->block_reason) ? $this->block_reason : "",
            'views_count' => ($this->views_count) ? $this->views_count : "0",
            'updated_at' => $this->created_at->toDateString(),
            'created_at' => $this->created_at->toDateString()
        ];
    }
}
