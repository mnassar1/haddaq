<?php

namespace App\Http\Resources\ProblemTypes;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed icon
 * @property mixed problem_type_ar
 * @property mixed problem_type
 * @property mixed id
 */
class ProblemTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'problem_type_en' => $this->problem_type,
            'problem_type_ar' => $this->problem_type_ar,
            'icon' => ($this->icon != NULL || $this->icon != "") ? asset("storage/" . $this->icon) : "",

        ];
    }
}
