<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed expire_date
 * @property mixed cvv
 * @property mixed card_number
 * @property mixed user
 */
class UserPaymentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name
            ],
            'card_number' => $this->card_number,
            'cvv' => $this->cvv,
            'expire_date' => $this->expire_date
        ];
    }
}
