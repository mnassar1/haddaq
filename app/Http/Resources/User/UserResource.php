<?php

namespace App\Http\Resources\User;

use App\Http\Resources\City\CityResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed is_mobile_verified
 * @property mixed created_at
 * @property mixed about
 * @property mixed another_token
 * @property mixed access_token
 * @property mixed is_admin
 * @property mixed is_blocked
 * @property mixed is_verified
 * @property mixed profile_image_url
 * @property mixed savior_with_price
 * @property mixed company_type_id
 * @property mixed user_types_id
 * @property mixed address
 * @property mixed city_id
 * @property mixed mobile
 * @property mixed country_code
 * @property mixed email
 * @property mixed full_name
 * @property mixed id
 * @property mixed sliders
 */
class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $city_details = \App\Model\City::find($this->city_id);

        $images_array = json_decode($this->sliders);

        $images = array();
        if (!empty($images_array)) {
            foreach ($images_array as $image) {
                $images[] = asset("storage/" . $image);
            }
        }

        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'email' => ($this->email) ? $this->email : "",
            'country_code' => ($this->country_code) ? $this->country_code : "",
            'phone' => $this->mobile,
            'city' => ($this->city_id) ? $this->city_id : "",
            'city_id' => ($this->city_id) ? $this->city_id : "",
            'city_details' => ($this->city_id) ? new CityResource($city_details) : [],
            'address' => ($this->address) ? $this->address : "",
            'user_type_id' => $this->user_types_id,
            'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'savior_with_price' => $this->savior_with_price,
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",
            'sliders' => (!empty($images)) ? $images : [],
            'is_verified' => ($this->is_verified && $this->is_verified == 1) ? true : false,
            'is_blocked' => ($this->is_blocked && $this->is_blocked == 1) ? true : false,
            'is_admin' => ($this->is_admin && $this->is_admin == 1) ? true : false,
            'access_token' => ($this->access_token) ? $this->access_token['token'] : "",
            'expires_at' => ($this->access_token) ? $this->access_token['expires_at'] : "",
            'login_token' => ($this->another_token) ? $this->another_token : "",
            'about' => ($this->about) ? $this->about : "",
            'created_at' => ($this->created_at) ? $this->created_at->toDateString() . ' ' . $this->created_at->toTimeString() : "",
            'is_mobile_verified' => ($this->is_mobile_verified) ? $this->is_mobile_verified : "0",
        ];
    }
}