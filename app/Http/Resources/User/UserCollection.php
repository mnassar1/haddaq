<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed is_admin
 * @property mixed created_at
 * @property mixed is_blocked
 * @property mixed is_verified
 * @property mixed profile_image_url
 * @property mixed address
 * @property mixed city_id
 * @property mixed company_type_id
 * @property mixed user_types_id
 * @property mixed mobile
 * @property mixed email
 * @property mixed full_name
 * @property mixed id
 */
class UserCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'email' => $this->email,
            'phone' => $this->mobile,
            'user_type_id' => $this->user_types_id,
            'company_type_id' => ($this->company_type_id) ? $this->company_type_id : "",
            'city' => ($this->city_id) ? $this->city_id : "",
            'address' => ($this->address) ? $this->address : "",
            'profile_image_url' => ($this->profile_image_url != NULL || $this->profile_image_url != "") ? asset("storage/" . $this->profile_image_url) : "",
            'is_verified' => ($this->is_verified && $this->is_verified == 1) ? true : false,
            'is_blocked' => ($this->is_blocked && $this->is_blocked == 1) ? true : false,
            'created_at' => ($this->created_at) ? $this->created_at->toDateString() . ' ' . $this->created_at->toTimeString() : "",
            'is_admin' => $this->is_admin
        ];
    }
}
