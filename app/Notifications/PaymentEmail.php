<?php

namespace App\Notifications;

use App\Helpers\RequestHelper;
use App\Model\Deal;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PaymentEmail extends Notification
{
    use Queueable;
    use RequestHelper;
    public $user;
    public $shipper;
    public $deal;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param User $shipper
     * @param Deal $deal
     */
    public function __construct(User $user, User $shipper, Deal $deal)
    {
        $this->user = $user;
        $this->shipper = $shipper;
        $this->deal = $deal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $amount = $this->deal->price;
        $commission = $this->calculate_commission($amount);
        return (new MailMessage)
            ->line('Payment Confirmed')
            ->line("$this->shipper->name has been paid $commission$ to your account.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
