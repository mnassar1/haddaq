<?php

namespace App;

// use App\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PasswordResets extends Model
{
    use Notifiable;
    public $timestamps = false;
    protected $primaryKey = "email";
    protected $fillable = ['email', 'token'];

    /**
     * send reset password token
     * @return void
     */
    public function sendResetPassword()
    {
        $this->notify(new ResetPassword($this));
    }
}
