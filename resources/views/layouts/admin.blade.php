<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'HaddaQ') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<style>
    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
        color: #fff;
        font-weight: bold;
        background-color: #ff8c4c;
    }

    .navbar-default .navbar-nav > li > a, .navbar-default .navbar-text {
        color: #fff;
    }

    .navbar-default {
        background-color: #4b95b9;
    }

    .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover {
        color: #e8e8e8;
        background-color: #4b95b9;
    }

    .navbar-default .navbar-brand {
        color: #fff;
    }


</style>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'HaddaQ') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                @if(Session::has('is_logged_in') && Session::get('is_logged_in') == true)

                    @if(Session::has('is_admin') && Session::get('is_admin') == 1)
                        <!-- <li class="nav-item {{Request::segment(2) == 'categories' ? 'active':''}}">
                            <a class="nav-link" href="{{route('categories.index')}}">Categories</a>
                          </li>

			                   <li class="nav-item {{Request::segment(2) == 'category_groups' ? 'active':''}}">
                            <a class="nav-link" href="{{route('category_groups.index')}}">Category Groups</a>
                          </li> -->
                            {{--
                                                        <li class="dropdown {{Request::segment(2) == 'categories' ? 'active':''}} {{Request::segment(2) == 'category_groups' ? 'active':''}}" >
                                                        <a href="{{route('categories.index')}}" class="dropbtn {{Request::segment(2) == 'categories' ? 'active':''}}">Categories</a>
                                                        <div class="dropdown-content">
                                                           <a class="nav-link {{Request::segment(2) == 'category_groups' ? 'active':''}}" href="{{route('category_groups.index')}}">Category Groups</a>

                                                        </div>

                                                      </li> --}}






                            @if(!(new \App\Helpers\Helper())->isMainDomain())
                                <li class="nav-item {{Request::segment(2) == 'app_ads' ? 'active':''}}">
                                    <a class="nav-link"
                                       href="{{route('app_ads.index')}}">@lang("labels.subscribers_ads")</a>
                                </li>
                                <li class="dropdown  {{Request::segment(2) == 'sos_requests' ? 'active':''}}">
                                    <a href="{{route('sos_requests.index')}}"
                                       class="dropbtn {{Request::segment(2) == 'sos_requests' ? 'active':''}}">@lang("labels.sos")</a>

                                </li>

                                <li class="nav-item {{Request::segment(2) == 'admin_ads' ? 'active':''}}">
                                    <a class="nav-link" href="{{route('admin_ads.index')}}">@lang("labels.banner")</a>
                                </li>
                                <li class="nav-item {{Request::segment(2) == 'fixing_shops' ? 'active':''}}">
                                    <a class="nav-link" href="{{route('fixing_shops.index')}}">@lang("labels.fixing")</a>
                                </li>



                                <li class="nav-item {{Request::segment(2) == 'contact_us' ? 'active':''}}">
                                    <a class="nav-link" href="{{route('contact_us.index')}}">@lang("labels.contact")</a>
                                </li>

                                <li class="dropdown  {{Request::segment(2) == 'users_push_setting' ? 'active':''}} {{Request::segment(2) == 'notifications' ? 'active':''}} {{Request::segment(2) == 'problem_types' ? 'active':''}} {{Request::segment(2) == 'company_types' ? 'active':''}} {{Request::segment(2) == 'categories' ? 'active':''}} {{Request::segment(2) == 'category_groups' ? 'active':''}} {{Request::segment(2) == 'cities' ? 'active':''}} {{Request::segment(2) == 'cms' ? 'active':''}} {{Request::segment(2) == 'emergency' ? 'active':''}}">
                                    <a href="#"
                                       class="dropbtn {{Request::segment(2) == 'users_push_setting' ? 'active':''}} {{Request::segment(2) == 'notifications' ? 'active':''}} {{Request::segment(2) == 'problem_types' ? 'active':''}} {{Request::segment(2) == 'company_types' ? 'active':''}} {{Request::segment(2) == 'categories' ? 'active':''}} {{Request::segment(2) == 'category_groups' ? 'active':''}} {{Request::segment(2) == 'cms' ? 'active':''}}  {{Request::segment(2) == 'emergency' ? 'active':''}}">@lang("labels.setting")</a>
                                    <div class="dropdown-content">

                                        <a class="nav-link {{Request::segment(2) == 'categories' ? 'active':''}}"
                                           href="{{route('categories.index')}}">@lang("labels.categories")</a>
                                        <a class="nav-link {{Request::segment(2) == 'category_groups' ? 'active':''}}"
                                           href="{{route('category_groups.index')}}">@lang("labels.Category_Groups")</a>
                                        <a class="nav-link {{Request::segment(2) == 'emergency' ? 'active':''}}"
                                           href="{{route('emergency.index')}}">@lang("labels.Emergency")</a>
                                        <a class="nav-link {{Request::segment(2) == 'cms' ? 'active':''}}"
                                           href="{{route('cms.index')}}">@lang("labels.CMS")</a>
                                        <a class="nav-link {{Request::segment(2) == 'cities' ? 'active':''}}"
                                           href="{{route('cities.index')}}">@lang("labels.Cities")</a>
                                        <a class="nav-link {{Request::segment(2) == 'problem_types' ? 'active':''}}"
                                           href="{{route('problem_types.index')}}">@lang("labels.Problem_Types")</a>
                                        <a class="nav-link {{Request::segment(2) == 'company_types' ? 'active':''}}"
                                           href="{{route('company_types.index')}}">@lang("labels.Company_Types")</a>
                                        <a class="nav-link {{Request::segment(2) == 'users_push_setting' ? 'active':''}}"
                                           href="{{route('users_push_setting.index')}}">@lang("labels.User_Push_Setting")</a>
                                        <a class="nav-link {{Request::segment(2) == 'notifications' ? 'active':''}}"
                                           href="{{route('notifications.index')}}">@lang("labels.Send_Notifications")</a>
                                        <a class="nav-link {{Request::segment(2) == 'covered_areas' ? 'active':''}}"
                                           href="{{route('covered_areas.index')}}">@lang("labels.Covered_Areas")</a>
                                    </div>
                                </li>
                            @else
                            <li class="dropdown {{Request::segment(2) == 'user' ? 'active':''}}">
                                <a href="{{route('user.index')}}"
                                   class="dropbtn {{Request::segment(2) == 'user' ? 'active':''}}">@lang("labels.subscribers")</a>

                            </li>
                            @endif



                        <!-- <li class="nav-item {{Request::segment(2) == 'settings' ? 'active':''}}">
                              <a class="nav-link" href="{{route('settings.index')}}">Settings</a>
                          </li> -->

                        @else
                            <li class="nav-item {{Request::segment(2) == 'company_profile' ? 'active':''}}">
                                <a class="nav-link"
                                   href="{{route('company_profile.index')}}">@lang("labels.profile")</a>
                            </li>

                            <li class="nav-item {{Request::segment(2) == 'company_branches' ? 'active':''}}">
                                <a class="nav-link"
                                   href="{{route('company_branches.index')}}">@lang("labels.branches")</a>
                            </li>


                            <li class="nav-item {{Request::segment(2) == 'company_ads' ? 'active':''}}">
                                <a class="nav-link" href="{{route('company_ads.index')}}">@lang("labels.my_ads")</a>
                            </li>


                        @endif

                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">

                    @if(!Session::has('is_logged_in') || Session::get('is_logged_in') != true)
                    <?php $domans = \App\Model\Domains::all(); ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true" v-pre>
                                Select Country <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo env('APP_URL', '');?>">
                                        <?php echo 'Main'?>
                                    </a>
                                </li>
                                <?php
                                    foreach ($domans as $item):
                                ?>
                                <li>
                                    <a href="<?php echo $item->sub_domain.'/'.Request::path();?>">
                                        <?php echo $item->name_en?>
                                    </a>
                                </li>
                                <?php
                                endforeach;
                                ?>

                            </ul>
                        </li>

                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        <li><a href="{{ route('password.request') }}">Forget Password</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true" v-pre>
                                Select Country <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <?php
                                $domans = \App\Model\Domains::all();
                                foreach ($domans as $item):
                                ?>
                                <li>
                                    <a href="<?php echo $item->sub_domain.'/'.Request::path();?>">
                                        <?php echo $item->name_en?>
                                    </a>
                                </li>
                                <?php
                                endforeach;
                                ?>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true" v-pre>
                                {{ Session::get('name') }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('profile') }}">
                                        @lang("labels.profile")
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('admin_logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('admin_logout') }}" method="GET"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true" v-pre>
                                @lang("labels.". Session::get('language')) <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <?php $LANG = (Session::get('language') == 'en') ? "ar" : "en";
                                    ?>
                                    <a href="{{ route('update_language_dashboard',$LANG)}}">
                                        <?php
                                        echo trans("labels." . $LANG);
                                        ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <dev class="row">
        @if($errors->any())
            <p class="alert alert-danger text-center">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </p>
        @endif
    </dev>
    @yield('content')
</div>
<style>
    .dropbtn {
        /*background-color: #4CAF50;*/
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }

    .dropdown .active {
        color: #555;
        background-color: #eee;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover { /*background-color: #f1f1f1*/
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown:hover .dropbtn {
        /* background-color: #3e8e41;*/
    }
</style>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=eaaz0jsd2yr30g69jj0dg51899p4huaougjn5q5jwnf7g6d9"></script>
<script> tinymce.init({
        selector: 'textarea',
        height: 200,
        menubar: false,
        file_browser_callback_types: 'file image media',
        plugins: [
            'advlist autolink lists link image charmap print preview textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code wordcount'
        ],
        toolbar: 'link | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat ',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });</script>

<script type="text/javascript">
    $(document).ready(function () {

        $.post("{{route('get_cities')}}", {country_id: 1}, function (res) {
            if (res.status_code == 200) {
                var cities = res.response.cities;
                var $el = $("#city_from_id");
                $el.empty(); // remove old options
                $el.append($("<option></option>")
                    .attr("value", '').text('Select City'));
                $.each(cities, function (idx, city) {
                    $el.append($("<option></option>")
                        .attr("value", city.id).text(city.name_en));
                });
                $el.removeAttr('disabled');
            } else {
                $el.attr("disabled", "disabled");
                alert(res.error.message);
            }
        });
    });


</script>
<script type="text/javascript">
    function update_user_flag(select, field, user_id) {
        var value = select.value;
        $.post("{{route('update_user_flag')}}",
            {
                field: field,
                user_id: user_id,
                value: value
            }, function (response) {
                if (response.status_code == 200) {
                    alert('User successfully updated!');
                } else {
                    alert('Unexpected Error!');
                }
            });
    }
</script>


<script type="text/javascript">

    var category_type = $('input[type=radio][name=category_type]:checked').val();
    //console.log(category_type);
    if (category_type == '1' || category_type == '2') {
        $('#category_group_id').show();
    }
    else if (category_type == '3') {
        $('#category_group_id').hide();
    }

    $('input[type=radio][name=category_type]').change(function () {
        if (this.value == '1' || this.value == '2') {
            $('#category_group_id').show();
        }
        else if (this.value == '3') {
            $('#category_group_id').hide();
        }
    });
</script>

</body>
</html>
