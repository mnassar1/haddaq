<div class="row">
	<div class="col-sm-12">
		<form action="" method="">
			<table class="table">
				<tbody>
					<tr class="active">
						<th colspan="2" class="text-center">@lang('labels.from')</th>
						<th colspan="2" class="text-center">@lang('labels.to')</th>
					</tr>
					<tr>
						<td colspan="4">
							<div class="row">
								<div class="col-sm-3">
									<select class="form-control" name="country_from" id="country_from_id">
										<option value="0">@lang('labels.admin.select_country')</option>
										@foreach($countries as $country)
										<option value="{{$country->id}}" {{(Request::get('country_from') == $country->id) ? 'selected':''}}>{{$country->name_en}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-3">
									<select class="form-control" name="city_from" id="city_from_id" {{(isset($cities_from) && count($cities_from)) ? '':'disabled'}}>
										<option value="0">@lang('labels.admin.select_city')</option>
										@if(isset($cities_from) && count($cities_from))
										@foreach($cities_from as $city)
										<option value="{{$city->id}}" {{(Request::get('city_from') == $city->id) ? 'selected':''}}>{{$city->name_en}}</option>
										@endforeach
										@endif
									</select>
								</div>
								<div class="col-sm-3">
									<select class="form-control" name="country_to" id="country_to_id">
										<option value="0">@lang('labels.admin.select_country')</option>
										@foreach($countries as $country)
										<option value="{{$country->id}}" {{(Request::get('country_to') == $country->id) ? 'selected':''}}>{{$country->name_en}}</option>
										@endforeach
									</select>
								</div>	
								<div class="col-sm-3">
									<select class="form-control" name="city_to" id="city_to_id" {{(isset($cities_to) && count($cities_to)) ? '':'disabled'}}>
										<option value="0">@lang('labels.admin.select_city')</option>
										@if(isset($cities_to) && count($cities_to))
										@foreach($cities_to as $city)
										<option value="{{$city->id}}" {{(Request('city_to') == $city->id) ? 'selected' : ''}}>{{$city->name_en}}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
						</td>
					</tr>

					<!-- deal pickup and delivery date and time -->
					<tr class="active">
						<th colspan="2" class="text-center">@lang('labels.admin.pickup_time_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.delivery_time_range')</th>
					</tr>
					<tr>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input class="form-control" type="date" name="pickup_time_from" value="{{Request::get('pickup_time_from')}}" />
								</div>
								<div class="col-sm-6">
									<input class="form-control" type="date" name="pickup_time_to" value="{{Request::get('pickup_time_to')}}" />
								</div>
							</div>
						</td>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input class="form-control" type="date" name="delivery_time_from" value="{{Request::get('delivery_time_from')}}"/>
								</div>
								<div class="col-sm-6">
									<input class="form-control" type="date" name="delivery_time_to" value="{{Request::get('delivery_time_to')}}"/>
								</div>
							</div>
						</td>
					</tr>
					<!--  deal's status and shipment's weight filter-->
					<tr class="active">
						<th colspan="2">@lang('labels.weight')</th>
						<th colspan="1">@lang('labels.status')</th>
						<th colspan="1">@lang('labels.created_at')</th>
					</tr>
					<tr>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input class="form-control" type="text" name="weight_from" placeholder="Shipment weight from" value="{{Request::get('weight_from')}}" />
								</div>
								<div class="col-sm-6">
									<input class="form-control" type="text" name="weight_to" placeholder="Shipment weight to" value="{{Request::get('weight_to')}}" />
								</div>
							</div>
						</td>

						<td colspan="1">
							<select name="status" class="form-control">
								<option value="0">@lang('labels.admin.select_deal_status')</option>
								<option value="request" {{(Request::get('status') == 'request') ? 'selected' :''}}>Request</option>
								<option value="waiting" {{(Request::get('status') == 'waiting') ? 'selected' :''}}>Waiting</option>
								<option value="negotiation" {{(Request::get('status') == 'negotiation') ? 'selected' :''}}>Negotiation</option>
								<option value="confirmation" {{(Request::get('status') == 'confirmation') ? 'selected' :''}}>Confirmation</option>
								<option value="payment" {{(Request::get('status') == 'payment') ? 'selected' :''}}>Payment </option>
								<option value="confirm_payment" {{(Request::get('status') == 'confirm_payment') ? 'selected' :''}}>Confirm Payment</option>
								<option value="pickup_point" {{(Request::get('status') == 'pickup_point') ? 'selected' :''}}>Meet at pickup point</option>
								<option value="trip_started" {{(Request::get('status') == 'trip_started') ? 'selected' :''}}>Trip Started</option>
								<option value="delivery_point" {{(Request::get('status') == 'delivery_point') ? 'selected' :''}}>Delivery Point</option>
								<option value="closed" {{(Request::get('status') == 'closed') ? 'selected' :''}}>Closed</option>
								<option value="cancelled" {{(Request::get('status') == 'cancelled') ? 'selected' :''}}>Cancelled</option>
							</select>
						</td>
						<td colspan="1">
							<input class="form-control" type="date" name="created_at" value="{{Request::get('created_at')}}" />
						</td>
					</tr>

					<!-- creation date range -->
					<!-- search and reset buttons -->
					<tr>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
							</div>
						</td>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-12">
									<a class="btn btn-danger px-4 col-sm-6" href="{{route('deals.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>