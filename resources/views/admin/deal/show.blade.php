@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<div class="col-sm-12">#{{$deal->id}} {{ucfirst($deal->status)}} @lang('labels.admin.deal')</div>
					</div>
				</div>
				<div class="panel-body">

					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row text-center">
								<div class="col-sm-12">@lang('labels.admin.shipment')</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr>
										<td>
											<img src="http://via.placeholder.com/20x20" class="rounded-circle">
											<strong>
											<u>
												<a href="{{($deal->shipment) ? route('user.show',$deal->shipment->owner->id):"#"}}" target="_blank">{{($deal->shipment) ? $deal->shipment->owner->name:""}}</a>
											</u>
											</strong>
										</td>
									</tr>

								<!-- shipment data -->
									@if($shipment)
									<tr>
										<th colspan="4" class="text-center">{{$shipment->title}}</th>
									</tr>
									<tr class="active">
										<th colspan="2" class="text-center">Shipping From</th>
										<th colspan="2" class="text-center">Shipping to</th>
									</tr>
									<tr>
										<th colspan="2" class="text-center">{{$shipment->from->name_en}} \ {{$shipment->from->city->name_en}}</th>
										<th colspan="2" class="text-center">
											{{$shipment->to->name_en}} \ {{$shipment->to->city->name_en}}
										</th>
									</tr>
									<tr>
										<td colspan="4" class="text-center">
											<a href="{{route('shipment_show',$shipment->id)}}" target="_blank">@lang('labels.admin.shipment_details').....</a>
										</td>
									</tr>
									@endif
								</tbody>
							</table>

						</div>
					</div>

					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row text-center">
								<div class="col-sm-12">@lang('labels.admin.trip')</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr>
										<td>
											<img src="http://via.placeholder.com/20x20" class="rounded-circle">
											<strong><u><a href="{{($deal->trip->traveller) ? route('user.show',$deal->trip->traveller->id):"#"}}" target="_blank">{{($deal->trip->traveller) ? $deal->trip->traveller->name:""}}</a></u></strong>
										</td>
									</tr>
									<!-- trip data -->
									@if($trip)
									<tr class="active">
										<th colspan="2" class="text-center">@lang('labels.admin.traveling_from')</th>
										<th colspan="2" class="text-center">@lang('labels.admin.traveling_to')</th>
									</tr>
									<tr>
										<th colspan="2" class="text-center">{{$trip->from->name_en}} \ {{$trip->from->city->name_en}}</th>
										<th colspan="2" class="text-center">
											{{$trip->to->name_en}} \ {{$trip->to->city->name_en}}
										</th>
									</tr>
									<tr>
										<td colspan="4" class="text-center">
											<a href="{{route('trip_show',$trip->id)}}" target="_blank">@lang('labels.admin.show_trip_details')....</a>
										</td>
									</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>

					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row text-center">
								<div class="col-sm-12">@lang('labels.admin.deal')</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr>
										<th colspan="1">@lang('labels.admin.pickup_point')</th>
										<td colspan="3" class="danger">{{$deal->information->pickup->place}}</td>
									</tr>
									<tr>
										<th colspan="1">@lang('labels.admin.pickup_time')</th>
										<td colspan="3" class="danger">{{$deal->information->pickup->time}}</td>
									</tr>
									<tr>
										<th colspan="1">@lang('labels.admin.delivery_poing')</th>
										<td colspan="3" class="info">{{$deal->information->delivery->place}}</td>
									</tr>
									<tr>
										<th colspan="1"> @lang('labels.admin.delivery_time')</th>
										<td colspan="3" class="info">{{$deal->information->delivery->time}}</td>
									</tr>
									<!-- price and commission -->
									<tr>
										<th colspan="1">@lang('labels.price')</th>
										<td colspan="3">{{$deal->information->price}}</td>
									</tr>
									<tr>
										<th colspan="1">@lang('labels.comission')</th>
										<td colspan="3">{{$deal->information->commission}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row text-center">
								<div class="col-sm-12">@lang('labels.admin.deal_note')</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<!-- deal notes -->
									@if($notes)
									@if(count($notes))
									@foreach($notes as $note)
									<tr>
										<th colspan="1">{{$note->type}}</th>
										<td colspan="3">{{$note->note}}</td>
									</tr>
									@endforeach
									@endif
									@else
									<tr>
										<th class="text-center">@lang('labels.admin.deal_has_no_note')</th>
									</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>

					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row text-center">
								<div class="col-sm-12">@lang('labels.admin.deal_image')</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									@if($images)
									@if(count($images))
									@foreach($images as $image)
									<tr>
										<td colspan="4" class="text-center">
											<figure class="figure">
											  <img src="{{$image->image}}" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
											  <figcaption class="figure-caption">{{$image->caption}}</figcaption>
											</figure>
										</td>
									</tr>
									@endforeach	
									@endif
									@else
									<tr>
										<th class="text-center">
											@lang('labels.admin.deal_has_no_image').
										</th>
									</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>

					<div class="panel panel-warning">
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr class="warning">
										<td colspan="4">
											<div class="row">
												<div class="col-sm-12">
													<form method="POST" action="{{$deal->id}}/change_status">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
														<div class="col-sm-6">
															<select class="form-control" name="status">
																<option value="0">@lang('labels.admin.select_deal_status')</option>
																<option value="cancelled">Cancelled</option>
																<option value="closed">Closed</option>
															</select>
														</div>
														<div class="col-sm-6">
															<button class="btn btn-success">@lang('labels.change')</button>
														</div>
													</form>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection