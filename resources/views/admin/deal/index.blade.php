@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.deals')</div>
				<div class="panel-body">
					<!-- include shipment filter here -->
					@include('admin.deal.search')
					@if(count($deals))
					@foreach($deals as $deal)
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-12 text-center">
									#{{$deal->id}} {{ucfirst($deal->status)}} @lang('labels.admin.deal') 
								</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead></thead>
								<tbody>
									<tr class="active">
										<th colspan="1">
											@lang('labels.admin.shipper')
										</th>
										<th colspan="2">
											<img src="http://via.placeholder.com/20x20" class="rounded-circle">
													<strong><u><a href="javascript:void(0)">{{($deal->shipment) ? $deal->shipment->owner->name:""}}</a></u></strong>
										</th>
									</tr>
									<tr>
										<th colspan="1">
											@lang('labels.admin.pickup_point')
										</th>
										<td colspan="2" class="danger">
											{{$deal->information->pickup->place}}
										</td>
									</tr>
									<tr>
										<th colspan="1">@lang('labels.admin.pickup_time')</th>
										<td colspan="2" class="danger">
											{{$deal->information->pickup->time}}
										</td>
									</tr>
									<!-- traveller and deliver point-->
									<tr class="active">
										<th>@lang('labels.admin.traveler')</th>
										<td>
											<img src="http://via.placeholder.com/20x20" class="rounded-circle">
											<strong><u><a href="javascript:void(0)">{{($deal->trip->traveller) ? $deal->trip->traveller->name:""}}</a></u></strong>
										</td>
									</tr>
									<tr>
										<th colspan="1">
											@lang('labels.admin.delivery_point')
										</th>
										<td colspan="2" class="info">
											{{$deal->information->delivery->place}}
										</td>
									</tr>
									<tr>
										<th colspan="1">@lang('labels.admin.delivery_time')</th>
										<td colspan="2" class="info">
											{{$deal->information->delivery->time}}
										</td>
									</tr>

									<tr>
										<td colspan="3" class="text-center">
											<a href="deals/{{$deal->deal_id}}">@lang('labels.show_details').....</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					@endforeach
					@endif
					<div class="row">
						{{$paginate->links()}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection