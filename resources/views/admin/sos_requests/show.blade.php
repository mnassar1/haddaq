@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b> <?php $problem = ($problem_type) ? $problem_type->problem_type : "Problem Not Defind"; 
									echo $problem;
								?>  |  {{$categories->description}}</b>
					</div>
				</div>
				<div class="panel-body">
					

					<table class="table">
						
						<tr>
							<th class="active"> @lang('labels.created_at') </th>
							<td>
								{{$categories->created_at}}
								
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.problem_types')</th>
							<td>
								<?php $problem = ($problem_type) ? $problem_type->problem_type : "Problem Not Defind"; 
									echo $problem;
								?> 
								
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.problem_desc')</th>
							<td>
								{{$categories->description}}
							</td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.admin.sos_request_user')</th>
							
							<td>
									<?php 
										if($user_details){?>
											<a href="{{route('user.show',$user_details->id)}}" target="_blank"> {{$user_details->full_name}} </a>
										<?php }else{
											echo "Guest"."   - Mobile: ".$categories->mobile ;
										}	
									?>
								
							</td>
							
						</tr>
	
						<tr>
							<th class="active"># @lang('labels.admin.sos_saviors')</th>
							<td>
								<?php if($sos_responses){
											echo count($sos_responses);
								}else{
											echo "0";
								}
								?>
								
							</td>
						</tr>
					
						<tr>
							<th class="active"> @lang('labels.map')</th>
							<td>
								<iframe src="{{URL::to('/admin/show_on_map/'.$categories->id)}}" width="100%" height="600"></iframe>
							</td>
						</tr>
							


						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('sos_requests.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a target="_blank" class="btn btn-block btn-warning" href="{{action('Web\SOSRequestsController@show_on_map', $categories->id)}}">@lang('labels.admin.show_map_on_new_window')</a>
											</div>
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\SOSRequestsController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to close this SOS Request?')">@lang('labels.admin.close_request')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
