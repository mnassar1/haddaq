<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="sender_name" class="form-control" placeholder=" @lang('labels.admin.sender_name')" value="{{Request::get('sender_name')}}" />
								</div>

									<!-- register type -->
								<div class="col-sm-4">
									
										{{-- <div class="col-sm-6 lead"> @lang('labels.admin.problem_type') </div> --}}
										{{-- <div class="col-sm-6"> --}}
											<select name="problem_type" class="form-control">
												<option value="">@lang('labels.admin.select_problem_type')</option>
                                                <?php foreach ($problem_types as $problem_type) {
                                                   ?>
                                                   <option value="<?php echo $problem_type->id ?>" <?php if($problem_type->id  == Request::get('problem_type') ){echo "selected";}?> > <?php echo $problem_type->problem_type;?></option>
                                                   <?php 
                                                }?>
											</select>
										{{-- </div> --}}
									
								</div>


                            	<!-- register type -->
								<div class="col-sm-4">
									
										{{-- <div class="col-sm-6 lead"> @lang('labels.status')</div> --}}
										{{-- <div class="col-sm-6"> --}}
											<select name="status" class="form-control">
												<option value="">Select Status</option>
												<option value="Opening" {{(Request::get('status') == 'Opening') ? 'selected' : ''}}>@lang('labels.open')</option>
												<option value="Closed" {{(Request::get('status') == 'Closed') ? 'selected' : ''}}>@lang('labels.closed')</option>

											</select>
										{{-- </div> --}}
									
                                </div>
                                
                                
                               
								

								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('sos_requests.index')}}">@lang('labels.reset') </a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
