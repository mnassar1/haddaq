@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
                <div class="panel-heading">
                    @lang('labels.admin.sos_requests')
                </div>
				<div class="panel-body">
					@include('admin.sos_requests.search')
					<table class="table table-hover">
						<thead>
							<tr>
								
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.sender_name')</th>
								<th>@lang('labels.admin.problem_types')</th>
								<th>@lang('labels.admin.request_desc')</th>
								<th>@lang('labels.status')</th>
								<th>@lang('labels.created_at')</th>
								<th>@lang('labels.closed_by')</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
								$page = ($page > 0) ? ($page -1) : 0;
								$i = ($page*20)+1;
								?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('sos_requests.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
							 	<td>{{$page->sender_name}}</td>	
								<td>{{$page->sos_problem_type_name}}</td>
								<td>{{$page->description}}</td>
								<td>{{$page->sos_request_status}}</td>
								<td>{{$page->created_at}}</td>
								<td>{{$page->closed_by}}</td>
								<?php $i++; ;?>
							</tr>
							
							@endforeach
							<tr>
								<td colspan="10" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any SOS Requests <!-- , lets to <a href="{{route('problem_types.create')}}">add new Problem Type</a> -->
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
