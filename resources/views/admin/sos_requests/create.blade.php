@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
					    @lang('labels.admin.create_problem_type')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('problem_types.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						
  <!-- content image -->
					  <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.problem_type_icon')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.admin.problem_type_icon')" name="icon" required>
					    </div>
					  </div>

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_problem_type')</label>
						    <div class="col-sm-10" {{ $errors->has('problem_type') ? ' has-error' : '' }}>
						    	@if ($errors->has('problem_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('problem_type') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="problem_type" placeholder="@lang('labels.please_enter') @lang('labels.admin.eng_problem_type')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_problem_type')</label>
						    <div class="col-sm-10" {{ $errors->has('problem_type_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('problem_type_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('problem_type_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="problem_type_ar" placeholder="@lang('labels.please_enter') @lang('labels.admin.ar_problem_type')"  required/>
						    </div>
						  </div>

					 
					  


					  


					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('problem_types.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
