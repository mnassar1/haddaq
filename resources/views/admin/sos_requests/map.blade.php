<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title> @lang('labels.admin.sos_map_title')</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    
    <div id="map"></div>
    <script>

      function initMap() {
     
  var center = {lat: <?php echo $sos_lat?>, lng: <?php echo $sos_long ?>};
  // var locations = JSON.parse(data.replace(/&quot;/g,'"'));
  var locations =
  [
    [ 'Sender : <?php echo $name?>', 
       <?php echo $sos_lat?> ,
       <?php echo $sos_long ?>,
       "1"],
    ];

var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: center
  });

var infowindow =  new google.maps.InfoWindow({});
var marker, count;

for (count = 0; count < locations.length; count++) {
  if(locations[count][3] == "2") { showIcon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png"; }
  if(locations[count][3] == "3") {  showIcon = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"; }
  if(locations[count][3] == "1") {  showIcon = "http://maps.google.com/mapfiles/ms/icons/green-dot.png"; }
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[count][1], locations[count][2]),
      map: map,
      title: locations[count][0],
      icon: showIcon
    });
google.maps.event.addListener(marker, 'click', (function (marker, count) {
      return function () {
        infowindow.setContent(locations[count][0]);
        infowindow.open(map, marker);
      }
    })(marker, count));
  }
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhGw9jcsdAmE3NfT03Rd-3Y8MuodBmLTM&callback=initMap">
    </script>
  </body>
</html>
