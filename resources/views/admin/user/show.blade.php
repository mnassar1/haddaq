@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$user->name}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php if ($user->profile_image_url != "" || $user->profile_image_url != NULL) {?>


                            <div class="col-md-3 col-lg-3 " align="center"><img style="height: 100px;width: 100px"
                                                                                alt="User Pic"
                                                                                src="<?php echo $user->profile_image_url;?>"
                                                                                class="img-circle img-responsive"></div>

                            <?php }else{?>
                            <div class="col-md-3 col-lg-3 " align="center"><img alt="User Pic"
                                                                                src="http://via.placeholder.com/300x300"
                                                                                class="img-circle img-responsive"></div>
                            <?php } ?>

                            <div class=" col-sm-3 col-md-9 col-lg-9 ">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td> @lang('labels.email')</td>
                                        <td><a href="mailto:{{$user->email}}" target="_blank">{{$user->email}}</a></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('labels.phone')</td>
                                        <td>
                                            {{$user->phone}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.created_at')</td>
                                        <td>
                                            {{$user->created_at}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.admin.membership_type')</td>
                                        <td>
                                            <?php
                                            switch ($user->user_type_id) {
                                                case 1:
                                                    echo "Regular Membership";
                                                    break;
                                                case 2:
                                                    echo "User & Savior Membership";
                                                    break;
                                                case 3:
                                                    echo "Company Membership";
                                                    break;
                                                default:
                                                    echo "Admin";

                                            }?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($user->user_type_id == 2) {?>
                                    <tr>
                                        <td>@lang('labels.savior_type')</td>
                                        <td>
                                            <?php if ($user->savior_with_price > 0)
                                                echo "Paid";
                                            else
                                                echo "Free";
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.savior_fees')</td>
                                        <td>
                                            <?php if ($user->savior_with_price > 0)
                                                echo $user->savior_with_price;
                                            else
                                                echo "Free";
                                            ?>
                                        </td>
                                    </tr>

                                    <?php }
                                    ?>

                                    <tr>
                                        <td>@lang('labels.email_verified')</td>
                                        <td>
											<span class="badge badge-{{($user->is_verified) ? 'success' : 'danger'}}">
												{{($user->is_verified) ? 'Yes' : 'No'}}
											</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.mobile_verified')</td>
                                        <td>
											<span class="badge badge-{{($user->is_mobile_verified) ? 'success' : 'danger'}}">
												{{($user->is_mobile_verified) ? 'Yes' : 'No'}}
											</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.blocked')</td>
                                        <td>
											<span class="badge badge-{{($user->is_blocked) ? 'success' : 'danger'}}">
												{{($user->is_blocked) ? 'Yes' : 'No'}}
											</span>
                                        </td>
                                    </tr>
                                    <?php if($user->is_admin){?>
                                    <tr>
                                        <td>@lang('labels.admin')</td>
                                        <td>
											<span class="badge badge-{{($user->is_admin) ? 'success' : 'danger'}}">
												{{($user->is_admin) ? 'Yes' : 'No'}}
											</span>
                                        </td>
                                    </tr>
                                    <?php }?>
                                    <tr>
                                        <td>@lang('labels.address')</td>
                                        <td>
                                            {{$user->address}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('labels.city')</td>
                                        <td>
                                            <?php if ($city) echo $city->name_en;
                                            if (!$city) echo "Not Selected Yet";
                                            ?>
                                        </td>
                                    </tr>

                                    <tr class="active">
                                        <td colspan="2">
                                            <div class="row">
                                                <!-- is_verified -->
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>@lang('labels.email_verified')</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select onchange="javascript:if (confirm('Are you sure you want to change user Verified flag?')) { update_user_flag(this,'is_verified',{{$user->id}});}">
                                                                <option value="0" {{($user->is_verified == 0) ? 'selected' : ''}}>
                                                                    @lang('labels.no')
                                                                </option>
                                                                <option value="1" {{($user->is_verified == 1) ? 'selected':''}}>
                                                                    @lang('labels.yes')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php if(!$user->is_admin && $user->user_type_id != 3 ){?>
                                            <!-- is_verified -->
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>@lang('labels.mobile_verified')</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select onchange="javascript:if (confirm('Are you sure you want to change user mobile Verified flag?')) { update_user_flag(this,'is_mobile_verified',{{$user->id}});}">
                                                                <option value="0" {{($user->is_mobile_verified == 0) ? 'selected' : ''}}>
                                                                    @lang('labels.no')
                                                                </option>
                                                                <option value="1" {{($user->is_mobile_verified == 1) ? 'selected':''}}>
                                                                    @lang('labels.yes')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <!-- is_blocked -->
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>@lang('labels.blocked')</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select onchange="javascript:if (confirm('Are you sure you want to change user blocked flag?')) { update_user_flag(this,'is_blocked',{{$user->id}});}">
                                                                <option value="0" {{($user->is_blocked == 0) ? 'selected' : ''}}>
                                                                    @lang('labels.no')
                                                                </option>
                                                                <option value="1" {{($user->is_blocked == 1) ? 'selected':''}}>
                                                                    @lang('labels.yes')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php if($user->is_admin){?>
                                            <!-- is_admin -->
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>@lang('labels.admin')</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select onchange="javascript:if (confirm('Are you sure you want to change user admin flag?')) { update_user_flag(this,'is_admin',{{$user->id}});}">
                                                                <option value="0" {{($user->is_admin == 0) ? 'selected' : ''}}>
                                                                    @lang('labels.no')
                                                                </option>
                                                                <option value="1" {{($user->is_admin == 1) ? 'selected' : ''}}>
                                                                    @lang('labels.yes')
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }?>

                                            <?php if($user->user_type_id == 3){?>
                                            <!-- is_admin -->
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>@lang('labels.company_type')</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select onchange="javascript:if (confirm('Are you sure you want to change Company Type?')) { update_user_flag(this,'company_type_id',{{$user->id}});}">

                                                                <?php
                                                                foreach ($company_types as $company_type) {?>
                                                                <option value="<?php echo $company_type->id;?>" {{($user->company_type_id == $company_type->id) ? 'selected' : ''}}><?php echo $company_type->type_name;?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php }?>


                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table">
                                    <?php
                                    if ($user->user_type_id == 1 || $user->user_type_id == 2) {?>
                                    <tr colspan='3'>
                                        <td><label>@lang('labels.admin.user_reminders')</label></td>


                                    </tr>
                                    <tr>
                                        <td>@lang('labels.title')</td>
                                        <td>
                                            @lang('labels.admin.reminder_date')
                                        </td>
                                        <td>
                                            @lang('labels.admin.expiration_date')
                                        </td>

                                    </tr>

                                    <?php

                                    if(count($reminders) > 0){
                                    foreach ($reminders as $reminder) {
                                    ?>

                                    <tr>
                                        <td>
                                            {{$reminder->title}}
                                        </td>
                                        <td>
                                            {{$reminder->reminder_date}}
                                        </td>
                                        <td>
                                            {{$reminder->expiration_date}}
                                        </td>

                                    </tr>
                                    <?php }
                                    }else{?>
                                    <tr style="color: red">
                                        <td colspan="3">
                                            No Reminders Added yet.
                                        </td>


                                    </tr>
                                    <?php }

                                    }
                                    ?>

                                </table>

                                <table class="table">
                                    <?php
                                    if ($user->user_type_id == 3) {?>

                                    <tr>
                                        <td>@lang('labels.slider_images')</td>
                                        <td>
                                            <?php if (!empty($user->sliders)) {
                                            foreach ($user->sliders as $image) {?>
                                            <div class="col-md-3 col-lg-3 " align="center"><img
                                                        style="height: 100px;width: 100px" alt="User Pic"
                                                        src="<?php echo $image;?>" class="img-circle img-responsive">
                                            </div>

                                            <?php } }?>
                                        </td>
                                    </tr>

                                    <tr colspan='3'>
                                        <td><label>@lang('labels.company_branches')</label></td>


                                    </tr>
                                    <tr>
                                        <td>@lang('labels.title')</td>
                                        <td>
                                           @lang('labels.address') 
                                        </td>

                                    </tr>

                                    <?php

                                    if(count($branches) > 0){
                                    foreach ($branches as $branch) {
                                    ?>

                                    <tr>
                                        <td>
                                            {{$branch->title}}
                                        </td>
                                        <td>
                                            {{$branch->address}}
                                        </td>

                                    </tr>
                                    <?php }
                                    }else{?>
                                    <tr style="color: red">
                                        <td colspan="2">
                                            No Branches Added yet.
                                        </td>


                                    </tr>
                                    <?php }

                                    }
                                    ?>
                                    <?php if ($user->is_admin) {?>


                                    <?php
                                    echo "<td colspan='2'>";
                                    echo "<h2> Add Roles To Admin </h2>";
                                    ?>
                                    <form action="{{url("admin/user/".$user->id."/add-role")}}" method="post">
                                        {{ csrf_field() }}
                                        <div class="col-sm-12">
                                            <?php
                                            foreach ($roles as $role) {

                                            ?>
                                            <div class="col-sm-4">
                                                <input type="checkbox" name="roles[]"
                                                       value="{{$role->name}}"
                                                       @if ($user_role != null and in_array($role->name,$user_role)) checked @endif
                                                > {{$role->display_name}}
                                            </div>

                                            <?php }
                                            }?>
                                        </div>
                                        <br> <br>
                                        <div class="row pull-right">
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn  btn-info">@lang('labels.submit')</button>
                                            </div>
                                        </div>

                                    </form>
                                    </td>

                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <a href="{{route('user.index')}}" class="btn btn-block btn-default">@lang('labels.email')Back</a>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
