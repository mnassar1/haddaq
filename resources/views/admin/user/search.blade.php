<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name" class="form-control" placeholder="@lang('labels.name')" value="{{Request::get('name')}}" />
								</div>

								<!-- email -->
								<div class="col-sm-4">
									<input type="text" name="email" class="form-control" placeholder="@lang('labels.email')" value="{{Request::get('email')}}" />
								</div>

								<!-- phone -->
								<div class="col-sm-4">
									<input type="text" name="mobile" class="form-control" placeholder="@lang('labels.phone')" value="{{Request::get('mobile')}}" />
								</div>
							</div>
						</td>
					</tr>

					<tr>
						<td>
							<div class="row">
								
								<!-- city -->
								<div class="col-sm-4">
									<select class="form-control" name="city_id" id="city_from_id" disabled>
										<option value="0"> @lang('labels.admin.select_city')</option>
									</select>
								</div>
								<!-- register type -->
								<div class="col-sm-4">
									 	<select name="user_types_id" class="form-control">
												<option value="">@lang('labels.admin.select_membership_type')</option>
												
												<?php
												foreach ($user_types as $type) {?>
													<option value="<?php echo $type->id;?>" <?php if (Request::get('user_types_id') == $type->id) {echo "selected";}   ?>
														><?php echo $type->type_name;?></option>
												<?php }
												?>
											</select>
								</div>

									<div class="col-sm-4">
											<select name="is_verified" class="form-control">
												<option value="">@lang('labels.admin.select_verified_status')</option>
												<option value="0" {{(Request::get('is_verified') && Request::get('is_verified') == 0) ? 'selected' : ''}}>@lang('labels.no')</option>
												<option value="1" {{(Request::get('is_verified') == 1) ? 'selected' : ''}}>@lang('labels.yes')</option>
											</select>
										
									</div>
								
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="row">
								

								<!-- blocked -->
								<div class="col-sm-4">
									        <select name="is_blocked" class="form-control">
												<option value="">@lang('labels.admin.select_blocked_status')</option>
												<option value="0" {{(Request::get('is_blocked') && Request::get('is_blocked') == 0) ? 'selected' : ''}}>@lang('labels.no')</option>
												<option value="1" {{(Request::get('is_blocked') == 1) ? 'selected' : ''}}>@lang('labels.yes')</option>
											</select>
								</div>

								<!-- admin -->
								<div class="col-sm-4">
											<select name="is_admin" class="form-control">
												<option value="">@lang('labels.admin.select_admin')</option>
												<option value="0" {{(Request::get('is_admin') && Request::get('is_admin') == 0) ? 'selected' : ''}}>@lang('labels.no')</option>
												<option value="1" {{(Request::get('is_admin') == 1) ? 'selected' : ''}}>@lang('labels.yes')</option>
											</select>
								</div>
									
							</div>
						</td>
					</tr>
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('user.index')}}">@lang('labels.reset') </a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
