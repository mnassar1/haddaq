@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row text-center">
                           @lang('labels.admin.create_admin') 
                        </div>
                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" method="POST" action="{{route('user.store')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- cms type -->


                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.admin_name')</label>
                                    <input type="text" class="form-control col-sm-12" name="full_name"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.admin_name')" required/>
                                </div>
                                <br/>
                                <br/>
                                <div class="col-sm-12">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.admin_email')</label>
                                    <input type="text" class="form-control col-sm-12" name="email"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.admin_email')" required/>
                                </div>

                                <div class="col-sm-12">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.admin_mobile')</label>
                                    <input type="text" class="form-control col-sm-12" name="mobile"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.admin_mobile')" required/>
                                </div>

                                <div class="col-sm-12">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.admin_password')</label>
                                    <input type="password" class="form-control col-sm-12" name="password"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.admin_password')" required/>
                                </div>


                            </div>
                            <br/>
                            <br/>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <button class="btn btn-block btn-success">@lang('labels.create')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
