@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset( $_GET["from_home"] ))
                           @lang('labels.admins') 
                        @else
                           @lang('labels.subscribers') 
                        @endif

                    </div>
                    <div class="panel-body">
                        @if (!isset($_GET["from_home"]))
                        @include('admin.user.search')
                        @else
                        <div class="col-sm-6">
                            <a type="submit" href="{{url('admin/user/create')}}" class="btn btn-primary px-4 col-sm-6">@lang('labels.admin.add_admin')</a>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        @endif
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.name') </th>
                                <th>@lang('labels.email') </th>
                                <th>@lang('labels.phone') </th>
                                <th>@lang('labels.created_at') </th>

                                <th>@lang('labels.verified')</th>
                                <th>@lang('labels.blocked')</th>
                                <th>@lang('labels.admin') </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
                            $page = ($page > 0) ? ($page - 1) : 0;
                            $i = ($page * 20) + 1;
                            ?>
                            @if(isset($users) && !empty($users))
                                @foreach($users as $user)
                                    <tr style="cursor: pointer;"
                                        onclick="window.location='{{route('user.show',$user->id)}}';">
                                        <td><?php echo $i;?></td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->created_at}}</td>

                                        <td class="{{($user->is_verified) ? 'success' : 'danger'}} text-center">{{($user->is_verified) ? 'Yes' : 'No'}}</td>
                                        <td class="{{($user->is_blocked) ? 'danger' : 'success'}} text-center">{{($user->is_blocked) ? 'Yes' : 'No'}}</td>
                                        <td class="text-center {{($user->is_admin) ? 'success' : 'danger'}}">{{($user->is_admin) ? 'Yes' : 'No'}}</td>
                                        <?php $i++; ;?>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="10" class="text-center">
                                        {{$paginate->appends(request()->input())->links()}}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="10" class="text-center">
                                        <b>No Users registered.</b>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
