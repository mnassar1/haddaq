@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.settings')</div>
				<div class="panel-body">
					<table class="table table-hover">
						<tbody>
						<thead>
							<tr>
								<td><a href="{{route('settings.create')}}" class="btn btn-primary">+ @lang('labels.admin.create_new_settings')</a></td>
							</tr>
							<tr>
								<th>#</th>
								<th>@lang('labels.title')</th>
								<th>@lang('labels.key')</th>
                                <th>@lang('labels.value')</th>
							</tr>
						</thead>
						<tbody>
							@if(count($settings))
							@foreach($settings as $setting)
							<tr onclick="window.location='{{route('settings.show',$setting->id)}}';" style="cursor : pointer">
								<th>{{$setting->id}}</th>
								<th>{{$setting->title}}</th>
								<th>{{$setting->setting_key}}</th>
								<th>{{$setting->setting_value}}</th>
							</tr>
							@endforeach
							@endif
						</tbody>
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
