@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$setting->title}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<tbody>
							<tr>
								<th class="active">@lang('labels.key')</th>
								<td>{{$setting->setting_key}}</td>
							</tr>
							<tr>
								<th class="active">@lang('labels.value')</th>
								<td>{{$setting->setting_value}}</td>
							</tr>
							<tr>
								<td colspan="2">
									<div class="row">
										<div class="col-sm-3">
											<a href="{{route('settings.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
										</div>
										<div class="col-sm-9">
											<a href="{{route('settings.edit',$setting->id)}}" class="btn btn-block btn-warning">@lang('labels.edit')</a>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
