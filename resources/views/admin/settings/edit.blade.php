@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_settings')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{action('Web\SettingsController@update', $setting->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label"> @lang('labels.title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Arabic Title" name="title" value="{{$setting->title}}" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.key')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Setting Key" name="setting_key" value="{{$setting->setting_key}}" disabled="" />
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.value')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Setting Value" name="setting_value" value="{{$setting->setting_value}}" required/>
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">
					    	<a href="{{route('settings.show',$setting->id)}}" class="btn btn-block btn-default">@lang('labels.back')</a>
					    </label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.edit')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
