@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_new_settings')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{route('settings.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.title')" name="title" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.key')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.key')" name="setting_key" required/>
					    </div>
					  </div>

					 <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.value')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.value')" name="setting_value" required/>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('settings.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
