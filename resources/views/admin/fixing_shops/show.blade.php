@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$fixing_shops->shop_name_en}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						
						<tr>
							<th class="active">@lang('labels.ar_name')</th>
							<td>
								{{$fixing_shops->shop_name_ar}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.eng_name')</th>
							<td>
								{{$fixing_shops->shop_name_en}}
							</td>
						</tr>
						


						<tr>
							<th class="active">@lang('labels.Logo')</th>
							<td>
								<?php if ($fixing_shops->images != "" || $fixing_shops->images != NULL) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $fixing_shops->images;?>" class="img-circle img-responsive"> </div>
						
						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>
							</td>
						</tr>

					    <tr>
							<th class="active">@lang('labels.slider_images')</th>
							<td>
								<?php if (!empty($fixing_shops->sliders)) {
									foreach ($fixing_shops->sliders as $image) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $image;?>" class="img-circle img-responsive"> </div>
						
						<?php } }?>

							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.type')</th>
							<td><?php
									switch ($fixing_shops->fixing_shops_types_id) {
										case 1:
											echo "Gold";
											break;
										case 2:
											echo "Silver";
											break;
										case 3:
											echo "Bronze";
											break;
											
										default:
											# code...
											break;
									}
								?></td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.admin.eng_details')</th>
							<td>
								{{ strip_tags($fixing_shops->about_en)}}
							</td>
						</tr>
						

						<tr>
							<th class="active">@lang('labels.admin.ar_details')</th>
							<td>
								{{ strip_tags($fixing_shops->about_ar)}}
							</td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.mobile')</th>
							<td>
								{{$fixing_shops->mobile}}
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.email')</th>
							<td>
								{{$fixing_shops->email}}
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.city')</th>
							<td>
								{{$City->name_en}}
							</td>
						</tr>

							<tr>
							<th class="active">@lang('labels.admin.eng_address')</th>
							<td>
								{{$fixing_shops->address_en}}
							</td>
						</tr>


						<tr>
							<th class="active">@lang('labels.admin.ar_address')</th>
							<td>
								{{$fixing_shops->address_ar}}
							</td>
						</tr>


						<tr>
							<th class="active">@lang('labels.location_string')</th>
							<td>
								{{$fixing_shops->location}}
							</td>
						</tr>


						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('fixing_shops.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('fixing_shops.edit',$fixing_shops->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\FixingShopsController@destroy', $fixing_shops->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Category?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
