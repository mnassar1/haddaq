@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_fixing_shops')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('fixing_shops.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_name')</label>
						    <div class="col-sm-10" {{ $errors->has('shop_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('shop_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="shop_name" placeholder="@lang('labels.please_enter') @lang('labels.eng_name')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_name')</label>
						    <div class="col-sm-10" {{ $errors->has('shop_name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('shop_name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="shop_name_ar" placeholder="@lang('labels.please_enter') @lang('labels.ar_name')"  required/>
						    </div>
						  </div>

					 

							
  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.Logo')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.Logo')" name="images" required>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.slider_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.slider_images')" name="sliders[]" multiple >
					    </div>
					  </div>

						
					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.type')</label>
					    <div class="col-sm-10">
						<select class="form-control" name="fixing_shops_types_id" required="">
						<?php 
						if(!empty($fixing_shops_types)){
							foreach ($fixing_shops_types as $type) {?>
								<option value="<?php echo $type->id?>"><?php echo $type->type_name?></option>
							<?php }
						}
						?>
						</select>
					    </div>
					  </div>


					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.eng_details')" name="about" ></textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.ar_details')" name="about_ar" ></textarea>
					    </div>
					  </div>


					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.phone')</label>
						    <div class="col-sm-10" {{ $errors->has('mobile') ? ' has-error' : '' }}>
						    	@if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="mobile" placeholder="@lang('labels.please_enter') @lang('labels.admin.phone')"  required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.email')</label>
						    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
						    	@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
						    	<input type="email" class="form-control" name="email" placeholder="@lang('labels.please_enter') @lang('labels.email')"  required/>
						    </div>
						  </div>

				 <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.city')</label>
					    <div class="col-sm-10">
						<select class="form-control" name="city_id" required="">
						<?php 
						if(!empty($cities)){
							foreach ($cities as $type) {?>
								<option value="<?php echo $type->id?>"><?php echo $type->name_en?></option>
							<?php }
						}
						?>
						</select>
					    </div>
					  </div>

					
					 <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_address')</label>
						    <div class="col-sm-10" {{ $errors->has('address') ? ' has-error' : '' }}>
						    	@if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="address" placeholder="@lang('labels.please_enter') @lang('labels.admin.eng_address')"  required/>
						    </div>
						  </div>

				 <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_address')</label>
						    <div class="col-sm-10" {{ $errors->has('address_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('address_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="address_ar" placeholder="@lang('labels.please_enter') @lang('labels.admin.ar_address')"  required/>
						    </div>
						  </div>
						  		  
		  
		   <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.location')</label>
						    <div class="col-sm-10" {{ $errors->has('location') ? ' has-error' : '' }}>
						    	@if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="location" placeholder="@lang('labels.please_enter') @lang('labels.admin.location')"  required/>
						    </div>
						  </div>


					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('fixing_shops.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
