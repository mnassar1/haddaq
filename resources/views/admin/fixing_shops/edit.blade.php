@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_fixing_shops')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\FixingShopsController@update', $fixing_shops->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

					<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_name')</label>
						    <div class="col-sm-10" {{ $errors->has('shop_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('shop_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="shop_name" placeholder="please enter Shop Name" value="{{$fixing_shops->shop_name}}" required/>
					</div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_name')</label>
						    <div class="col-sm-10" {{ $errors->has('shop_name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('shop_name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="shop_name_ar" placeholder="please enter Arabic  Name" value="{{$fixing_shops->shop_name_ar}}"  required/>
						    </div>
						  </div>

					 

							
  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.Logo')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="Category icon" name="images">
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.slider_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="Slider Images" name="sliders[]" multiple >
					    </div>
					  </div>

						
					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.type')</label>
					    <div class="col-sm-10">
						<select class="form-control" name="fixing_shops_types_id" required="">
						<?php 
						if(!empty($fixing_shops_types)){
							foreach ($fixing_shops_types as $type) {?>
								<option <?php if($type->id == $fixing_shops->fixing_shops_types_id) echo "selected";?> value="<?php echo $type->id?>"><?php echo $type->type_name?></option>
							<?php }
						}
						?>
						</select>
					    </div>
					  </div>


					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="English Details Content" name="about" >{{$fixing_shops->about}}</textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Arabic Details Content" name="about_ar" >{{$fixing_shops->about_ar}}</textarea>
					    </div>
					  </div>


					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.phone')</label>
						    <div class="col-sm-10" {{ $errors->has('mobile') ? ' has-error' : '' }}>
						    	@if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="mobile" placeholder="please enter Phone Number" value="{{$fixing_shops->mobile}}"  required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.email')</label>
						    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
						    	@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
						    	<input type="email" class="form-control" name="email" placeholder="please enter Email "  value="{{$fixing_shops->email}}"  required/>
						    </div>
						  </div>

				 <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.city')</label>
					    <div class="col-sm-10">
						<select class="form-control" name="city_id" required="">
						<?php 
						if(!empty($cities)){
							foreach ($cities as $type) {?>
								<option <?php if($type->id == $fixing_shops->city_id) echo "selected";?> value="<?php echo $type->id?>"><?php echo $type->name_en?></option>
							<?php }
						}
						?>
						</select>
					    </div>
					  </div>

					
					 <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_address')</label>
						    <div class="col-sm-10" {{ $errors->has('address') ? ' has-error' : '' }}>
						    	@if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="address" placeholder="please enter English Address"  value="{{$fixing_shops->address}}" required/>
						    </div>
						  </div>

				 <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_address')</label>
						    <div class="col-sm-10" {{ $errors->has('address_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('address_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="address_ar" placeholder="please enter Arabic Address"  value="{{$fixing_shops->address_ar}}"  required/>
						    </div>
						  </div>
						  		  
		  
		   <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.location')</label>
						    <div class="col-sm-10" {{ $errors->has('location') ? ' has-error' : '' }}>
						    	@if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="location" placeholder="please enter Location" value="{{$fixing_shops->location}}"  required/>
						    </div>
						  </div>



					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('fixing_shops.show',$fixing_shops->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection