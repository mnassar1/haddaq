@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{{$user->name}}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<?php if ($user->profile_image_url != "" || $user->profile_image_url != NULL) {?>


						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $user->profile_image_url;?>" class="img-circle img-responsive"> </div>

						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>

						<div class=" col-sm-3 col-md-9 col-lg-9 ">
							<table class="table">
								<tbody>
									<tr>
										<td>Email</td>
										<td><a href="mailto:{{$user->email}}" target="_blank">{{$user->email}}</a></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td>

											{{$user->phone}}
										</td>
									</tr>

									<?php if($user->is_admin != 1){?>
									<tr>
										<td>Membership Type</td>
										<td>
											<?php
			switch($user->user_type_id){
				case 1:
					echo  "Regular Membership";
					break;
				case 2:
					echo  "User & Savior Membership";
					break;
				case 3:
					echo  "Company Membership";
					break;

				}?>
										</td>

										<?php } ?>
									</tr>
				

									<tr>
										<td>Verified</td>
										<td>
											<span class="badge badge-{{($user->is_verified) ? 'success' : 'danger'}}">
												{{($user->is_verified) ? 'Yes' : 'No'}}
											</span>
										</td>
									</tr>
									
									
									


								</tbody>
							</table>
							<table class="table" >
							
									<tr>
										<td colspan="2">
											<div class="row">
												
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-9">
															<a class="btn btn-block btn-warning" href="{{route('admin.edit',$user->id)}}">Edit Profile</a>
														</div>

													</div>
												</div>
											</div>
										</td>
									</tr>
									</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
