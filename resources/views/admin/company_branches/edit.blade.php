@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.edit_cat')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\CompanyBranchController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->


						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="please enter Title" value="{{$categories->title}}" required/>
						    </div>
						  </div>

							<div class="form-group row">
									<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.address')</label>
									<div class="col-sm-10" {{ $errors->has('address') ? ' has-error' : '' }}>
										@if ($errors->has('address'))
																			<span class="help-block">
																					<strong>{{ $errors->first('address') }}</strong>
																			</span>
																	@endif
										<input type="text" class="form-control" name="address" placeholder="please enter Address" value="{{$categories->address}}" required/>
									</div>
								</div>


								<div class="form-group row">
										<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.location')</label>
										<div class="col-sm-10" {{ $errors->has('location') ? ' has-error' : '' }}>
											@if ($errors->has('location'))
																				<span class="help-block">
																						<strong>{{ $errors->first('location') }}</strong>
																				</span>
																		@endif
											<input type="text" class="form-control" name="location" placeholder="please enter Location" value="{{$categories->location}}" required/>
										</div>
									</div>



					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('company_branches.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
