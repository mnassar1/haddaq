@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$categories->group_name_en}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
					

						<tr>
							<th class="active">@lang('labels.admin.cat.ar_group_name')</th>
							<td>
								{{$categories->group_name_ar}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.cat.eng_group_name')</th>
							<td>
								{{$categories->group_name_en}}
							</td>
						</tr>
						
				
						
						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('category_groups.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('category_groups.edit',$categories->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\CategoryGroupController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Category Group?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection