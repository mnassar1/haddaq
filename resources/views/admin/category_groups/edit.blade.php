@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.cat.edit_cat_group')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\CategoryGroupController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->


						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat.eng_group_name')</label>
						    <div class="col-sm-10" {{ $errors->has('group_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('group_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('group_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="group_name" placeholder="please enter Group Name" value="{{$categories->group_name}}" required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat.ar_group_name')</label>
						    <div class="col-sm-10" {{ $errors->has('group_name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('group_name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('group_name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="group_name_ar" placeholder="please enter Arabic Group Name" value="{{$categories->group_name_ar}}" required/>
						    </div>
						  </div>

					  

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('category_groups.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
