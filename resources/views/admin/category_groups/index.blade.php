@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.cat.cat_groups')</div>
				<div class="panel-body">
					@include('admin.category_groups.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('category_groups.create')}}" class="btn btn-primary">+ @lang('labels.admin.cat.new_cat_group')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.cat.ar_group_name')</th>
								<th>@lang('labels.admin.cat.eng_group_name')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('category_groups.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->group_name_ar}}</td>
								<td>{{$page->group_name_en}}</td>

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="10" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any Category Groups , lets to <a href="{{route('category_groups.create')}}">add new Category Group</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection