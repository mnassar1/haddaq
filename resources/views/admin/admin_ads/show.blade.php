@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$admin_ads->title_en}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						
						<tr>
							<th class="active">@lang('labels.admin.eng_title')</th>
							<td>
								{{$admin_ads->title_en}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.ar_title')</th>
							<td>
								{{$admin_ads->title_ar}}
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.eng_details')</th>
							<td>
								{{ strip_tags($admin_ads->details_en)}}
							</td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.admin.ar_details')</th>
							<td>
								{{ strip_tags($admin_ads->details_ar)}}
							</td>
						</tr>
						

						<tr>
							<th class="active">@lang('labels.admin.eng_ads_images')</th>
							<td>
								<?php if ($admin_ads->images != "" || $admin_ads->images != NULL) {?>


						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $admin_ads->images;?>" class="img-circle img-responsive"> </div>
						
						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.ar_ads_images')</th>
							<td>
								<?php if ($admin_ads->images_ar != "" || $admin_ads->images_ar != NULL) {?> 


						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $admin_ads->images_ar;?>" class="img-circle img-responsive"> </div>

						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>
							</td>
						</tr>


						<tr>
							<th class="active">@lang('labels.admin.active')</th>
							<td>
								
								{{($admin_ads->active) ? 'Yes' : 'No'}}
							</td>
						</tr>


						<tr>
							<th class="active">@lang('labels.admin.order')</th>
							<td>
								{{$admin_ads->ordering}}
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.is_banner')</th>
							<td>

								<?php if ($admin_ads->is_banar ): ?>
									@switch ($admin_ads->is_banar)

										@case (1)
											@lang('labels.admin.main_slider')
											@break
										@case (2)
											@lang('labels.admin.offers_slider')
											@break
										@case (3)
											@lang('labels.admin.first_banner')
											@break
										@case (4)
											@lang('labels.admin.second_banner')
											@break
										@case (5)
											@lang('labels.admin.third_banner')
											@break	
										@default
											@lang('labels.admin.no_banner')
											@break
									@endswitch
								<?php else: ?>
									@lang('labels.admin.no_banner')
								<?php endif ?>
								
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.ads_display_on');</th>
							<td>
								@switch ($admin_ads->fixing_shops_or_home)
									@case (1)
										@lang('labels.admin.home_page')
										@break
									@case (0)
										@lang('labels.admin.fixing_shops')
										@break
									@case (2)
										@lang('labels.admin.used_page')
										@break
									@case (3)
										@lang('labels.admin.main_home_page')
										@break		
								@endswitch
							
							
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.posted_at')</th>
							<td>{{$admin_ads->created_at}}</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('admin_ads.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('admin_ads.edit',$admin_ads->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\AdsController@destroy', $admin_ads->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Admin Ads?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection