@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.banner_and_sliders')</div>
				<div class="panel-body">
					@include('admin.admin_ads.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="9">
									<a href="{{route('admin_ads.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_banner_and_sliders')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.eng_title')</th>
								<th>@lang('labels.admin.ar_title')</th>
								<th>@lang('labels.view_cnt')</th>
								<th>@lang('labels.admin.active')</th>
								<th>@lang('labels.location')</th>
								<th>@lang('labels.admin.banner')</th>
								<th>@lang('labels.admin.order')</th>
								<th>@lang('labels.created_at')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i=1;?>
							@if(isset($admin_ads) && !empty($admin_ads))
							@foreach($admin_ads as $page)
							<tr  onclick="window.location='{{route('admin_ads.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i;?></td>
								<td>{{$page->title}}</td>
								<td>{{$page->title_ar}}</td>
								<td>{{$page->views_count}}</td>
								<td>{{($page->active) ? 'Yes' : 'No'}}</td>
								<td>
								@switch ($page->fixing_shops_or_home)
									@case (1)
										@lang('labels.admin.home_page')
										@break
									@case (0)
										@lang('labels.admin.fixing_shops')
										@break
									@case (2)
										@lang('labels.admin.used_page')
										@break
									@case (3)
										@lang('labels.admin.main_home_page')
										@break		
								@endswitch
							
							</td>
								<td>
									@switch ($page->is_banar)
										@case (1)
											@lang('labels.admin.main_slider')
											@break
										@case (2)
											@lang('labels.admin.offers_slider')
											@break
										@case (3)
											@lang('labels.admin.first_banner')
											@break
										@case (4)
											@lang('labels.admin.second_banner')
											@break
										@case (5)
											@lang('labels.admin.third_banner')
											@break	
										@default
											@lang('labels.admin.no_banner')
									@endswitch


								</td>
								<td>{{$page->ordering}}</td>
								<td>{{$page->created_at}}</td>
								<?php $i++;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="9" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="9" class="text-center">
									You have not any Banner & Sliders, lets to <a href="{{route('admin_ads.create')}}">add new Banner & Slider</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
