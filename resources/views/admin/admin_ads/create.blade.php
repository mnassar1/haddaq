@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_ads')
						
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('admin_ads.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="@lang('labels.please_enter') @lang('labels.admin.eng_title')"  required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_title')</label>
						    <div class="col-sm-10" {{ $errors->has('title_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('title_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title_ar" placeholder="@lang('labels.please_enter') @lang('labels.admin.ar_title')"  required/>
						    </div>
						  </div>

					  
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.eng_details')" name="details" ></textarea>
					    </div>
					  </div>


					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.ar_details')" name="details_ar"></textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_ads_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.admin.eng_ads_images')" name="images" required>
					    </div>
					  </div>

					   <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_ads_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.admin.ar_ads_images')" name="images_ar" required>
					    </div>
					  </div>


					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.active')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="active" >
					    				<option value="1">@lang('labels.admin.active')</option>
										<option value="0">@lang('labels.admin.not_active')</option>
									</select>
					    </div>
					  </div>

					  <!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.order')</label>
						    <div class="col-sm-10" {{ $errors->has('ordering') ? ' has-error' : '' }}>
						    	@if ($errors->has('ordering'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ordering') }}</strong>
                                    </span>
                                @endif
						    	<input type="number" class="form-control" name="ordering" placeholder="@lang('labels.please_enter') @lang('labels.admin.order')"  required/>
						    </div>
						  </div>
					  
					 

					   <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.slider_location')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="is_banar" >
										<option value="0">@lang('labels.admin.not_banner')</option>
										<option value="1">@lang('labels.admin.main_slider') </option>
										<option value="2">@lang('labels.admin.offers_slider')</option>
										<option value="3">@lang('labels.admin.first_banner')</option>
										<option value="4">@lang('labels.admin.second_banner')</option>
										<option value="5">@lang('labels.admin.third_banner')</option>
									</select>
					    </div>
					  </div>

					    <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ads_display_on') </label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="fixing_shops_or_home" >
										<option value="0">@lang('labels.admin.fixing_shops')</option>
										<option value="1">@lang('labels.admin.home_page')</option>
										<option value="2">@lang('labels.admin.used_page')</option>
										<option value="3">@lang('labels.admin.main_home_page')</option>
									</select>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('admin_ads.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
