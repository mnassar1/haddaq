@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_ads')
					</div>
				</div>
				<div class="panel-body">
					<form  method="POST" enctype="multipart/form-data" action="{{action('Web\AdsController@update', $admin_ads->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="please enter title" value="{{$admin_ads->title}}" required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_title')</label>
						    <div class="col-sm-10" {{ $errors->has('title_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('title_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title_ar" placeholder="please enter Arabic title" value="{{$admin_ads->title_ar}}" required/>
						    </div>
						  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Ads Details Content" name="details" >{{$admin_ads->details}}</textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Ads Details Content" name="details_ar" >{{$admin_ads->details_ar}}</textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_ads_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="Ads Image" name="images">
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_ads_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="Ads Image" name="images_ar">
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.active')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="active" >
										<option {{($admin_ads->active) ? "" : "selected"}}  value="0">@lang('labels.admin.not_active')</option>
										<option {{($admin_ads->active)?"selected": "" }} value="1">@lang('labels.admin.active')</option>
									</select>
					    </div>
					  </div>

					  <!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.order')</label>
						    <div class="col-sm-10" {{ $errors->has('ordering') ? ' has-error' : '' }}>
						    	@if ($errors->has('ordering'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ordering') }}</strong>
                                    </span>
                                @endif
						    	<input type="number" class="form-control" name="ordering" placeholder="please enter order" value="{{$admin_ads->ordering}}"  required/>
						    </div>
						  </div>
					  

					   <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.is_banner')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="is_banar" >
										<option 
										 {{(!$admin_ads->is_banar)?"selected": "" }} value="0">@lang('labels.admin.not_banner')</option>
										<option {{($admin_ads->is_banar == 1)?"selected": "" }} value="1">@lang('labels.admin.main_slider')</option>
										<option  {{($admin_ads->is_banar == 2)?"selected": "" }} value="2">@lang('labels.admin.offers_slider')</option>
										<option  {{($admin_ads->is_banar == 3)?"selected": "" }} value="3">@lang('labels.admin.first_banner')</option>
										<option {{($admin_ads->is_banar == 4)?"selected": "" }} value="4">@lang('labels.admin.second_banner')</option>
										<option {{($admin_ads->is_banar == 5)?"selected": "" }} value="5">@lang('labels.admin.third_banner')</option>
									</select>
					    </div>
					  </div>

					    <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ads_display_on')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="fixing_shops_or_home" >
										<option  {{($admin_ads->fixing_shops_or_home == 0)?"selected": "" }} value="0">@lang('labels.admin.fixing_shops')</option>
										<option {{($admin_ads->fixing_shops_or_home == 1)?"selected": "" }} value="1">@lang('labels.admin.home_page')</option>
										<option {{($admin_ads->fixing_shops_or_home == 2)?"selected": "" }} value="2">@lang('labels.admin.used_page')</option>
										<option {{($admin_ads->fixing_shops_or_home == 3)?"selected": "" }} value="3">@lang('labels.admin.main_home_page')</option>

									</select>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('admin_ads.show',$admin_ads->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection