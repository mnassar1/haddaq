<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- register type -->
								<div class="col-sm-4">
										
											<select name="fixing_shops_or_home" class="form-control">
												<option value="7">@lang('labels.admin.select_location')</option>
												<option value="1" <?php if (isset($_GET['fixing_shops_or_home']) && Request::get('fixing_shops_or_home') == 1) {echo "selected";}   ?>
														>@lang('labels.admin.home_page')</option>
												<option value="0" <?php if (isset($_GET['fixing_shops_or_home']) && Request::get('fixing_shops_or_home') == 0) {echo "selected";}   ?>
														>@lang('labels.admin.fixing_shops')</option>
												<option value="2" <?php if (isset($_GET['fixing_shops_or_home']) && Request::get('fixing_shops_or_home') == 2) {echo "selected";}   ?>
														>@lang('labels.admin.used_page')</option>
												<option value="3" <?php if (isset($_GET['fixing_shops_or_home']) && Request::get('fixing_shops_or_home') == 3) {echo "selected";}   ?>
														>@lang('labels.admin.main_home_page')</option>				
														
												
												
											</select>
									
								</div>

								<div class="col-sm-4">
										
											<select name="is_banar" class="form-control">
												<option value="">@lang('labels.admin.select_banner')</option>
												<option value="1" <?php if (Request::get('is_banar') == 1) {echo "selected";}   ?>
														>@lang('labels.admin.main_slider')</option>
												<option value="2" <?php if (Request::get('is_banar') == 2) {echo "selected";}   ?>
														>@lang('labels.admin.offers_slider') </option>
												<option value="3" <?php if (Request::get('is_banar') == 3) {echo "selected";}   ?>
														>@lang('labels.admin.first_banner') </option>		
												<option value="4" <?php if (Request::get('is_banar') == 4) {echo "selected";}   ?>
														>@lang('labels.admin.second_banner')</option>		
												<option value="5" <?php if (Request::get('is_banar') == 5) {echo "selected";}   ?>
														>@lang('labels.admin.third_banner') </option>
												
														
												
											</select>
									
								</div>

							</div>
						</td>
					</tr>

					

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('admin_ads.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>