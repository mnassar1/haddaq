@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						Edit Profile : {{$user->full_name}}
					</div>
				</div>
				<div class="panel-body">
					<form enctype="multipart/form-data" method="POST" action="{{action('Web\AdminController@update', $user->id)}}">
						<input type="hidden" name="_method" value="post" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">Name</label>
						    <div class="col-sm-10" {{ $errors->has('full_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('full_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="full_name" placeholder="please enter Name" value="{{$user->full_name}}" required/>
						    </div>
						  </div>
							<!-- content image -->
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-2 col-form-label">Logo</label>
								<div class="col-sm-10">
									<input type="file" class="form-control" placeholder="Logo" name="profile_image_url" />
								</div>
							</div>


							<div class="form-group row">
							    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
							    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
							    	@if ($errors->has('email'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
							    	<input type="text" class="form-control" name="email" placeholder="please enter email" value="{{$user->email}}" required/>
							    </div>
							  </div>

								<div class="form-group row">
										<label for="inputPassword" class="col-sm-2 col-form-label">Mobile</label>
										<div class="col-sm-10" {{ $errors->has('mobile') ? ' has-error' : '' }}>
											@if ($errors->has('mobile'))
																				<span class="help-block">
																						<strong>{{ $errors->first('mobile') }}</strong>
																				</span>
																		@endif
											<input type="text" class="form-control" name="mobile" placeholder="please enter mobile" value="{{$user->mobile}}" required/>
										</div>
									</div>

					<div class="form-group row">
										<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
										<div class="col-sm-10" {{ $errors->has('password') ? ' has-error' : '' }}>
											@if ($errors->has('password'))
																				<span class="help-block">
																						<strong>{{ $errors->first('mobile') }}</password>
																				</span>
																		@endif
											<input type="text" class="form-control" name="password" placeholder="Leave empty if you  don't to change it" value="" />
										</div>
									</div>

				
									
									



										



					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('profile')}}" class="btn btn-default btn-block">Back</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">Update</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
