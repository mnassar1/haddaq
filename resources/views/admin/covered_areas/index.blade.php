@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading"> @lang('labels.admin.coverd_area')</div>
				<div class="panel-body">
					
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('covered_areas.create')}}" class="btn btn-primary">+  @lang('labels.admin.new_coverd_area')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>  @lang('labels.name')</th>
								<th> @lang('labels.admin.action')</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr>
								<td><?php echo $i ;?></td>
								
								<td>{{$page->area_name}}</td>
								<td> 
									<form class="col-md-4" method="POST" action="{{action('Web\CoveredAreasController@destroy', $page->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Area?')"> @lang('labels.delete')</button>
												</form>

								</td>

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="10" class="text-center">
									You have not any Covered Areas , lets to <a href="{{route('covered_areas.create')}}">add new Covered Area</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
