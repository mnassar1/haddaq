@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b> @lang('labels.admin.create_new_coverd_area')</b>
					</div>
				</div>
				<div class="panel-body">
					

					<table class="table">
						
						

						
						<tr>
							<th class="active">  @lang('labels.admin.draw_area')</th>
							<td>
								<iframe src="{{URL::to('/admin/create_covered_area')}}" width="100%" height="600"></iframe>
							</td>
						</tr>
							


						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('covered_areas.index')}}" class="btn btn-block btn-default"> @lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											
											
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection