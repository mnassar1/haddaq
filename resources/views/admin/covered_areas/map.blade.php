<meta name="csrf-token" content="{{ csrf_token() }}">

				<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('covered_areas.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
				
						    
						    	  <div id="map"></div>
    <script>
      // This example requires the Drawing library. Include the libraries=drawing
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 29.3117845, lng: 46.4143271},
          zoom: 5
        });

        var drawingManager = new google.maps.drawing.DrawingManager({
          //drawingMode: google.maps.drawing.OverlayType.MARKER,
          drawingControl: true,
					drawingMode : "polygon",
					drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [  'polygon']
					  //drawingModes: [ 'circle', 'polygon', 'rectangle']
          },
          markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
          }
        });
        drawingManager.setMap(map);


				  //assign map to the drawaing manager
      drawingManager.setMap(map)

      //listening on ploygoncomplete event
      google.maps.event.addListener(
        drawingManager,
        'polygoncomplete',
        (polygon) => {

            let coordinates = polygon.getPath().getArray()

            //map cordinates to be array of objects like [{lat:?, lng:?}]
            let mapedCoords = coordinates.map((point) => {
                return {
                lat: point.lat(),
                lng: point.lng()
                }
            })
            
            var check_confirm = confirm('Are You Sure , You want to Save This polygon ?');
            //console.log(check);
						
						//the mapped coordinates will be save to db
						if(check_confirm == true){
						
          var area_name = prompt("Please enter your Area name:", "Area Name");
          if (area_name == null || area_name == "") {
            txt = "You Must Enter Name";

          }

          //document.getElementById("demo").innerHTML = txt;


             $.post("{{route('save_covered_areas')}}",
                {
                    country_id:1,
                    area_name: area_name,
                    Coords:mapedCoords,
                },function(response){
                    if(response.status_code == 200){
                        alert('Area successfully Saved!');
                    }else{
                        alert('Unexpected Error!');
                    }
                });
        }else{
            

        }
            
        }
      )

      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhGw9jcsdAmE3NfT03Rd-3Y8MuodBmLTM&libraries=drawing&callback=initMap"
         async defer></script>
		<script src="{{ asset('js/app.js') }}"></script>						
	
	
	

					 
					  

						
					  {{-- <div id="category_group_id" class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label"> @lang('labels.admin.cat_group')</label>
					    <div class="col-sm-10">
					    	<select required name="category_group_id" class="form-control">
					    	<option value="0"> @lang('labels.admin.select_cat_group')</option>	
					    
					    
					    	</select>
					    </div>
					  </div> --}}



					
					   	{{-- <label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('covered_areas.index')}}" class="btn btn-default btn-block"> @lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success"> @lang('labels.create')</button>
						 --}}