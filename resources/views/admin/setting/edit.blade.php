@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
                        @lang('labels.admin.edit_user_push_notification')						
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\PushController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

					  

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-4 col-form-label"># @lang('labels.admin.notification_per_month')</label>
						    <div class="col-sm-6" {{ $errors->has('number_of_notifications') ? ' has-error' : '' }}>
						    	@if ($errors->has('number_of_notifications'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number_of_notifications') }}</strong>
                                    </span>
                                @endif
						    	<input type="number" class="form-control" name="number_of_notifications" placeholder="please enter # Notifications Per Month" value="{{$categories->number_of_notifications}}" required/>
						    </div>
						  </div>

					  

				


					   

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('users_push_setting.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
