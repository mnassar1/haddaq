@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b> @lang('labels.admin.user_push_notification_per_month') </b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						
						

						<tr>
							<th  class="active"># @lang('labels.admin.notification_per_month')</th>
							<td>
								{{$categories->number_of_notifications}}
							</td>
						</tr>
					
						

					

						<tr>
							<td >
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('users_push_setting.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-7">
										<div class="row">
											<div class="col-sm-7">
												<a class="btn btn-block btn-warning" href="{{route('users_push_setting.edit',$categories->id)}}">@lang('labels.edit')</a>
											</div>	
											{{-- <div class="col-sm-3">
												<form method="POST" action="{{action('Web\CategoryController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Category?')">@lang('labels.delete')</button>
												</form>
											</div> --}}
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
