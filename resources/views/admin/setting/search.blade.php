<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name" class="form-control" placeholder="@lang('labels.admin.eng_cat_name')" value="{{Request::get('name')}}" />
								</div>

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name_ar" class="form-control" placeholder="@lang('labels.admin.ar_cat_name')" value="{{Request::get('name_ar')}}" />
								</div>


								<!-- register type -->
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-6 lead"> @lang('labels.admin.cat_type')</div>
										<div class="col-sm-6">
											<select name="category_type" class="form-control">
												<option value="">Select</option>
												<option value="1" {{(Request::get('category_type') == '1') ? 'selected' : ''}}>User Type</option>
												
												<option value="3" {{(Request::get('category_type') == '3') ? 'selected' : ''}}>Company Type</option>
												
												<option value="2" {{(Request::get('category_type') == '2') ? 'selected' : ''}}>Both</option>

											</select>
										</div>
									</div>
								</div>

								

								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('categories.index')}}">@lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
