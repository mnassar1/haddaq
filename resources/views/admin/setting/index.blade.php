@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
                <div class="panel-heading">
 @lang('labels.admin.user_push_settings')   </div>
				<div class="panel-body">
					
					<table class="table table-hover">
						<thead>
							{{-- <tr>
								<td colspan="5">
									<a href="{{route('users_push_setting.create')}}" class="btn btn-primary">+  @lang('labels.admin.new_category') </a>
								</td>
							</tr> --}}
							<tr class="active">
								<th>#</th>
								
								<th>#  @lang('labels.admin.notification_per_month')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('users_push_setting.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->number_of_notifications}}</td>
								
								

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="10" class="text-center">
									You have not any Setting , lets to <a href="{{route('users_push_setting.create')}}">add new Setting</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
