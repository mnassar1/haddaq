@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
					 	@lang('labels.admin.edit_emr')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\EmergencyController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

				

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">					 	@lang('labels.eng_name')
</label>
						    <div class="col-sm-10" {{ $errors->has('name') ? ' has-error' : '' }}>
						    	@if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name" placeholder="Enter English Name" value="{{$categories->name}}" required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">					 	@lang('labels.ar_name')
</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" placeholder="Enter Arabic Name" name="name_ar" value="{{$categories->name_ar}}" required/>
					    </div>
					  </div>

					<div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">					 	@lang('labels.mobile')
</label>
					    <div class="col-sm-10">
					    	<input type="text" class="form-control" placeholder="Enter Mobile" name="mobile" value="{{$categories->mobile}}" required/>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('emergency.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')
</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')
</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection