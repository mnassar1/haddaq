@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
					 	@lang('labels.admin.create_emr')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('emergency.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name') ? ' has-error' : '' }}>
						    	@if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name" placeholder="@lang('labels.please_enter') @lang('labels.eng_name')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name_ar" placeholder="@lang('labels.please_enter') @lang('labels.ar_name')"  required/>
						    </div>
						  </div>


						   <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.mobile')</label>
						    <div class="col-sm-10" {{ $errors->has('mobile') ? ' has-error' : '' }}>
						    	@if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="mobile" placeholder="@lang('labels.please_enter') @lang('labels.mobile')"  required/>
						    </div>
						  </div>


					 
					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('emergency.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
