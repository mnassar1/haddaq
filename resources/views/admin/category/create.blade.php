@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row text-center">
                            @lang('labels.create_cat')
                        </div>
                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" method="POST" action="{{route('categories.store')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- cms type -->

                            <!-- content image -->
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_icon')</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" placeholder="@lang('labels.admin.cat_icon')" name="icon"
                                           required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_cat_name')</label>
                                <div class="col-sm-10" {{ $errors->has('category_name') ? ' has-error' : '' }}>
                                    @if ($errors->has('category_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                    @endif
                                    <input type="text" class="form-control" name="category_name"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.eng_cat_name')" required/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_cat_name')</label>
                                <div class="col-sm-10" {{ $errors->has('category_name_ar') ? ' has-error' : '' }}>
                                    @if ($errors->has('category_name_ar'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_name_ar') }}</strong>
                                    </span>
                                    @endif
                                    <input type="text" class="form-control" name="category_name_ar"
                                           placeholder="@lang('labels.please_enter') @lang('labels.admin.ar_cat_name')" required/>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_type')</label>
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <input required type="radio" value="1" name="category_type">@lang('labels.admin.user_cat')
                                    </label>
                                    <label class="radio-inline">
                                        <input required type="radio" value="3" name="category_type" checked>
                                        @lang('labels.admin.company_cat')
                                    </label>

                                </div>
                            </div>


                            <div id="category_group_id" class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_group')</label>
                                <div class="col-sm-10">
                                    <select required name="category_group_id" class="form-control">
                                        <option value="0">@lang('labels.admin.select_cat_group')</option>
                                        <?php
                                        if(!empty($groups)){
                                        foreach ($groups as $group) {
                                        ?>
                                        <option value="<?php echo $group->id?>"><?php echo $group->group_name?></option>
                                        <?php    }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">
                                    <a href="{{route('categories.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
                                </label>
                                <div class="col-sm-10">
                                    <button class="btn btn-block btn-success">@lang('labels.create')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
