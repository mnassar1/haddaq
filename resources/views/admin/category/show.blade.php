@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$categories->category_name}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						<tr>
							<th class="active">@lang('labels.admin.cat_icon')</th>
							<td>
								<?php if ($categories->icon != "" || $categories->icon != NULL) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $categories->icon;?>" class="img-circle img-responsive"> </div>
						
						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.ar_cat_name')</th>
							<td>
								{{$categories->category_name_ar}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.eng_cat_name')</th>
							<td>
								{{$categories->category_name}}
							</td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.admin.cat_type')</th>
							<td><?php
									switch ($categories->category_type) {
										case 1:
											echo "User Type";
											break;
										case 2:
											echo "Both";
											break;
										case 3:
											echo "Company Type";
											break;
											
										default:
											# code...
											break;
									}
								?></td>
						</tr>
						

						<tr>
							<th class="active">@lang('labels.admin.cat_group')</th>
							<td>
								<?php 
								if(!empty($groups)){
									echo $groups->group_name;
								}else{
									echo "Not Belong to any Category Group";
								}
								?>
							</td>
						</tr>
						

						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('categories.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('categories.edit',$categories->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\CategoryController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Category?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection