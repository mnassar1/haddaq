<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name_en" class="form-control" placeholder="@lang('labels.admin.eng_cat_name')" value="{{Request::get('name_en')}}" />
								</div>

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name_ar" class="form-control" placeholder="@lang('labels.admin.ar_cat_name')" value="{{Request::get('name_ar')}}" />
								</div>


								<!-- register type -->
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-6 lead"> @lang('labels.admin.cat_type')</div>
										<div class="col-sm-6">
											<select name="category_type" class="form-control">
												<option value="">@lang('labels.select')</option>
												<option value="1" {{(Request::get('category_type') == '1') ? 'selected' : ''}}>@lang('labels.user_type')</option>
												
												<option value="3" {{(Request::get('category_type') == '3') ? 'selected' : ''}}>@lang('labels.company_type')</option>
												
												<option value="2" {{(Request::get('category_type') == '2') ? 'selected' : ''}}>@lang('labels.both')</option>

											</select>
										</div>
									</div>
								</div>

								

								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('categories.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
