@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row text-center">
                            @lang('labels.edit_cat')
                        </div>
                    </div>
                    <div class="panel-body">
                        <form method="POST" enctype="multipart/form-data"
                              action="{{action('Web\CategoryController@update', $categories->id)}}">
                            <input type="hidden" name="_method" value="put"/>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- cms type -->

                            <!-- content image -->
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_icon')</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" placeholder="Category icon" name="icon">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_cat_name')</label>
                                <div class="col-sm-10" {{ $errors->has('category_name') ? ' has-error' : '' }}>
                                    @if ($errors->has('category_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                    @endif
                                    <input type="text" class="form-control" name="category_name"
                                           placeholder="please enter Category Name"
                                           value="{{$categories->category_name}}" required/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_cat_name')</label>
                                <div class="col-sm-10" {{ $errors->has('category_name_ar') ? ' has-error' : '' }}>
                                    @if ($errors->has('category_name_ar'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_name_ar') }}</strong>
                                    </span>
                                    @endif
                                    <input type="text" class="form-control" name="category_name_ar"
                                           placeholder="please enter Arabic Category Name"
                                           value="{{$categories->category_name_ar}}" required/>
                                </div>
                            </div>

                            <?php
                            if ($count_of_ads == 0) {
                            ?>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_type')</label>
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <input required type="radio" value="1"
                                               name="category_type" <?php if ($categories->category_type == 1) echo "checked"; ?> >@lang('labels.admin.user_cat')
                                    </label>
                                    <label class="radio-inline">
                                        <input required type="radio" value="3"
                                               name="category_type" <?php if ($categories->category_type == 3) echo "checked"; ?>>@lang('labels.admin.company_cat')
                                    </label>
                                </div>
                            </div>

                            <?php
                            }
                            ?>


                            <div id="category_group_id" class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cat_group')</label>
                                <div class="col-sm-10">
                                    <select required name="category_group_id" class="form-control">
                                        <option value="0">@lang('labels.admin.select_cat_group')</option>
                                        <?php
                                        if(!empty($groups)){
                                        foreach ($groups as $group) {
                                        ?>
                                        <option
                                            <?php if ($group->id == $categories->category_group_id) echo "selected";?>  value="<?php echo $group->id?>"><?php echo $group->group_name?></option>
                                        <?php    }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">
                                    <a href="{{route('categories.show',$categories->id)}}"
                                       class="btn btn-default btn-block">@lang('labels.back')</a>
                                </label>
                                <div class="col-sm-10">
                                    <button class="btn btn-block btn-success">@lang('labels.update')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection