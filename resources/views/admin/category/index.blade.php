@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.cats')</div>
				<div class="panel-body">
					@include('admin.category.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('categories.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_cat')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.ar_cat_name')</th>
								<th>@lang('labels.admin.eng_cat_name')</th>
								<th>@lang('labels.admin.cat_type')</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('categories.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->category_name_ar}}</td>
								<td>{{$page->category_name}}</td>
								<td><?php
									switch ($page->category_type) {
										case 1:
											echo "User Type";
											break;
										case 2:
											echo "Both";
											break;
										case 3:
											echo "Company Type";
											break;
											
										default:
											# code...
											break;
									}
								?></td>

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="10" class="text-center">
									You have not any Categories , lets to <a href="{{route('categories.create')}}">add new Category</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection