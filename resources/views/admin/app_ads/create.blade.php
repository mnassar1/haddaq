@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
                        @lang('labels.admin.create_new_admin_ads')	
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('admin_ads.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="@lang('labels.please_enter') @lang('labels.title')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.ads_details')" name="details" ></textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_image')</label>
					    <div class="col-sm-10">
					    	<input type="file" class="form-control" placeholder="@lang('labels.ads_image')" name="images" >
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.active')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="active" >
										<option value="0">@lang('labels.admin.not_active')</option>
										<option value="1">@lang('labels.admin.active')</option>
									</select>
					    </div>
					  </div>

					  <!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.order')</label>
						    <div class="col-sm-10" {{ $errors->has('ordering') ? ' has-error' : '' }}>
						    	@if ($errors->has('ordering'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ordering') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="ordering" placeholder="@lang('labels.please_enter') @lang('labels.admin.ordering')"  required/>
						    </div>
						  </div>
					  
					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.is_offer')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="is_offer" >
										<option value="0">@lang('labels.not_offer')</option>
										<option value="1">@lang('labels.offer')</option>
									</select>
					    </div>
					  </div>


					   <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.is_banner')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="is_banar" >
										<option value="0">@lang('labels.admin.not_banner')</option>
										<option value="1">@lang('labels.admin.first_banner')</option>
										<option value="2">@lang('labels.admin.second_banner')</option>
										<option value="3">@lang('labels.admin.third_banner')</option>
										<option value="4">@lang('labels.admin.fourth_banner')</option>
									</select>
					    </div>
					  </div>

					    <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ads_display_on')</label>
					    <div class="col-sm-10">
					    	<select class="form-control" name="fixing_shops_or_home" >
										<option value="0">@lang('labels.admin.fixing_shops')</option>
										<option value="1">@lang('labels.admin.home_page')</option>
									</select>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('admin_ads.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
