<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="title" class="form-control" placeholder=" @lang('labels.title') " value="{{Request::get('title')}}" />
								</div>

								<!-- email -->
								<div class="col-sm-4">
									<input type="text" name="owner" class="form-control" placeholder=" @lang('labels.owner_name') " value="{{Request::get('owner')}}" />
								</div>

								<!-- register type -->
								<div class="col-sm-4">
										
											<select name="user_types_id" class="form-control">
												<option value=""> @lang('labels.admin.select_membership_type')</option>
												
												<?php
												foreach ($user_types as $type) {?>
													<option value="<?php echo $type->id;?>" <?php if (Request::get('user_types_id') == $type->id) {echo "selected";}   ?>
														><?php echo $type->type_name;?></option>
												<?php }
												?>
												
												
											</select>
									
								</div>

								
							</div>
						</td>
					</tr>

					<tr>
						<td>
							<div class="row">
								<!-- name -->
								<div class="col-sm-4">
									<input type="text" onfocus="(this.type='date')" name="from" class="form-control" placeholder="@lang('labels.choose_date_from')" value="{{Request::get('from')}}" />
								</div>

								<!-- email -->
								<div class="col-sm-4">
									<input type="text" onfocus="(this.type='date')" name="to" class="form-control" placeholder="@lang('labels.choose_date_to')" value="{{Request::get('to')}}" />
								</div>
						
						<div class="col-sm-4">
									
											<select name="approved_status" class="form-control">
												<option value=""  {{(Request::get('approved_status') == "") ? 'selected' : ''}}>@lang('labels.select_approved_status')</option>
												{{-- <option value="2"  {{(Request::get('approved_status') == "2") ? 'selected' : ''}}>New</option> --}}
												<option value="0" {{(Request::get('approved_status') == '0') ? 'selected' : ''}}>@lang('labels.no')</option>
												<option value="1" {{(Request::get('approved_status') == '1') ? 'selected' : ''}}>@lang('labels.yes')</option>
											</select>
									
								</div>
								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6"> @lang('labels.search') </button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('app_ads.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
