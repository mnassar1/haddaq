@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading"> @lang('labels.subscribers_ads') </div>

                    <div class="panel-body">
                        @include('admin.app_ads.search')
                        <table class="table table-hover">
                            <thead>

                            <tr class="active">
                                <th>#</th>
                                <th>@lang('labels.title')</th>
                                <th>@lang('labels.view_cnt')</th>
                                <th>@lang('labels.approval_status') </th>
                                <th>@lang('labels.owner')</th>
                                <th>@lang('labels.created_at') </th>
                                <th>@lang('labels.updated_at') </th>
                                <th>@lang('labels.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;?>
                            @if(isset($app_ads) && !empty($app_ads))
                                @foreach($app_ads as $page)
                                    <tr>
                                        <td onclick="window.location='{{route('app_ads.show',$page->id)}}';"
                                            style="cursor : pointer"><?php echo $i;?></td>
                                        <td onclick="window.location='{{route('app_ads.show',$page->id)}}';"
                                            style="cursor : pointer">{{$page->title}}</td>
                                        <td onclick="window.location='{{route('app_ads.show',$page->id)}}';"
                                            style="cursor : pointer">{{$page->views_count}}</td>
                                        <td onclick="window.location='{{route('app_ads.show',$page->id)}}';"
                                            style="cursor : pointer">
                                            <?php
                                            $approved_status = $page->approved_status;
                                            switch ($approved_status) {
                                                case '0':
                                                    $approved_status = "No";
                                                    break;
                                                case '1':
                                                    $approved_status = "Yes";
                                                    break;
                                                case NULL:
                                                    $approved_status = "";
                                                    break;
                                            }
                                            echo $approved_status;
                                            ?>
                                        </td>


                                        <?php
                                        $User = \App\User::find($page->company_id);
                                        ?>

                                        <td>
                                            <a target="_blank" href="{{route('user.show',$User->id)}}">
                                                {{$User->full_name}}</a>
                                        </td>

                                        <td>{{$page->created_at}}</td>
                                        <td>{{$page->updated_at}}</td>
                                        <td>
                                            <button type="button" class="btn-block btn btn-danger" data-toggle="modal"
                                                    data-target="#myModal<?php echo $i;?>">@lang('labels.show_details')</button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title">{{$page->title}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table">
                                                                <!-- English Content -->
                                                                <tr>
                                                                    <th class="active">@lang('labels.title')</th>
                                                                    <td>
                                                                        {{$page->title}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="active">@lang('labels.details')</th>
                                                                    <td>
                                                                        {{ strip_tags($page->details)}}
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <th class="active">@lang('labels.ads_image')</th>
                                                                    <td>

                                                                        <?php
                                                                        $images_array = json_decode($page->images);
                                                                        //return $images_array;
                                                                        $images = array();
                                                                        foreach ($images_array as $image) {
                                                                            $images[] = asset("storage/" . $image);
                                                                        }
                                                                        if (!empty($images)) {
                                                                        foreach ($images as $image) {?>
                                                                        <div class="col-md-3 col-lg-3 " align="center">
                                                                            <img style="height: 100px;width: 100px"
                                                                                 alt="User Pic"
                                                                                 src="<?php echo $image;?>"
                                                                                 class="img-circle img-responsive">
                                                                        </div>

                                                                        <?php } }?>

                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <th class="active">@lang('labels.approved_status')</th>
                                                                    <td>
                                                                        <?php

                                                                        $approved_status = $page->approved_status;
                                                                        switch ($approved_status) {
                                                                            case '0':
                                                                                $approved_status = "No";
                                                                                break;
                                                                            case '1':
                                                                                $approved_status = "Yes";
                                                                                break;
                                                                            case NULL:
                                                                                $approved_status = "";
                                                                                break;
                                                                        }
                                                                        echo $approved_status;


                                                                        ?>
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <th class="active">@lang('labels.posted_at')</th>
                                                                    <td>{{$page->created_at}}</td>
                                                                </tr>


                                                                <tr>
                                                                    <th class="active">@lang('labels.used_status')</th>
                                                                    <td>

                                                                        {{($page->used_status) ? 'Yes' : 'No'}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="active">@lang('labels.price')</th>
                                                                    <td>
                                                                        {{$page->price}}
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <th class="active">@lang('labels.block_reason')</th>
                                                                    <td>
                                                                        {{$page->block_reason}}
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td colspan="2">
                                                                        <div class="row">

                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-3">
                                                                                        <a class="btn btn-block btn-warning"
                                                                                           href="{{route('app_ads.edit',$page->id)}}">@lang('labels.block')</a>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <form method="POST"
                                                                                              action="{{action('Web\AppAdsController@destroy', $page->id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="_method"
                                                                                                   value="delete"/>
                                                                                            <input type="hidden"
                                                                                                   name="approval"
                                                                                                   value="approval"/>
                                                                                            <input type="hidden"
                                                                                                   name="_token"
                                                                                                   value="{{ csrf_token() }}">
                                                                                            <button class="btn-block btn btn-danger"
                                                                                                    onclick="return confirm('Are you sure to Approve this Company Ads?')">@lang('labels.approval')</button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class=" col-sm-3">
                                                                                        <button type="button"
                                                                                                class="btn btn-default"
                                                                                                data-dismiss="modal">@lang('labels.close')</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </td>


                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                                <tr>
                                    <td colspan="10" class="text-center">
                                        <div class="row">
                                            {{$paginate->appends(request()->input())->links()}}
                                        </div>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <th colspan="4" class="text-center">
                                        You have not any Users
                                        Ads, <!-- lets to <a href="{{route('admin_ads.create')}}">add new Admin Ads</a> -->
                                    </th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
