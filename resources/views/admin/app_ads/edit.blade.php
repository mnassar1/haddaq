@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
					    @lang('labels.admin.edit_ads_publisher') : {{$user->full_name}}
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{action('Web\AppAdsController@update', $app_ads->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input readonly type="text" class="form-control" name="title" placeholder="@lang('labels.please_enter') @lang('labels.title')" value="{{$app_ads->title}}" required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_details')</label>
					    <div class="col-sm-10">
					    	<textarea readonly class="form-control" placeholder="@lang('labels.ads_details')" name="details" >{{$app_ads->details}}</textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_image')</label>
					    <div class="col-sm-10">
					    	<input disabled="" type="file" class="form-control" placeholder="@lang('labels.ads_image')" name="images">
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label"> @lang('labels.select_cat')</label>
					    <div class="col-sm-10">
					    	<select disabled="" required name="category_id" class="form-control">
					    		
					    	<?php 
					    	if(!empty($categories)){
					    		foreach ($categories as $category) {
					    			?>
					    	<option <?php if($app_ads->category_id == $category->id)echo "selected"?> value="<?php echo $category->id?>"><?php echo $category->category_name?></option>
					    	<?php 	}
					    	}
					    	?>
					    	</select>
					    </div>
					  </div>
					  
					<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.price')</label>
						    <div class="col-sm-10" {{ $errors->has('price') ? ' has-error' : '' }}>
						    	@if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
						    	<input readonly value="<?php echo $app_ads->price;?>" type="text" class="form-control" name="price" placeholder="@lang('labels.price')"  required/>
						    </div>
						  </div>

					 <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.used_status')</label>
					    <div class="col-sm-10">
					    	<select disabled="" required name="used_status" class="form-control">
					    		<option <?php if($app_ads->used_status == 0)echo "selected"?> value="0">@lang('labels.no')</option>
					    		<option <?php if($app_ads->used_status == 1)echo "selected"?> value="1">@lang('labels.yes')</option>
					    	
					    	</select>
					    </div>
					  </div>
					  

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.approval_status')</label>
					    <div class="col-sm-10">
					    	<select required name="approved_status" class="form-control">
					    		<option <?php if($app_ads->approved_status == 0)echo "selected"?> value="0">@lang('labels.no')</option>
					    		<option <?php if($app_ads->approved_status == 1)echo "selected"?> value="1">@lang('labels.yes')</option>
					    	
					    	</select>
					    </div>
					  </div>

<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.block_reason')</label>
						    <div class="col-sm-10" {{ $errors->has('block_reason') ? ' has-error' : '' }}>
						    	@if ($errors->has('block_reason'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('block_reason') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="block_reason" placeholder="@lang('labels.please_enter') @lang('labels.block_reason')" value="{{$app_ads->block_reason}}" />
						    </div>
						  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('app_ads.show',$app_ads->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
