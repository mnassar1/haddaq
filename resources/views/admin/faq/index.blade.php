@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.faqs')</div>
				<div class="panel-body">
					<table class="table">
						<tr>
							<td colspan="3">
								<a href="{{route('faq.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_faq')</a>
							</td>
						</tr>
						<tr>
							<th>#</th>
							<th>@lang('labels.english')</th>
							<th>@lang('labels.arabic')</th>
						</tr>
					@if(count($faqs))
					@foreach($faqs as $faq)
					<tr onclick="window.location='{{route('faq.show',$faq->id)}}';" style="cursor : pointer">
						<th>Q{{$faq->id}}</th>	
						<td>{{$faq->question_en}}</td>
						<td>{{$faq->question_ar}}</td>
					</tr>
					@endforeach
					@else
					<tr>
						<th class="text-center">There are not any FAQ.</th>
					</tr>
					@endif
					</table>
					<div class="row">
						<div class="col-sm-12 text-center">
						{{$paginate->links()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection