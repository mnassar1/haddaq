@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-12">
							Q{{$faq->id}}# {{$faq->question_en}}?
							
						</div>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<tr>
							<th class="active">
								{{$faq->question_ar}}
							</th>
						</tr>
						<tr>
							<td>{{$faq->answer_ar}}</td>
						</tr>
						<tr>
							<th class="active">
								{{$faq->question_en}}
							</th>
						</tr>
						<tr>
							<td>{{$faq->answer_en}}</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('faq.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a href="{{route('faq.edit',$faq->id)}}" class="btn btn-block btn-warning">@lang('labels.edit')</a>
											</div>
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\FaqController@destroy', $faq->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this FAQ?')">@lang('labels.delete')</button>
														</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection