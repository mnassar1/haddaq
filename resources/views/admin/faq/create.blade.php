@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_faq')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{route('faq.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_question')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.admin.ar_question')" name="question_ar" required/>
					    </div>
					  </div>
					
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_answer')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.ar_answer')" name="answer_ar" required></textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_question')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.admin.eng_question')" name="question_en" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_answer')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.eng_answer')" name="answer_en" required></textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.order')</label>
					    <div class="col-sm-10">
					    	  <input type="number" class="form-control" id="inputPassword" placeholder="@lang('labels.admin.order')" name="question_order" required/>
					    </div>
					  </div>

					    <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label"><a class="btn btn-block btn-default" href="route('faq.index')">@lang('labels.back')</a></label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
