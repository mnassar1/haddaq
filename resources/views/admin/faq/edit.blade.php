@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_faq')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{action('Web\FaqController@update', $faq->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">						@lang('labels.admin.ar_question')
</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Arabic Question" name="question_ar" value="{{$faq->question_ar}}" required/>
					    </div>
					  </div>
					
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">						@lang('labels.admin.ar_answer')
</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Arabic Answer" name="answer_ar" required>{{$faq->answer_ar}}</textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">						@lang('labels.admin.eng_question')
</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="English Question" name="question_en" value="{{$faq->question_en}}" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">						@lang('labels.admin.eng_answer')
</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="English Answer" name="answer_en" required>{{$faq->answer_en}}</textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">						@lang('labels.admin.order')
</label>
					    <div class="col-sm-10">
					    	  <input type="number" class="form-control" id="inputPassword" placeholder="Question Order" name="question_order" value="{{$faq->order}}" required/>
					    </div>
					  </div>

					    <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label"><a class="btn btn-block btn-default" href="{{route('faq.show',$faq->id)}}">						@lang('labels.back')
</a></label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.edit')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection