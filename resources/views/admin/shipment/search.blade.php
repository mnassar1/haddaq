<div class="row">
	<div class="col-sm-12">
		<form action="" method="">
			<table class="table">
				<tbody>
					<tr>
						<th colspan="4" class="text-center"> @lang('labels.admin.shipping_from')</th>
						<th colspan="4" class="text-center">@lang('labels.admin.shipping_to')</th>
					</tr>
					<tr>
						<td colspan="4">
							<div class="row">
								<div class="col-sm-6">
									<select class="form-control" name="country_from" id="country_from_id">
										<option value="0">@lang('labels.admin.select_country')</option>
										@foreach($countries as $country)
										<option value="{{$country->id}}" {{(Request::get('country_from') == $country->id)? 'selected':''}}>{{$country->name_en}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="city_from" id="city_from_id" disabled>
										<option value="0">@lang('labels.admin.select_city')</option>
									</select>
								</div>
							</div>
						</td>
						<td colspan="4">
							<div class="row">
								<div class="col-sm-6">
									<select class="form-control" name="country_to" id="country_to_id">
										<option value="0">@lang('labels.admin.select_country')</option>
										@foreach($countries as $country)
										<option value="{{$country->id}}" {{(Request::get('country_to') == $country->id)? 'selected':''}}>{{$country->name_en}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="city_to" id="city_to_id" disabled>
										<option value="0">@lang('labels.admin.select_city')</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th colspan="2" class="text-center">@lang('labels.admin.weight_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.height_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.width_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.length_range')</th>
					</tr>
					<tr>
						<td>
                            <input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.weight_from')" name="weight_from" value="{{Request::get('weight_from')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.weight_to')" name="weight_to" value="{{Request::get('weight_to')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.height_from')" name="height_from" value="{{Request::get('height_from')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.height_to')" name="height_to" value="{{Request::get('height_to')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.width_from')" name="width_from" value="{{Request::get('width_from')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.width_to')" name="width_to" value="{{Request::get('width_to')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.length_from')" name="length_from" value="{{Request::get('length_from')}}">
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.length_to')" name="length_to" value="{{Request::get('length_to')}}">
						</td>
					</tr>
					
					<tr>
						<th colspan="2" class="text-center"> @lang('labels.admin.fees_range') </th>
						<th colspan="2" class="text-center">@lang('labels.price')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.days_from_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.days_to_range')</th>
					</tr>
					<tr>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.fees_from')" name="potential_fees_from" value="{{Request::get('potential_fees_from')}}">
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.fees_to')" name="potential_fees_to" value="{{Request::get('potential_fees_to')}}">
								</div>
							</div>
						</td>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.price_from')" name="price_from" value="{{Request::get('price_from')}}">
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="inputState" placeholder="@lang('labels.admin.price_to')" name="price_to" value="{{Request::get('price_to')}}">
								</div>
							</div>
						</td>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input type="date" class="form-control" id="inputState" placeholder="@lang('labels.admin.days_from_from')" name="days_from_start" value="{{Request::get('days_from_start')}}">
								</div>
								<div class="col-sm-6">
									<input type="date" class="form-control" id="inputState" placeholder="@lang('labels.admin.days_from_to')" name="days_from_end" value="{{Request::get('days_from_end')}}">
								</div>
							</div>
						</td>
						<td colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<input type="date" class="form-control" id="inputState" placeholder="@lang('labels.admin.days_to_from')" name="days_to_start" value="{{Request::get('days_to_start')}}">
									
								</div>
								<div class="col-sm-6">
									<input type="date" class="form-control" id="inputState" placeholder="@lang('labels.admin.days_to_to')" name="days_to_end" value="{{Request::get('days_to_end')}}">
									
								</div>
							</div>
						</td>
					</tr>

					<tr>
						<td colspan="4" class="text-center">
							<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
						</td>
						<td colspan="4" class="text-center">
							<a class="btn btn-danger px-4 col-sm-6" href="{{route('shipments')}}"> @lang('labels.reset')</a>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
