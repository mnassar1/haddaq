@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
                <div class="panel-heading">
                    @lang('labels.admin.shipment')
                </div>
				<div class="panel-body">
					<!-- include shipment filter here -->
					@if($show_filter)
					@include('admin.shipment.search')
					@endif
					@if(count($shipments))
					@foreach($shipments as $ship)
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<img src="http://via.placeholder.com/20x20" class="rounded-circle">
									<strong><u>{{($ship->owner) ? $ship->owner->name:""}}</u> @lang('labels.admin.needs_to_ship')</strong>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-3">
									<img src="{{$ship->photo}}" width="200" height="150">
								</div>
								<div class="col-sm-9">
									<table class="table">
										<thead>
											<tr class="lead warning">
												<td colspan="3">{{$ship->title}}</td>
											</tr>
										</thead>
										<tbody>
											<tr class="active">
												<th>@lang('labels.admin.shipping_from')</th>
												<th>@lang('labels.admin.shipping_to')</th>
											</tr>
											<tr>
												<td>{{$ship->from->name_en}}</td>
												<td>{{$ship->from->city->name_en}}</td>
											</tr>
											<tr>
												<td>{{$ship->to->name_en}}</td>
												<td>{{$ship->to->city->name_en}}</td>
											</tr>
											<tr>
												<td class="text-center" colspan="2">
													<a href="{{route('shipment_show',$ship->id)}}">@lang('labels.show_details')...</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif
					<div class="row">
						<div class="col-sm-12 text-center">
						{{$paginate->links()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
