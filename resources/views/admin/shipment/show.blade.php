@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-12">
							<img src="http://via.placeholder.com/20x20" class="rounded-circle">
							<strong><u>{{($shipment->owner) ? $shipment->owner->name:""}}</u> @lang('labels.admin.needs_to_ship')</strong>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-3">
							<img src="{{$shipment->photo}}" width="200" height="150">
						</div>
						<div class="col-sm-9">
							<div class="row">
								<!--  Measurments -->
								<div class="col-sm-12">
									<table class="table">
										<thead>
											<tr>
												<th colspan="3" class="active">
													Shipment #{{$shipment->id}}
												</th>
											</tr>
											<tr class="lead warning">
												<td colspan="3">{{$shipment->title}}</td>
											</tr>
										</thead>
										<thead>
											<tr class="active">
												<th colspan="3">@lang('labels.admin.shipping_from')</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>@lang('labels.country')</th>
												<td>{{$shipment->from->name_en}}</td>
											</tr>
											<tr>
												<th>@lang('labels.city')</th>
												<td>{{$shipment->from->city->name_en}}</td>
											</tr>
										</tbody>
										<thead>
											<tr class="active">
												<th colspan="3">@lang('labels.admin.shipping_to')</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>@lang('labels.country')</th>
												<td>{{$shipment->to->name_en}}</td>
											</tr>
											<tr>
												<th>@lang('labels.city')</th>
												<td>{{$shipment->to->city->name_en}}</td>
											</tr>
										</tbody>
										<thead>
											<tr class="active">
												<th colspan="3">@lang('labels.Measurements')</th>
											</tr>
										</thead>
										<tbody>
											<tr>
                                                <th>@lang('labels.weight')</th>
												<td>{{$shipment->weight}} kg</td>
											</tr>
											<tr class="active">
												<th colspan="3">@lang('labels.dimensions')</th>
											</tr>
											<tr>
												<th>@lang('labels.height')Height</th>
												<td>{{$shipment->dimensions->height}} cm</td>
											</tr>
											<tr>
												<th>@lang('labels.width')Width</th>
												<td>{{$shipment->dimensions->width}} cm</td>
											</tr>
											<tr>
												<th>@lang('labels.length')Length</th>
												<td>{{$shipment->dimensions->length}} cm</td>
											</tr>
										</tbody>
										<thead>
											<tr class="active">
												<th colspan="3">@lang('labels.potential_days')</th>
											</tr>
										</thead>
										<tbody>
											<tr>
                                                <th>@lang('labels.from')</th>
												<td>{{$shipment->potential_days->from}}</td>
											</tr>
											<tr>
												<th>@lang('labels.to')</th>
												<td>{{$shipment->potential_days->to}}</td>
											</tr>
										</tbody>
										<thead>
											<tr class="active">
												<th colspan="3">@lang('labels.extras')</th>
											</tr>
										</thead>
										<tbody>
											<tr>
                                                <th>@lang('labels.potential_fees')</th>
												<td>{{$shipment->potential_fees}} $</td>
											</tr>
											<tr>
												<th>@lang('labels.price')</th>
												<td>{{$shipment->price}} $</td>
											</tr>
											<tr>
                                                <th>@lang('labels.notes')</th>
												<td>{{$shipment->note}}</td>
											</tr>
										</tbody>
										<thead>
											<tr class="active">
												<th>@lang('labels.posted_at')</th>
												<td>{{date('d-m-Y H:i:s', $shipment->posted_at)}}</td>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
