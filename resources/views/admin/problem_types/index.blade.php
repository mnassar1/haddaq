@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.problem_types')</div>
				<div class="panel-body">
					@include('admin.problem_types.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('problem_types.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_problem_type')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.ar_name')</th>
								<th>@lang('labels.eng_name')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('problem_types.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->problem_type_ar}}</td>
								<td>{{$page->problem_type_en}}</td>
							

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any Problem Types , lets to <a href="{{route('problem_types.create')}}">add new Problem Type</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection