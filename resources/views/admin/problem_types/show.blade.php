@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$categories->problem_type_en}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						<tr>
							<th class="active"> @lang('labels.admin.problem_type_icon')  </th>
							<td>
								<?php if ($categories->icon != "" || $categories->icon != NULL) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $categories->icon;?>" class="img-circle img-responsive"> </div>
						
						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.admin.ar_problem_type')</th>
							<td>
								{{$categories->problem_type_ar}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.eng_problem_type')</th>
							<td>
								{{$categories->problem_type_en}}
							</td>
						</tr>
						
						

						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('problem_types.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('problem_types.edit',$categories->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\ProblemTypesController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Problem Type , This will delete related SOS Requests related to it?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection