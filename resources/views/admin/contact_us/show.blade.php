@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{ucfirst($message->type)}}</b>	
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<tbody>
							<tr class="active">
								<td>
									<div class="row">
										<div class="col-sm-9">
											<b>{{$message->name}}</b> <<a href="">{{$message->email}}</a>> <b>Phone</b>: {{$message->phone}}
										</div>
										<div class="col-sm-3">
											{{$message->created_at}}
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><div class="lead">
									{{$message->description}}
								</div>
							</td>
							</tr>
							<tr>
								<td>
									<div class="row">
									<form method="POST" action="{{$message->id}}/change_status">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="col-sm-2">
											<select class="form-control" name="status">
												<option value="new" {{($message->status == 'new') ? 'selected':''}}>@lang('labels.new')</option>
												<option value="processing" {{($message->status == 'processing') ? 'selected':''}}>@lang('labels.processing')</option>
												<option value="archived" {{($message->status == 'archived') ? 'selected':''}}>@lang('labels.archived')</option>
											</select>
										</div>
										<div class="col-sm-3">
											<button class="btn btn-success">@lang('labels.change_status')</button>
										</div>
										</form>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
