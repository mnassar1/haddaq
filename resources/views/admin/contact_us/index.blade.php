@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">Contact Us</div>
				<div class="panel-body">
					@include('admin.contact_us.search')
					<table class="table table-hover">
						<thead>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.from')</th>
								<th>@lang('labels.mobile')</th>
								<th>@lang('labels.message')</th>
								<th>@lang('labels.type')</th>
								<th>@lang('labels.status')</th>
							</tr>
						</thead>
						<tbody>
								<?php 
								$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
								$page = ($page > 0) ? ($page -1) : 0;
								$i = ($page*20)+1;
								?>
							@if($contact_us && !empty($contact_us))
							@foreach($contact_us as $message)
							<tr  onclick="window.location='{{route('contact_us.show',$message->id)}}';" style="cursor : pointer" class="{{($message->status !== 'new') ? 'active' : ''}}">
								<th><?php echo $i;?></th>
								<th>{{$message->name}}</th>
								<th>{{$message->phone}}</th>
								<th>{{substr($message->description,0,35)}}...</th>
								<th>{{ucfirst($message->type)}}</th>
								<th>{{ucfirst($message->status)}}</th>
								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="5" class="text-center">
									<div class="row">
										{{$paginate->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr class="danger">
								<th colspan="5" class="text-center">There are not any contact us messages!</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection