<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- register type -->
								<div class="col-sm-4">
				
											<select name="type" class="form-control">
												<option value="">@lang('labels.select_type')</option>
												<option value="suggestion" <?php if(Request::get('type')== "suggestion") echo "selected";?> >@lang('labels.suggestion') </option>
												<option value="complain" <?php if(Request::get('type')== "complain") echo "selected";?>>@lang('labels.complain') </option>
												<option value="enquiry" <?php if(Request::get('type')== "enquiry") echo "selected";?>>@lang('labels.enquiry') </option>
												
											</select>
										
								</div>

								
								<!-- register type -->
								<div class="col-sm-4">
									
											<select name="status" class="form-control">
												<option value="">@lang('labels.select_status')</option>
												<option value="new"  <?php if(Request::get('status')== "new") echo "selected";?>>@lang('labels.new')</option>
												<option value="processing"  <?php if(Request::get('status')== "processing") echo "selected";?>>@lang('labels.processing')</option>
												<option value="archived"  <?php if(Request::get('status')== "archived") echo "selected";?>>@lang('labels.archived')</option>

											</select>
									
								</div>

								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('contact_us.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
