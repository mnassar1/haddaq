@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.edit_ads')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{action('Web\CompanyAdsController@update', $company_ads->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="please enter title" value="{{$company_ads->title}}" required/>
						    </div>
						  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Ads Details Content" name="details" >{{$company_ads->details}}</textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_images')</label>
					    <div class="col-sm-10">
					    	<input type="file" multiple class="form-control" placeholder="Ads Image" name="images[]">
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.select_cat')</label>
					    <div class="col-sm-10">
					    	<select required name="category_id" class="form-control">

					    	<?php
					    	if(!empty($categories)){
					    		foreach ($categories as $category) {
					    			?>
					    	<option <?php if($company_ads->category_id == $category->id)echo "selected"?> value="<?php echo $category->id?>"><?php echo $category->category_name?></option>
					    	<?php 	}
					    	}
					    	?>
					    	</select>
					    </div>
					  </div>

					<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.price')</label>
						    <div class="col-sm-10" {{ $errors->has('price') ? ' has-error' : '' }}>
						    	@if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
						    	<input value="<?php echo $company_ads->price;?>" type="number" class="form-control" name="price" placeholder="please enter price"  required/>
						    </div>
						  </div>

					 <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.used_status')</label>
					    <div class="col-sm-10">
					    	<select required name="used_status" class="form-control">
					    		<option <?php if($company_ads->used_status == 0)echo "selected"?> value="0"> @lang('labels.no')</option>
					    		<option <?php if($company_ads->used_status == 1)echo "selected"?> value="1"> @lang('labels.yes')</option>

					    	</select>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('company_ads.show',$company_ads->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
