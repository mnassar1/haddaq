@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">   @lang('labels.company_ads')</div>
				<div class="panel-body">
					@include('admin.company_ads.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									 <a href="{{route('company_ads.create')}}" class="btn btn-primary">+ 
									 @lang('labels.new_ads')</a> 
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.title')</th>
								<th>@lang('labels.view_cnt')</th>
								<th>@lang('labels.approved_status')</th>
								<th>@lang('labels.created_at')</th>
								<th>@lang('labels.updated_at')</th>	
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($app_ads) && !empty($app_ads))
							@foreach($app_ads as $page)
							<tr  onclick="window.location='{{route('company_ads.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->title}}</td>
								<td>{{$page->views_count}}</td>
								<td>{{($page->approved_status) ? 'Yes' : 'No'}}</td>
								<td>{{$page->created_at}}</td>
								<td>{{$page->updated_at}}</td>
								<?php $i++;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="10" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any Ads,  lets to <a href="{{route('company_ads.create')}}">@lang('labels.new_ads')</a>		</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection