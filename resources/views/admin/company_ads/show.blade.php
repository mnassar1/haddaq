@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$company_ads->title}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						
						<tr>
							<th class="active"> @lang('labels.title')</th>
							<td>
								{{$company_ads->title}}
							</td>
						</tr>
						<tr>
							<th class="active"> @lang('labels.ads_detail')</th>
							<td>
								{{-- {{$company_ads->details}} --}}
								{{ strip_tags($company_ads->details)}}
							</td>
						</tr>
						
						<tr>
							<th class="active">@lang('labels.ads_image')</th>
							<td>
								<?php if (!empty($company_ads->images)) {
									foreach ($company_ads->images as $image) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $image;?>" class="img-circle img-responsive"> </div>
						
						<?php } }?>
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.approval_status')</th>
							<td>
								
								{{($company_ads->approved_status) ? 'Yes' : 'No'}}
							</td>
						</tr>
					

						<tr>
							<th class="active">@lang('labels.posted_at')</th>
							<td>{{$company_ads->created_at}}</td>
						</tr>


						<tr>
							<th class="active">@lang('labels.used_status')</th>
							<td>
								{{($company_ads->used_status) ? 'Yes' : 'No'}}								
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.price')</th>
							<td>
								{{$company_ads->price}}
							</td>
						</tr>

						<tr>
							<th class="active">@lang('labels.block_reason')</th>
							<td>
								{{$company_ads->block_reason}}
							</td>
						</tr>


						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('company_ads.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('company_ads.edit',$company_ads->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\CompanyAdsController@destroy', $company_ads->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this Company Ads?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection