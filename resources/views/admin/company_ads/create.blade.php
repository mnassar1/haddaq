@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.create_ads')
					</div>
				</div>
				<div class="panel-body">
<form enctype="multipart/form-data" method="POST" action="{{route('company_ads.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="@lang('labels.please_enter') @lang('labels.title')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_details')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.ads_details')" name="details" ></textarea>
					    </div>
					  </div>

					  <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ads_images')</label>
					   <div class="col-sm-10" {{ $errors->has('images') ? ' has-error' : '' }}>
						    	@if ($errors->has('images'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
					    	<input type="file" multiple class="form-control" placeholder="@lang('labels.ads_images')" name="images[]" required>
					    </div>
					  </div>

					   <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.select_cat')</label>
					    <div class="col-sm-10">
					    	<select required name="category_id" class="form-control">
					    		
					    	<?php 
					    	if(!empty($categories)){
					    		foreach ($categories as $category) {
					    			?>
					    	<option value="<?php echo $category->id?>"><?php echo $category->category_name?></option>
					    	<?php 	}
					    	}
					    	?>
					    	</select>
					    </div>
					  </div>


					  <!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.price')</label>
						    <div class="col-sm-10" {{ $errors->has('price') ? ' has-error' : '' }}>
						    	@if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
						    	<input type="number" class="form-control" name="price" placeholder="@lang('labels.please_enter') @lang('labels.price')"  required/>
						    </div>
						  </div>

					  
					 <!-- content image -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.used_status')</label>
					    <div class="col-sm-10">
					    	<select required name="used_status" class="form-control">
					    		<option value="0"> @lang('labels.no') </option>
					    		<option value="1"> @lang('labels.yes')</option>
					    	
					    	</select>
					    </div>
					  </div>


					  

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('company_ads.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
