<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								<!-- name -->
								<div class="col-sm-3">
									<label> @lang('labels.title')
									<input type="text" name="title" class="form-control" placeholder="@lang('labels.title')" value="{{Request::get('title')}}" />
									</label>
								</div>

								<div class="col-sm-3">
									<label> @lang('labels.created_from')
									<input type="date" name="from" class="form-control" placeholder="@lang('labels.created_from')" value="{{Request::get('from')}}" /></label>
								</div>

								<!-- email -->
								<div class="col-sm-3">
									<label> @lang('labels.to')
									<input type="date" name="to" class="form-control" placeholder="@lang('labels.to')" value="{{Request::get('to')}}" /></label>
								</div>
							
								<div class="col-sm-3">
									<label> @lang('labels.status')
											<select name="approved_status" class="form-control">
												<option value=""  {{(Request::get('approved_status') == "") ? 'selected' : ''}}>@lang('labels.select_status')</option>
												<option value="0" {{(Request::get('approved_status') == '0') ? 'selected' : ''}}>@lang('labels.no')</option>
												<option value="1" {{(Request::get('approved_status') == '1') ? 'selected' : ''}}>@lang('labels.yes')</option>
											</select></label>
										</div>
								
							</div>
						</td>
					</tr>

					

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6"> @lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('company_ads.index')}}">  @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
