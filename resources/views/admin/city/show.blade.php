@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$categories->name_en}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
					

						<tr>
							<th class="active">@lang('labels.admin.ar_city_name')</th>
							<td>
								{{$categories->name_ar}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.eng_city_name')</th>
							<td>
								{{$categories->name_en}}
							</td>
						</tr>
						
						
						

		
						

						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('cities.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('cities.edit',$categories->id)}}">@lang('labels.edit')</a>
											</div>	
											{{-- <div class="col-sm-3">
												<form method="POST" action="{{action('Web\CityController@destroy', $categories->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this City?')">@lang('labels.delete')</button>
												</form>
											</div> --}}
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection