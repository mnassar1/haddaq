@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.cities')</div>
				<div class="panel-body">
					@include('admin.city.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('cities.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_city')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.ar_city_name')</th>
								<th>@lang('labels.admin.eng_city_name')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('cities.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->name_ar}}</td>
								<td>{{$page->name_en}}</td>
								
								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="10" class="text-center">
									You have not any Cities , lets to <a href="{{route('cities.create')}}">add new City</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection