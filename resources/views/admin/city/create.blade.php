@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_city')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('cities.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
					<input type="hidden" class="form-control" name="country_id" placeholder="@lang('labels.please_enter')  @lang('labels.country')" value="1" />
					
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_city_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name_en') ? ' has-error' : '' }}>
						    	@if ($errors->has('name_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_en') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name_en" placeholder="@lang('labels.please_enter')  @lang('labels.admin.eng_city_name')"  required/>
						    </div>
						  </div>
					  
					  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_city_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name_ar" placeholder="@lang('labels.please_enter')  @lang('labels.admin.ar_city_name')"  required/>
						    </div>
						  </div>

					 
					  

						
		


					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('cities.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
