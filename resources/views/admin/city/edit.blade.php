@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_city')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\CityController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

					<input type="hidden" class="form-control" name="country_id" placeholder="please enter Country Name" value="1" />
					
					<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_city_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name_en') ? ' has-error' : '' }}>
						    	@if ($errors->has('name_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_en') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name_en" placeholder="please enter English City Name" value="{{$categories->name_en}}" required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_city_name')</label>
						    <div class="col-sm-10" {{ $errors->has('name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="name_ar" placeholder="please enter Arabic City Name" value="{{$categories->name_ar}}" required/>
						    </div>
						  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('cities.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection