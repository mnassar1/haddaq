@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_contact')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{action('Web\ContactsController@update', $contact->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cms_type')</label>
						    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
						    	<select name="type" class="form-control">
						    		@foreach($types as $type)
						    		<option value="{{$type}}" {{($contact->type == $type) ? 'selected':''}}>{{ucfirst($type)}}</option>
						    		@endforeach
						    	</select>
						    </div>
						  </div>
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Arabic Title" name="title_ar" value="{{$contact->title_ar}}" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="English Title" name="title_en" value="{{$contact->title_en}}" required/>
					    </div>
					  </div>

					  <!-- content textares -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admi.ar_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Arabic Content" name="content_ar" required>{{$contact->content_ar}}</textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_contnet')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="English Content" name="content_en" required>{{$contact->content_en}}</textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label"><a class="btn btn-block btn-default" href="{{route('contacts.show',$contact->id)}}">@lang('labels.back')</a></label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.edit')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection