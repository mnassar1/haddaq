@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_new_contact')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{route('contacts.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cms_type')</label>
						    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
						    	<select name="type" class="form-control">
						    		@foreach($types as $type)
						    		<option value="{{$type}}">{{ucfirst($type)}}</option>
						    		@endforeach
						    	</select>
						    </div>
						  </div
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.ar_title')" name="title_ar" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.eng_title')" name="title_en" required/>
					    </div>
					  </div>

					  <!-- content textares -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.ar_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.ar_content')" name="content_ar" required></textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.eng_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.eng_content')" name="content_en" required></textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('contacts.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
