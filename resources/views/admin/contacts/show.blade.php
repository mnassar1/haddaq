@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{ucfirst($contact->type)}}</b>	
					</div>
				</div>
				<div class="panel-body">

					<!-- title -->
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row text-center">
								<b>@lang('labels.eng_title')</b>	
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr class="text-center">
										<td>{{$contact->title->en}}</td>
										<td>{{$contact->title->ar}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end title -->

					<!-- title -->
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row text-center">
								<b>@lang('labels.eng_content')</b>	
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr class="text-center">
										<td>{{$contact->content->en}}</td>
										<td>{{$contact->content->ar}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end title -->

					<!-- title -->
					<div class="panel panel-info">
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr class="text-center">
										<td>
											<div class="row">
												<div class="col-sm-2">
													<a href="{{route('contacts.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
												</div>
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-9">
															<a href="{{route('contacts.edit',$contact->id)}}" class="btn btn-block btn-warning">@lang('labels.edit')</a>
														</div>
														<div class="col-sm-3">
														<form method="POST" action="{{action('Web\ContactsController@destroy', $contact->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this contact?')">@lang('labels.delete')</button>
														</form>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end title -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection