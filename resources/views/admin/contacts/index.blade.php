@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.contacts')</div>
				<div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td>
									<a href="{{route('contacts.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_contact')</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.eng_title')</th>
								<th>@lang('labels.eng_content')</th>
							</tr>
						</thead>
						<tbody>
							@if(count($contacts))
							@foreach($contacts as $contact)
							<tr onclick="window.location='{{route('contacts.show',$contact->id)}}';" style="cursor : pointer">
								<td>{{$contact->id}}</td>
								<td>{{$contact->title->en}}</td>
								<td>{{$contact->content->en}}</td>
							</tr>
							@endforeach
							@else
							<tr>
								<th colspan="3" class="text-center">
									There are not any contacts
								</th>
							</tr>
							@endif
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->links()}}
									</div>
								</td>
							</tr>
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection