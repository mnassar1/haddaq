@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.create_cms')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{route('cms.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">	@lang('labels.admin.cms_type')
</label>
						    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
						    	@if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="type" placeholder="please enter CMS type and add _ between words" name="type" required/>
						    </div>
						  </div
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">	@lang('labels.admin.ar_title')
</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.admin.ar_title')" name="title_ar" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="@lang('labels.admin.eng_title')" name="title_en" required/>
					    </div>
					  </div>

					  <!-- content textares -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.ar_content')" name="content_ar" ></textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="@lang('labels.admin.eng_content')" name="content_en" ></textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('cms.index')}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.create')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
