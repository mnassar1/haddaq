@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.admin.cms_title')</div>
				<div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td>
									<a href="{{route('cms.create')}}" class="btn btn-primary">+ @lang('labels.admin.new_cms')</a>
								</td>
							</tr>
							<tr class="active">
								<th>@lang('labels.admin.cms_type')</th>
								<th>@lang('labels.admin.eng_title')</th>
								<th>@lang('labels.admin.eng_content')</th>
								<th>#</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($cms) && !empty($cms))
							@foreach($cms as $page)
							<tr  onclick="window.location='{{route('cms.show',$page->id)}}';" style="cursor : pointer">
								<td>{{$page->type}}</td>
								<td>{{$page->title_en}}</td>
								<td>{{substr($page->content_en,0,50)}}</td>
								<td>{{$page->created_at}}</td>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any CMS, lets to <a href="{{route('cms.create')}}">add new cms</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection