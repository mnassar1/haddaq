@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						<b>{{$cms->type}}</b>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
						<!-- English Content -->
						<tr class="active">
							<th class="text-center" colspan="2">@lang('labels.admin.eng_content')</th>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.eng_title')</th>
							<td>
								{{$cms->title_en}}
							</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.eng_content')</th>
							<td>
								{{$cms->content_en}}
							</td>
						</tr>
						<!-- Arabic Content -->
						<tr class="active">
							<th class="text-center" colspan="2">@lang('labels.admin.ar_content')</th>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.ar_title')</th>
							<td>{{$cms->title_ar}}</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.admin.ar_content')</th>
							<td>{{$cms->content_ar}}</td>
						</tr>
						<tr>
							<th class="active">@lang('labels.posted_at')</th>
							<td>{{$cms->created_at}}</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="row">
									<div class="col-sm-2">
										<a href="{{route('cms.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-9">
												<a class="btn btn-block btn-warning" href="{{route('cms.edit',$cms->id)}}">@lang('labels.edit')</a>
											</div>	
											<div class="col-sm-3">
												<form method="POST" action="{{action('Web\CMSController@destroy', $cms->id)}}">
															<input type="hidden" name="_method" value="delete" />
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<button class="btn-block btn btn-danger" onclick="return confirm('Are you sure to delete this CMS?')">@lang('labels.delete')</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection