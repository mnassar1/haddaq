@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.edit_cms')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" action="{{action('Web\CMSController@update', $cms->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.cms_type')</label>
						    <div class="col-sm-10">
						    	<input type="text" class="form-control" value="{{$cms->type}}" disabled/>
						    </div>
						  </div>
						<!-- title text fields -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="Arabic Title" name="title_ar" value="{{$cms->title_ar}}" required/>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_title')</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputPassword" placeholder="English Title" name="title_en" value="{{$cms->title_en}}" required/>
					    </div>
					  </div>

					  <!-- content textares -->
					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="Arabic Content" name="content_ar" >{{$cms->content_ar}}</textarea>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_content')</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" placeholder="English Content" name="content_en" >{{$cms->content_en}}</textarea>
					    </div>
					  </div>

					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">
					    	<a href="{{route('cms.show',$cms->id)}}" class="btn btn-block btn-default">@lang('labels.back')</a>
					    </label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.edit')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection