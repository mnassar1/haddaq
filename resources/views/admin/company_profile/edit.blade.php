@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						Edit Company Profile : {{$user->full_name}}
					</div>
				</div>
				<div class="panel-body">
					<form enctype="multipart/form-data" method="POST" action="{{action('Web\CompanyProfileController@update', $user->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.name')</label>
						    <div class="col-sm-10" {{ $errors->has('full_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('full_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="full_name" placeholder="please enter Name" value="{{$user->full_name}}" required/>
						    </div>
						  </div>

						  <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.about')</label>
					    <div class="col-sm-10">
					    	<textarea  class="form-control" placeholder="Enter About " name="about">{{$user->about}}</textarea>
					    </div>
					  </div>

							<!-- content image -->
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.Logo')</label>
								<div class="col-sm-10">
									<input type="file" class="form-control" placeholder="Logo" name="profile_image_url" />
								</div>
							</div>

							<div class="form-group row">
								<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.slider_images')</label>
								<div class="col-sm-10">
									<input type="file" class="form-control" placeholder="Slider Images" name="sliders[]" multiple />
								</div>
							</div>


							<div class="form-group row">
							    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.email')</label>
							    <div class="col-sm-10" {{ $errors->has('email') ? ' has-error' : '' }}>
							    	@if ($errors->has('email'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
							    	<input type="text" class="form-control" name="email" placeholder="please enter email" value="{{$user->email}}" required/>
							    </div>
							  </div>

								<div class="form-group row">
										<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.Mobile')</label>
										<div class="col-sm-10" {{ $errors->has('mobile') ? ' has-error' : '' }}>
											@if ($errors->has('mobile'))
																				<span class="help-block">
																						<strong>{{ $errors->first('mobile') }}</strong>
																				</span>
																		@endif
											<input type="text" class="form-control" name="mobile" placeholder="please enter mobile" value="{{$user->mobile}}" required/>
										</div>
									</div>


									<!-- content image -->
									<div class="form-group row">
										<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.city')</label>
										<div class="col-sm-10">
											<select  required name="city_id" class="form-control">

											<?php
											if(!empty($cities)){
												foreach ($cities as $city) {
													?>
											<option <?php if($user->city_id == $city->id)echo "selected"?> value="<?php echo $city->id?>"><?php echo $city->name_en?></option>
											<?php 	}
											}
											?>
											</select>
										</div>
									</div>


									<div class="form-group row">
											<label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.address')</label>
											<div class="col-sm-10" {{ $errors->has('address') ? ' has-error' : '' }}>
												@if ($errors->has('address'))
																					<span class="help-block">
																							<strong>{{ $errors->first('address') }}</strong>
																					</span>
																			@endif
												<input type="text" class="form-control" name="address" placeholder="please enter address" value="{{$user->address}}" required/>
											</div>
										</div>



										<div class="form-group row">
						 						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.location')</label>
						 						    <div class="col-sm-10" {{ $errors->has('location') ? ' has-error' : '' }}>
						 						    	@if ($errors->has('location'))
						                                     <span class="help-block">
						                                         <strong>{{ $errors->first('location') }}</strong>
						                                     </span>
						                                 @endif
						 						    	<input type="text" class="form-control" name="location" placeholder="please enter Location" value="{{$user->location}}"  required/>
						 						    </div>
						 						  </div>



					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('company_profile.show',$user->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
