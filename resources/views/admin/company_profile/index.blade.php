@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">Profile</div>
				<div class="panel-body">
					@include('admin.company_profile.search')
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>@lang('labels.name')</th>
								<th>@lang('labels.email')</th>
								<th>@lang('labels.phone')</th>
								<th>@lang('labels.address')</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($users) && !empty($users))
							@foreach($users as $user)
							<tr style="cursor: pointer;" onclick="window.location='{{route('company_profile.show',$user->id)}}';">
								<td>#</td>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
								<td>{{$user->phone}}</td>
								<td>{{$user->address}}</td>

							</tr>
							@endforeach
							<tr>
								<td colspan="10" class="text-center">
									{{$paginate->links()}}
								</td>
							</tr>
							@else
							<tr>
								<td colspan="10" class="text-center">
									<b>No Users registered.</b>
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
