@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{{$user->name}}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<?php if ($user->profile_image_url != "" || $user->profile_image_url != NULL) {?>


						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $user->profile_image_url;?>" class="img-circle img-responsive"> </div>

						<?php }else{?>
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://via.placeholder.com/300x300" class="img-circle img-responsive"> </div>
					<?php } ?>

						<div class=" col-sm-3 col-md-9 col-lg-9 ">
							<table class="table">
								<tbody>
									<tr>
										<td>@lang('labels.email')</td>
										<td><a href="mailto:{{$user->email}}" target="_blank">{{$user->email}}</a></td>
									</tr>
									<tr>
										<td>@lang('labels.phone')</td>
										<td>

											{{$user->phone}}
										</td>
									</tr>
									<tr>
										<td>@lang('labels.membership')</td>
										<td>
											<?php
			switch($user->user_type_id){
				case 1:
					echo  "Regular Membership";
					break;
				case 2:
					echo  "User & Savior Membership";
					break;
				case 3:
					echo  "Company Membership";
					break;

				}?>
										</td>
									</tr>
				<?php
									if ($user->user_type_id==2) {?>
									<tr>
										<td>@lang('labels.savior_type')</td>
										<td>
											<?php if($user->savior_with_price > 0)
												echo "Paid";
												else
												echo "Free";
											 ?>
										</td>
									</tr>

									<tr>
										<td>@lang('labels.savior_fees')</td>
										<td>
											<?php if($user->savior_with_price > 0)
												echo $user->savior_with_price;
												else
												echo "Free";
											 ?>
										</td>
									</tr>

									<?php }
									?>

									<tr>
										<td>@lang('labels.verified')</td>
										<td>
											<span class="badge badge-{{($user->is_verified) ? 'success' : 'danger'}}">
												{{($user->is_verified) ? 'Yes' : 'No'}}
											</span>
										</td>
									</tr>
									<tr>
										<td>@lang('labels.blocked') </td>
										<td>
											<span class="badge badge-{{($user->is_blocked) ? 'success' : 'danger'}}">
												{{($user->is_blocked) ? 'Yes' : 'No'}}
											</span>
										</td>
									</tr>
									<?php if($user->is_admin){?>
									<tr>
										<td>@lang('labels.admin')</td>
										<td>
											<span class="badge badge-{{($user->is_admin) ? 'success' : 'danger'}}">
												{{($user->is_admin) ? 'Yes' : 'No'}}
											</span>
										</td>
									</tr>
									<?php }?>
									<tr>
										<td>@lang('labels.address')</td>
										<td>
											{{$user->address}}
										</td>
									</tr>

									<tr>
										<td>@lang('labels.city')</td>
										<td>
											<?php if($city) echo $city->name_en;
												if(!$city) echo "Not Selected Yet";
											 ?>
										</td>
									</tr>


								</tbody>
							</table>
							<table class="table" >
							<?php
									if ($user->user_type_id==3) {?>

									<tr>
							<td>@lang('labels.slider_images')</td>
							<td>
								<?php if (!empty($user->sliders)) {
									foreach ($user->sliders as $image) {?> 
						<div class="col-md-3 col-lg-3 " align="center"> <img style="height: 100px;width: 100px" alt="User Pic" src="<?php echo $image;?>" class="img-circle img-responsive"> </div>
						
						<?php } }?>
							</td>
						</tr>

									<tr colspan = '3'>
										<td><label>@lang('labels.company_branches')</label></td>


									</tr>
									<tr>
										<td>@lang('labels.title')</td>
										<td>
											@lang('labels.address')
										</td>

									</tr>

									<?php

									if(count($branches) > 0){
										foreach ($branches as $branch) {
											?>

									<tr>
										<td>
											{{$branch->title}}
										</td>
										<td>
											{{$branch->address}}
										</td>

									</tr>
										<?php }
									 }else{?>
									 	<tr style="color: red">
										<td colspan="2">
											No Branches Added yet.
										</td>


									</tr>
									 <?php }

									}
									?>
									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-sm-2">
													<a href="{{route('company_profile.index')}}" class="btn btn-block btn-default">@lang('labels.back')</a>
												</div>
												<div class="col-sm-10">
													<div class="row">
														<div class="col-sm-9">
															<a class="btn btn-block btn-warning" href="{{route('company_profile.edit',$user->id)}}">@lang('labels.edit_profile')</a>
														</div>

													</div>
												</div>
											</div>
										</td>
									</tr>
									</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
