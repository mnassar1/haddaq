@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-6">
							<img src="http://via.placeholder.com/20x20" class="rounded-circle">
								<strong>{{$trip->traveller->name}}</strong> @lang('labels.admin.travel_by') <strong>{{ucfirst($trip->transportation)}}</strong>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<table class="table">
								<tbody>
									<thead>
										<tr>
											<th colspan="3" class="info">@lang('labels.admin.trip') #{{$trip->id}}</th>
										</tr>
									</thead>
									<tr>
										<th colspan="2" class="active">@lang('labels.admin.traveling_from')</th>
										<th colspan="1" class="active">@lang('labels.admin.departure_at')</th>
									</tr>
									<tr>
										<th>@lang('labels.country')</th>
										<td>{{$trip->from->name_en}}</td>
										<td class="info">{{$trip->departure_time}}</td>
									</tr>
									<tr>
										<th>@lang('labels.city')</th>
										<td>{{$trip->from->city->name_en}}</td>
										<td class="info">{{$trip->departure_date}}</td>
									</tr>
									<tr>
										<th colspan="2" class="active">@lang('labels.admin.traveling_to')</th>
										<th colspan="1" class="active">@lang('labels.admin.arrive_at')</th>
									</tr>
									<tr>
										<th>@lang('labels.country')</th>
										<td>{{$trip->to->name_en}}</td>
										<td class="danger">{{$trip->arrival_time}}</td>
									</tr>
									<tr>
										<th>@lang('labels.city')</th>
										<td>{{$trip->to->city->name_en}}</td>
										<td class="danger">{{$trip->arrival_date}}</td>
									</tr>
									<tr>
										<th colspan="1" class="active">@lang('labels.weight')</th>
										<th colspan="2" class="active">@lang('labels.notes')</th>
									</tr>
									<tr>
										<td>{{$trip->kilos}}</td>
										<td>{{$trip->notes}}</td>
									</tr>
									<tr class="active">
                                        <th colspan="1">
@lang('labels.posted_at')
										</th>
										<td colspan="2">{{date('d-m-Y H:i:s', $trip->posted_at)}}</td>
									</tr>
								</tbody>
							</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
