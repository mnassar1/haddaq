@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading"> @lang('labels.admin.trips') </div>
				<div class="panel-body">
					@if($show_filter)
					@include('admin.trip.search')
					@endif
					@if(count($trips))
					@foreach($trips as $trip)
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<img src="http://via.placeholder.com/20x20" class="rounded-circle">
									<strong>{{($trip->traveller) ? $trip->traveller->name:""}}</strong> @lang('labels.admin.traveling_by') <strong>{{ucfirst($trip->transportation)}}</strong>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table">
								<tbody>
									<tr>
										<th colspan="2" class="active">@lang('labels.admin.traveling_from')</th>
										<th colspan="1" class="active">@lang('labels.admin.departure_at')</th>
									</tr>
									<tr>
										<th>@lang('labels.country')</th>
										<td>{{$trip->from->name_en}}</td>
										<td class="info">{{$trip->departure_time}}</td>
									</tr>
									<tr>
										<th>@lang('labels.city')</th>
										<td>{{$trip->from->city->name_en}}</td>
										<td class="info">{{$trip->departure_date}}</td>
									</tr>

									<tr>
										<th colspan="2" class="active">@lang('labels.admin.traveling_to')</th>
										<th colspan="1" class="active">@lang('labels.admin.arrive_at')Arrive at</th>
									</tr>
									<tr>
										<th>@lang('labels.country')</th>
										<td>{{$trip->to->name_en}}</td>
										<td class="danger">{{$trip->arrival_time}}</td>
									</tr>
									<tr>
										<th>@lang('labels.city')</th>
										<td>{{$trip->to->city->name_en}}</td>
										<td class="danger">{{$trip->arrival_date}}</td>
									</tr>
									<tr>
										<td colspan="3" class="text-center">
											<a href="{{route('trip_show',$trip->id)}}">@lang('labels.show_details')...</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					@endforeach
					@endif
					<div class="row">
						<div class="col-sm-12 text-center">
							{{$paginate->links()}}
						</div>
							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
