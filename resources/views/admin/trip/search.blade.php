	<div class="row">
		<div class="col-sm-12">
			<form method="" action="">
				<table class="table">
				<tbody>
					<tr>
						<th colspan="2" class="text-center"> @lang('labels.admin.traveling_from')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.traveling_to')</th>
					</tr>
					<tr>
						<td>
							<select class="form-control" name="country_from" id="country_from_id">
								<option value="0">@lang('labels.admin.select_country')</option>
								@foreach($countries as $country)
								<option value="{{$country->id}}" {{(Request::get('country_from') == $country->id)? 'selected':''}}>{{$country->name_en}}</option>
								@endforeach
							</select>
						</td>
						<td>
							<select class="form-control" name="city_from" id="city_from_id" disabled>
								<option value="0">@lang('labels.admin.select_city')</option>
							</select>
						</td>
						<td>
							<select class="form-control" name="country_to" id="country_to_id">
								<option value="0">@lang('labels.admin.select_country') </option>
								@foreach($countries as $country)
								<option value="{{$country->id}}" {{(Request::get('country_to') == $country->id)? 'selected':''}}>{{$country->name_en}}</option>
								@endforeach
							</select>
						</td>
						<td>
							<select class="form-control" name="city_to" id="city_to_id" disabled>
								<option value="0">@lang('labels.admin.select_city') </option>
							</select>
						</td>
					</tr>
					<tr>
						<th colspan="2" class="text-center">@lang('labels.admin.departure_date_range')</th>
                        <th colspan="2" class="text-center">@lang('labels.admin.arrival_date_range')</th>
					</tr>
					<tr>
						<td>
							<input type="date" class="form-control" name="departure_date_from" value="{{Request::get('departure_date_from')}}">
						</td>
						<td>
							<input type="date" class="form-control" name="departure_date_to" value="{{Request::get('departure_date_to')}}">
						</td>
						<td>
							<input type="date" class="form-control" name="arrival_date_from" value="{{Request::get('arrival_date_from')}}">
						</td>
						<td>
							<input type="date" class="form-control" name="arrival_date_to" value="{{Request::get('arrival_date_to')}}">
						</td>
					</tr>
					<tr>
						<th colspan="2" class="text-center">@lang('labels.admin.departure_date_range')</th>
						<th colspan="2" class="text-center">@lang('labels.admin.arrival_date_range')</th>
					</tr>
					<tr>
						<td>
							<input type="time" class="form-control" name="departure_time_from" value="Request::get('departure_time_from')">
						</td>
						<td>
							<input type="time" class="form-control" name="departure_time_to" value="Request::get('departure_time_to')">
						</td>
						<td>
							<input type="time" class="form-control"  name="arrival_time_from" value="Request::get('arrival_time_from')">
						</td>
						<td>
							<input type="time" class="form-control"  name="arrival_time_to" value="Request::get('arrival_time_to')">
						</td>
					</tr>
					<tr>
						<th>@lang('labels.transportation')Transportation</th>
						<th>@lang('labels.weight')Weight</th>
						<th colspan="2">@lang('labels.notes')Notes</th>
					</tr>
					<tr>
						<td>
							<select name="transportation" class="form-control">
								<option value="0">@lang('labels.admin.select_transportation')</option>
								<option value="autobus">@lang('labels.autobus')</option>
								<option value="plane">@lang('labels.plane')</option>
								<option value="train">@lang('labels.train')</option>
							</select>
						</td>
						<td>
							<input type="text" class="form-control" id="inputState" placeholder="Kilos (optional)" name="kilos">
						</td>
						<td colspan="2">
							<Textarea class="form-control" placeholder="Notes (optional)"></Textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="text-center">
							<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
						</td>
						<td colspan="2" class="text-center">
							<a class="btn btn-danger px-4 col-sm-6" href="{{route('trips')}}">@lang('labels.reset') </a>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>
	</div>
