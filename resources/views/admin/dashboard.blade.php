@extends('layouts.admin')
@section('content')
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"> @lang('labels.admin_dashboard')</div>
                    <div class="panel-body">
                    @if(Session::has('is_admin') && Session::get('is_admin') == 1)
                        <table  class="table">
                            <tr style="background-color:blueviolet;color:white">
                                <th># @lang('labels.admin_dashboard')</th>
                                <th># @lang('labels.savior_users')</th>
                                <th># @lang('labels.companies')</th>
                                <th># @lang('labels.ads')</th>
                                <th># @lang('labels.contact_us_messages')</th>
                                <th># @lang('labels.admin.sos_requests')</th>
                            </tr>
                            <tr style="background-color:aquamarine;color:white">
                                <td><a href="{{url('admin\user?user_types_id=1')}}">{{$users}} @lang('labels.users')</a></td>
                                <td><a href="{{url('admin\user?user_types_id=2')}}">{{$Saviors}} @lang('labels.saviors')</a></td>
                                <td><a href="{{url('admin\user?user_types_id=3')}}">{{$companies}} @lang('labels.companies')</a></td>
                                <td><a href="{{url('admin\app_ads')}}">{{$AppAds}} @lang('labels.ads')</a></td>
                                <td><a href="{{url('admin\contact_us')}}">{{$ContactUs}} @lang('labels.messages')</a></td>
                                <td><a href="{{url('admin\sos_requests')}}">{{$SOSRequest}} @lang('labels.requests')</a></td>

                            </tr>
                        </table>
                    @endif

                    @if(Session::has('user_type_id') && Session::get('user_type_id') == 3)
                        <table  class="table">
                            <tr style="background-color:blueviolet;color:white">
                                <th># @lang('labels.branches') </th>
                                <th># @lang('labels.approved_ads')</th>
                                <th># @lang('labels.blocked_ads')</th>

                            </tr>
                            <tr style="background-color:aquamarine;color:white">
                                <td><a href="{{url('admin\company_branches')}}">{{$branches}} @lang('labels.branches')</a></td>
                                <td><a href="{{url('admin\company_ads')}}">{{$approved_ads}} @lang('labels.approved_ads')</a></td>
                                <td><a href="{{url('admin\company_ads')}}">{{$blocked_ads}} @lang('labels.blocked_ads')</a></td>


                            </tr>
                        </table>
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection


<style>

    body {
        font: 16px/22px "Open Sans", sans-serif;
    }

    .content_div {

        top: 50px;
        position: absolute;
        margin: 0 132px;
        max-width: 980px;
    }

    .panel_div {
        position: relative;
        display: inline-block;
        width: 9em;
        height: 160px;
        margin: 4px;
        font-size: 32px;
        font-weight: 600;
        color: #fff;
        overflow: hidden;
        border-radius: 5px;
        box-shadow: 1px 0 9px #ccc;
        border: 2px solid #fff;
    }

    .panel_div a {
        position: relative;
        display: block;
        padding: 12px 25px 25px 25px;
        color: #fff;
        text-decoration: none;
        z-index: 2;
    }

    .panel_div a span {
        display: block;
        font-size: 96px;
        font-weight: 700;
        line-height: 96px;
    }

    .panel_div a:focus, .panel_div a:hover {
        color: white;
        text-decoration: none;
    }

    .panel_div :after {
        position: absolute;
        font-family: FontAwesome;
        color: rgba(0, 0, 0, 1);
        z-index: 1;
        transition: all .5s;
        line-height: normal;
    }

    .post_class {
        background-color: #b8aa0e;
    }

    .post_class :after {
        content: "\f008";
        font-size: 150px;
        color: rgba(0, 0, 0, 4%);
        top: 45px;
        right: 40px;
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .post_class:hover :after {
        top: 8px;
    }

    .comment_class {
        background-color: #0377c0;
    }

    .comment_class :after {
        content: "\f1cd";
        font-size: 150px;
        color: rgba(0, 0, 0, 4%);
        top: 30px;
        right: 30px;
    }

    .comment_class:hover :after {
        top: -5px;
    }

    .page {
        background-color: #279824;
    }

    .page :after {
        content: "\f1ad";
        font-size: 150px;
        color: rgba(0, 0, 0, 4%);
        top: 62px;
        right: 30px;
    }

    .page:hover :after {
        top: 24px;
    }

    .user {
        background-color: #fc1c3e;
    }

    .user :after {
        content: "\f0c0";
        font-size: 150px;
        color: rgba(0, 0, 0, 8%);
        top: 45px;
        right: 30px;
    }

    .user:hover :after {
        top: 7px;
    }

    .request {
        background-color: #555;
    }

    .request :after {
        content: "\f07c";
        font-size: 150px;
        color: rgba(0, 0, 0, 8%);
        top: 45px;
        right: 30px;
    }

    .request:hover :after {
        top: 7px;
    }

    .message {
        background-color: #2f7f80;
    }

    .message :after {
        content: "\f0e0";
        font-size: 150px;
        color: rgba(0, 0, 0, 8%);
        top: 45px;
        right: 30px;
    }

    .message:hover :after {
        top: 7px;
    }

    .dtitle {
        padding: 0px 10px;
        margin: 5px 0;
        color: #555;
        font-size: 19px;
    }

    .seprator {
        border-top: 1px solid #ccc;
        height: 2px;
        margin: 0 0 30px 0;
        width: 96%;
    }
</style>
<script src="https://use.fontawesome.com/bf5faef366.js"></script>

<div class="container">
    <div class="row">
        @if(Session::has('is_admin') && Session::get('is_admin') == 1)
            <div class="content_div">
                <div class="seprator"></div>
                @if($isMainDomian)
                    <div class="panel_div comment_class">
                        <a href="{{url('admin\user?user_types_id=2')}}"><span>{{$Saviors}} </span>@lang('labels.saviors')</a>
                    </div>
                    <div class="panel_div page">
                        <a href="{{url('admin\user?user_types_id=3')}}"><span>{{$companies}} </span>@lang('labels.companies')</a>
                    </div>
                    <div class="panel_div user">
                        <a href="{{url('admin\user?user_types_id=1')}}"><span>{{$users}} </span>@lang('labels.regular_users')</a>
                    </div>
                    <div class="panel_div user">
                        <a href="{{url('admin/user?is_admin=1&from_home=1')}}"><span>{{$admins}} </span>@lang('labels.admins')</a>
                    </div>
                @endif
                @if(!$isMainDomian)
                    <div class="panel_div post_class">
                        <a href="{{url('admin\app_ads?approved_status=2')}}"><span>{{$AppAds}}  </span>@lang('labels.new_ads')
                        </a>
                    </div>
                    <div class="panel_div request">
                        <a href="{{url('admin\sos_requests?status=Opening')}}"><span>{{$SOSRequest}}  </span>@lang('labels.admin.new_sos')</a>
                    </div>

                    <div class="panel_div message">
                        <a href="{{url('admin\contact_us')}}"><span>{{$ContactUs}}  </span>@lang('labels.messages')</a>
                    </div>

                    <div class="panel_div page">
                        <a href="{{url('admin\fixing_shops')}}"><span>{{$fixing_shops}} </span>@lang('labels.admin.fixing_shops')</a>
                    </div>
                @endif
            </div>
@endif
@if(Session::has('user_type_id') && Session::get('user_type_id') == 3)

    <div class="content_div">

        <div class="seprator"></div>

        <div class="panel_div post_class">
            <a href="{{url('admin\company_ads')}}"><span>{{$approved_ads}}  </span>@lang('labels.approved_ads')</a>
        </div>

        <div class="panel_div request">
            <a href="{{url('admin\company_ads')}}"><span>{{$blocked_ads}}  </span>@lang('labels.blocked_ads')</a>
        </div>

        <div class="panel_div page">
            <a href="{{url('admin\company_branches')}}"><span>{{$branches}} </span>@lang('labels.branches')</a>
        </div>


    </div>
@endif

</div>
</div>
