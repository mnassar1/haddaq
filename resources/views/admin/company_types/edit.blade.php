@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.edit_cat')
					</div>
				</div>
				<div class="panel-body">
					<form method="POST" enctype="multipart/form-data" action="{{action('Web\CompanyTypesController@update', $categories->id)}}">
						<input type="hidden" name="_method" value="put" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- cms type -->

						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.eng_cat_name')</label>
						    <div class="col-sm-10" {{ $errors->has('type_name') ? ' has-error' : '' }}>
						    	@if ($errors->has('type_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type_name') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="type_name" placeholder="please enter English Type Name" value="{{$categories->type_name}}" required/>
						    </div>
						  </div>

						  <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.admin.ar_cat_name')</label>
						    <div class="col-sm-10" {{ $errors->has('type_name_ar') ? ' has-error' : '' }}>
						    	@if ($errors->has('type_name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type_name_ar') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="type_name_ar" placeholder="please enter Arabic Name" value="{{$categories->type_name_ar}}" required/>
						    </div>
						  </div>

						   <div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.notification_cnt')</label>
						    <div class="col-sm-10" {{ $errors->has('number_of_notifications') ? ' has-error' : '' }}>
						    	@if ($errors->has('number_of_notifications'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number_of_notifications') }}</strong>
                                    </span>
                                @endif
						    	<input type="number" class="form-control" name="number_of_notifications" placeholder="please enter Number of Notifications" value="{{$categories->number_of_notifications}}" required/>
						    </div>
						  </div>

					  

					


					  

					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('company_types.show',$categories->id)}}" class="btn btn-default btn-block">@lang('labels.back')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.update')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
