@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('labels.Company_Types')</div>
				<div class="panel-body">
					@include('admin.company_types.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<!-- <td>
									<a href="{{route('company_types.create')}}" class="btn btn-primary">+ New Company Type</a>
								</td> -->
							</tr>
							<tr class="active">
								<th>#</th>
								<th>@lang('labels.admin.ar_cat_name')</th>
								<th>@lang('labels.admin.eng_cat_name')</th>
								<th># @lang('labels.notification_cnt')</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($categories) && !empty($categories))
							@foreach($categories as $page)
							<tr  onclick="window.location='{{route('company_types.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->type_name_ar}}</td>
								<td>{{$page->type_name_en}}</td>
								<td>{{$page->number_of_notifications}}</td>
								
								

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="4" class="text-center">
									<div class="row">
										{{$paginate->appends(request()->input())->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="10" class="text-center">
									You have not any Company Types ,<!--  lets to <a href="{{route('company_types.create')}}">add new Category</a> -->
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
