@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">Fixing Shops</div>
				<div class="panel-body">
					@include('admin.fixing_shops.search')
					<table class="table table-hover">
						<thead>
							<tr>
								<td colspan="5">
									<a href="{{route('fixing_shops.create')}}" class="btn btn-primary">+ New Fixing Shops</a>
								</td>
							</tr>
							<tr class="active">
								<th>#</th>
								<th>Arabic Name</th>
								<th>English Name</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ;?>
							@if(isset($fixing_shops) && !empty($fixing_shops))
							@foreach($fixing_shops as $page)
							<tr  onclick="window.location='{{route('fixing_shops.show',$page->id)}}';" style="cursor : pointer">
								<td><?php echo $i ;?></td>
								<td>{{$page->shop_name_ar}}</td>
								<td>{{$page->shop_name_en}}</td>
								<td><?php
									switch ($page->fixing_shops_types_id) {
										case 1:
											echo "Gold";
											break;
										case 2:
											echo "Silver";
											break;
										case 3:
											echo "Bronze";
											break;
											
										default:
											# code...
											break;
									}
								?></td>

								<?php $i++; ;?>
							</tr>
							@endforeach
							<tr>
								<td colspan="10" class="text-center">
									<div class="row">
										{{$paginate->links()}}
									</div>
								</td>
							</tr>
							@else
							<tr>
								<th colspan="4" class="text-center">
									You have not any Fixing Shops , lets to <a href="{{route('fixing_shops.create')}}">add new Fixing Shops</a>
								</th>
							</tr>
							@endif
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection