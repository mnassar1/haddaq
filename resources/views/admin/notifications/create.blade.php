@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row text-center">
						@lang('labels.admin.send_notifications_to_all')
					</div>
				</div>
				<div class="panel-body">
					<form  enctype="multipart/form-data" method="POST" action="{{route('notifications.store')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<div class="form-group row">
						    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.title')</label>
						    <div class="col-sm-10" {{ $errors->has('title') ? ' has-error' : '' }}>
						    	@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
						    	<input type="text" class="form-control" name="title" placeholder="@lang('labels.please_enter') @lang('labels.title')"  required/>
						    </div>
						  </div>
				
					   <div class="form-group row">
					    <label for="inputPassword" class="col-sm-2 col-form-label">@lang('labels.details')</label>
					    <div class="col-sm-10">
								@if ($errors->has('details'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                                @endif
					    	<textarea class="form-control" placeholder="@lang('labels.details')" name="details" ></textarea>
					    </div>
					  </div>


					



					   <div class="form-group row">
					   	<label for="inputPassword" class="col-sm-2 col-form-label">
					   		<a href="{{route('notifications.index')}}" class="btn btn-default btn-block">@lang('labels.reset')</a>
					   	</label>
						    <div class="col-sm-10">
						    	<button class="btn btn-block btn-success">@lang('labels.admin.send_notifications')</button>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
