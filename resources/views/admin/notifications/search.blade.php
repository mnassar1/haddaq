<div class="row">
	<div class="col-sm-12">
		<form method="" action="">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="row">
								

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name_en" class="form-control" placeholder="@lang('labels.eng_name')" value="{{Request::get('name_en')}}" />
								</div>

								<!-- name -->
								<div class="col-sm-4">
									<input type="text" name="name_ar" class="form-control" placeholder="@lang('labels.ar_name')" value="{{Request::get('name_ar')}}" />
								</div>


								<!-- register type -->
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-6 lead"> @lang('labels.type')</div>
										<div class="col-sm-6">
											<select name="fixing_shops_types_id" class="form-control">
												<option value="">@lang('labels.select')</option>
												<?php
												if(!empty($fixing_shops_types)) 
												foreach ($fixing_shops_types as $type) {
													?>
											<option <?php if(Request::get('fixing_shops_types_id')== $type->id) echo "selected";?> value="<?php echo $type->id;?>">
												<?php echo $type->type_name?>
											</option>

													<?php
												}
												?>

											</select>
										</div>
									</div>
								</div>

								
								<!-- register type -->
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-6 lead"> @lang('labels.admin.city')</div>
										<div class="col-sm-6">
											<select name="city_id" class="form-control">
												<option value="">Select</option>
												<?php
												if(!empty($cities)) 
												foreach ($cities as $type) {
													?>
											<option <?php if(Request::get('city_id')== $type->id) echo "selected";?> value="<?php echo $type->id;?>">
												<?php echo $type->name_en?>
											</option>

													<?php
												}
												?>

											</select>
										</div>
									</div>
								</div>

								
							</div>
						</td>
					</tr>

					
					<tr>
						<td class="text-center" colspan="2">
							<div class="row">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary px-4 col-sm-6">@lang('labels.search')</button>
								</div>
								<div class="col-sm-6">
									<a style="float:right;" class="btn btn-danger px-4 col-sm-6" href="{{route('fixing_shops.index')}}"> @lang('labels.reset')</a>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
